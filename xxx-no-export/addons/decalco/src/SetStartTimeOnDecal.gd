@tool
extends MeshInstance3D

# use this script on one shot flipbook animations

func _ready():
	var cur_time = Time.get_ticks_msec() / 1000.0
	var mat = get_surface_override_material(0).duplicate(true)
	set_surface_override_material(0, mat)
	mat.set_shader_parameter("start_time", cur_time)
	
