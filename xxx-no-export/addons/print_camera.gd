extends Camera3D


@export_dir var png_file_path: String = String()
@export_file var png_file_name: String = "saved_image"
@export var camera_size: float = 100


func _ready():
	set_size(camera_size)
	# WARNING: don't use await in _ready()
	# Wait until the frame has finished before getting the texture.
	RenderingServer.frame_post_draw.connect(_on_frame_post_draw)


func _on_frame_post_draw():
	# You can get the image after this.
	# Retrieve the captured Image using get_data().
	var img:Image = get_viewport().get_texture().get_image()
	# Flip on the Y axis.
	# You can also set "V Flip" to true if not on the root Viewport.
	#img.flip_y()
	await get_tree().create_timer(2).timeout
	var png_file = png_file_path + "/" + png_file_name + ".png"
	var error = img.save_png(png_file)
	var saved_label = Label.new()
	if error == OK:
		saved_label.text = "SAVED!"
	else:
		saved_label.text = "ERROR - NOT SAVED!"
	add_child(saved_label)
