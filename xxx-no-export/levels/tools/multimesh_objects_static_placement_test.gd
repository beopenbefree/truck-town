#tool
extends Node3D


@export var enabled: bool = false

@export var object_total_num: int = 100

@export var object_types: Array[PackedScene] = []
@export var visual_only: bool = false
@export_flags_3d_physics var collision_layer: int = 0b1
@export_flags_3d_physics var collision_mask: int = 0b1
@export var object_collision_shape_scales: Array[Vector3] = []
@export_range(0,1,0.05) var object_max_delta_scale: float = 0.5
@export var physics_material: PhysicsMaterial = null

@export var placement_space_center: Vector3 = Vector3.ZERO
@export var placement_space_size: Vector3 = Vector3(400, 200, 400)

@export var placement_objects_names: Array[String] = []

@export var save_scene: bool = false
@export_dir var save_scene_path: String = ""
@export var save_scene_name: String = ""

var ray_cast:RayCast3D = null

@onready var placements_finished:bool = false


func _ready():
	if not enabled:
		# do nothing
		return
	randomize()
	if object_types.size() < 1:
		return
	# make sure placement_space_size has no negative component
	placement_space_size = placement_space_size.abs()
	if object_collision_shape_scales.size() < object_types.size():
		var delta_size = object_types.size() - object_collision_shape_scales.size()
		for i in delta_size:
			object_collision_shape_scales.append(Vector3.ONE)
	print(get_child_count())
	ray_cast = RayCast3D.new()
	add_child(ray_cast)
	# create MeshInstance3D children
	if object_total_num > 0:
		_create_additional_children(object_total_num)
	else:
		return
	
	# create multimesh objects from children by material
	ScriptLibrary.create_multimesh_objects_from_children_by_material(
		self, object_types, visual_only, object_max_delta_scale,
		collision_layer, collision_mask, object_collision_shape_scales,
		physics_material, ray_cast, 10.0 * placement_space_size.y,
		save_scene, save_scene_name, save_scene_path)
	
	# set ready is finished
	placements_finished = true


func _create_additional_children(children_num):
	var total_attempts = 0
	var child_count = 0
	var dummy_meshes = []
	for i in object_types.size():
		dummy_meshes.append(_create_dummy_mesh())
	#
	var space_half_size = placement_space_size / 2.0
	var space_center = placement_space_center
	while child_count < children_num:
		total_attempts += 1
		var pos_x = randf_range(-space_half_size.x, space_half_size.x) + space_center.x
		var pos_y = space_half_size.y + space_center.y
		var pos_z = randf_range(-space_half_size.z, space_half_size.z) + space_center.z
		# cast ray vertically
		ray_cast.global_transform.origin = Vector3(pos_x, pos_y, pos_z)
		ray_cast.target_position = Vector3(0.0, -placement_space_size.y, 0.0)
		ray_cast.force_raycast_update()
		if ray_cast.is_colliding():
			var hit_object_name = ray_cast.get_collider().get_parent().name
			if hit_object_name in placement_objects_names:
				var mesh_instance:MeshInstance3D = MeshInstance3D.new() 
				var dummy_mesh = dummy_meshes[randi() % object_types.size()]
				mesh_instance.mesh = dummy_mesh
				add_child(mesh_instance)
				var b = Basis.IDENTITY
				var o = ray_cast.get_collision_point()
				# set object transform
				mesh_instance.set_global_transform(Transform3D(b, o))
				child_count += 1
	#
	print("placements/attempts = ", children_num, "/", total_attempts, 
			" (", float(children_num)/float(total_attempts), ")" )


func _create_dummy_mesh():
	var vertices = PackedVector3Array()
	var UVs = PackedVector2Array()
	var mat = StandardMaterial3D.new()
	var color = Color(0.9, 0.1, 0.1)
	vertices.push_back(Vector3(0,0,1))
	vertices.push_back(Vector3(0,0,0))
	vertices.push_back(Vector3(1,0,1))
	vertices.push_back(Vector3(1,0,0))
	UVs.push_back(Vector2(0,0))
	UVs.push_back(Vector2(0,1))
	UVs.push_back(Vector2(1,1))
	UVs.push_back(Vector2(1,0))
	mat.albedo_color = color
	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
	st.set_material(mat)
	for v in vertices.size(): 
		st.set_color(color)
		st.set_uv(UVs[v])
		st.add_vertex(vertices[v])
	return st.commit()
