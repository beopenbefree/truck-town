class_name VehicleAIDriverTest
extends VehicleAIDriverSP


@export var chassis_show: bool = false
@export var buttons_show: bool = true

# display
@export var display_info: bool = true

var follow_camera: FollowCamera = null
var fixed_camera: FixedCamera = null
var current_vehicle_cameras: CameraGroup = null
var current_vehicle_rearview: RearviewMirror = null


func _ready():
	super()
	vehicle_body.chassis.visible = chassis_show
	vehicle_body.get_node("MapTarget").visible = chassis_show
	follow_camera = vehicle_body.find_child("FollowCamera", true)
	fixed_camera = vehicle_body.find_child("FixedCamera", true)
	follow_camera.current = true
	current_vehicle_cameras = globals.current_vehicle_body.get_node(
			"CameraGroup")
	current_vehicle_rearview = ScriptLibrary.find_nodes_by_type(RearviewMirror, 
			current_vehicle_cameras, false)[0]


func _input(event):
	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_O:
			if follow_camera.current == true:
				follow_camera.current = false
				var vehicle_camera
				for camera in current_vehicle_cameras.get_children():
					if is_instance_of(camera, FollowCamera):
						vehicle_camera = camera
						break
				current_vehicle_cameras._set_enabled_and_current(vehicle_camera)
				# show HUD
				globals.get_current_scene().get_node("HUD").visible = true
				current_vehicle_rearview.visibility = true
			else:
				follow_camera.current = true
				# hide HUD
				globals.get_current_scene().get_node("HUD").visible = false
				current_vehicle_rearview.visibility = false


func _on_switch_camera_pressed():
	if follow_camera.current == true:
		follow_camera.current = false
		fixed_camera.current = true
	elif fixed_camera.current == true:
		follow_camera.current = true
		fixed_camera.current = false


func _on_unlock_ai_pressed():
	vehicle_body.apply_blast(randf_range(5.0, 10.0))


func _get_class():
	return "VehicleAIDriverTest"


func _is_class(value):
	return "VehicleAIDriverTest" == value


func _on_switch_ai_camera_pressed():
	pass # Replace with function body.
