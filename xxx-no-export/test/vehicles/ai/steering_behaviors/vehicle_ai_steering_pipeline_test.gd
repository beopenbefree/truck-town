class_name VehicleAISteeringPipelineTest
extends VehicleAISteeringPipeline

#<DEBUG
@export_enum("NONE", "Chase", "StandStill") var debug_targeter: String = "NONE"
@export_enum("NONE", "Planning") var debug_decomposer: String = "NONE"
@export_enum("NONE", "ObstacleAvoidance", "Separation", "MinSpeed") var debug_constraint: String = "NONE"
@export_enum("NONE", "Seek", "Arrive") var debug_actuator: String = "NONE"
@export var debug_deadlock: bool = false
@export var debug_goal: bool = false
@export var debug_steering_output: bool = false

@export var dbg_draw_path_scene: PackedScene = preload("res://ai/debug_draw_path.tscn")
@export var dbg_drawing_size: float = 0.0
@export var dbg_path_point_size: float = 1.0

var dbg_steering_pipeline_name = {} # {AI.SteeringPipeline:String}
var dbg_draw_path: Node3D= null
var dbg_lib: Script = preload("res://ai/debug_draw.gd")
var dbg_c_targeter: Dictionary = {} # {String:AI.Targeter}
var dbg_ss_targeter: Dictionary = {} # {String:AI.Targeter}
var dbg_p_decomposer: Dictionary = {} # {String:AI.Decomposer}
var dbg_oa_constraint: Dictionary = {} # {String:AI.Constraint}
var dbg_s_constraint: Dictionary = {} # {String:AI.Constraint}
var dbg_ms_constraint: Dictionary = {} # {String:AI.Constraint}
var dbg_s_actuator: Dictionary = {} # {String:AI.Actuator}
var dbg_a_actuator: Dictionary = {} # {String:AI.Actuator}
var dbg_deadlock: Dictionary = {} # {String:AI.SteeringBehavior}

@onready var dbg_steering_output_geom := ImmediateMesh.new()
@onready var dbg_goal_mesh := MeshInstance3D.new()
@onready var dbg_oa_collision_point_mesh := MeshInstance3D.new()
@onready var dbg_oa_wisker_r_geom := ImmediateMesh.new()
@onready var dbg_oa_wisker_l_geom := ImmediateMesh.new()
@onready var dbg_oa_goal_position_mesh := MeshInstance3D.new()
@onready var dbg_s_result_linear_geom := ImmediateMesh.new()
@onready var dbg_materials: Dictionary = {
	Color.RED: StandardMaterial3D.new(),
	Color.GREEN: StandardMaterial3D.new(),
	Color.BLUE: StandardMaterial3D.new(),
	Color.WHITE: StandardMaterial3D.new(),
	Color.BLACK: StandardMaterial3D.new(),
	Color.YELLOW: StandardMaterial3D.new(),
	Color.VIOLET: StandardMaterial3D.new(),
	Color.BROWN: StandardMaterial3D.new(),
}
const POINT_HEIGHT = 0.75
const POINT_RADIUS = 0.125
#DEBUG>


func _ready():
	super()
	
	#<DEBUG
	for sp_name in steering_pipeline_dict:
		dbg_steering_pipeline_name[steering_pipeline_dict[sp_name]] = sp_name
	# initialize debug drawing
	for color in dbg_materials:
		dbg_materials[color].albedo_color = color
		dbg_materials[color].emission_enabled = true
		dbg_materials[color].emission = color
		dbg_materials[color].emission_energy_multiplier = 2.0
	# dbg_draw_path
	var current_scene = globals.get_current_scene()
	var mesh_inst_tmp: MeshInstance3D = null
	if ((not dbg_draw_path) and (dbg_draw_path_scene != null)):
		dbg_draw_path = dbg_draw_path_scene.instantiate()
		dbg_draw_path.material = dbg_materials[Color.RED]
		dbg_draw_path.point_size = dbg_path_point_size
		current_scene.add_child(dbg_draw_path)
	# dbg_oa_wisker_r_geom
	mesh_inst_tmp = MeshInstance3D.new()
	mesh_inst_tmp.mesh = dbg_oa_wisker_r_geom
	body.add_child(mesh_inst_tmp)
	# dbg_oa_wisker_l_geom
	mesh_inst_tmp = MeshInstance3D.new()
	mesh_inst_tmp.mesh = dbg_oa_wisker_l_geom
	body.add_child(mesh_inst_tmp)
	# dbg_steering_output_geom
	mesh_inst_tmp = MeshInstance3D.new()
	mesh_inst_tmp.mesh = dbg_steering_output_geom
	body.add_child(mesh_inst_tmp)
	# goal point
	var point = SphereMesh.new()
	point.height = POINT_HEIGHT
	point.radius = POINT_RADIUS
	point.material = dbg_materials[Color.GREEN]
	dbg_goal_mesh.mesh = point
	body.add_child(dbg_goal_mesh)
	dbg_goal_mesh.visible = true if debug_goal else false
	# collision point
	point = SphereMesh.new()
	point.height = POINT_HEIGHT
	point.radius = POINT_RADIUS
	point.material = dbg_materials[Color.BLACK]
	dbg_oa_collision_point_mesh.mesh = point
	body.add_child(dbg_oa_collision_point_mesh)
	dbg_oa_collision_point_mesh.visible = true if debug_constraint == "ObstacleAvoidance" else false
	# obstacle avoidance goal position point
	point = SphereMesh.new()
	point.height = POINT_HEIGHT
	point.radius = POINT_RADIUS
	point.material = dbg_materials[Color.RED]
	dbg_oa_goal_position_mesh.mesh = point
	body.add_child(dbg_oa_goal_position_mesh)
	dbg_oa_goal_position_mesh.visible = true if debug_constraint == "ObstacleAvoidance" else false
	# separation constraint result_linear
	mesh_inst_tmp = MeshInstance3D.new()
	mesh_inst_tmp.mesh = dbg_s_result_linear_geom
	body.add_child(mesh_inst_tmp)
	#DEBUG>
	
	#<DEBUG
	# initialize steering pipeline related items
	## TARGETERS
	for sp_name in steering_pipeline_dict:
		for targeter in steering_pipeline_dict[sp_name].targeters:
			match targeter.type_name:
				"ChaseTargeter":
					dbg_c_targeter[sp_name] = targeter
				"StandStillTargeter":
					dbg_ss_targeter[sp_name] = targeter
				_:
					print(sp_name, "NO TARGETERS!")
	#DEBUG>
	
	## DECOMPOSERS
	
	#<DEBUG
	for sp_name in steering_pipeline_dict:
		for decomposer in steering_pipeline_dict[sp_name].decomposers:
			match decomposer.type_name:
				"PlanningDecomposer":
					dbg_p_decomposer[sp_name] = decomposer
				_:
					print(sp_name, "NO DECOMPOSERS!")
	#DEBUG>
	
	## CONSTRAINTS
	
	#<DEBUG
	for sp_name in steering_pipeline_dict:
		for constraint in steering_pipeline_dict[sp_name].constraints:
			match constraint.type_name:
				"ObstacleAvoidanceConstraint":
					dbg_oa_constraint[sp_name] = constraint
				"SeparationConstraint":
					dbg_s_constraint[sp_name] = constraint
				"MinSpeedConstraint":
					dbg_ms_constraint[sp_name] = constraint
				_:
					print(sp_name, "NO CONSTRAINTS!")
	#DEBUG>
	
	### ACTUATOR
	
	#<DEBUG
	for sp_name in steering_pipeline_dict:
		var actuator = steering_pipeline_dict[sp_name].actuator
		match actuator.type_name:
			"SeekActuator":
				dbg_s_actuator[sp_name] = actuator
			"ArriveActuator":
				dbg_a_actuator[sp_name] = actuator
			_:
				print(sp_name, "NO ACTUATORS!")
	#DEBUG>
	
	### DEADLOCK BEHAVIOR
	
	#<DEBUG
	for sp_name in steering_pipeline_dict:
		dbg_deadlock[sp_name] = steering_pipeline_dict[sp_name].deadlock
		if not dbg_deadlock[sp_name]:
			print(sp_name, "NO DEADLOCK!")
	#DEBUG>


func _update(_delta):
	super(_delta)
	
	#<DEBUG
	var steering_pipeline_name = dbg_steering_pipeline_name[steering_pipeline]
	#
	match debug_targeter:
		"Chase":
			if dbg_c_targeter.get(steering_pipeline_name, null):
				# chase targeter
				var c_targeter = dbg_c_targeter[steering_pipeline_name]
				print("Targeter:", c_targeter.type_name)
		"StandStill":
			if dbg_ss_targeter.get(steering_pipeline_name, null):
				# stand still targeter
				var ss_targeter = dbg_ss_targeter[steering_pipeline_name]
				print("Targeter:", ss_targeter.type_name)
	#
	match debug_decomposer:
		"Planning":
				if dbg_draw_path:
					var path = steering_pipeline.decomposers[0].path.path
					dbg_draw_path.draw_path(path)
	#
	match debug_constraint:
		"ObstacleAvoidance":
			if dbg_oa_constraint.get(steering_pipeline_name, null):
				# obstacle avoidance constraint
				var oa_constraint = dbg_oa_constraint[steering_pipeline_name]
				dbg_lib.debug_draw_obstacle_avoidance(oa_constraint, body,
					dbg_oa_wisker_r_geom, dbg_oa_wisker_l_geom, dims,
					dbg_drawing_size, dbg_materials[Color.BLACK])
				if oa_constraint._has_collisions:
					dbg_oa_collision_point_mesh.show()
					dbg_oa_collision_point_mesh.global_transform.origin = oa_constraint._mean_collision_pos
					dbg_oa_goal_position_mesh.show()
					var new_goal_position = oa_constraint._new_goal.position
					if dims == AI_TOOLS.Dimensions.TWO:
						dbg_oa_goal_position_mesh.global_transform.origin = Vector3(
								new_goal_position.x, 0.0, new_goal_position.y)
					else:
						dbg_oa_goal_position_mesh.global_transform.origin = new_goal_position
				else:
					dbg_oa_collision_point_mesh.hide()
					dbg_oa_goal_position_mesh.hide()
		"Separation":
			if dbg_s_constraint.get(steering_pipeline_name, null):
				# separation constraint
				var s_constraint = dbg_s_constraint[steering_pipeline_name]
				dbg_lib.debug_draw_vector(body, s_constraint._result_linear * 10, 
						dbg_s_result_linear_geom, dims, dbg_drawing_size,
						dbg_materials[Color.YELLOW])
		"MinSpeedConstraint":
			if dbg_ms_constraint.get(steering_pipeline_name, null):
				# min speed constraint
				var ms_constraint = dbg_ms_constraint[steering_pipeline_name]
				print("Constraint:", ms_constraint.type_name)
	#
	match debug_actuator:
		"Seek":
			if dbg_s_actuator.get(steering_pipeline_name, null):
				# seek actuator
				var s_actuator = dbg_s_actuator[steering_pipeline_name]
				print("Actuator:", s_actuator.type_name)
		"Arrive":
			if dbg_a_actuator.get(steering_pipeline_name, null):
				# arrive actuator
				var a_actuator = dbg_a_actuator[steering_pipeline_name]
				print("Actuator:", a_actuator.type_name)
	#
	if debug_goal:
		# goal
		if steering_pipeline._path_is_valid:
			dbg_goal_mesh.visible = true
			var new_goal_position = steering_pipeline._goal.position
			if dims == AI_TOOLS.Dimensions.TWO:
				dbg_goal_mesh.global_transform.origin = Vector3(
						new_goal_position.x, 0.0, new_goal_position.y)
			else:
				dbg_goal_mesh.global_transform.origin = new_goal_position
		else:
			dbg_goal_mesh.visible = false
	#
	if debug_steering_output:
		# steering output
		dbg_lib.debug_draw_vector(body, steering_output.linear, 
				dbg_steering_output_geom, dims, dbg_drawing_size,
				dbg_materials[Color.VIOLET])
	#DEBUG>


func _get_class():
	return "VehicleAISteeringPipelineTest"


func _is_class(value):
	return "VehicleAISteeringPipelineTest" == value
