extends Node3D


signal test_player_signal


var sounds = {	
	"vehicle_collision_static": preload("res://assets/sounds/qubodup-crash.ogg"),
	"vehicle_collision_dynamic": preload("res://assets/sounds/cast iron clangs - Marker #10.ogg"),
	"vehicle_engine_1": preload("res://assets/sounds/435656__broken-head-productions__ford-ltl-9000-engine-start-and-idle.ogg"),
} 
const PLAYER = preload("res://audio/sound_player.tscn")
const TIMELAPSE = 6.0
var timer = TIMELAPSE
var one_shot1 = false
var one_shot2 = false
var one_shot3 = false


func _ready():
	test_player_signal.connect(_test_player)


func _physics_process(delta):
	if (timer <= (TIMELAPSE / 3.0) * 2.0) and (not one_shot1):
		emit_signal("test_player_signal", sounds["vehicle_collision_static"], false)
		one_shot1 = true
	elif (timer <= TIMELAPSE / 3.0) and (not one_shot2):
		emit_signal("test_player_signal", sounds["vehicle_collision_dynamic"], false)
		one_shot2 = true
	elif (timer <= 0.0):
		if not one_shot3:
			emit_signal("test_player_signal", sounds["vehicle_engine_1"], true)
			one_shot3 = true
		timer = TIMELAPSE
		one_shot1 = false
		one_shot2 = false
	else:
		timer -= delta
	print(timer)


func _test_player(sound,loop_sound):
	var new_audio = PLAYER.instantiate()
	new_audio.should_loop = loop_sound
	globals.add_child(new_audio)
	globals.created_audio[sound] = new_audio
	new_audio.play_sound(sound, null)
