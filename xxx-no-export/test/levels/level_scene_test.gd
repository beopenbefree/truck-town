class_name LevelSceneTest
extends LevelScene


@export_enum("bus", "car01", "pickup", "sedan", "wagon") var vehicle: String = "sedan" 
@export var enable_fullscreen: bool = false
@export var update_counter_enable: bool = false
@export var show_debug_info: bool = true
@export var show_hud: bool = false
@export var show_map_target: bool = false
@export var play_song: bool = false
@export_range(0, 10, 1, "or_greater") var game_session: int = 0
@export_range(0, 5000, 1, "or_greater") var score: int = 0

var globals_GAME_DATA = {
	"vehicles": [
		{
			"name": "sedan",
			"path": "res://xxx-no-export/test/vehicles/models/sedan_model_test.tscn",
			"sound": "vehicle_engine_2",
			"image": "res://assets/images/choose-image-sedan.png",
			"icon": "res://assets/images/choose-icon-sedan.png",
			"icon_off": "res://assets/images/choose-icon-off-sedan.png",
		},
		{
			"name": "wagon",
			"path": "res://xxx-no-export/test/vehicles/models/wagon_model_test.tscn",
			"sound": "vehicle_engine_2",
			"image": "res://assets/images/choose-image-wagon.png",
			"icon": "res://assets/images/choose-icon-wagon.png",
			"icon_off": "res://assets/images/choose-icon-off-wagon.png",
		},
		{
			"name": "pickup",
			"path": "res://xxx-no-export/test/vehicles/models/pickup_model_test.tscn",
			"sound": "vehicle_engine_2",
			"image": "res://assets/images/choose-image-pickup.png",
			"icon": "res://assets/images/choose-icon-pickup.png",
			"icon_off": "res://assets/images/choose-icon-off-pickup.png",
		},
		{
			"name": "bus",
			"path": "res://xxx-no-export/test/vehicles/models/bus_model_test.tscn",
			"sound": "vehicle_engine_1",
			"image": "res://assets/images/choose-image-bus.png",
			"icon": "res://assets/images/choose-icon-bus.png",
			"icon_off": "res://assets/images/choose-icon-off-bus.png",
		},
		{
			"name": "car01",
			"path": "res://xxx-no-export/test/vehicles/models/car_01_model_test.tscn",
			"sound": "vehicle_engine_2",
			"image": "res://assets/images/choose-image-car01.png",
			"icon": "res://assets/images/choose-icon-car01.png",
			"icon_off": "res://assets/images/choose-icon-off-car01.png",
		},
	]
	}

@onready var debug_info: Control = get_node_or_null("DEBUGINFO")
@onready var AI_PHYSICS_FRAME_UPDATE: int = globals.AI_PHYSICS_FRAME_UPDATE


func _ready():
	super()
	globals.score = score
	globals.score_current_level = score
	globals.enable_end_zones()
	globals.current_game_session = game_session
	# add current vehicle
	_add_vehicle()
	globals.current_vehicle_sound = _get_current_vehicle_sound(vehicle)
	# WARNING: deferred setup (don't use await in _ready())
	var delta = $FadeAnimation.get_animation("fade").length * 3.0
	var ready_timer = Timer.new()
	ready_timer.timeout.connect(_on_ready_timer_timeout)
	ready_timer.wait_time = delta
	ready_timer.one_shot = true
	add_child(ready_timer)
	ready_timer.start()


func _physics_process(delta):
	if update_counter_enable:
		super(delta)
	elif starting:
		return
	
	#<DEBUG
	if (show_debug_info and (Engine.get_physics_frames() % AI_PHYSICS_FRAME_UPDATE == 0)):
		if (vehicle_camera_idx == 0):
			if debug_info.visible:
				debug_info.visible = false
		else:
			if not debug_info.visible:
				debug_info.visible = true
			# vehicle ai driver infos
			var vehicle_ai_driver = all_vehicle_ai_drivers[vehicle_camera_idx - 1]
			if is_instance_valid(vehicle_ai_driver.vehicle_steering.target):
				debug_info.get_node("HBoxContainer/INFO/Target/Value").text = (
						vehicle_ai_driver.vehicle_steering.target.name)
				debug_info.get_node("HBoxContainer/INFO/State/Value").text = (
						vehicle_ai_driver.fsm.current_state)
			# vehicle ai behavior infos
			var vehicle_ai_behavior = ScriptLibrary.find_nodes_by_type(
					VehicleAISteeringPipelineTest, vehicle_ai_driver, false)[0]
			var behavior_label = debug_info.get_node("HBoxContainer/INFO/SteeringBehavior/Value")
			var constraint_violated_label = debug_info.get_node("HBoxContainer/INFO/ConstraintViolated/Value")
			var steering_pipeline_name = vehicle_ai_behavior.dbg_steering_pipeline_name[vehicle_ai_behavior.steering_pipeline]
			if vehicle_ai_behavior.steering_pipeline._path_is_valid:
				behavior_label.text = steering_pipeline_name
			else:
				behavior_label.text = vehicle_ai_behavior.steering_pipeline.deadlock.name
			if vehicle_ai_behavior.steering_pipeline._last_constraint_violated:
				constraint_violated_label.text = vehicle_ai_behavior.steering_pipeline._last_constraint_violated.type_name
			elif (not vehicle_ai_behavior.steering_pipeline._last_constraint_violated):
				constraint_violated_label.text = "NONE"
	#DEBUG>


func _on_ready_timer_timeout():
	var camera_group = globals.current_vehicle_body.camera_group_node
	camera_group.get_current_camera().current = true
	if enable_fullscreen:
		get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN
	if debug_info:
		debug_info.visible = show_debug_info
	$HUD.visible = show_hud
	var music_name = globals.get_song_name(globals.music_track_index)
	if not play_song:
		globals.clear_sound(music_name)
	globals.current_vehicle_body.get_node("MapTarget").visible = show_map_target


func _add_vehicle():
	var vehicle_scene: PackedScene = null
	for vehicle_data in globals_GAME_DATA["vehicles"]:
		if vehicle_data["name"] == vehicle:
			vehicle_scene = load(vehicle_data["path"])
			break
	globals.current_vehicle = vehicle_scene.instantiate()
	globals.current_vehicle_body = ScriptLibrary.find_nodes_by_type(Vehicle,
			globals.current_vehicle, false)[0]
	get_node("InstancePos").add_child(globals.current_vehicle)


func _get_current_vehicle_sound(p_vehicle_name):
	var vehicle_data
	for item in globals_GAME_DATA["vehicles"]:
		if item["name"] == p_vehicle_name:
			vehicle_data = item
			break
	return vehicle_data["sound"]


func _get_class():
	return "LevelSceneTest"


func _is_class(value):
	return "LevelSceneTest" == value
