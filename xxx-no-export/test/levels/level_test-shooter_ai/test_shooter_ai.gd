extends Node3D


@export var shooter_path: NodePath = ""

var commands = [
	"weapon_change_number_0",
	"weapon_change_number_1",
	"weapon_change_number_2",
	"weapon_change_number_3",
	"shift_weapon_positive",
	"shift_weapon_negative",
	"want_reload",
	"want_fire",
	"want_change_grenade",
	"want_fire_grenade",
]
@onready var cameras = [
	$OrbitCamera,
	$Target/OrbitCamera
]
@onready var camera_idx := 0
var shooter

var total_count_down := 0
var current_count := 0 :
	set(value):
		current_count = 0


func _ready():
	globals.enable_all_sounds(true)
	if not shooter_path.is_empty():
		shooter = call_deferred("get_node", shooter_path)
	for cmd in commands:
		var btn:Button = get_node("VBoxContainer/" + cmd)
		btn.pressed.connect(_on_command.bind(cmd))


func _input(event:InputEvent):
	# Receives key input
	if event.is_action_pressed("swap_camera"):
		cameras[camera_idx].current = false
		camera_idx = (camera_idx + 1) % cameras.size()
		cameras[camera_idx].current = true


func _on_command(cmd):
	shooter.set(cmd, true)


func _on_area_3d_body_entered(body):
	print("_on_area_3d_body_entered")


func _on_area_3d_body_exited(body):
	print("_on_area_3d_body_exited")
