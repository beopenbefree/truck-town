class_name LevelSceneDebugTest
extends LevelSceneTest


@export_enum("City", "Landscape", "Mixed") var environment: int = 1
@export var city_textures: Array[Array] = []
@export var landscape_textures: Array[Array] = []

var environments: Array[String] = ["City", "Landscape", "Mixed"]


func _ready():
	LevelSceneDebug.prepare_debug_environment(weakref(self), environment)
	super()


func _get_class():
	return "LevelSceneDebugTest"


func _is_class(value):
	return "LevelSceneDebugTest" == value
