extends Node3D


var grenade = preload("res://levels/elements/grenade.tscn")
var sticky_grenade = preload("res://levels/elements/sticky_grenade.tscn")


func _on_button_pressed():
	var grenade_inst:RigidBody3D = grenade.instantiate()
	var sticky_grenade_inst:RigidBody3D = sticky_grenade.instantiate()
	add_child(grenade_inst)
	add_child(sticky_grenade_inst)
	grenade_inst.transform.origin = Vector3(-2, 0.67, 2)
	sticky_grenade_inst.transform.origin = Vector3(-2, 0.67, -2)
