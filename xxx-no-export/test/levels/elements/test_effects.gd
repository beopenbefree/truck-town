extends Node3D


func _ready():
	$LaserBeamHolder.emit(false)
	$PhysicBodyDynamicTest.dissolve(true)
	$PhysicBodyDynamicTest2.dissolve(true)


func _on_button_pressed():
	if $LaserBeamHolder.is_emitting():
		$LaserBeamHolder.emit(false)
	else:
		$LaserBeamHolder.emit(true, -5.0)
