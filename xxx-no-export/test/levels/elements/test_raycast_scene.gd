extends Node3D


@export var target_path := NodePath()

var current_count: float = 0.0 :
	set(value):
		current_count = 0.0
var total_count_down: float = 0.0
@onready var target:Node = get_node(target_path)


func _on_button_pressed():
#	var from_point = $RayCast3D.global_transform.origin
#	var dist = float($TextEdit.text)
#	var to_point = from_point + Vector3(0.0, 0.0, dist)
#	$RayCast3D.emit_shot_trail(from_point, to_point)
	$RayCast3D.shoot(target)
