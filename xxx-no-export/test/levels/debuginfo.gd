extends Control


@export_node_path("LevelScene") var scene_path = NodePath()

@onready var scene: LevelScene = get_node_or_null(scene_path)


func _on_unlock_ai_pressed():
	if scene and (scene.vehicle_camera_idx != 0):
		var vehicle_driver: VehicleAIDriverTest = scene.all_vehicle_ai_drivers[scene.vehicle_camera_idx - 1]
		vehicle_driver._on_unlock_ai_pressed()
