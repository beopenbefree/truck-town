extends Node3D


var steer_limit:float = 0.4
var steering_wheel_angle_max:float = 80
var steering_wheel_scale: Vector3 = Vector3.ONE

# steering wheel stuff
var steering_wheel_node:Node3D = null
var steering_wheel_angle_factor:float = 0.0

var steering_enabled:bool = true
var steer_target:float = 0
var steering:float = 0


func _ready():
	# steering wheel node (if any)
	steering_wheel_node = get_node("Body/SteeringWheel")
	if steering_wheel_node:
		steering_wheel_angle_factor = ((1.0 / steer_limit) * 
				deg_to_rad(steering_wheel_angle_max))
		var scaling = Vector3.ONE * steering_wheel_scale
		steering_wheel_node.scale = scaling
	# HACK
	globals.current_vehicle = self


func _physics_process(delta):
	_manage_input(delta)
	steer_target = 0.0
	if steering_enabled and steering_wheel_node:
		if Input.is_action_pressed("turn_left") or Input.is_action_pressed("turn_right"):
			steer_target = Input.get_action_strength("turn_left") - Input.get_action_strength("turn_right")
		steer_target *= steer_limit
		steering += (steer_target - steering) * delta
		# turn the steering wheel
		steering_wheel_node.update(steering * steering_wheel_angle_factor)


func _manage_input(delta):
	var camera = $Body/CameraGroup/FixedCamera/Camera3D/RealCamera
	if Input.is_action_just_pressed("zoom"):
		camera.fov = 35.0
	elif Input.is_action_just_released("zoom"):
		camera.fov = 75.0
	if Input.is_action_pressed("ui_left"):
		$Body.rotate_y(0.01)
	elif Input.is_action_pressed("ui_right"):
		$Body.rotate_y(-0.01)
