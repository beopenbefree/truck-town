extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export var target_scene: NodePath = "Target"
@export var nav_mesh_scene: NodePath = NodePath()
@export var navmesh_agent_radius: float = 2.0
@export var vehicle_pipeline_scene: PackedScene = preload(
		"res://vehicles/ai/models/wagon_ai.tscn")
@export var environment_scene: PackedScene = preload(
		"res://levels/level_debug/debug_scene.tscn")

@export var flock_num: int = 5

@export var pipeline_params_file = "" # (String, FILE)

@export var placement_boundary: float = 250.0

var backing_time: float = 0.0

@onready var target:Node3D = get_node(target_scene)
@onready var nav_mesh:NavigationRegion3D = get_node(nav_mesh_scene)
@onready var environment:Node3D = environment_scene.instantiate()

var total_count_down := 0
var current_count := 0 :
	set(value):
		current_count = 0

#<DEBUG
@export var dbg_draw_path_scene: PackedScene = preload(
		"res://ai/debug_draw_path.tscn")
#DEBUG>


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled


func _on_nav_mesh_ready():
	print("backing time: ", Time. get_unix_time_from_system() - backing_time)
	randomize()
	# create a common collision detector: for obstacle avoidance constraints
	var collision_detector = AI.CollisionDetector.new(self)
	# create the characters
	var all_targets = []
	for i in flock_num:
		var vehicle_pipeline = vehicle_pipeline_scene.instantiate()
		vehicle_pipeline.pipeline_params_file = pipeline_params_file
		vehicle_pipeline.enabled = enabled
		vehicle_pipeline.collision_detector = collision_detector
		add_child(vehicle_pipeline)
		
		#<DEBUG
		var dbg_draw_path = dbg_draw_path_scene.instantiate()
		add_child(dbg_draw_path)
		vehicle_pipeline.dbg_draw_path = dbg_draw_path
		#DEBUG>
		
		# place it randomly
		var x = AI_TOOLS.random_binomial() * placement_boundary
		var z = AI_TOOLS.random_binomial() * placement_boundary
		vehicle_pipeline.body.global_transform.origin = Vector3(x, 1.0, z)
		vehicle_pipeline.target = target
		all_targets.append(vehicle_pipeline.steering_pipeline.character)
	# updates s_targets: i.e. the others characters
	for vehicle_pipeline in get_children():
		if not "steering_pipeline" in vehicle_pipeline:
			continue
		for constraint in vehicle_pipeline.steering_pipeline.constraints:
			if constraint is AISteeringPipelineImpl.SeparationConstraint:
				var targets = []
				for other in all_targets:
					if other != vehicle_pipeline.steering_pipeline.character:
						targets.append(other)
				constraint.targets.append_array(targets)


func _set_enabled(value):
	enabled = value
	visible = value
	for child in get_children():
		if "visible" in child:
			child.visible = value
		if "enabled" in child:
			child.enabled = value
