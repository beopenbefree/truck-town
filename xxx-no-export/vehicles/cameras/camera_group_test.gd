class_name CameraGroupTest
extends CameraGroup


func _ready():
	CAMERA_OBJECTS.append(FollowCameraTest)
	super()


func _get_class():
	return "CameraGroupTest"


func _is_class(value):
	return "CameraGroupTest" == value
