class_name ShooterBase
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export_node_path("VehicleBase") var vehicle_body = NodePath()

@export_node_path("RemoteTransform3D") var remote_transform_target_right = NodePath()
@export_node_path("RemoteTransform3D") var remote_transform_target_left = NodePath()

@export_flags_3d_render var visual_instance_layers: int = 0b0

@export var weapon_scenes: Dictionary = {
	"UNARMED":null, 
	"GUN":preload("res://assets/player/weapons/pistol_scene.tscn"), 
	"RIFLE":preload("res://assets/player/weapons/rifle_scene.tscn"),
	"LAUNCHER":preload("res://assets/player/weapons/rocket_launcher_scene.tscn"), 
}

@export_group("Shooting properties")
@export var fire_enabled: bool = true
@export var launch_grenade_enabled: bool = true
@export var grab_object_enabled: bool = true
@export var weapons_by_session: bool = true
@export var weapons_by_score: bool = false

@export var AMMO_AMOUNT: int = 400

@export var GRENADE_AMOUNT: int = 10
var grenade_amounts:Dictionary = {
	"Grenade":0,
	"Sticky Grenade":0,
	}
@export var GRENADE_THROW_FORCE: float = 25.0

@export var OBJECT_THROW_FORCE: float = 20.0
@export var OBJECT_GRAB_DISTANCE: float = 7.0
@export var OBJECT_GRAB_RAY_DISTANCE: float = 7.0

@export_range(0.0, 1.0, 0.05) var throw_max_delta_force: float = 0.5 # fraction
# HACK
@export_range(0.0, 1.0, 0.01, "or_greater") var grenade_throw_delta_height: float = 0.0

@export_group("Pointing modes")
@export var aimer_use: bool = false: set = _set_aimer_use

@export var aim_direct_use: bool = false: set = _set_aim_direct_use
@export_subgroup("Aim Direct properties")
@export_range(0.0, 90.0, 1.0) var aim_max_yaw_angle: float = 30.0
@export_range(0.05, 1.0, 0.05) var aim_yaw_speed: float = 0.3
@export_range(0.0, 90.0, 1.0) var aim_max_pitch_angle: float = 15.0
@export_range(0.05, 1.0, 0.05) var aim_pitch_speed: float = 0.3

@onready var aimer: Aimer = ScriptLibrary.find_nodes_by_type(
		Aimer, self, true)[0]
@onready var remote_transform_target_right_node: RemoteTransform3D = get_node_or_null(
		remote_transform_target_right)
@onready var target_right: NodePath = remote_transform_target_right_node.get_remote_node()
@onready var remote_transform_target_left_node: RemoteTransform3D = get_node_or_null(
		remote_transform_target_left)
@onready var target_left: NodePath = remote_transform_target_left_node.get_remote_node()

var mouse_scroll_value: float = 0

var animation_manager: AnimationPlayer = null

var current_weapon_name: String = "UNARMED"
var weapons: Dictionary = {"UNARMED":null,}
const WEAPON_NUMBER_TO_NAME:Dictionary = {0:"UNARMED", 1:"GUN", 2:"RIFLE", 3:"LAUNCHER"}
const WEAPON_NAME_TO_NUMBER:Dictionary = {"UNARMED":0, "GUN":1, "RIFLE":2, "LAUNCHER":3}
var changing_weapon:bool = false
var changing_weapon_name:String = "UNARMED"
var reloading_weapon:bool = false

var current_grenade:String = "Grenade"
var grenade_scene = preload("res://levels/elements/grenade.tscn")
var sticky_grenade_scene = preload("res://levels/elements/sticky_grenade.tscn")

var grabbed_object:PhysicsBody3D = null
var grabbed_object_old_parent:Node3D = null

var head:Node3D = null

var aim_max_yaw_angle_rad: float = 0.0
var aim_max_pitch_angle_rad: float = 0.0

var touch_aim_direction:Vector2 = Vector2.ZERO
var touch_touching:bool = false

var start_aiming_targets_transform:Transform3D = Transform3D.IDENTITY

@onready var aiming_target_right:Node3D = $RotationHelper/AimingTargets/TargetRight

@onready var vehicle_body_node:VehicleBase = get_node_or_null(vehicle_body)

@onready var model_mesh_instances: Array = ScriptLibrary.find_nodes_by_type(
		MeshInstance3D, self)


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	aimer_use = aimer_use
	aim_direct_use = aim_direct_use
	#
	_initialize()


func _physics_process(delta):
	process_input(delta)
	
	process_aim_weapon(delta)
	
	if grabbed_object == null:
		process_changing_weapons(delta)
		process_reloading(delta)


func process_input(_delta):
	# to be overridden
	push_warning("Unimplemented process_input(delta)")


func process_aim_weapon(_delta):
	# to be overridden
	push_warning("Unimplemented process_aim_weapon(delta)")


func process_changing_weapons(_delta):
	if changing_weapon == true:
		
		var weapon_unequipped = false
		var current_weapon = weapons[current_weapon_name]
		if current_weapon == null:
			weapon_unequipped = true
		else:
			if current_weapon.enabled == true:
				weapon_unequipped = current_weapon.unequip_weapon()
			else:
				weapon_unequipped = true
		
		if weapon_unequipped == true:
			
			var weapon_equiped = false
			var weapon_to_equip = weapons[changing_weapon_name]
			
			if weapon_to_equip == null:
				weapon_equiped = true
			else:
				if weapon_to_equip.enabled == false:
					# HACK: to tell wether changing weapon or normal equipping
					# (since we have only one equip animation)
					weapon_equiped = weapon_to_equip.equip_weapon(changing_weapon)
				else:
					weapon_equiped = true
			
			if weapon_equiped == true:
				changing_weapon = false
				current_weapon_name = changing_weapon_name
				changing_weapon_name = ""


func process_reloading(_delta):
	if reloading_weapon == true:
		var current_weapon = weapons[current_weapon_name]
		if current_weapon != null:
			current_weapon.reload_weapon()
		reloading_weapon = false


func fire_weapon():
	if fire_enabled:
		if changing_weapon == true:
			return
		var current_weapon = weapons[current_weapon_name]
		if current_weapon:
			current_weapon.fire_weapon()


func add_ammo(additional_ammo):
	var ammo_added = 0
	var weapon_to_refill = null
	if current_weapon_name != "UNARMED":
		# choose to refill the current weapon
		weapon_to_refill = weapons[current_weapon_name]
	else:
		# choose to refill the weapon with the least ammo
		var min_ammo = AMMO_AMOUNT
		for weapon:WeaponBase in weapons.values():
			if weapon and (weapon.spare_ammo < min_ammo):
				weapon_to_refill = weapon
				min_ammo = weapon.spare_ammo
	if weapon_to_refill:
		# some weapon needs refill (spare_ammo < AMMO_AMOUNT)
		var prev_ammo = weapon_to_refill.spare_ammo
		if weapon_to_refill.CAN_REFILL == true:
			weapon_to_refill.spare_ammo += weapon_to_refill.AMMO_IN_MAG * additional_ammo
			weapon_to_refill.spare_ammo = clamp(weapon_to_refill.spare_ammo,
					0, AMMO_AMOUNT)
		ammo_added = weapon_to_refill.spare_ammo - prev_ammo
	return ammo_added


func add_grenade(additional_grenade):
	# choose to refill the current grenade
	var prev_grenade = grenade_amounts[current_grenade]
	grenade_amounts[current_grenade] += additional_grenade
	grenade_amounts[current_grenade] = clamp(grenade_amounts[current_grenade], 
			0, GRENADE_AMOUNT)
	# return added grenade number
	return (grenade_amounts[current_grenade] - prev_grenade)


func reset_aiming_targets_transform():
	aiming_target_right.transform = start_aiming_targets_transform


func vehicle_body_steering_enable(value:bool):
	if vehicle_body_node:
		vehicle_body_node.steering_enabled = value


func set_aimer_target(target:Node3D):
	if aimer:
		aimer.target_node = target


func on_start_fighting(arm:String):
	if arm.to_lower() == "right":
		remote_transform_target_right_node.set_remote_node("")
	if arm.to_lower() == "left":
		remote_transform_target_left_node.set_remote_node("")


func on_end_fighting(arm:String):
	if arm.to_lower() == "right":
		remote_transform_target_right_node.set_remote_node(target_right)
	if arm.to_lower() == "left":
		remote_transform_target_left_node.set_remote_node(target_left)


func on_weapon_instantiation(weapon_instance, p_scale, p_position,
		p_rotation, fire_point):
	weapon_instance.scale = p_scale
	weapon_instance.position = p_position
	weapon_instance.rotation = p_rotation
	aiming_target_right.add_child(weapon_instance)
	fire_point.shot_point_node = weapon_instance.get_node("ShotPoint")
	fire_point.aim_point_node = weapon_instance.get_node("AimPoint")
	fire_point.shot_animation_player = weapon_instance.get_node("AnimationPlayer")
	if fire_point.has_method("on_weapon_instantiation"):
		fire_point.on_weapon_instantiation(weapon_instance)
	if vehicle_body_node.has_method("on_weapon_instantiation"):
		vehicle_body_node.on_weapon_instantiation(changing_weapon_name)


func on_weapon_freeing():
	if vehicle_body_node.has_method("on_weapon_freeing"):
		if changing_weapon_name == "UNARMED":
			vehicle_body_node.on_weapon_freeing(
					changing_weapon_name, true)
		else:
			vehicle_body_node.on_weapon_freeing(
					changing_weapon_name, false)


func _initialize():
	head = $RotationHelper/Head
	aim_max_yaw_angle_rad = deg_to_rad(aim_max_yaw_angle)
	aim_max_pitch_angle_rad = deg_to_rad(aim_max_pitch_angle)
	#
	animation_manager =$RotationHelper/AimingTargets/AnimationPlayer
	animation_manager.callback_function = Callable(self, "fire_weapon")
	# set visual layers for mesh instances
	for mesh_instance in model_mesh_instances:
		mesh_instance.layers = visual_instance_layers
	# globally available weapons
	var available_weapons = (
			globals.GAME_DATA["LEVELS"][globals.current_level_idx]["shooter_weapons"])
	# compute current number of allowed weapons: start with all available weapons
	var weapons_number: int = available_weapons.size()
	if weapons_by_session:
		# limit number for current game session: at least one weapon
		weapons_number = clampi(floori(1.0 + globals.current_game_session * 0.5),
				1, available_weapons.size())
	elif weapons_by_score:
		# limit number for current game score: at least one weapon
		var score_gap = globals.GAME_DATA["score_gap_for_next_player_upgrade"]
		weapons_number = clampi(floori(1.0 + globals.score / score_gap),
				1, available_weapons.size())
	# allocate the weapons allowed for the current level
	if (weapons_number >= 1) and ("GUN" in available_weapons):
		weapons["GUN"] = $RotationHelper/FirePoints/Gun
	if (weapons_number >= 2) and ("RIFLE" in available_weapons):
		weapons["RIFLE"] = $RotationHelper/FirePoints/Rifle
	if (weapons_number >= 3) and ("LAUNCHER" in available_weapons):
		weapons["LAUNCHER"] = $RotationHelper/FirePoints/Launcher
	# setup allowed weapons
	for weapon in weapons:
		var weapon_node = weapons[weapon]
		if weapon_node != null:
			weapon_node.player_node = self
			weapon_node.rotate_object_local(Vector3(0, 1, 0), deg_to_rad(180))
	#
	# set currente settings
	current_weapon_name = "UNARMED"
	changing_weapon_name = "UNARMED"
	current_grenade = "Grenade"
	# refill grenades
	for grenade in grenade_amounts:
		grenade_amounts[grenade] = GRENADE_AMOUNT
	#
	start_aiming_targets_transform = aiming_target_right.transform
	$RotationHelper/AnimatedModel/AnimationPlayer.play("Steer-Pose")
	# not mobile platforms don't use touch controls
	if not OS.has_feature("mobile"):
		$Controls.queue_free()


func _process_input(_delta,
		weapon_change_number_0:bool,
		weapon_change_number_1:bool,
		weapon_change_number_2:bool,
		weapon_change_number_3:bool,
		shift_weapon_positive:bool,
		shift_weapon_negative:bool,
		want_reload:bool,
		want_fire:bool,
		want_change_grenade:bool,
		want_fire_grenade:bool,
		sticky_area_exclude_mask:int):
	#
	var weapon_change_number = WEAPON_NAME_TO_NUMBER[current_weapon_name]
	if weapon_change_number_0:
		weapon_change_number = 0
	if weapon_change_number_1:
		weapon_change_number = 1
	if weapon_change_number_2:
		weapon_change_number = 2
	if weapon_change_number_3:
		weapon_change_number = 3
	if shift_weapon_positive:
		weapon_change_number += 1
	if shift_weapon_negative:
		weapon_change_number -= 1
	weapon_change_number = clamp(weapon_change_number, 0, weapons.size() - 1)
	
	if changing_weapon == false:
		if reloading_weapon == false:
			if WEAPON_NUMBER_TO_NAME[weapon_change_number] != current_weapon_name:
				changing_weapon_name = WEAPON_NUMBER_TO_NAME[weapon_change_number]
				changing_weapon = true
				mouse_scroll_value = weapon_change_number
	
	if reloading_weapon == false:
		if changing_weapon == false:
			if want_reload:
				var current_weapon = weapons[current_weapon_name]
				if current_weapon != null:
					if current_weapon.CAN_RELOAD == true:
						var current_anim_state = animation_manager.fsm.current_state
						var is_reloading = false
						for weapon in weapons:
							var weapon_node = weapons[weapon]
							if weapon_node != null:
								if current_anim_state == weapon_node.RELOADING_ANIM_NAME:
									is_reloading = true
						if is_reloading == false:
							reloading_weapon = true
	
	if fire_enabled:
		if want_fire:
			if reloading_weapon == false:
				if changing_weapon == false:
					var current_weapon = weapons[current_weapon_name]
					if current_weapon != null:
						if current_weapon.ammo_in_weapon > 0:
							if animation_manager.fsm.current_state == current_weapon.IDLE_ANIM_NAME:
								animation_manager.set_animation(current_weapon.FIRE_ANIM_NAME)
						else:
							reloading_weapon = true
		
	if want_change_grenade:
		if current_grenade == "Grenade":
			current_grenade = "Sticky Grenade"
		elif current_grenade == "Sticky Grenade":
			current_grenade = "Grenade"
	
	if launch_grenade_enabled:
		if want_fire_grenade:
			if grenade_amounts[current_grenade] > 0:
				grenade_amounts[current_grenade] -= 1
				
				var grenade_clone
				if (current_grenade == "Grenade"):
					grenade_clone = grenade_scene.instantiate()
				elif (current_grenade == "Sticky Grenade"):
					grenade_clone = sticky_grenade_scene.instantiate()
					grenade_clone.player_body = self
					# HACK: remove collisions with Body/Trailer from StickyArea
					# SEE: collision-handling.txt
					grenade_clone.get_node("StickyArea").collision_mask &= sticky_area_exclude_mask
				
				# avoid collisions between the grenade and
				# the body of the vehicle containing this shooter
				grenade_clone.add_collision_exception_with(vehicle_body_node)
				globals.get_current_scene().add_child(grenade_clone)
				grenade_clone.global_transform = $RotationHelper/GrenadeTossPosition.global_transform
				var throw_force = GRENADE_THROW_FORCE * randf_range(
						1.0 - throw_max_delta_force, 1.0 + throw_max_delta_force)
				# HACK: when used with shooter ai set grenade_throw_delta_height >= 0.0
				var throw_dir = ($RotationHelper.global_transform.basis.z.normalized() + 
						Vector3(0.0, grenade_throw_delta_height, 0.0))
				grenade_clone.apply_impulse(throw_dir * throw_force,
						Vector3.ZERO)
	
	# grabbed object logic
	if grab_object_enabled:
		if (not changing_weapon) and want_fire and current_weapon_name == "UNARMED":
			if grabbed_object == null:
				var state = get_world_3d().direct_space_state
				var ray_from = head.get_global_transform().origin 
				var ray_to = (ray_from + 
						head.get_global_transform().basis.z.normalized() * OBJECT_GRAB_RAY_DISTANCE)
				var ray_params = PhysicsRayQueryParameters3D.new()
				ray_params.from = ray_from
				ray_params.to = ray_to
				var ray_result = state.intersect_ray(ray_params)
				if ray_result:
					var hit_object = ray_result["collider"]
					if ((hit_object is RigidBody3D) and 
							(not hit_object is Vehicle) and
							(not hit_object is VehicleAI)):
						grabbed_object = ray_result["collider"]
						grabbed_object.freeze_mode = RigidBody3D.FREEZE_MODE_STATIC
						grabbed_object.freeze = true
						# actually grabs the object
						grabbed_object_old_parent = grabbed_object.get_parent()
						grabbed_object.reparent(self)
			else:
				# actually release the object
				var grabbed_object_trans = grabbed_object.global_transform
				remove_child(grabbed_object)
				grabbed_object_old_parent.add_child(grabbed_object)
				grabbed_object.set_owner(grabbed_object_old_parent)
				grabbed_object.global_transform = grabbed_object_trans
				# actually throw the object
				grabbed_object.freeze = false
				var throw_force = OBJECT_THROW_FORCE * (1.0 + 
						randf_range(-throw_max_delta_force, throw_max_delta_force))
				grabbed_object.apply_impulse(
						head.global_transform.basis.z.normalized() * throw_force,
						Vector3(0,0,0))
				grabbed_object = null


func _process_aim_weapon(delta,
		action_pressed_aim_left:bool,
		action_pressed_aim_right:bool,
		action_pressed_aim_up:bool,
		action_pressed_aim_down:bool,
		action_strength_aim_left:float,
		action_strength_aim_right:float,
		action_strength_aim_down:float,
		action_strength_aim_up:float):
	#
	if aim_direct_use:
		var left = action_pressed_aim_left
		var right = action_pressed_aim_right
		var up = action_pressed_aim_up
		var down = action_pressed_aim_down
		# check if aiming is requested
		if (left or right or up or down or touch_touching):
			var yaw:float = 0.0
			var pitch:float = 0.0
			if !touch_touching:
				# from keyboard & joystick
				yaw += action_strength_aim_left - action_strength_aim_right
				pitch += action_strength_aim_down - action_strength_aim_up
			else:
				# virtual analog & mouse
				yaw = -touch_aim_direction.x
				pitch = touch_aim_direction.y			
			yaw *= aim_yaw_speed * delta
			pitch *= aim_pitch_speed * delta
			# clamp
			var new_rotation_y = rotation.y + yaw
			if new_rotation_y > aim_max_yaw_angle_rad:
				new_rotation_y = aim_max_yaw_angle_rad
			elif new_rotation_y < -aim_max_yaw_angle_rad:
				new_rotation_y = -aim_max_yaw_angle_rad
			var new_rotation_x = $RotationHelper.rotation.x + pitch
			if new_rotation_x > aim_max_pitch_angle_rad:
				new_rotation_x = aim_max_pitch_angle_rad
			elif new_rotation_x < -aim_max_pitch_angle_rad:
				new_rotation_x = -aim_max_pitch_angle_rad
			# actual rotation
			rotation.y = new_rotation_y
			$RotationHelper.rotation.x = new_rotation_x


func _take_weapon():
	for child in aiming_target_right.get_children():
		child.queue_free()
	var weapon_instance = weapon_scenes[changing_weapon_name].instantiate()
	# set visual layers for mesh instances
	var weapon_mesh_instances: Array = ScriptLibrary.find_nodes_by_type(
			MeshInstance3D, weapon_instance)
	for mesh_instance in weapon_mesh_instances:
		mesh_instance.layers = visual_instance_layers
	#
	if changing_weapon_name == "GUN":
		on_weapon_instantiation(weapon_instance,
				Vector3.ONE * 1.5,
				Vector3(0.009, 0.213, 0.033), 
				Vector3(deg_to_rad(-3.4), deg_to_rad(-176.5), deg_to_rad(72.1)),
				$RotationHelper/FirePoints/Gun)
		$RotationHelper/AnimatedModel/AnimationPlayer.play("Aim-Pistol-Pose")
	if changing_weapon_name == "RIFLE":
		on_weapon_instantiation(weapon_instance,
				Vector3.ONE * 1.5,
				Vector3(0.033, 0.225, 0.036), 
				Vector3(deg_to_rad(-1.1), deg_to_rad(-179.7), deg_to_rad(75.9)),
				$RotationHelper/FirePoints/Rifle)
		$RotationHelper/AnimatedModel/AnimationPlayer.play("Aim-Rifle-Pose")
	if changing_weapon_name == "LAUNCHER":
		on_weapon_instantiation(weapon_instance,
				Vector3.ONE * 0.22,
				Vector3(0.155, 0.165, 0.025), 
				Vector3(deg_to_rad(-1.1), deg_to_rad(-179.7), deg_to_rad(69)),
				$RotationHelper/FirePoints/Launcher)
		$RotationHelper/AnimatedModel/AnimationPlayer.play("Aim-Rocket-Launcher-Pose")


func _leave_weapon():
	on_weapon_freeing()
	for child in aiming_target_right.get_children():
		child.queue_free()
	
	$RotationHelper/AnimatedModel/AnimationPlayer.play("Steer-Pose")


func _on_VirtualAim_aim(direction):
	touch_aim_direction = direction


func _on_VirtualAim_aim_stop(touching):
	touch_touching = touching
	if not touching:
		touch_aim_direction = Vector2(0, 0)


func _set_aimer_use(value):
	aimer_use = value
	if aimer:
		aimer.enabled = value


func _set_aim_direct_use(value):
	aim_direct_use = value
	if has_node("Controls/Aim"):
		get_node("Controls/Aim").enabled = value


func _get_class():
	return "ShooterBase"


func _is_class(value):
	return "ShooterBase" == value


func _set_enabled(value):
	enabled = value
	set_process_input(enabled)
	set_physics_process(enabled)
