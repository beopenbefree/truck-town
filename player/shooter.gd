class_name Shooter
extends ShooterBase


# HACK: should be like BURST_GRENADE_TIMER of ShooterAIDriver
const GRENADE_LAUNCH_WAIT_TIME: float = preload(
		"res://player/ai/steering_drivers/shooter_ai_driver.gd").BURST_GRENADE_TIMER
var grenade_launch_wait_timer: float = 0.0

var ui_status_label: Control = null


func _ready():
	super()
	# Get the UI label so we can show our health and ammo, and get the flashlight spotlight
	ui_status_label = globals.get_current_scene().get_node_or_null(
			"HUD/MarginContainer/VBoxContainer/HBoxContainer2/ScoreInfos")


# FIXME: TO_BE_REMOVED
#func _input(event):
	#if event is InputEventMouseButton :
		#if event.button_index == MOUSE_BUTTON_WHEEL_UP or event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
			#if event.button_index == MOUSE_BUTTON_WHEEL_UP:
				#mouse_scroll_value += MOUSE_SENSITIVITY_SCROLL_WHEEL
			#elif event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
				#mouse_scroll_value -= MOUSE_SENSITIVITY_SCROLL_WHEEL
			#
			#mouse_scroll_value = clamp(mouse_scroll_value, 0, WEAPON_NUMBER_TO_NAME.size()-1)
			#
			#if changing_weapon == false:
				#if reloading_weapon == false:
					#var round_mouse_scroll_value = clamp(int(round(mouse_scroll_value)), 0, weapons.size() - 1)
					#if WEAPON_NUMBER_TO_NAME[round_mouse_scroll_value] != current_weapon_name:
						#changing_weapon_name = WEAPON_NUMBER_TO_NAME[round_mouse_scroll_value]
						#changing_weapon = true
						#mouse_scroll_value = round_mouse_scroll_value
			#get_viewport().set_input_as_handled()


func _physics_process(delta):
	super(delta)
	
	process_ui(delta)
	
	# HACK: update grenade timer
	if grenade_launch_wait_timer > 0.0:
		grenade_launch_wait_timer -= delta


func process_input(delta):
	# HACK: check whether throwing grenades is allowed or not
	var launch_grenade = Input.is_action_just_pressed("launch_grenade")
	if launch_grenade:
		if grenade_launch_wait_timer > 0.0:
			# launch disabled: still wait 
			launch_grenade = false
		else:
			# launch enabled: reset timer
			grenade_launch_wait_timer = GRENADE_LAUNCH_WAIT_TIME
	#
	_process_input(delta,
		Input.is_action_just_pressed("weapon_unarmed"),
		Input.is_action_just_pressed("weapon_1"),
		Input.is_action_just_pressed("weapon_2"),
		Input.is_action_just_pressed("weapon_3"),
		Input.is_action_just_pressed("shift_weapon_positive"),
		Input.is_action_just_pressed("shift_weapon_negative"),
		Input.is_action_just_pressed("reload"),
		Input.is_action_pressed("fire"), # NOTE: to fire continuously don't use "is_action_just_pressed"
		Input.is_action_just_pressed("change_grenade"), 
		launch_grenade,
		0xFFFFFFF9)


func process_aim_weapon(delta):
	_process_aim_weapon(delta,
		Input.is_action_pressed("aim_left"),
		Input.is_action_pressed("aim_right"),
		Input.is_action_pressed("aim_up"),
		Input.is_action_pressed("aim_down"),
		Input.get_action_strength("aim_left"),
		Input.get_action_strength("aim_right"),
		Input.get_action_strength("aim_down"),
		Input.get_action_strength("aim_up"))


func process_ui(_delta):
	if ui_status_label:
		ui_status_label.set_weapon_ammo_label(
				current_weapon_name, weapons,
				current_grenade, grenade_amounts)


func _get_class():
	return "Shooter"


func _is_class(value):
	return "Shooter" == value
