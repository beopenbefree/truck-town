class_name Rifle
extends WeaponBase


@export_range(10, 100, 0.1) var vision_area_radius: float = 36.0
@export_flags_3d_physics var bullet_collisions: int = 0b0: set = _set_bullet_collisions


func _ready():
	super()
	# HACK: call setters with (potential) side effects
	bullet_collisions = bullet_collisions
	# 
	$BulletShooter.DAMAGE = DAMAGE
	$BulletShooter.collisions = bullet_collisions
	$BulletShooter.with_hole = true
	$BulletShooter.with_dust = true
	$BulletShooter.hole_dissolve_time = 7.0


func fire_weapon():
	if (fire_enabled) and (ammo_in_weapon > 0):
		var begin_pos = shot_point_node.global_transform.origin
		var aim_pos = aim_point_node.global_transform.origin
		var end_pos = begin_pos + (aim_pos - begin_pos) * vision_area_radius
		$BulletShooter.shoot(begin_pos, end_pos)
		
		# play "Fire" animation
		shot_animation_player.play("Fire")
		# emit sound
		globals.play_sound("rifle_shot", null, true, false, begin_pos,
				sound_player)
		
		ammo_in_weapon -= 1


func reload_weapon():
	var can_reload = false
	
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		can_reload = true
	
	if spare_ammo <= 0 or ammo_in_weapon == AMMO_IN_MAG:
		can_reload = false
	
	if can_reload == true:
		var ammo_needed = AMMO_IN_MAG - ammo_in_weapon
		
		if spare_ammo >= ammo_needed:
			spare_ammo -= ammo_needed
			ammo_in_weapon = AMMO_IN_MAG
		else:
			ammo_in_weapon += spare_ammo
			spare_ammo = 0
		
		player_node.animation_manager.set_animation(RELOADING_ANIM_NAME)
		
		# HACK: called from the "reload" animation
		#globals.play_sound("rifle_cock", null, true, false, player_node.head.global_transform.origin)
		
		return true
	
	return false


func on_reload_weapon():
	# HACK: called from the "reload" animation
	globals.play_sound("rifle_cock", null, true, false, player_node.head.global_transform.origin)


func equip_weapon(changing_weapon=false): # HACK: changing_weapon
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		enabled = true
		return true
		
	# enable aiming (1)
	super(changing_weapon)
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		player_node.animation_manager.set_animation("Rifle_equip")
		# HACK: changing_weapon FIXME
#		if changing_weapon:
#			# Rifle_equip should begin later
#			player_node.animation_manager.advance(1.2)
		
		# HACK: called from the "equip" animation
		#globals.play_sound("rifle_cock", null, true, false, player_node.head.global_transform.origin)
	
	return false


func on_equip_weapon():
	# HACK: called from the "equip" animation
	globals.play_sound("rifle_cock", null, true, false, 
			player_node.head.global_transform.origin)


func unequip_weapon():
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		if player_node.animation_manager.fsm.current_state != "Rifle_unequip":
			player_node.animation_manager.set_animation("Rifle_unequip")
	
	# disable aiming (2 & 1)
	super()
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		enabled = false
		return true
	
	return false


func reset_weapon():
	ammo_in_weapon = 50
	spare_ammo = 100


func _set_bullet_collisions(value):
	bullet_collisions = value
	$BulletShooter.collisions = value


func _get_class():
	return "Rifle"


func _is_class(value):
	return "Rifle" == value
