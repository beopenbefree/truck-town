class_name Launcher
extends WeaponBase


@export_flags_3d_physics var rocket_layers: int = 0b0
@export_flags_3d_physics var rocket_collisions: int = 0b0
@export var ROCKET_IMPULSE_RANGE: Array[float] = [5000.0, 5000.0]
@export var ROCKET_MASS_RANGE: Array[float] = [5.0, 5.0]
@export var rocket_scaling_factor: float = 0.4
@export_range(0.0, 1.0, 0.01) var rocket_impulse_decreasing_factor: float = 0.8
@export var rocket_add_effects: bool = true

# HACK: 
const MAX_ROCKETS_IN_LAUNCHER = 1

var rocket_scene: PackedScene = preload("res://levels/elements/rocket.tscn")
var rocket_mesh_scene: PackedScene = preload("res://assets/player/weapons/rocket_mesh_scene.tscn")
var rocket_mesh: Node3D = null


func _ready():
	super()
	# HACK: force number of rockets in launcher
	AMMO_IN_MAG = MAX_ROCKETS_IN_LAUNCHER
	ammo_in_weapon = MAX_ROCKETS_IN_LAUNCHER


func on_weapon_instantiation(weapon_instance=null):
	# enable aiming (2)
	super(weapon_instance)
	
	# HACK: create a graphical rocket when a laucher is instantiated
	for child in shot_point_node.get_children():
		child.queue_free()
	if ammo_in_weapon > 0:
		_instantiate_rocket_mesh()


func fire_weapon():
	if (fire_enabled) and (ammo_in_weapon > 0):
		# replace the graphical rocket with a physical one
		if rocket_mesh:
			rocket_mesh.queue_free()
			rocket_mesh = null
		var rocket:Rocket = rocket_scene.instantiate()
		rocket.DAMAGE = DAMAGE
		rocket.scaling *= rocket_scaling_factor
		rocket.impulse_decreasing_factor = rocket_impulse_decreasing_factor
		rocket.add_effects = rocket_add_effects
		globals.get_current_scene().add_child(rocket)
		rocket.set_collision(rocket_layers, rocket_collisions)
		rocket.mass = randf_range(ROCKET_MASS_RANGE[0], ROCKET_MASS_RANGE[1])
		# initialize the rocket state
		var begin_pos = shot_point_node.global_transform.origin
		var end_pos = aim_point_node.global_transform.origin
		var direction = (end_pos - begin_pos)
		var velocity = shooter_node.vehicle_body_node.linear_velocity
		rocket.linear_velocity = velocity
		rocket.global_position = begin_pos
		# orient rocket along direction due to this particular bullet scene
		rocket.look_at(begin_pos + direction)
		# fire
		var impulse_size = randf_range(ROCKET_IMPULSE_RANGE[0], ROCKET_IMPULSE_RANGE[1])
		rocket.fire(impulse_size, direction)
		# play "Fire" animation
		shot_animation_player.play("Fire")
		# emit sound
		globals.play_sound("gun_shot", null, true, false,
				rocket.global_transform.origin, sound_player)
		#
		ammo_in_weapon -= 1


func reload_weapon():
	var can_reload = false
	
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		can_reload = true
	
	if spare_ammo <= 0 or ammo_in_weapon == AMMO_IN_MAG:
		can_reload = false
	
	if can_reload == true:
		var ammo_needed = AMMO_IN_MAG - ammo_in_weapon
		
		if spare_ammo >= ammo_needed:
			spare_ammo -= ammo_needed
			ammo_in_weapon = AMMO_IN_MAG
		else:
			ammo_in_weapon += spare_ammo
			spare_ammo = 0
		
		player_node.animation_manager.set_animation(RELOADING_ANIM_NAME)
		
		# HACK: called from the "reload" animation
		#globals.play_sound("launcher_cock", null, true, false, 
				#player_node.head.global_transform.origin, sound_player)
		
		return true
	
	return false


func on_reload_weapon():
	# HACK: called from the "reload" animation
	globals.play_sound("launcher_cock", null, true, false, 
			player_node.head.global_transform.origin, sound_player)
	# HACK: re-add a graphical rocket as in weapon instantiation
	_instantiate_rocket_mesh()


func equip_weapon(changing_weapon=false): # HACK: changing_weapon
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		enabled = true
		return true
	
	# enable aiming (1)
	super(changing_weapon)
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		player_node.animation_manager.set_animation("Launcher_equip")
		# HACK: changing_weapon FIXME
#		if changing_weapon:
#			# Launcher_equip should begin later
#			player_node.animation_manager.advance(1.2)
		
		# HACK: called from the "equip" animation
		#globals.play_sound("launcher_cock", null, true, false,
				#player_node.head.global_transform.origin, sound_player)
	
	return false


func on_equip_weapon():
	# HACK: called from the "equip" animation
	globals.play_sound("launcher_cock", null, true, false,
			player_node.head.global_transform.origin, sound_player)


func unequip_weapon():
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		if player_node.animation_manager.fsm.current_state != "Launcher_unequip":
			player_node.animation_manager.set_animation("Launcher_unequip")
	
	# disable aiming (2 & 1)
	super()
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		enabled = false
		return true
	else:
		return false


func reset_weapon():
	ammo_in_weapon = 10
	spare_ammo = 20


func _instantiate_rocket_mesh():
	rocket_mesh = rocket_mesh_scene.instantiate()
	rocket_mesh.rotate_y(PI)
	shot_point_node.add_child(rocket_mesh)


func _get_class():
	return "Launcher"


func _is_class(value):
	return "Launcher" == value
