class_name WeaponBase
extends Node3D


@export var enabled: bool = false: set = _set_enabled

@export_node_path("ShooterBase") var shooter_path = NodePath("../../..")

@export var fire_enabled: bool = true: set = _set_fire_enabled

@export_range(0, 50, 1, "or_greater") var AMMO_IN_MAG: int = 0
@export_range(0, 100, 1, "or_greater") var DAMAGE: int = 0
@export_range(0, 50, 1, "or_greater") var ammo_in_weapon: int = 0
@export_range(0, 100, 1, "or_greater") var spare_ammo: int = 0

@export var sound_player: Node3D = null

@export var RELOADING_ANIM_NAME := String()
@export var IDLE_ANIM_NAME := String()
@export var FIRE_ANIM_NAME := String()

var player_node: ShooterBase = null

var shot_point_node: Node3D = null
var aim_point_node: Node3D = null
var shot_animation_player: AnimationPlayer = null

var weapon: Node3D = null
var view_finder: Camera3D = null

@onready var shooter_node: ShooterBase = get_node_or_null(shooter_path)
# Can this weapon reload?
@onready var CAN_RELOAD: bool = true
# Can this weapon be refilled
@onready var CAN_REFILL: bool = true


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	fire_enabled = fire_enabled
	#
	set_process_input(false)


func on_weapon_instantiation(weapon_instance=null):
	# enable aiming (2)
	weapon = weapon_instance
	view_finder = weapon_instance.get_node("ViewFinder")


func equip_weapon(_changing_weapon=false): # HACK: changing_weapon
	# enable aiming (1)
	set_process_input(true)


func unequip_weapon():
	# disable aiming (2 & 1)
	set_process_input(false)
	view_finder = null
	weapon = null


func _set_fire_enabled(value):
	fire_enabled = value


func _get_class():
	return "WeaponBase"


func _is_class(value):
	return "WeaponBase" == value


func _set_enabled(value):
	enabled = value
