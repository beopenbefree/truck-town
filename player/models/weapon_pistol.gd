class_name Pistol
extends WeaponBase


@export_flags_3d_physics var bullet_layers: int = 0b0
@export_flags_3d_physics var bullet_collisions: int = 0b0
@export var BULLET_IMPULSE_RANGE: Array[float] = [150.0, 150.0]
@export var BULLET_MASS_RANGE: Array[float] = [0.15, 0.18]
@export var bullet_scaling_factor: float = 0.1
@export var bullet_add_effects: bool = true

var bullet_scene: PackedScene = preload("res://levels/elements/bullet.tscn")


func fire_weapon():
	if  (fire_enabled) and (ammo_in_weapon > 0):
		var bullet:Bullet = bullet_scene.instantiate()
		bullet.DAMAGE = DAMAGE
		bullet.scaling *= bullet_scaling_factor
		bullet.collision_layer = bullet_layers
		bullet.collision_mask = bullet_collisions
		bullet.add_effects = bullet_add_effects
		globals.get_current_scene().add_child(bullet)
		bullet.mass = randf_range(BULLET_MASS_RANGE[0], BULLET_MASS_RANGE[1])
		# initialize the bullet state
		var begin_pos = shot_point_node.global_position
		var end_pos = aim_point_node.global_position
		var direction = (end_pos - begin_pos)
		var velocity = shooter_node.vehicle_body_node.linear_velocity
		bullet.linear_velocity = velocity
		bullet.global_position = begin_pos
		# orient bullet along direction due to this particular bullet scene
		bullet.look_at(begin_pos - direction)
		# fire
		var impulse_size = randf_range(BULLET_IMPULSE_RANGE[0], BULLET_IMPULSE_RANGE[1])
		bullet.fire(impulse_size, direction)
		# play "Fire" animation
		shot_animation_player.play("Fire")
		# emit sound
		globals.play_sound("gun_shot", null, true, false,
				bullet.global_transform.origin, sound_player)
		#
		ammo_in_weapon -= 1


func reload_weapon():
	var can_reload = false
	
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		can_reload = true
	
	if spare_ammo <= 0 or ammo_in_weapon == AMMO_IN_MAG:
		can_reload = false
	
	if can_reload == true:
		var ammo_needed = AMMO_IN_MAG - ammo_in_weapon
		
		if spare_ammo >= ammo_needed:
			spare_ammo -= ammo_needed
			ammo_in_weapon = AMMO_IN_MAG
		else:
			ammo_in_weapon += spare_ammo
			spare_ammo = 0
		
		player_node.animation_manager.set_animation(RELOADING_ANIM_NAME)
		
		# HACK: called from the "reload" animation 
		#globals.play_sound("gun_cock", null, true, false, player_node.head.global_transform.origin)
		
		return true
	
	return false


func on_reload_weapon():
	# HACK: called from the "reload" animation
	globals.play_sound("gun_cock", null, true, false, 
			player_node.head.global_transform.origin)


func equip_weapon(changing_weapon=false): # HACK: changing_weapon
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		enabled = true
		return true
	
	# enable aiming (1)
	super(changing_weapon)
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		player_node.animation_manager.set_animation("Pistol_equip")
		# HACK: changing_weapon FIXME
#		if changing_weapon:
#			# Pistol_equip should begin later
#			player_node.animation_manager.advance(1.2)
		
		# HACK: called from the "equip" animation
		#globals.play_sound("gun_cock", null, true, false, player_node.head.global_transform.origin)
	
	return false


func on_equip_weapon():
	# HACK: called from the "equip" animation
	globals.play_sound("gun_cock", null, true, false, player_node.head.global_transform.origin)


func unequip_weapon():
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		if player_node.animation_manager.fsm.current_state != "Pistol_unequip":
			player_node.animation_manager.set_animation("Pistol_unequip")
	
	# disable aiming (2 & 1)
	super()
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		enabled = false
		return true
	else:
		return false


func reset_weapon():
	ammo_in_weapon = 10
	spare_ammo = 20


func _get_class():
	return "Pistol"


func _is_class(value):
	return "Pistol" == value
