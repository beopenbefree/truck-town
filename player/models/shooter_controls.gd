extends Node3D


@export_node_path("ShooterBase") var shooter_path = NodePath()


func _ready():
	var shooter = get_node(shooter_path)
	if shooter:
		set_signal_handling(shooter)


func set_signal_handling(shooter:ShooterBase):
	# fire
	$Fire.analog_touch_true.connect(_feed_action.bind("fire", true))
	$Fire.analog_touch_false.connect(_feed_action.bind("fire", false))
	# weapon up
	$WeaponUp.analog_touch_true.connect(_feed_action.bind("shift_weapon_positive", true))
	$WeaponUp.analog_touch_false.connect(_feed_action.bind("shift_weapon_positive", false))
	# weapon down
	$WeaponDown.analog_touch_true.connect(_feed_action.bind("shift_weapon_negative", true))
	$WeaponDown.analog_touch_false.connect(_feed_action.bind("shift_weapon_negative", false))

	# aim
	$Aim.analog_move.connect(Callable(shooter, "_on_VirtualAim_aim"))
	$Aim.analog_touch.connect(Callable(shooter, "_on_VirtualAim_aim_stop"))


func _feed_action(_name, pressed):
	ScriptLibrary._feed_action(_name, pressed)
