extends AnimationPlayer


@export_node_path("ShooterBase") var shooter_path = NodePath()

@onready var shooter_node: ShooterBase = get_node_or_null(shooter_path)


class Idle_unarmed extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 2.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)


class Pistol_equip extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 2
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.shooter_node.reset_aiming_targets_transform()
			target.play(animation_name, -1, animation_speed)

class Pistol_fire extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 2.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.stop()
			target.play(animation_name, -1, animation_speed)

class Pistol_idle extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 0.5
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Pistol_reload extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 2.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Pistol_unequip extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 2.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)


class Rifle_equip extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.shooter_node.reset_aiming_targets_transform()
			target.play(animation_name, -1, animation_speed)

class Rifle_fire extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 8.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.stop()
			target.play(animation_name, -1, animation_speed)

class Rifle_idle extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 0.5
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Rifle_reload extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Rifle_unequip extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)


class Launcher_equip extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.shooter_node.reset_aiming_targets_transform()
			target.play(animation_name, -1, animation_speed)

class Launcher_fire extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.stop()
			target.play(animation_name, -1, animation_speed)

class Launcher_idle extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Launcher_reload extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Launcher_unequip extends StateMachine.State:
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)



var callback_function = null

var fsm: StateMachine
@onready var fsm_factory = StateMachineFactory.new()


func _ready():
	fsm = fsm_factory.create({
		"target": self,
		"current_state": "Idle_unarmed",
		"states": [
			{"id": "Idle_unarmed", "state": Idle_unarmed},
			
			{"id": "Pistol_equip", "state": Pistol_equip},
			{"id": "Pistol_fire", "state": Pistol_fire},
			{"id": "Pistol_idle", "state": Pistol_idle},
			{"id": "Pistol_reload", "state": Pistol_reload},
			{"id": "Pistol_unequip", "state": Pistol_unequip},
			
			{"id": "Rifle_equip", "state": Rifle_equip},
			{"id": "Rifle_fire", "state": Rifle_fire},
			{"id": "Rifle_idle", "state": Rifle_idle},
			{"id": "Rifle_reload", "state": Rifle_reload},
			{"id": "Rifle_unequip", "state": Rifle_unequip},
			
			{"id": "Launcher_equip", "state": Launcher_equip},
			{"id": "Launcher_fire", "state": Launcher_fire},
			{"id": "Launcher_idle", "state": Launcher_idle},
			{"id": "Launcher_reload", "state": Launcher_reload},
			{"id": "Launcher_unequip", "state": Launcher_unequip},
		],
		"transitions": [
			{"state_id": "Idle_unarmed", "to_states": ["Launcher_equip", "Pistol_equip", "Rifle_equip", "Idle_unarmed"]},
			
			{"state_id": "Pistol_equip", "to_states": ["Pistol_idle"]},
			{"state_id": "Pistol_fire", "to_states": ["Pistol_idle"]},
			{"state_id": "Pistol_idle", "to_states": ["Pistol_fire", "Pistol_reload", "Pistol_unequip", "Pistol_idle"]},
			{"state_id": "Pistol_reload", "to_states": ["Pistol_idle"]},
			{"state_id": "Pistol_unequip", "to_states": ["Idle_unarmed"]},
			
			{"state_id": "Rifle_equip", "to_states": ["Rifle_idle"]},
			{"state_id": "Rifle_fire", "to_states": ["Rifle_idle"]},
			{"state_id": "Rifle_idle", "to_states": ["Rifle_fire", "Rifle_reload", "Rifle_unequip", "Rifle_idle"]},
			{"state_id": "Rifle_reload", "to_states": ["Rifle_idle"]},
			{"state_id": "Rifle_unequip", "to_states": ["Idle_unarmed"]},
			
			{"state_id": "Launcher_equip", "to_states": ["Launcher_idle"]},
			{"state_id": "Launcher_fire", "to_states": ["Launcher_idle"]},
			{"state_id": "Launcher_idle", "to_states": ["Launcher_fire", "Launcher_reload", "Launcher_unequip", "Launcher_idle"]},
			{"state_id": "Launcher_reload", "to_states": ["Launcher_idle"]},
			{"state_id": "Launcher_unequip", "to_states": ["Idle_unarmed"]},
		]
	})
	set_animation("Idle_unarmed")
	animation_finished.connect(animation_ended)


func set_animation(animation_name):
	fsm.transition(animation_name)


func animation_ended(_anim_name):
	# UNARMED transitions
	if fsm.current_state == "Idle_unarmed":
		pass
	# PISTOL transitions
	elif fsm.current_state == "Pistol_equip":
		set_animation("Pistol_idle")
	elif fsm.current_state == "Pistol_idle":
		pass
	elif fsm.current_state == "Pistol_fire":
		set_animation("Pistol_idle")
	elif fsm.current_state == "Pistol_unequip":
		set_animation("Idle_unarmed")
	elif fsm.current_state == "Pistol_reload":
		set_animation("Pistol_idle")
	# RIFLE transitions
	elif fsm.current_state == "Rifle_equip":
		set_animation("Rifle_idle")
	elif fsm.current_state == "Rifle_idle":
		pass;
	elif fsm.current_state == "Rifle_fire":
		set_animation("Rifle_idle")
	elif fsm.current_state == "Rifle_unequip":
		set_animation("Idle_unarmed")
	elif fsm.current_state == "Rifle_reload":
		set_animation("Rifle_idle")
	# LAUNCHER transitions
	elif fsm.current_state == "Launcher_equip":
		set_animation("Launcher_idle")
	elif fsm.current_state == "Launcher_idle":
		pass
	elif fsm.current_state == "Launcher_fire":
		set_animation("Launcher_idle")
	elif fsm.current_state == "Launcher_unequip":
		set_animation("Idle_unarmed")
	elif fsm.current_state == "Launcher_reload":
		set_animation("Launcher_idle")


func animation_callback():
	if callback_function == null:
		print ("AnimationPlayer_Manager.gd -- WARNING: No callback function for the animation to call!")
	else:
		callback_function.call_func()
