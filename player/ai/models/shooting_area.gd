class_name ShootingArea
extends Area3D


@export var radius: float = 20.0 : set = _set_radius
@export_range(0.0, 1.0, 0.01) var position_z_factor: float = 0.75


func _enter_tree():
	# assign unique shape
	$CollisionShape3D.shape = $CollisionShape3D.shape.duplicate(true)
	# HACK: call setters with (potential) side effects
	radius = radius


func _set_radius(value:float):
	radius = abs(value)
	($CollisionShape3D.shape as SphereShape3D).radius = radius
	position.z = position_z_factor * radius


func _get_class():
	return "ShootingArea"


func _is_class(value):
	return "ShootingArea" == value
