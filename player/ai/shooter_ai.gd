class_name ShooterAI
extends ShooterBase


# AI
var weapon_change_number_0: bool = false
var weapon_change_number_1: bool = false
var weapon_change_number_2: bool = false
var weapon_change_number_3: bool = false
var shift_weapon_positive: bool = false
var shift_weapon_negative: bool = false
var want_reload: bool = false
var want_fire: bool = false
var want_change_grenade: bool = false
var want_fire_grenade: bool = false
# aim
var action_pressed_aim_left: bool = false
var action_strength_aim_left: float = 0.0
var action_pressed_aim_right: bool = false
var action_strength_aim_right: float = 0.0
var action_pressed_aim_up: bool = false
var action_strength_aim_up: float = 0.0
var action_pressed_aim_down: bool = false
var action_strength_aim_down: float = 0.0
# AI: when used as target
var velocity: Vector3 = Vector3.ZERO
var yaw_speed:float = 0.0
var pitch_speed:float = 0.0

@onready var last_origin: Vector3 = global_transform.origin
@onready var last_yaw: float = rotation.y
@onready var last_pitch: float = $RotationHelper.rotation.x


func _physics_process(delta):
	super(delta)
	
	# update velocities
	var delta_inv = 1.0 / delta
	var origin = global_transform.origin
	velocity = (origin - last_origin) * delta_inv
	last_origin = origin
	var yaw = rotation.y
	yaw_speed = (yaw - last_yaw) * delta_inv
	last_yaw = yaw
	var pitch = $RotationHelper.rotation.x
	pitch_speed = (pitch - last_pitch) * delta_inv
	last_pitch = pitch


func process_input(delta):
	_process_input(delta,
		weapon_change_number_0,
		weapon_change_number_1,
		weapon_change_number_2,
		weapon_change_number_3,
		shift_weapon_positive,
		shift_weapon_negative,
		want_reload,
		want_fire,
		want_change_grenade, 
		want_fire_grenade,
		0xFFFFFFE7)
	# last: reset all commands
	weapon_change_number_0 = false
	weapon_change_number_1 = false
	weapon_change_number_2 = false
	weapon_change_number_3 = false
	shift_weapon_positive = false
	shift_weapon_negative = false
	want_reload = false
	want_fire = false
	want_change_grenade = false
	want_fire_grenade = false


func process_aim_weapon(delta):
	_process_aim_weapon(delta,
		action_pressed_aim_left,
		action_pressed_aim_right,
		action_pressed_aim_up,
		action_pressed_aim_down,
		action_strength_aim_left,
		action_strength_aim_right,
		action_strength_aim_down,
		action_strength_aim_up)


func can_change_state():
	return (not reloading_weapon) and (not changing_weapon)


func on_weapon_instantiation(weapon_instance, p_scale, p_position,
		p_rotation, fire_point):
	super(weapon_instance, p_scale, p_position,
		p_rotation, fire_point)
	fire_point.aim_point_node.visible = false


func _get_class():
	return "ShooterAI"


func _is_class(value):
	return "ShooterAI" == value
