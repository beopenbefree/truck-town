class_name ShooterAIPrioritySteering
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export var dims: AI_TOOLS.Dimensions = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

# priority steering parameters
@export_file var behaviors_file: String = String()
@export_file var behavior_groups_file: String = String()
@export_range(0.0, 1.0, 0.01) var damping: float = 0.0
@export var epsilon: float = 1.0e-3
@export var max_acceleration: float = 5.0
@export var max_rotation: float = 2.0
@export var max_speed: float = 5.0

# ai data definitions
var priority_steering_factory: AI.PrioritySteeringFactory
var priority_steering: AI.PrioritySteering
var steering_output: AI.SteeringOutput

@onready var target: Node3D = null
@onready var shooter: ShooterAI = $ShooterAI

#<DEBUG
#var debug_lib = preload("res://ai/debug_draw.gd")
#var character_instance = MeshInstance3D.new()
#var collision_avoidance: AI.CollisionAvoidance
#var collision_avoidance_instance = MeshInstance3D.new()
#var obstacle_avoidance: AI.ObstacleAvoidance
#var obstacle_avoidance_instance = MeshInstance3D.new()
#@onready var circle = MeshInstance3D.new()
#@onready var segment_r = ImmediateMesh.new()
#@onready var segment_l = ImmediateMesh.new()
#@onready var segment_steering_output = ImmediateMesh.new()
#DEBUG>


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	_setup()
	_initialize()


func _physics_process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _setup():
	priority_steering_factory = AI.PrioritySteeringFactory.new(shooter, dims)
	priority_steering = priority_steering_factory.create_and_setup(behaviors_file,
			behavior_groups_file, max_acceleration, max_rotation, epsilon, damping)
	
	#<DEBUG
	#for group:AI.BlendedSteering in priority_steering.groups:
		#for bhv in group.behaviors:
			#if bhv is AI.ObstacleAvoidance:
				#obstacle_avoidance = bhv
				#break
		#_add_mesh_instance(Color(1,0,1,1), obstacle_avoidance_instance)
		##
		#for bhv in group.behaviors:
			#if bhv is AI.CollisionAvoidance:
				#collision_avoidance = bhv
				#break
		#_add_mesh_instance(Color(1,1,0,1), collision_avoidance_instance)
	#_add_mesh_instance(Color(0,1,1,1), character_instance)
	#circle.mesh = debug_lib.get_circle_mesh(collision_avoidance.radius)
	#shooter.add_child(circle)
	#shooter.add_child(segment_r)
	#shooter.add_child(segment_l)
	#var mat = StandardMaterial3D.new()
	#mat.albedo_color = Color(1,0,0,1)
	#segment_steering_output.material_override = mat
	#shooter.add_child(segment_steering_output)
	#DEBUG>


func _initialize():
	var p_position = shooter.global_transform.origin
	var velocity = shooter.velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		priority_steering.character.position = Vector2(p_position.x, p_position.z)
		priority_steering.character.velocity = Vector2(velocity.x, velocity.z)
	else:
		priority_steering.character.position = p_position
		priority_steering.character.velocity = velocity
	priority_steering.character.orientation = AI_TOOLS.map_to_range(
			shooter.global_transform.basis.get_euler().y)


func _pre_update(delta):
	# synchronize character's motion state with the shooter's one
	var p_position = shooter.global_transform.origin
	var velocity = shooter.velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		priority_steering.character.position = Vector2(p_position.x, p_position.z)
		priority_steering.character.velocity = Vector2(velocity.x, velocity.z)
	else:
		priority_steering.character.position = p_position
		priority_steering.character.velocity = velocity
	priority_steering.character.orientation = AI_TOOLS.map_to_range(
			shooter.global_transform.basis.get_euler().y)
	# pre-update every inner behavior
	for group in priority_steering.groups:
		for bhv_wght in group.behaviors:
			var bhv = bhv_wght.behavior
			priority_steering_factory.behaviors_pre_update[bhv_wght].call(bhv, delta, target)


func _update(_delta):
	steering_output = priority_steering.get_steering()
	
	#<DEBUG
	#character_instance.global_transform.origin = (
			#priority_steering.character.position + Vector3(0.0, 3.0, 0.0))
	#obstacle_avoidance_instance.global_transform.origin = (
			#obstacle_avoidance.character.position + Vector3(0.0, 4.0, 0.0))
	#collision_avoidance_instance.global_transform.origin = (
			#collision_avoidance.character.position + Vector3(0.0, 5.0, 0.0))
	#debug_lib.debug_draw_obstacle_avoidance(obstacle_avoidance, shooter, 
			#segment_r, segment_l, dims)
	#debug_lib.debug_draw_vector(shooter, steering_output.linear, 
			#segment_steering_output, dims)
	#DEBUG>


func _post_update(_delta):
	# this implements the "actuator"
	shooter.action_pressed_aim_left = false
	shooter.action_strength_aim_left = 0.0
	shooter.action_pressed_aim_right = false
	shooter.action_strength_aim_right = 0.0
	shooter.action_pressed_aim_up = false
	shooter.action_strength_aim_up = 0.0
	shooter.action_pressed_aim_down = false
	shooter.action_strength_aim_down = 0.0
	# compute desired yaw/pitch
	var yaw_accel = steering_output.angular
	if yaw_accel > 0.0:
		shooter.action_pressed_aim_left = true
		shooter.action_strength_aim_left = yaw_accel
	elif yaw_accel < 0.0:
		shooter.action_pressed_aim_right = true
		shooter.action_strength_aim_right = -yaw_accel
	var pitch_accel = 0.0 # TODO
	if pitch_accel > 0.0:
		shooter.action_pressed_aim_left = true
		shooter.action_strength_aim_left = pitch_accel
	elif pitch_accel < 0.0:
		shooter.action_pressed_aim_right = true
		shooter.action_strength_aim_right = -pitch_accel


func _on_steering_behavior_signal(_name, action):
	print(_name, " - ", action)


#<DEBUG
#func _add_mesh_instance(color, mesh_instance, root=self):
	#var mesh = SphereMesh.new()
	#var mat = StandardMaterial3D.new()
	#mesh.radius = 0.25
	#mesh.height = 0.5
	#mat.albedo_color = color
	#mesh_instance.mesh = mesh
	#mesh_instance.material_override = mat
	#root.add_child(mesh_instance)
#DEBUG>


func _get_class():
	return "ShooterAIPrioritySteering"


func _is_class(value):
	return "ShooterAIPrioritySteering" == value


func _set_enabled(value):
	enabled = value
	set_physics_process(value)
