class_name ShooterAIDriver
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export_group("Shooting properties")
@export var FIRE_TIME: float = 1.0
@export var FIRE_GRENADE_TIME: float = 5.0
@export var shooting_area_radius: float = 20.0
@export_range(0.0, 1.0, 0.05) var shooting_area_max_delta_radius: float = 0.2 # fraction
@export_range(0.0, 1.0, 0.05) var fire_max_delta_time: float = 0.5 # fraction
# HACK
@export_range(0.0, 1.0, 0.01, "or_greater") var grenade_throw_delta_height: float = 0.15

@export_group("Pointing modes")
@export_enum("Aimer Use", "Aim Direct Use") var shooter_pointing_mode: String = "Aimer Use"

var fire_timer: float = 0.0
var fire_enable: bool = true
var fire_grenade_timer: float = 0.0
var fire_grenade_enable: bool = true
var burst_grenade_number: int = 0
const BURST_GRENADE_NUMBER_MAX: int = 3
const BURST_GRENADE_TIMER: float = 0.3
var current_target: Node3D = null

@onready var shooter: ShooterAI = null
@onready var shooting_area: ShootingArea = null
@onready var shooter_steering: ShooterAIPrioritySteering = null
@onready var visibility_checker: RayCast3D = null


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	_setup()
	# start with physics_process disabled
	set_physics_process(false)


func _physics_process(delta):
	# HACK
	# 1: affine compute
	#var target_local_trans = (global_transform.inverse() * 
			#current_target.global_transform)
	#visibility_checker.target_position = target_local_trans * Vector3.ZERO
	# 2: godot compute (may be it's quite optimized)
	visibility_checker.target_position = visibility_checker.to_local(
			current_target.global_position)
	visibility_checker.force_raycast_update()
	# check if current_target is really visible 
	var is_target_visible:bool = (visibility_checker.is_colliding() and
			("visible_target" in visibility_checker.get_collider()) and
			(visibility_checker.get_collider().visible_target == current_target))
	# check if shooter.aimer rotation angle is less than maximum.
	# When using the aimer and a max yaw rotation is
	# set (< PI), when the target is out of the view angle,
	# the aimer will correct the yaw rotation, so we could
	# use this information (set in the previous physical
	# frame)
	var is_aimer_rotation_ok = (shooter.aimer_use and 
			(shooter.get_node("RotationHelper") is Aimer) and 
			(not shooter.get_node("RotationHelper").yaw_rotation_corrected))
	if is_target_visible and is_aimer_rotation_ok:
		shooter.fire_enabled = true
		shooter.launch_grenade_enabled = true
		shooter.grab_object_enabled = true
	else:
		# no fight
		shooter.fire_enabled = false
		shooter.launch_grenade_enabled = false
		shooter.grab_object_enabled = false
	
	# switch fire on/off
	if fire_timer > 0:
		fire_timer -= delta
	else:
		fire_enable = not fire_enable
		fire_timer = FIRE_TIME * randf_range(
				1.0 - fire_max_delta_time, 1.0 + fire_max_delta_time)
	if fire_enable:
		shooter.want_fire = true
	
	# switch launch grenades on/off
	if fire_grenade_timer > 0:
		if fire_grenade_enable:
			fire_grenade_enable = false
			shooter.want_fire_grenade = false
		fire_grenade_timer -= delta
	else:
		fire_grenade_enable = true
		shooter.want_fire_grenade = true
		_compute_fire_grenade_timer()


func _compute_fire_grenade_timer():
	if burst_grenade_number > 0:
		burst_grenade_number -= 1
		fire_grenade_timer = BURST_GRENADE_TIMER
	else:
		# randomly change grenade type (for next time)
		shooter.want_change_grenade = (50 - (randi() % 100)) >= 1
		# randomly choose number of grenades in a burst (for next time)
		burst_grenade_number = randi_range(1, BURST_GRENADE_NUMBER_MAX)
		fire_grenade_timer = FIRE_GRENADE_TIME * randf_range(
				1.0 - fire_max_delta_time, 1.0 + fire_max_delta_time)


# FIXME: to be removed?
func assign_targets_to_behaviors(all_shooter_drivers):
	# get all characters (i.e. the targets)
	var all_targets = []
	for shooter_driver in all_shooter_drivers:
		all_targets.append(
				shooter_driver.vehicle.priority_steering.character)
	# assign the targets (i.e. the others characters) to the 
	# "target" attribute of all the behaviors that has it 
	for group in shooter_steering.priority_steering.groups:
		for bhv_wght in group.behaviors:
			var bhv = bhv_wght.behavior
			# check if targets is needed
			if "targets" in bhv:
				var targets = []
				for other in all_targets:
					if other != bhv.character:
						targets.append(other)
				bhv.targets = targets


func _setup():
	# initialize timer
	fire_timer = FIRE_TIME * randf_range(
			1.0 - fire_max_delta_time, 1.0 + fire_max_delta_time)
	# find shooter
	shooter = ScriptLibrary.find_nodes_by_type(ShooterAI, self, false)[0]
	# setup aiming capabilities
	if shooter_pointing_mode == "Aimer Use":
		# for performance disable initially shooter's aimer
		shooter.aimer_use = false
		shooter.set_aimer_target(null)
	elif shooter_pointing_mode == "Aim Direct Use":
		# enable shooter's aim direct
		shooter.aim_direct_use = true
		# get and setup the shooter_steering
		shooter_steering = ScriptLibrary.find_nodes_by_type(
				ShooterAIPrioritySteering, self, false)[0]
		assert(shooter_steering)
		# start initially disabled
		shooter_steering.enabled = false
		# complete the inner behaviors' configuration
		# create a common collision detector: for obstacle avoidance behaviors
		var collision_detector = globals.get_collision_detector()
		# iterate over all inner behaviors of the characters
		# to set the detector for behaviors requiring it
		for group in shooter_steering.priority_steering.groups:
			for bhv_wght in group.behaviors:
				var bhv = bhv_wght.behavior
				# check if collision_detector is needed
				if "detector" in bhv:
					bhv.detector = collision_detector
				# check errors
				assert(shooter_steering.priority_steering.character == bhv.character,
						"ERROR: characters don't match!")
	# set up shooting properties
	shooter.grenade_throw_delta_height = grenade_throw_delta_height
	# set up shooting area
	shooting_area = ScriptLibrary.find_nodes_by_type(ShootingArea, self, false)[0]
	shooting_area.radius = shooting_area_radius * randf_range(
			1.0 - shooting_area_max_delta_radius,
			1.0 + shooting_area_max_delta_radius)
	# HACK: shooter area could have (uniform) scale, so radius, 
	# which is given in global space, should be corrected
	var scale_vec = shooting_area.global_basis.get_scale()
	assert(is_equal_approx(scale_vec.x, scale_vec.y) and 
			is_equal_approx(scale_vec.y, scale_vec.z))
	var scale_correction = 1.0 / scale_vec.y
	shooting_area.radius *= scale_correction
	#
	shooting_area.body_entered.connect(_body_entered_shooting)
	shooting_area.body_exited.connect(_body_exited_shooting)
	# create the visibility check
	_create_visibility_check()


func _body_entered_shooting(body):
	if enabled:
		# body's collision layer: 2|3 (= Vehicle|Trailer)
		while not shooter.can_change_state():
			await get_tree().process_frame
		set_physics_process(true)
		# 
		current_target = body.visible_target
		if shooter_pointing_mode == "Aimer Use":
			shooter.aimer_use = true
			shooter.set_aimer_target(current_target)
		elif shooter_pointing_mode == "Aim Direct Use":
			shooter_steering.enabled = true
			shooter_steering.target = current_target
		# enable visibility checker
		visibility_checker.enabled = true
		# perform firing process
		# choose weapon randomly: FIXME
		_choose_random_weapon()
		# start firing/grenading
		fire_timer = FIRE_TIME * randf_range(
				1.0 - fire_max_delta_time, 1.0 + fire_max_delta_time)
		fire_grenade_timer = FIRE_GRENADE_TIME * randf_range(
				1.0 - fire_max_delta_time, 1.0 + fire_max_delta_time)


func _body_exited_shooting(_body):
	if enabled:
		# body's collision layer: 2|3
		while not shooter.can_change_state():
			await get_tree().process_frame
		# perform unarming process
		shooter.weapon_change_number_0 = true
		# 
		current_target = null
		if shooter_pointing_mode == "Aimer Use":
			shooter.set_aimer_target(current_target)
			shooter.aimer_use = false
		elif shooter_pointing_mode == "Aim Direct Use":
			shooter_steering.target = current_target
			shooter_steering.enabled = false
		#
		set_physics_process(false)
		visibility_checker.enabled = false


func _choose_random_weapon():
	var idx_weapon = randi() % shooter.weapons.size()
	match idx_weapon:
		2:
			shooter.weapon_change_number_2 = true
		3:
			shooter.weapon_change_number_3 = true
		_:
			shooter.weapon_change_number_1 = true


func _create_visibility_check():
	if not is_instance_valid(visibility_checker):
		visibility_checker = RayCast3D.new()
		# add visibility check to this node
		add_child(visibility_checker)
		# HACK: The raycast needs to be translated at the
		# UP level of the vehicle body due to its strange
		# behavior when inside a collision shape
		visibility_checker.global_position.y = (
				shooter.vehicle_body_node.global_position.y)
		# visibility check against all objects but
		# its parent and its vehicle body
		visibility_checker.collision_mask = 0xFFFFFFFF
		visibility_checker.add_exception(shooter.vehicle_body_node)
		visibility_checker.exclude_parent = true
		# <DEBUG
		#visibility_checker.debug_shape_custom_color = Color.HOT_PINK
		#visibility_checker.debug_shape_thickness = 5
		# DEBUG>


func _get_class():
	return "ShooterAIDriver"


func _is_class(value):
	return "ShooterAIDriver" == value


func _set_enabled(value):
	enabled = value
