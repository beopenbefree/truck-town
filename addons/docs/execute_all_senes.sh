#!/bin/bash

if [ "$1" == "" ] ; then
	echo "$0 <DIR> [ <ELAPSE_TIME> ]"
	exit 1
fi

dir_path=$(realpath $1)

elapsed_time=$2
if [ "$elapsed_time" == "" ] ; then
	elapsed_time=5
fi

for s in  $(find $dir_path  -iname "*.*scn") ; do echo "$s" ; /REPOSITORY/KProjects/godot_engine $s & disown && prog_pid=$! ; sleep $elapsed_time ; kill -15 $prog_pid ; done
