# Fighty  Driver

**Single player** arcade driving game with AI combat,  
developed with the [Godot game engine](https://godotengine.org/).  

## ABOUT THE GAME

The game is *demo version* (pre-release) of the official version 1.0.

Please report any bugs and feel free to make any suggestions!  
*It will be much appreciated!*

## GAMEPLAY

During the game sessions, the player must accumulate a minimum score  
for each level by collecting **coins** (gold, silver, bronze), scattered  
throughout the level, and must do so with a **limited amount of health/fuel**.  

During the search he may encounter **enemies**, also looking for coins,  
who will try to hit him by stealing his health/fuel: therefore it may be  
necessary to **go into combat**.  

Other **dangers** or **aids** are scattered throughout the level: shooting  
turrets, explosive zones, accelerator zones, ammunition, health/fuel refills....  

Once the minimum score is reached, the **yellow end-of-session zone** will  
be activated, where the player can complete the level, acquire the  
accumulated score (plus a bonus), earn a new vehicle and start a new level.  

As the player increases the score by completing the levels, he will acquire  
new weapons to fight more effectively, but, at the same time, he will find  
it increasingly difficult to collect the coins, facing more and more dangers  
and more aggressive enemies.  

The final **goal** of the game is to complete the levels, acquire score and  
set more and more new **records**.

## CONTROLS


![CONTROLS](addons/docs/controls.png)  


**NOTES**:
   * You can **configure/change controls** by using the **CONFIGURE INPUT** button.
   * You can use **joypad/joystick** too; you can re-map your joypad  
   buttons/axes by using the **JOYPAD REMAPPING** button and save the configuration 
   for the future.
   * Weapon types will be made available as sessions played 
   increase (regardless of accumulated score)
   * Several features (such as "aiming with mouse") are only available  
   in "First Person" mode.

   [beopenbefree@gmx.com](mailto:beopenbefree@gmx.com)

    #shooter  #driver #arcade #driving #action #other
 

## DCC rules
The game uses different types of *environments *, which are made automatically  
using the tool [Sceelix](https://sceelix.com/) and refined with  
[Blender](https://www.blender.org/).  

### City
1. cities are generated using the corresponding *sceelix*'s graph.
2. *Blender*:  
    - to import *sceelix*'s generated *Wavefront* `obj` files (`buildings.obj`, `roads.obj`,
    `sidewalks.obj` and `navmesh.obj`), set the *Transform* option with `Forward Axis = Y` and
    `Up Axis = Z`
    - replace the imported materials with those of the *assets* library (for all meshes)
    - possibly modify the *roads* mesh by connecting the separate roads, merge the roads,  
    in which case also check the *navmesh* mesh to restore the overlaps if necessary
    - possibly (re)assign materials to the faces of the *roads*' mesh
    - possibly rescale/recalculate the `uv`s of the *roads*' textures
    - creates the *terrain* mesh by filling the *holes* in the mesh of *roads* and *sidewalks*
    - **hint:** use the following steps:  
        - use the *knife project* to cut out the *Plane-Knife-Project* mesh using the *roads* and 
        *sidewalks* meshes as a cutting template and then separate the produced *terrain* mesh
        - place the *terrain* mesh at the desired height (minimum, maximum or intermediate  
        value of the height of the mesh *roads*) and use the *Snap* tool (magnet icon on  
        the menu bar) to connect the corresponding vertices of the 2 meshes
    - add the suffix `-col` to the names of the meshes of *buildings*, *roads*, *sidewalks* and 
    *terrain*
    - apply *Triangulate* modifier to the **all** the meshes
    - apply *Terrain Displacement* modifier to the *terrain* mesh
    - export as `glTF` (*GL Transmission Format*) all the meshes with name *environments.glb*
3. *Godot*:
    - prepare the inherited sub-scenes: *buildings*, *roads*, *sidewalks*, *terrain* and *navmesh*
    - in the final *level scene* set different `PhysicsMaterial` for the *roads*, *sidewalks* and
    *terrain*'s `StaticBody3D` sub-nodes
    - in the final *level scene* add a `StaticBody3D` to the *navmesh* and add `31` to its
    *collision layer*
    - in the final *level scene* add a `PhysicsMaterial` to the `StaticBody3D` of  *buildings*,
    *roads*, *sidewalks*, *terrain* meshes and set different properties for these materials: *terrain*  
    should have lower `friction` and `bounce` than the other meshes
    - in the final *level scene* add `30` to the *collision layer* of the `StaticBody3D` of *buildings* and 
    *terrain* meshes while to those of the `StaticBody3D` of the *roads* and *sidewalks* meshes  
    add both `30` and `29`
    

### Landscape
1. landscapes are generated using the corresponding *sceelix*'s graph.
2. *blend maps* should be ready for use if you recreate `uv` mapping as explained  
below (otherwise they should be rotated multiples of 90 degrees ccw or cw).
3. *Blender*:
    - to import *sceelix*'s generated *Wavefront* `obj` files (`actors.obj`, `landscape.obj`,
    `roads.obj`, `roads-guardrail.obj` and `navmesh.obj`), set the *Transform* option with
    `Forward Axis = Y` and `Up Axis = Z`
    - replace the imported materials with those of the *assets* library (for all meshes)
    - when importing landscapes:
       - remove (and clean up) material from the *landscape* mesh,
       - (re)create the `uv` mapping: *Edit Mode* -> *Top Orthographic* view -> select all faces ->
       *Unwrap* (*U* keystroke) -> select *Project from View (Bounds)*
    - **hint:** use the *knife project* to cut out the *landscape* and *actors* meshes using the mesh
    *Plane-Knife-Project* as a cutting template and then separate the produced relevant meshes
    - separate the *actors* mesh into individual elements (*By Loose Parts* in edit mode) and  
    origins (*Origin to Geometry* in object mode)
    - possibly (re)assign materials to the faces of the roads' mesh 
    - possibly rescale/recalculate the `uv`s of the roads' textures
    - add the suffix `-col` to the names of the meshes of *landscape*, *roads* and *roads-guardrail*
    - apply *Triangulate* modifier to the **all** the meshes
    - export as `glTF` (*GL Transmission Format*) all the meshes with name *environments.glb*
4. *Godot*:
    - prepare the inherited sub-scenes: *landscape*, *roads*, *navmesh* and *actors*
    - add `multimesh_landscape-actors_placement` script to *actors* inherited sub-scenes
    - override surface material of the *landscape* mesh with the shader material for *blend maps*  
    (`landscape.gdshader`)
    - in the final *level scene* add a `StaticBody3D` to the *navmesh* and add `31` to its
    *collision layer*
    - in the final *level scene* add a `PhysicsMaterial` to the `StaticBody3D` of *landscape*, 
    *roads-guardrail* and *roads* meshes and set different properties for these materials: *landscape*  
    should have lower `friction` and `bounce` than the other meshes
    - in the final *level scene* add `30` to the *collision layer* of the `StaticBody3D` of *landscape* and 
    *roads-guardrail* meshes while to that of the `StaticBody3D` of the *roads* mesh add both `30` and `29`


### Vegetation

#### Tree
1. The tree is made using 3D models with foliage consisting of texturized 
*patches*, i.e. polygons
2. there should be only one tree mesh composed of *trunk* and *foliage*
sub-meshes with different materials
3. the color attribute of *foliage* sub-mesh should be white for all vertices
4. the color attribute of *trunk* sub-mesh should be black for all vertices 
with the exception of the ends of the branches closest to the leaves that must
gradually tend to white.
5. *Blender*:
    * assign two different materials to the *foliage* and *trunk*
    * ensure *vertex color* attribute is present, with *Byte Color* Data Type
    * *paint* the vertices
    * export a single mesh to `.obj` (*Wavefront*) format, by ensuring that *geometry -> color*
    option is selected 
6. *Godot*:
    * import the `.obj` files and the textures for the *foliage* and *trunk*
    * create tree instances according to the various texture combinations
    * override the materials of the surfaces with the shader `tree.gdshader` by assigning the 
    various texture combinations



***FIXME**

Each environment is *centered* at `(0, 0, 0)` and has an *extension* of `500x500`.

Any  sub-scene of a environment, to be visible on the map (`map_viewport.tscn`) contained in the *HUD*, should have its` layers` property set to `0b1001`.  
Typically this sub-scene represents the *path* that the vehicle should follow (e.g. the "*roads****" `MeshInstance` present in several environments).

### Common elements

##### Instancing multiple physic objects
To place at *runtime* and *randomly* any kind of physic objects, the script `res://levels/tools/objects_placement.gd` could be attached to a sub scene inside the environment scene.  
The sub scene can be optionally saved and instanced *offline*.  
An object to be instanced should be *prepared* by wrapping it into a `res://levels/elements/physic_body_dynamic.tscn` (dynamic object) or `res://levels/elements/physic_body_static.tscn` (static object) scenes.  
**NOTE**: these physic objects works properly only when attached directly to the *globals.get_current_scene()* or indirectly to it, but with all intermediate *Tranform*s equal to *identity*.  
**NOTE**: a *saved scene* still has the attached script, so it has to be detached when instancing the scene.

##### Instancing multiple physic (or visual only) static objects into `MultiMesh`es
To place at *runtime* and *randomly* physic static objects with the same `Mesh` and `Material` into `MultiMesh`es, the script `res://levels/tools/multimesh_objects_static_placement.gd` could be attached to a sub scene inside the environment scene.  
The option `visual_only` could be enabled to place objects without physics behavior, just "*visual only*".  
The sub scene can be optionally saved and instanced *offline*.  
An object to be instanced should be *prepared* by wrapping it into a `res://levels/elements/multimesh-physic_body_static` (static object) scene. 
**NOTE**: dynamic objects cannot be instanced using `MultiMesh`es.  
**NOTE**: a *saved scene* could still has the attached script, so it has to be detached when instancing the scene.

### Cities

### Landscapes

A landscape scene has to be composed by two sub-scenes:

- the **static environment**, ie the *landscapeXX.tscn* scene
- the (static) **actors**, ie trees, houses, obstacles etc...whose placeholders are located in the *landscape_actorsXX.tscn* scene (each placeholder color represents a different kind of actor)

The overall scene is obtained with the following steps:

1. add the script `res://levels/tools/landscape-actors_spatial_material_multimesh.gd` to the *landscape_actorsXX.tscn* and complete the parameters
2. now there are two options, and the one with the best result should be chosen:
    - instantiate directly *landscape_actorsXX.tscn* into *landscapeXX.tscn*, so the entire process of actors' creation will take place during scene loading (it's quite fast since it uses multimesh instances). 
    *NOTE*: disable `save_scene` option in *landscape_actorsXX.tscn*
    - create *landscape_actorsXX_saved.tscn* by enabling `save_scene` (and setting `save_scene_path`) and executing *landscape_actorsXX.tscn*; then instantiate directly *landscape_actorsXX_saved.tscn* into *landscapeXX.tscn*

## Vehicles

### Shooter

*Shooter* is implemented using an *AnimatedModel* node composed of a rigged model (`Skeleton`) having its own `AnimationPlayer` with these animations:

1. *T-Pose*: a default animation
2. *Steer-Pose*: played during steering
3. *Aim-Pistol-Pose*: played during pistol aiming
4. *Aim-Rifle-Pose*: played during rifle aiming
5. *Aim-Rocket-Launcher-Pose*: played during rocket launcher aiming

The *AnimatedModel* is augmented with two `SkeletonIK`, one for each arm, and with a node *AimingTargets* which has *TargetRight* node (`Position3D`) as the target of the right `SkeletonIK` (*SkeletonIKRight*) and an `AnimationPlayer` responsible for playing the animations of the right arm, that is:

- *Idle_unarmed*
- **XXX***_idle*
- **XXX***_equip*
- **XXX***_unequip*
- **XXX***_fire*
- **XXX***_reload*

where **XXX** is one of: *Pistol*, *Rifle*, *Launcher*. Each of these animations is implemented by moving the *TargetRight* node (which is followed by *SkeletonIKRight*, being its target node).

To add a *Shooter* to a vehicle, we need to perform the following steps:

1. instance a *Shooter* under the vehicle's *Body/CameraGroup/FixedCamera* node
2. add/instance a *SteeringWheel* node (having a steer wheel `MeshInstance`) under the vehicle's *Body* node
3. under *SteeringWheel* node add:
    - a `RemoteTransform` (called *RemoteTransformTargetRight*) whose `remote_path` should be set to *Shooter/RotationHelper/AimingTargets/TargetRight* node 
    - a *TargetLeft* node (`Position3D`); this node should be the target of the left `SkeletonIK` (*SkeletonIKLeft*) of the *Shooter*'s *AnimatedModel*
    - a script that has an `update()` which takes as argument the rotation (in radians) of the steering wheel
4. the *AimingTargets*'s `AnimationPlayer` has the parameter `remote_transform_target_right_node` whose value should be set to *RemoteTransformTargetRight*
5. the positioning/orientation of the *Shooter* and *SteeringWheel* consists of a series of attempts until the desired result is obtained
