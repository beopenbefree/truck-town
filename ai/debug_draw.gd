extends Node


static func get_segment(a, b):
	var segment = PackedVector3Array()
	segment.append(a)
	segment.append(b)
	return segment


static func get_segment_mesh(a,b):
	return _get_mesh(get_segment(a, b))


static func get_circle(radius, num_points=16):
	var delta = 2 * PI / num_points
	var circle = PackedVector3Array()
	for i in num_points:
		var p = Vector3(sin(delta * i), 0, cos(delta * i)) * radius
		circle.append(p)
	return circle


static func get_circle_mesh(radius, num_points=16):
	return _get_mesh(get_circle(radius, num_points))


static func _get_mesh(vertices):
	# Initialize the ArrayMesh.
	var arr_mesh = ArrayMesh.new()
	var arrays = []
	arrays.resize(ArrayMesh.ARRAY_MAX)
	arrays[ArrayMesh.ARRAY_VERTEX] = vertices
	# Create the Mesh.
	arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_LINE_STRIP, arrays)
	return arr_mesh


static func debug_draw_obstacle_avoidance(obstacle_avoidance, object,
		segment_r, segment_l, _dims, thickness=0, material=null):
	var a = Vector3.ZERO
	if obstacle_avoidance:
		var global_trans_inv = object.global_transform.inverse()
		var whisker_r = obstacle_avoidance._ray
		var b_r = global_trans_inv * (object.global_transform.origin + whisker_r)
		var points_r = get_segment(a, b_r)
		if thickness > 0.0:
			debug_draw_strip(segment_r, points_r[0], points_r[1],
					thickness, material)
		else:
			debug_draw_immediate(segment_r, Mesh.PRIMITIVE_LINE_STRIP,
					points_r, material)
		if obstacle_avoidance.num_rays == 2:
			var whisker_l = obstacle_avoidance._ray1
			var b_l = global_trans_inv * (object.global_transform.origin + whisker_l)
			var points_l = get_segment(a, b_l)
			if thickness > 0.0:
				debug_draw_strip(segment_l, points_l[0], points_l[1],
						thickness, material)
			else:
				debug_draw_immediate(segment_l, Mesh.PRIMITIVE_LINE_STRIP,
						points_l, material)	


static func debug_draw_vector(object, vector, immediate_geom, dims,
		thickness=0, material=null):
	var output_linear
	var a = Vector3.ZERO
	if dims == AI_TOOLS.Dimensions.TWO:
		output_linear = Vector3(vector.x, 0, vector.y)
	else:
		output_linear = vector
	var global_trans_inv = object.global_transform.inverse()
	var b = global_trans_inv * (object.global_transform.origin + output_linear)
	var points = get_segment(a, b)
	if thickness > 0.0:
		debug_draw_strip(immediate_geom, points[0], points[1],
			thickness, material)
	else:
		debug_draw_immediate(immediate_geom, Mesh.PRIMITIVE_LINE_STRIP,
			points, material)


static func debug_draw_immediate(drawable:ImmediateMesh, 
		primitive, points, material=null):
	drawable.clear_surfaces()
	drawable.surface_begin(primitive, material)
	for p in points:
		drawable.surface_add_vertex(p)
	drawable.surface_end()


static func debug_draw_strip(drawable:ImmediateMesh,
		from:Vector3, to:Vector3, thickness:float=1.0,
		material:BaseMaterial3D=null):
	# mesh always face camera and cull disabled
	#material.billboard_mode = BaseMaterial3D.BILLBOARD_ENABLED
	material.cull_mode = BaseMaterial3D.CULL_DISABLED
	# define n = to - from and nperp = unknown vector
	var n: Vector3 = to - from
	var nperp := Vector3.ZERO
	# condition: n.dot(nperp) = 0
	# to choose the free variable, to which to assign a
	# value, it is necessary to take into account the numerical
	# (in)stability: we choose the unknown with the largest absolute
	# value and calculate it as a function of the other 2
	var nxabs: float = abs(n.x)
	var nyabs: float = abs(n.y)
	var nzabs: float = abs(n.z)
	if (nxabs > nyabs) and (nxabs > nzabs):
		# n.x > (n.y, n.z)
		nperp.y = 1.0
		nperp.z = 1.0
		nperp.x = (n.y * nperp.y + n.z * nperp.z) / n.x
	elif (nyabs > nxabs) and (nyabs > nzabs):
		# n.y > (n.x, n.z)
		nperp.x = 1.0
		nperp.z = 1.0
		nperp.y = (n.x * nperp.x + n.z * nperp.z) / n.y
	elif (nzabs > nxabs) and (nzabs > nyabs):
		# n.z > (n.x, n.y)
		nperp.x = 1.0
		nperp.y = 1.0
		nperp.z = (n.x * nperp.x + n.y * nperp.y) / n.z
	# normalize nperp
	nperp = nperp.normalized()
	# costruct a quad with triangle strip (4 points):
	#  p1   o-----------------------o p3
	#       |                       |
	# from  o                       o to    ^
	#       |                       |       |  nperp
	#  p2   0-----------------------o p4
	var points = []
	points.append(from + nperp * thickness * 0.5) # p1
	points.append(from - nperp * thickness * 0.5) # p2
	points.append(to + nperp * thickness * 0.5) # p3
	points.append(to - nperp * thickness * 0.5) # p4
	# draw
	debug_draw_immediate(drawable, Mesh.PRIMITIVE_TRIANGLE_STRIP,
			points, material)
