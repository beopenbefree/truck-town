class_name AISteeringPipelineImpl
extends Node


### TARGETERS ###
class ChaseTargeter extends AI.Targeter:
	var dims = AI_TOOLS.Dimensions.THREE
	var map: RID
	var target: AI.Kinematic
	# private variables
	var _goal: AI.Goal
	
	# Controls how much to anticipate the movement.
	var lookahead: float
	
	func _init(p_object:AI.SteeringPipeline, p_lookahead:float, 
			p_dims, world:World3D):
		super(p_object, "position")
		type_name = "ChaseTargeter"
		dims = p_dims
		lookahead = p_lookahead
		target = AI.Kinematic.new(dims)
		map = world.get_navigation_map()
		_goal = AI.Goal.new(dims)
		_goal.has_position = true
	
	func get_goal(_kinematic:AI.Kinematic) -> AI.Goal:
		_goal.position = target.position + target.velocity * lookahead
		return _goal


class StandStillTargeter extends ChaseTargeter:
	
	func _init(p_object:AI.SteeringPipeline, p_dims, world:World3D):
		super(p_object, 0.0, p_dims, world)
		type_name = "StandStillTargeter"
		target = steering_pipeline.get_ref().character


### DECOMPOSERS ###
class PlanningDecomposer extends AI.Decomposer:
	var map: RID
	var optimize: bool
	var path: AI.NavMapPath
	var body: VehicleAI
	var min_distance: float
	var go_to_min_distance: bool
	var straight_cos_min: float # radians
	# private variables
	var _min_distance_2: float
	var _bounding_sphere_radius: float
	
	func _init(p_object:AI.SteeringPipeline, p_body, world:World3D,
			p_optimize:bool=false, p_min_distance:float=0.0,
			p_go_to_min_distance:bool=false,
			straight_angle_max:float=0.0):
		super(p_object)
		type_name = "PlanningDecomposer"
		body = p_body
		map = world.get_navigation_map()
		optimize = p_optimize
		min_distance = p_min_distance
		go_to_min_distance = p_go_to_min_distance
		_min_distance_2 = min_distance ** 2
		straight_cos_min = cos(straight_angle_max)
		var body_aabb = body.chassis.get_aabb().abs()
		_bounding_sphere_radius = body_aabb.get_longest_axis_size() * 0.5
	
	func decompose(character: AI.Kinematic, goal: AI.Goal) -> AI.Goal:
		# First we quantize our current location 
		# and our goal into nodes of the map.
		var start_pos = character.position
		var end_pos = goal.position
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			start_pos = Vector3(start_pos.x, 0.0, start_pos.y)
			end_pos = Vector3(end_pos.x, 0.0, end_pos.y)
		
		# FIXME: TO BE REMOVED
		# If they are equal, we don’t need to plan.
		if start_pos.is_equal_approx(end_pos):
			#assert(false)
			return goal
		
		# Plan the route.
		path = AI.NavMapPath.new(NavigationServer3D.map_get_path(
				map, start_pos, end_pos, optimize))
		
		# Get the furthest "straight" position on the path:
		# this is the potential goal's position.
		#var potential_pos: Vector3 = _get_furthest_straight_pos()
		var potential_pos = _get_next_straight_pos(start_pos, true)
		
		var goal_position
		
		# FIXME: TO BE REMOVED
		# check if the potential pos is far enough
		#var delta_pos = potential_pos - end_pos
		#if delta_pos.dot(delta_pos) < _min_distance_2:
			#if go_to_min_distance:                                                                                    
				## the potential pos is too close: go to a minimum distance 
				#goal_position = end_pos + (potential_pos - end_pos).normalized() * min_distance
			#else:
				## stay in place
				#goal_position = start_pos
		#else:
			#goal_position = potential_pos
		
		# check if the target pos is far enough
		var delta_pos = start_pos - end_pos
		if delta_pos.dot(delta_pos) < _min_distance_2:
			if go_to_min_distance:
				# to a minimum distance (approximation)
				goal_position = end_pos + delta_pos.normalized() * min_distance
				pass
			else:
				# stay in place
				goal_position = start_pos
		else:
			goal_position = potential_pos
		# Update the goal and return.
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			goal_position = Vector2(goal_position.x, goal_position.z)
		goal.position = goal_position
		goal.has_position = true
		return goal
	
	func _get_next_straight_pos(origin, use_projection:bool=false):
		# Get the next position on the path, that is outside
		# the vehicle bounding sphere
		# origin: the vehicle position
		# use_projection: if true checks for angle deviation
		# 		are performed with vector projections on the
		# 		body plane
		var body_x = body.global_basis.x
		var body_z = body.global_basis.z
		var node0 = origin
		# find the nearest path's node outside the body's
		# bounding sphere (projected on the body's x-z plane)
		var node1 
		for idx in path.size:
			node1 = path.get_point(idx)
			if (node1 - node0).length() > _bounding_sphere_radius:
				break
		var dir_real = node1 - node0
		var dir
		if use_projection:
			dir = (body_x * dir_real.dot(body_x) +
					body_z * dir_real.dot(body_z)).normalized()
		else:
			dir = dir_real.normalized()
		return node0 + dir * _bounding_sphere_radius
	
	func _get_furthest_straight_pos(use_projection:bool=false,
			max_distance:float=INF, num_nodes:int=path.size):
		# Get the furthest "straight" position on the path, that is,
		# the node whose segment, with respect to the path's origin,
		# has an angle less than straight_angle_max with the first
		# segment. 
		# use_projection: if true checks for angle deviation
		# 		are performed with vector projections on the
		# 		body plane
		# max_distance: distance the straight pos is limited to 
		# num_nodes: number of path's nodes to be considered in
		# the iteration
		var body_x = body.global_basis.x
		var body_z = body.global_basis.z
		#
		var node0 = path.get_point(0)
		var furthest_node = path.get_point(1)
		var range_size = clampi(num_nodes, 2, path.size)
		var dir_real0 = furthest_node - node0
		var dir0
		if use_projection:
			dir0 = (body_x * dir_real0.dot(body_x) +
					body_z * dir_real0.dot(body_z)).normalized()
		else:
			dir0 = dir_real0.normalized()
		var furthest_dir = dir0
		# perform iteration
		for idx in range(2, range_size):
			var node = path.get_point(idx)
			var dir_real = node - node0
			var dir
			if use_projection:
				dir = (body_x * dir_real.dot(body_x) +
						body_z * dir_real.dot(body_z)).normalized()
			else:
				dir = dir_real.normalized()
			if dir.dot(dir0) < straight_cos_min:
				# cos less than min <-> angle greater than max
				break
			furthest_node = node
			furthest_dir = dir
		#
		var furthest_straight_pos
		if max_distance < INF:
			furthest_straight_pos = node0 + furthest_dir * max_distance
		else:
			furthest_straight_pos = furthest_node
		return furthest_straight_pos


### CONSTRAINTS ###
class ConstraintBase extends AI.Constraint:
	var dims = AI_TOOLS.Dimensions.THREE
	var body: VehicleAI
	var map: RID
	# private variables
	var _bounding_sphere_radius: float
	
	func _init(p_object:AI.SteeringPipeline, p_body,
			p_dims, world:World3D):
		super(p_object)
		type_name = "ConstraintBase"
		dims = p_dims
		body = p_body
		map = world.get_navigation_map()
		var body_aabb = body.chassis.get_aabb().abs()
		_bounding_sphere_radius = body_aabb.get_longest_axis_size() * 0.5


class ObstacleAvoidanceConstraint extends ConstraintBase:
	# the inner obstacle avoidance behavior
	var num_rays: int
	var whiskers_angle: float
	var lookahead: float
	var avoid_distance: float
	var detector: AI.CollisionDetector
	var min_speed: float
	var plane_surface_slope_cos: float
	var suggested_pos_weight: float
	var max_maneuver_period: float # sec
	# private variables
	var _speed
	var _ray
	var _ray1
	var _has_collisions
	var _mean_ray
	var _mean_collision_pos
	var _mean_collision_norm
	var _stopped_ray_orientation # {-1=backward, 0=reset, 1=forward}
	var _new_goal
	var _current_maneuver_period
	var _maneuver_start_time
	# fsm
	var _fsm: StateMachine
	
	class OAC_BaseState extends StateMachine.State:
		
		func _init():
			process_enabled = false
			physics_process_enabled = false
			input_enabled = false
		
		func _on_enter_state() -> void:
			print("entering: ", id)
		
		func _on_leave_state() -> void:
			print("leaving: ", id)
	
	class OAC_NoCollision extends OAC_BaseState:
		# permanence in state as long as speed >= min
		
		func _on_enter_state() -> void:
			#super()
			pass
		
		func _on_leave_state() -> void:
			#super()
			pass
	
	class OAC_Maneuver extends OAC_BaseState:
		
		func _on_enter_state() -> void:
			#super()
			var curr_time = Time.get_ticks_msec()
			target.get_ref()._maneuver_start_time = curr_time
		
		func _on_leave_state() -> void:
			#super()
			target.get_ref()._current_maneuver_period = target.get_ref().max_maneuver_period
	
	
	func _init(p_object, p_body, p_collision_detector, p_min_speed,
			p_avoid_distance, p_lookahead, p_num_rays, p_whiskers_angle,
			plane_surface_slope, p_suggested_pos_weight,
			p_max_maneuver_period, p_dims, world:World3D):
		super(p_object, p_body, p_dims, world)
		type_name = "ObstacleAvoidanceConstraint"
		min_speed = p_min_speed
		detector = p_collision_detector
		avoid_distance = p_avoid_distance
		lookahead = p_lookahead
		num_rays = p_num_rays
		whiskers_angle = p_whiskers_angle
		_new_goal = AI.Goal.new(dims)
		_new_goal.has_position = true
		_stopped_ray_orientation = 0
		# HACK: "plane" surfaces are not considered colliders:
		# that is, surfaces that have a slope less than
		# PLANE_SURFACE_SLOPE, that is, whose collision normal
		# has an angle less than PLANE_SURFACE_SLOPE with respect
		# to the (global) Y-axis, and this means that the cosine,
		# which is the component along the Y-axis, is greater
		# than cos(PLANE_SURFACE_SLOPE) (= plane_surface_slope_cos)
		plane_surface_slope_cos = cos(plane_surface_slope)
		suggested_pos_weight = clampf(p_suggested_pos_weight, 0.0, 1.0)
		max_maneuver_period = p_max_maneuver_period
		_current_maneuver_period = max_maneuver_period
		_maneuver_start_time = 0.0
		#
		_fsm = StateMachineFactory.new().create({
			"target": weakref(self),
			"current_state": "OAC_NoCollision",
			"states": [
				{"id": "OAC_NoCollision", "state": OAC_NoCollision},
				{"id": "OAC_Maneuver", "state": OAC_Maneuver},
			],
			"transitions": [
				{"state_id": "OAC_NoCollision", "to_states": ["OAC_Maneuver"]},
				{"state_id": "OAC_Maneuver", "to_states": ["OAC_NoCollision"]},
			]
		})
	
	func will_violate(_path: AI.NavMapPath) -> bool:
		var current_state = _fsm.current_state
		var _has_collision: bool = false
		match current_state:
			"OAC_NoCollision":
				_has_collision = _check_collision()
				if _has_collision:
					# collision -> maneuver
					_fsm.transition("OAC_Maneuver")
			"OAC_Maneuver":
				# check if maneuver period elapsed
				var curr_time = Time.get_ticks_msec()
				if (curr_time - _maneuver_start_time) < _current_maneuver_period:
					# maneuver continues (= request suggested goal)
					_has_collision = true
				else:
					# maneuver period elapsed and speed normal -> unviolated
					_fsm.transition("OAC_NoCollision")
					_has_collision = false
		# return answer
		return _has_collision
		
	func suggest(character:AI.Kinematic, _path:AI.NavMapPath, 
			goal:AI.Goal) -> AI.Goal:
		# compute the current maneuver period: the more the vehicle is
		# directed towards the obstacle, the longer the maneuver time
		# must last, that is, the closer the size of the projection of
		# the (mean) ray w.r.t. to the collision's (mean) normal is to
		# that of the ray, the more the vehicle will have a direct
		# impact and, therefore, the longer the maneuver must last
		var ray_normal_projection = abs(_mean_ray.dot(_mean_collision_norm))
		#var ray_length = sqrt(_mean_ray.dot(_mean_ray))
		var ray_length = _mean_ray.length()
		_current_maneuver_period = max_maneuver_period * (ray_normal_projection / ray_length)
		#
		var body_z = body.global_basis.z
		var body_x = body.global_basis.x
		#
		# FIXME 1
		# the constraint has been violated: suggest a goal in
		# the "reflected" position, with respect the collision
		# plane and point, of the character (vehicle) position
		# P = character position, C = collision position, 
		# n = collision normal, V = P - C, P1 = reflected position
		# V1 = P1 - C = 2(V°n)n - V:
		#           P1 = C + V1 * avoid_distance
		#var V = character.position - _mean_collision_pos
		#var V1 = _mean_collision_norm * V.dot(_mean_collision_norm) * 2 - V
		## take the projection onto the vehicle x,z plane
		## HACK: corner case
		#var current_mean_collision_pos = _mean_collision_pos
		#if _reverse_new_position_on_steering():
			#current_mean_collision_pos = character.position - V1
			#V1 = V
		#var V1_xz = V1.dot(body_x) * body_x + V1.dot(body_z) * body_z
		#var suggested_pos = current_mean_collision_pos + V1_xz * avoid_distance
		
		# FIXME 2
		# the constraint has been violated: suggest a goal in
		# the "reflected" direction to the (mean) ray
		#var reflected_ray = (
			#_mean_collision_norm.dot(-_mean_ray) * _mean_collision_norm +
			#_mean_collision_norm.cross(
					#_mean_collision_norm.cross(-_mean_ray)))
		#var suggested_pos = character.position - reflected_ray * avoid_distance
		
		# FIXME 3
		# the constraint has been violated: suggest a goal in the
		# opposite direction to the (mean) radius,
		# i.e. essentially asking the vehicle to brake
		var suggested_pos = character.position - _mean_ray * avoid_distance

		# FIXME 4
		# the constraint has been violated: suggest a goal in the
		# (mean) collision normal direction;
		# take the projection onto the vehicle x,z plane
		#var norm_xz = _mean_collision_norm.dot(body_x) * body_x + _mean_collision_norm.dot(body_z) * body_z
		#var suggested_pos = _mean_collision_pos + norm_xz * avoid_distance
		
		# new position is a weighted sum with the original goal position
		var new_pos = (suggested_pos_weight * suggested_pos + 
				(1.0 - suggested_pos_weight) * goal.position)
		# FOR ALL METHODS
		# project new_pos on the vehicle's x-z plane and 
		# place on the bounding sphere
		var dir_real = new_pos - character.position
		var dir = (body_x * dir_real.dot(body_x) +
					body_z * dir_real.dot(body_z)).normalized()
		new_pos = character.position + dir * _bounding_sphere_radius
			
		# return the new goal
		if dims == AI_TOOLS.Dimensions.TWO:
			_new_goal.position = Vector2(new_pos.x, new_pos.z)
		else:
			_new_goal.position = new_pos
		return _new_goal
	
	func _check_collision():
		# let's assume that the path "never" encounters
		# obstacles (i.e. the path is "collision-free"
		# and never heads towards any obstacle); in this
		# situation we need to check for collisions along
		# the direction of motion or along the orientation
		# of the vehicle
		_has_collisions = false
		var collision = null
		var collision1 = null
		var character = steering_pipeline.get_ref().character
		
		# initial common tasks		
		if (dims == AI_TOOLS.Dimensions.TWO):
			_speed = Vector3(character.velocity.x, 0,
					character.velocity.y).length()
		else:
			_speed = character.velocity.length()
		var ray_orientation
		# check whether the vehicle is moving or (almost) stopped
		# FIXME
		if _speed >= min_speed:
			# vehicle is moving
			# reset _stopped_ray_orientation
			_stopped_ray_orientation = 0
			
			# 1): cast a ray along direction of motion
			ray_orientation = character.velocity.normalized()
			
			# 2): cast a ray along orientation of vehicle
			#if body.global_basis.z.dot(character.velocity) >= 0.0:
				## vehicle moves forward
				#ray_orientation = body.global_basis.z
			#else:
				## vehicle moves backwards
				#ray_orientation = -body.global_basis.z
		else:
			# vehicle is (almost) stopped: check if desired
			# path is ahead or behind
			match _stopped_ray_orientation:
				0, 1:
					# try casting ray forward first
					_stopped_ray_orientation = 1
					ray_orientation = body.global_basis.z
				-1:
					# cast ray backward
					_stopped_ray_orientation = -1
					ray_orientation = -body.global_basis.z
		
		# check collision(s): cast the ray(s)
		if num_rays == 1:
			# 1 ray (default)
			if (dims == AI_TOOLS.Dimensions.TWO):
				_ray = ray_orientation * (lookahead + _speed)
			else:
				_ray = ray_orientation * (lookahead + _speed)
			_ray1 = _ray
			_mean_ray = _ray
			# Find the collision.
			if dims == AI_TOOLS.Dimensions.TWO:
				collision = detector.get_collision(
						Vector3(character.position.x, 0,
						character.position.y), _ray)
			else:
				collision = detector.get_collision(character.position,
						_ray)
			# if a colliding body has collision layer = 0x10000000 = 0b00010000000000000000000000000000 (bit #29)
			# it will not be considered a collider
			if (collision != null) and (not (collision.body.collision_layer & 0x10000000)):
				if collision.normal.y < plane_surface_slope_cos:
					_has_collisions = true
			if _has_collisions:
				_mean_collision_pos = collision.position
				_mean_collision_norm = collision.normal
		elif num_rays == 2:
			# 2 rays
			var y
			var z
			if (dims == AI_TOOLS.Dimensions.TWO):
				z = ray_orientation * (lookahead + _speed)
				y = Vector3.UP
			else:
				z = ray_orientation * (lookahead + _speed)
				var x = z.cross(Vector3.UP)
				y = x.cross(z).normalized()
			_ray = Basis(y, -whiskers_angle) * (z)
			_ray1 = Basis(y, whiskers_angle) * (z)
			_mean_ray = (_ray + _ray1) * 0.5
			# Find the collisions.
			if dims == AI_TOOLS.Dimensions.TWO:
				collision = detector.get_collision(
						Vector3(character.position.x, 0,
						character.position.y), _ray)
				collision1 = detector.get_collision(
						Vector3(character.position.x, 0,
						character.position.y), _ray1)
			else:
				collision = detector.get_collision(character.position,
						_ray)
				collision1 = detector.get_collision(character.position,
						_ray1)
			# if a colliding body has collision layer = 0x10000000 = 0b00010000000000000000000000000000 (bit #29)
			# it will not be considered a collider
			if (collision != null) and (not (collision.body.collision_layer & 0x10000000)):
				if collision.normal.y < plane_surface_slope_cos:
					_has_collisions = true
			elif (collision1 != null) and (not (collision1.body.collision_layer & 0x10000000)):
				if collision1.normal.y < plane_surface_slope_cos:
					_has_collisions = true
			if _has_collisions:
				var position_r
				var normal_r
				var position_l
				var normal_l
				if dims == AI_TOOLS.Dimensions.TWO:
					if collision:
						position_r = Vector2(collision.position.x,
								collision.position.z)
						normal_r = Vector2(collision.normal.x, 
								collision.normal.z)
					else:
						position_r = character.position + Vector2(_ray.x, _ray.z)
						normal_r = Vector2.ZERO
					if collision1:
						position_l = Vector2(collision1.position.x, 
								collision1.position.z)
						normal_l = Vector2(collision1.normal.x, 
								collision1.normal.z)
					else:
						position_l = character.position + Vector2(_ray1.x, _ray1.z)  
						normal_l = Vector2.ZERO
				else:
					if collision:
						position_r = collision.position
						normal_r = collision.normal
					else:
						position_r = character.position + _ray
						normal_r = Vector3.ZERO
					if collision1:
						position_l = collision1.position
						normal_l = collision1.normal
					else:
						position_l = character.position + _ray1
						normal_l = Vector3.ZERO
				if not position_l:
					# approximation (optimization)
					_mean_collision_pos = position_r
				elif not position_r:
					# approximation (optimization)
					_mean_collision_pos = position_l
				else:
					_mean_collision_pos = (position_r + position_l) * 0.5
				#_mean_collision_norm = (normal_r + normal_l).normalized()
				_mean_collision_norm = normal_r + normal_l # approximation (optimization)
		
		# final common tasks
		if not _has_collisions:
			if _stopped_ray_orientation != 0:
				_stopped_ray_orientation = 0
		#
		return _has_collisions
	
	func _reverse_new_position_on_steering():
		# HACK corner case: the vehicle is steering right/left but the suggested
		# new position causes it to steer in the opposite direction;
		var do_reverse_pos = false
		# let's assume that the upward direction of the plane defined by 
		# (_mean_ray, _mean_collision_norm, up_dir) is pointing up with
		# respect to the global x-z plane;
		# body.steering </> 0 -> vehicle is steering on the right/left
		var motion_dir = body.global_basis.z.dot(_mean_ray)
		var up_dir = _mean_ray.cross(_mean_collision_norm).y
		if motion_dir >= 0.0:
			# vehicle "moves" forward
			do_reverse_pos = (
				((up_dir > 0.0) and (body.steering < 0.0)) 
				or
				((up_dir < 0.0) and (body.steering > 0.0)) 
			)
		else:
			# vehicle "moves" backward
			do_reverse_pos = (
				((up_dir < 0.0) and (body.steering < 0.0)) 
				or
				((up_dir > 0.0) and (body.steering > 0.0)) 
			)
		return do_reverse_pos


class SeparationConstraint extends ConstraintBase:
	# the inner separation behavior
	var targets: Array
	var max_acceleration: float
	# The (squared) threshold to take action.
	var threshold_2: float
	# The constant coefficient of decay for the inverse square law.
	var decay_coefficient: float
	# Controls how much to anticipate the movement.
	var lookahead: float
	# private variables
	var _steering_needed
	var _result_linear
	var _new_goal
	
	func _init(p_object:AI.SteeringPipeline, p_body, p_targets,
			p_threshold, p_decay_coefficient, p_lookahead,
			p_max_acceleration, p_dims, world:World3D):
		super(p_object, p_body, p_dims, world)
		type_name = "SeparationConstraint"
		targets = p_targets
		threshold_2 = p_threshold * p_threshold
		decay_coefficient = p_decay_coefficient
		lookahead = p_lookahead
		max_acceleration = p_max_acceleration
		if dims == AI_TOOLS.Dimensions.TWO:
			_result_linear = Vector2.ZERO
		else:
			_result_linear = Vector3.ZERO
		_new_goal = AI.Goal.new(dims)
		_new_goal.has_position = true
	
	func add_target(target):
		targets.append(target)
	
	func remove_target(target):
		targets.erase(target)
	
	func will_violate(_path: AI.NavMapPath) -> bool:
		# Loop through each target.
		_steering_needed = false
		_result_linear *= 0.0
		var character = steering_pipeline.get_ref().character
		for target in targets:
			# Check if the target is close.
			var direction = target.position - character.position
			var distance_2 = direction.dot(direction)
			if distance_2 < threshold_2:
				_steering_needed = true
				# Calculate the strength of repulsion
				# (here using the inverse square law).
				var strength = min(decay_coefficient / distance_2, 
						max_acceleration)
				# Add the acceleration.
				_result_linear += strength * (-direction.normalized())
		return _steering_needed
	
	func suggest(character:AI.Kinematic, _path:AI.NavMapPath, 
			_goal:AI.Goal) -> AI.Goal:
		var body_z = body.global_basis.z
		var body_x = body.global_basis.x
		# project new_pos on the vehicle's x-z plane and 
		# place on the bounding sphere
		var new_pos = character.position + _result_linear * lookahead
		var dir_real = new_pos - character.position
		var dir = (body_x * dir_real.dot(body_x) +
					body_z * dir_real.dot(body_z)).normalized()
		new_pos = character.position + dir * _bounding_sphere_radius

		_new_goal.position = new_pos
		return _new_goal


class AvoidObstacleConstraint extends ConstraintBase:
	# The obstacle bounding sphere.
	var center: Vector3
	var radius: float
	var radius_2: float
	
	# Holds a margin of error by which we’d ideally like
	# to clear the obstacle. Given as a proportion of the
	# radius (i.e. should be > 1.0).
	var margin: float
	
	# If a violation occurs, stores the part of the path
	# that caused the problem.
	var problem_index: int
	
	func _init(p_object:AI.SteeringPipeline, p_body,
			p_center:Vector3, p_radius:float, p_margin:float,
			p_dims, world:World3D):
		super(p_object, p_body, p_dims, world)
		type_name = "AvoidObstacleConstraint"
		center = p_center
		radius = p_radius
		radius_2 = p_radius * p_radius
		margin = p_margin
	
	func will_violate(path: AI.NavMapPath) -> bool:
		# Check each segment of the path in turn.
		for i in (path.size - 1):
			var segment = path.get_segment(i)
			# If we have a clash, store the current segment.
			if AI_TOOLS.point_segment_distance_squared(center, segment) < radius_2:
				problem_index = i
				return true
		# No segments caused a problem.
		return false
	
	func suggest(character:AI.Kinematic, path:AI.NavMapPath, 
			goal:AI.Goal) -> AI.Goal:
		var body_z = body.global_basis.z
		var body_x = body.global_basis.x
		# Find the closest point on the segment to the sphere center.
		var segment = path.get_segment(problem_index)
		var closest = AI_TOOLS.point_segment_closest_point(segment, center,
				false)
		
		# Check if we pass through the center point.
		var new_pos
		var offset = closest - center
		var offset_length = offset.length()
		if offset_length == 0:
			# Any vector at right angles to the segment will do.
			var direction = segment.direction
			var new_direction = direction.cross(Vector3.UP).normalized()
			new_pos = center + new_direction * radius * margin
		# Otherwise project the point out beyond the radius.
		else:
			new_pos = center + offset * radius * margin / offset_length
		# Set up the goal and return.
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			new_pos = Vector2(new_pos.x, new_pos.z)

		# project new_pos on the vehicle's x-z plane and 
		# place on the bounding sphere
		var dir_real = new_pos - character.position
		var dir = (body_x * dir_real.dot(body_x) +
					body_z * dir_real.dot(body_z)).normalized()
		new_pos = character.position + dir * _bounding_sphere_radius
		#
		goal.position = new_pos
		goal.has_position = true
		return goal


### ACTUATORS ###
class ActuatorBase extends AI.Actuator:
	var straight_cos_min: float # radians
	var body: VehicleAI
	# private variables
	var _bounding_sphere_radius: float
	
	func _init(p_object:AI.SteeringPipeline, p_body,
			straight_angle_max:float):
		super(p_object)
		straight_cos_min = cos(straight_angle_max)
		body = p_body
		var body_aabb = body.chassis.get_aabb().abs()
		_bounding_sphere_radius = body_aabb.get_longest_axis_size() * 0.5
	
	func _get_next_straight_pos(path, origin,
			use_projection:bool=false):
		# Get the next position on the path, that is outside
		# the vehicle bounding sphere
		# origin: the vehicle position
		# use_projection: if true checks for angle deviation
		# 		are performed with vector projections on the
		# 		body plane
		var body_x = body.global_basis.x
		var body_z = body.global_basis.z
		var node0 = origin
		# find the nearest path's node outside the body's
		# bounding sphere (projected on the body's x-z plane)
		var node1 
		for idx in path.size:
			node1 = path.get_point(idx)
			if (node1 - node0).length() > _bounding_sphere_radius:
				break
		var dir_real = node1 - node0
		var dir
		if use_projection:
			dir = (body_x * dir_real.dot(body_x) +
					body_z * dir_real.dot(body_z)).normalized()
		else:
			dir = dir_real.normalized()
		return node0 + dir * _bounding_sphere_radius
	
	func _get_furthest_straight_pos(path, use_projection:bool=false,
				max_distance:float=INF, num_nodes:int=path.size):
		# Get the furthest "straight" position on the path, that is,
		# the node whose segment, with respect to the path's origin,
		# has an angle less than straight_angle_max with the first
		# segment. 
		# use_projection: if true checks for angle deviation
		# 		are performed with vector projections on the
		# 		body plane
		# max_distance: distance the straight pos is limited to 
		# num_nodes: number of path's nodes to be considered in
		# the iteration
		var body_x = body.global_basis.x
		var body_z = body.global_basis.z
		#
		var node0 = path.get_point(0)
		var furthest_node = path.get_point(1)
		var range_size = clampi(num_nodes, 2, path.size)
		var dir_real0 = furthest_node - node0
		var dir0
		if use_projection:
			dir0 = (body_x * dir_real0.dot(body_x) +
					body_z * dir_real0.dot(body_z)).normalized()
		else:
			dir0 = dir_real0.normalized()
		var furthest_dir = dir0
		# perform iteration
		for idx in range(2, range_size):
			var node = path.get_point(idx)
			var dir_real = node - node0
			var dir
			if use_projection:
				dir = (body_x * dir_real.dot(body_x) +
						body_z * dir_real.dot(body_z)).normalized()
			else:
				dir = dir_real.normalized()
			if dir.dot(dir0) < straight_cos_min:
				# cos less than min <-> angle greater than max
				break
			furthest_node = node
			furthest_dir = dir
		#
		var furthest_straight_pos
		if max_distance < INF:
			furthest_straight_pos = node0 + furthest_dir * max_distance
		else:
			furthest_straight_pos = furthest_node
		return furthest_straight_pos


class SeekActuator extends ActuatorBase:
	var seek: AI.SeekFlee
	var map: RID
	
	func _init(p_object:AI.SteeringPipeline, p_body, max_acceleration, 
		damping, straight_angle_max, dims, world:World3D):
		super(p_object, p_body, straight_angle_max)
		type_name = "SeekActuator"
		seek = AI.SeekFlee.new()
		seek.setup(max_acceleration, damping, dims, AI_TOOLS.Behavior.SEEK)
		map = world.get_navigation_map()
	
	func get_path(character:AI.Kinematic, goal:AI.Goal) -> AI.NavMapPath:
		var start_pos = character.position
		var end_pos = goal.position
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			start_pos = Vector3(start_pos.x, 0.0, start_pos.y)
			end_pos = Vector3(end_pos.x, 0.0, end_pos.y)
		return AI.NavMapPath.new(PackedVector3Array([start_pos, end_pos]))
	
	func output(character:AI.Kinematic, path:AI.NavMapPath, 
			goal:AI.Goal) -> AI.SteeringOutput:
		seek.character.position = character.position
		#var new_pos = _get_furthest_straight_pos(path)
		var new_pos = _get_next_straight_pos(path,
				character.position, true)
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			new_pos = Vector2(new_pos.x, new_pos.z)
		#
		seek.target.position = new_pos
		return seek.get_steering()


class ArriveActuator extends ActuatorBase:
	var arrive: AI.ArriveLeave
	var map: RID
	
	func _init(p_object:AI.SteeringPipeline, p_body, max_acceleration,
			max_speed, target_radius, slow_radius,
			time_to_target, damping, straight_angle_max,
			dims, world:World3D):
		super(p_object, p_body, straight_angle_max)
		type_name = "ArriveActuator"
		arrive = AI.ArriveLeave.new()
		arrive.setup(max_acceleration, max_speed, target_radius, 
			slow_radius, time_to_target, damping, dims,
			AI_TOOLS.Behavior.ARRIVE)
		map = world.get_navigation_map()
	
	func get_path(character:AI.Kinematic, goal:AI.Goal) -> AI.NavMapPath:
		var start_pos = character.position
		var end_pos = goal.position
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			start_pos = Vector3(start_pos.x, 0.0, start_pos.y)
			end_pos = Vector3(end_pos.x, 0.0, end_pos.y)
		return AI.NavMapPath.new(PackedVector3Array([start_pos, end_pos]))
	
	func output(character:AI.Kinematic, path:AI.NavMapPath, 
			goal:AI.Goal) -> AI.SteeringOutput:
		arrive.character.position = character.position
		#var new_pos = _get_furthest_straight_pos(path)
		var new_pos = _get_next_straight_pos(path,
				character.position, true)
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			new_pos = Vector2(new_pos.x, new_pos.z)
		# HACK: the real goal is a "rectified" approximation:
		# real goal = the original goal after the targeters
		var real_target_position = steering_pipeline.get_ref()._original_goal.position
		var length = (real_target_position - character.position).length()
		var direction = (new_pos - character.position).normalized()
		arrive.target.position = character.position + direction * length
		return arrive.get_steering()
