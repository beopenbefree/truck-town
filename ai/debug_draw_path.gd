extends MeshInstance3D


@export var point_size: float = 1.0
@export var material: StandardMaterial3D = null
@export var material_point: StandardMaterial3D = null


func _ready():
	mesh = mesh.duplicate()
	if not material:
		material = StandardMaterial3D.new()
	material.flags_unshaded = true
	if point_size > 1.0:
		if not material_point:
			material_point = StandardMaterial3D.new()
		# draw the points too
		material_point.flags_unshaded = true
		material_point.flags_use_point_size = true
		material_point.point_size = point_size


func draw_path(path):
	mesh.clear_surfaces()
	mesh.surface_begin(Mesh.PRIMITIVE_LINE_STRIP, material)
	for x in path:
		mesh.surface_add_vertex(x)
	mesh.surface_end()
	if point_size > 1.0:
		# draw the points too
		mesh.surface_begin(Mesh.PRIMITIVE_POINTS, material_point)
		for x in path:
			mesh.surface_add_vertex(x)
		mesh.surface_end()
