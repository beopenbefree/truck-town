# see: AI4G - (Millington, I. (2019). Ai for games, Third edition. CRC Press.)
class_name AI
extends RefCounted


class SteeringOutput extends RefCounted:
	var dims = AI_TOOLS.Dimensions.THREE
	# used by Kinematic behaviors
	var velocity
	var rotation := 0.0
	# used by Steering behaviors
	var linear
	var angular := 0.0
	
	func _init(p_dims=AI_TOOLS.Dimensions.THREE):
		dims = p_dims
		reset()
	
	# operator overloading
	func _add(other:AI.SteeringOutput) -> AI.SteeringOutput:
		var result = AI.SteeringOutput.new(dims)
		result.velocity = velocity + other.velocity
		result.rotation = rotation + other.rotation
		result.linear = linear + other.linear
		result.angular = angular + other.angular
		return result
		
	func _iadd(other:AI.SteeringOutput) -> AI.SteeringOutput:
		velocity += other.velocity
		rotation += other.rotation
		linear += other.linear
		angular += other.angular
		return self
		
	func _mul(scalar:float) -> AI.SteeringOutput:
		var result = AI.SteeringOutput.new(dims)
		result.velocity = velocity * scalar
		result.rotation = rotation * scalar
		result.linear = linear * scalar
		result.angular = angular * scalar
		return result
		
	func _imul(scalar:float) -> AI.SteeringOutput:
		velocity *= scalar
		rotation *= scalar
		linear *= scalar
		angular *= scalar
		return self
		
	func reset():
		if dims == AI_TOOLS.Dimensions.TWO:
			velocity = Vector2.ZERO
			linear = Vector2.ZERO
		else:
			velocity = Vector3.ZERO
			linear = Vector3.ZERO
		rotation = 0.0
		angular = 0.0


class Static extends RefCounted:
	var dims
	var position
	var orientation := 0.0
	
	func _init(p_dims=AI_TOOLS.Dimensions.THREE):
		dims = p_dims
		if p_dims == AI_TOOLS.Dimensions.TWO:
			position = Vector2.ZERO
		else:
			position = Vector3.ZERO
	
	func update(steering:SteeringOutput, time:float, 
			_max_speed:float=-1.0):
		# Update the position and orientation.
		position += steering.velocity * time
		orientation = AI_TOOLS.map_to_range(orientation + steering.rotation * time)


class Kinematic extends Static:
	var velocity
	var rotation := 0.0
	var damping := 0.0: set = _set_damping
	
	func _init(p_dims=AI_TOOLS.Dimensions.THREE):
		super(p_dims)
		# HACK: call setters with (potential) side effects
		_set_damping(damping)
		#
		if p_dims == AI_TOOLS.Dimensions.TWO:
			velocity = Vector2.ZERO
		else:
			velocity = Vector3.ZERO
	
	func _set_damping(value:float):
		damping = clampf(1.0 - value, 0.0, 1.0)
	
	func update(steering:SteeringOutput, time:float, 
			max_speed:float=-1.0):
		# Update the position and orientation.
		position += velocity * time
		orientation = AI_TOOLS.map_to_range(orientation + rotation * time)
		
		# and the velocity and rotation.
		var linear_accel = steering.linear
		var angular_accel = steering.angular
		if linear_accel.dot(linear_accel) != 0.0:# i.e. linear_accel.length() != 0.0
			velocity += linear_accel * time
		else:
			velocity *= damping
		if angular_accel != 0.0:
			rotation += angular_accel * time
		else:
			rotation *= damping
		
		# Check for speeding and clip.
		if (max_speed > 0.0) and (velocity.length() > max_speed):
			velocity = velocity.normalized() * max_speed


class SteeringBehavior extends RefCounted:
	var type: int
	var dims: int
	var character: Static: set = _set_character
	var name: String = str(self): set = _set_name
	var signal_enable = false
	
	func get_steering() -> SteeringOutput:
		push_warning("Unimplemented get_steering")
		return SteeringOutput.new()
	
	func _send_signal(value:String, action:String, last_signal_action:Array):
		if signal_enable and (last_signal_action[0] != action):
			emit_signal(value, self.name, action)
			last_signal_action[0] = action
	
	func _set_character(value:Static):
		character = value
	
	func _set_name(value):
		name = value


class KinematicSeekFlee extends SteeringBehavior:
	var target:Static
	var max_speed:float
	
	func setup(p_max_speed, p_dims=AI_TOOLS.Dimensions.THREE,
			p_type=AI_TOOLS.Behavior.SEEK):
		type = p_type
		dims = p_dims
		character = Static.new(dims)
		target = Static.new(dims)
		max_speed = p_max_speed
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get the direction to the target.
		result.velocity = target.position - character.position
		if type == AI_TOOLS.Behavior.FLEE:
			result.velocity = -result.velocity
		# The velocity is along this direction, at full speed.
		result.velocity = result.velocity.normalized() * max_speed
		# Face in the direction we want to move.
		character.orientation = AI_TOOLS.new_orientation(
			character.orientation, result.velocity, dims)
		result.rotation = SteeringOutput.new(dims).rotation
		return result


class KinematicArriveLeave extends SteeringBehavior:
	@warning_ignore("unused_signal")
	signal arrive_leave(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	var target: Static
	var max_speed: float
	# The satisfaction radius.
	var radius: float
	# The time to target constant.
	var time_to_target: float
	
	func setup(p_max_speed, p_radius, p_time_to_target,
			p_dims=AI_TOOLS.Dimensions.THREE, 
			p_type=AI_TOOLS.Behavior.ARRIVE):
		type = p_type
		dims = p_dims
		character = Static.new(dims)
		target = Static.new(dims)
		max_speed = p_max_speed
		radius = p_radius
		time_to_target = p_time_to_target
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get the direction to the target.
		if type == AI_TOOLS.Behavior.LEAVE:
			result.velocity = -target.position + character.position
			# Check if we’re within radius.
			if result.velocity.length() > radius:
				# send signal
				_send_signal("arrive_leave", "stop_steering", last_start_stop)
				# Request no steering.
				return SteeringOutput.new(dims)
		else:
			result.velocity = target.position - character.position
			# Check if we’re within radius.
			if result.velocity.length() < radius:
				# send signal
				_send_signal("arrive_leave", "stop_steering", last_start_stop)
				# Request no steering.
				return SteeringOutput.new(dims)
		# send signal
		_send_signal("arrive_leave", "start_steering", last_start_stop)
		# We need to move to our target, we’d like to
		# get there in timeToTarget seconds.
		result.velocity /= time_to_target
		# If this is too fast, clip it to the max speed.
		if result.velocity.length() > max_speed:
			result.velocity = result.velocity.normalized() * max_speed
		# Face in the direction we want to move.
		character.orientation = AI_TOOLS.new_orientation(
			character.orientation, result.velocity, dims)
		result.rotation = SteeringOutput.new(dims).rotation
		return result


class KinematicWander extends SteeringBehavior:
	var max_speed: float
	# The maximum rotation speed we’d like, probably should be smaller
	# than the maximum possible, for a leisurely change in direction.
	var max_rotation: float
	
	func setup(p_max_speed, p_max_rotation, 
			p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.WANDER
		dims = p_dims
		character = Static.new(dims)
		max_speed = p_max_speed
		max_rotation = p_max_rotation
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get velocity from the vector form of the orientation.
		if dims == AI_TOOLS.Dimensions.TWO:
			result.velocity = (AI_TOOLS.as_vector2(character.orientation) *
					max_speed)
		else:
			result.velocity = (AI_TOOLS.as_vector3(character.orientation) * 
					max_speed)
		# Change our orientation randomly.
		result.rotation = AI_TOOLS.random_binomial() * max_rotation
		return result


class SeekFlee extends SteeringBehavior:
	var target: Kinematic
	var max_acceleration: float
	
	func setup(p_max_acceleration, p_damping = 0.0,
			p_dims=AI_TOOLS.Dimensions.THREE, 
			p_type=AI_TOOLS.Behavior.SEEK):
		type = p_type
		dims = p_dims
		character = Kinematic.new(dims)
		character.damping = p_damping
		target = Kinematic.new(dims)
		max_acceleration = p_max_acceleration
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get the direction to the target.
		result.linear = target.position - character.position
		if type == AI_TOOLS.Behavior.FLEE:
			result.linear = -result.linear
		# Give full acceleration along this direction.
		result.linear = result.linear.normalized() * max_acceleration
		result.angular = SteeringOutput.new(dims).angular
		return result


class ArriveLeave extends SteeringBehavior:
	@warning_ignore("unused_signal")
	signal arrive_leave(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	# in/out slow radius action
	var last_in_out_slow_radius: Array = [""]
	
	var target: Kinematic
	var max_acceleration: float
	var max_speed: float
	# The radius for arriving at the target.
	var target_radius: float
	# The radius for beginning to slow down.
	var slow_radius: float
	# The time over which to achieve target speed.
	var time_to_target: float
	# private variables
	var _direction
	var _distance
	var _target_speed
	var _target_velocity
	
	func setup(p_max_acceleration, p_max_speed, p_target_radius, 
			p_slow_radius, p_time_to_target, p_damping = 0.0, 
			p_dims=AI_TOOLS.Dimensions.THREE,
			p_type=AI_TOOLS.Behavior.ARRIVE):
		type = p_type
		dims = p_dims
		character = Kinematic.new(dims)
		character.damping = p_damping
		target = Kinematic.new(dims)
		max_acceleration = p_max_acceleration
		max_speed = p_max_speed
		target_radius = p_target_radius
		slow_radius = p_slow_radius
		time_to_target = p_time_to_target
		# initialize private variables
		if dims == AI_TOOLS.Dimensions.THREE:
			_direction = Vector3.ZERO
		else:
			_direction = Vector2.ZERO
		_distance = 0.0
		_target_speed = 0.0
		_target_velocity = _direction
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get the direction to the target.
		_direction = target.position - character.position
		if type == AI_TOOLS.Behavior.LEAVE:
			_direction = -_direction
		_distance = _direction.length()
		# Check if we are there, return no steering.
		if _distance < target_radius:
			# send signal
			_send_signal("arrive_leave", "stop_steering", last_start_stop)
			return SteeringOutput.new(dims)
		# send signal
		_send_signal("arrive_leave", "start_steering", last_start_stop)
		if _distance > slow_radius:
			# If we are outside the slowRadius, then move at max speed.
			_target_speed = max_speed
			# send signal
			_send_signal("arrive_leave", "out_slow_radius", last_in_out_slow_radius)
		else:
			# Otherwise calculate a scaled speed.
			_target_speed = max_speed * _distance / slow_radius
			# send signal
			_send_signal("arrive_leave", "in_slow_radius", last_in_out_slow_radius)
		# The target velocity combines speed and direction.
		_target_velocity = _direction
		_target_velocity = _target_velocity.normalized() * _target_speed
		# Acceleration tries to get to the target velocity.
		result.linear = _target_velocity - character.velocity
		result.linear /= time_to_target
		# Check if the acceleration is too fast.
		if result.linear.length() > max_acceleration:
			result.linear = result.linear.normalized() * max_acceleration
		result.angular = SteeringOutput.new(dims).angular
		return result


class Align extends SteeringBehavior:
	@warning_ignore("unused_signal")
	signal align(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	# in/out slow radius action
	var last_in_out_slow_radius: Array = [""]
	
	var target: Kinematic
	var max_angular_acceleration: float
	var max_rotation: float
	# The radius for arriving at the target.
	var target_radius: float
	# The radius for beginning to slow down.
	var slow_radius: float
	# The time over which to achieve target speed.
	var time_to_target: float
	# private variables
	var _rotation
	var _rotation_size
	
	func setup(p_max_angular_acceleration, p_max_rotation, 
			p_target_radius, p_slow_radius, p_time_to_target, 
			p_damping = 0.0, p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.ALIGN
		dims = p_dims
		character = Kinematic.new(dims)
		character.damping = p_damping
		target = Kinematic.new(dims)
		max_angular_acceleration = p_max_angular_acceleration
		max_rotation = p_max_rotation
		target_radius = p_target_radius
		slow_radius = p_slow_radius
		time_to_target = p_time_to_target
		# initialize private variables
		_rotation = 0.0
		_rotation_size = 0.0
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get the naive direction to the target.
		_rotation = target.orientation - character.orientation
		# Map the result to the (-pi, pi) interval.
		_rotation = AI_TOOLS.map_to_range(_rotation)
		_rotation_size = abs(_rotation)
		# Check if we are there, return no steering.
		if _rotation_size < target_radius:
			# send signal
			_send_signal("align", "stop_steering", last_start_stop)
			return SteeringOutput.new(dims)
		var target_rotation
		if _rotation_size > slow_radius:
			# If we are outside the slowRadius, then use maximum rotation.
			target_rotation = max_rotation
			# send signal
			_send_signal("align", "out_slow_radius", last_in_out_slow_radius)
		else:
			# Otherwise calculate a scaled rotation.
			target_rotation = max_rotation * _rotation_size / slow_radius
			# send signal
			_send_signal("align", "in_slow_radius", last_in_out_slow_radius)
		# send signal
		_send_signal("align", "start_steering", last_start_stop)
		# The final target rotation combines speed (already in the
		# variable) and direction.
		target_rotation *= _rotation / _rotation_size
		# Acceleration tries to get to the target rotation.
		result.angular = target_rotation - character.rotation
		result.angular /= time_to_target
		# Check if the acceleration is too great.
		var angular_acceleration = abs(result.angular)
		if angular_acceleration > max_angular_acceleration:
			result.angular /= angular_acceleration
			result.angular *= max_angular_acceleration
		result.linear = SteeringOutput.new(dims).linear
		return result


class VelocityMatch extends SteeringBehavior:
	var target: Kinematic
	var max_acceleration: float
	# The time over which to achieve target speed.
	var time_to_target: float
	
	func setup(p_max_acceleration, p_time_to_target, 
			p_damping = 0.0, p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.VELOCITY_MATCH
		dims = p_dims
		character = Kinematic.new(dims)
		character.damping = p_damping
		target = Kinematic.new(dims)
		max_acceleration = p_max_acceleration
		time_to_target = p_time_to_target
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Acceleration tries to get to the target velocity.
		result.linear = target.velocity - character.velocity
		result.linear /= time_to_target
		# Check if the acceleration is too fast.
		if result.linear.length() > max_acceleration:
			result.linear = result.linear.normalized() * max_acceleration
		result.angular = SteeringOutput.new(dims).angular
		return result


class PursueEvade extends SteeringBehavior:
	# This implementation uses SeekFlee sub-behavior component
	@warning_ignore("unused_signal")
	signal pursue_evade(object_name, signal_action)
	# on/off max prediction action
	var last_on_off_max_prediction: Array = [""]
	
	var target: Kinematic
	# The maximum prediction time.
	var max_prediction: float
	# delegation
	var seek_flee: SeekFlee
	# private variables
	var _direction
	var _distance
	var _speed
	var _prediction
	
	func _set_character(value):
		super(value)
		# override delegate's character
		seek_flee.character = value
	
	func _set_name(value):
		super(value)
		seek_flee.name = name
	
	func setup(max_acceleration_delegate, p_max_prediction, 
			p_damping = 0.0, p_dims=AI_TOOLS.Dimensions.THREE,
			p_type=AI_TOOLS.Behavior.PURSUE):
		type = p_type
		dims = p_dims
		# delegation setup
		var type_sf = AI_TOOLS.Behavior.SEEK
		if type == AI_TOOLS.Behavior.EVADE:
			type_sf = AI_TOOLS.Behavior.FLEE
		seek_flee = SeekFlee.new()
		seek_flee.setup(max_acceleration_delegate, p_damping, dims, type_sf)
		seek_flee.name = name
		#
		character = Kinematic.new(dims)
		character.damping = p_damping
		target = Kinematic.new(dims)
		max_prediction = p_max_prediction
		# initialize private variables
		if dims == AI_TOOLS.Dimensions.THREE:
			_direction = Vector3.ZERO
		else:
			_direction = Vector2.ZERO
		_distance = 0.0
		_speed = 0.0
		_prediction = 0.0
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to seek
		# Work out the distance to target.
		_direction = target.position - character.position
		if type == AI_TOOLS.Behavior.EVADE:
			_direction = -_direction
		_distance = _direction.length()
		# Work out our current speed.
		_speed = character.velocity.length()
		if _speed <= _distance / max_prediction:
			# Check if speed gives a reasonable prediction time.
			_prediction = max_prediction
			# send signal
			_send_signal("pursue_evade", "on_max_prediction", last_on_off_max_prediction)
		else:
			# Otherwise calculate the prediction time.
			_prediction = _distance / _speed
			# send signal
			_send_signal("pursue_evade", "off_max_prediction", last_on_off_max_prediction)
		# Put the target together.
		seek_flee.target.position = target.position
		seek_flee.target.velocity = target.velocity
		seek_flee.target.position += target.velocity * _prediction
		# 2. Delegate to seek.
		return seek_flee.get_steering()


class PursueEvade2 extends SteeringBehavior:
	# This implementation uses ArriveLeave sub-behavior component
	@warning_ignore("unused_signal")
	signal pursue_evade_2(object_name, signal_action)
	# on/off max prediction action
	var last_on_off_max_prediction: Array = [""]
	
	var target: Kinematic
	# The maximum prediction time.
	var max_prediction: float
	# delegation
	var arrive_leave: ArriveLeave
	# private variables
	var _direction
	var _distance
	var _speed
	var _prediction
	
	func _set_character(value):
		super(value)
		# override delegate's character
		arrive_leave.character = value
	
	func _set_name(value):
		super(value)
		arrive_leave.name = name
	
	func setup(max_acceleration_delegate, max_speed_delegate, 
			target_radius_delegate, slow_radius_delegate,
			time_to_target_delegate, p_max_prediction, p_damping = 0.0,
			p_dims=AI_TOOLS.Dimensions.THREE, 
			p_type=AI_TOOLS.Behavior.PURSUE2):
		type = p_type
		dims = p_dims
		# delegation setup
		var type_sf = AI_TOOLS.Behavior.ARRIVE
		if type == AI_TOOLS.Behavior.EVADE2:
			type_sf = AI_TOOLS.Behavior.LEAVE
		arrive_leave = ArriveLeave.new()
		arrive_leave.setup(max_acceleration_delegate, max_speed_delegate,
				target_radius_delegate, slow_radius_delegate, 
				time_to_target_delegate, p_damping, dims, type_sf)
		arrive_leave.name = name
		#
		character = Kinematic.new(dims)
		character.damping = p_damping
		target = Kinematic.new(dims)
		max_prediction = p_max_prediction
		# initialize private variables
		if dims == AI_TOOLS.Dimensions.THREE:
			_direction = Vector3.ZERO
		else:
			_direction = Vector2.ZERO
		_distance = 0.0
		_speed = 0.0
		_prediction = 0.0
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to seek
		# Work out the distance to target.
		_direction = target.position - character.position
		if type == AI_TOOLS.Behavior.EVADE2:
			_direction = -_direction
		_distance = _direction.length()
		# Work out our current speed.
		_speed = character.velocity.length()
		if _speed <= _distance / max_prediction:
			# Check if speed gives a reasonable prediction time.
			_prediction = max_prediction
			# send signal
			_send_signal("pursue_evade_2", "on_max_prediction", last_on_off_max_prediction)
		else:
			# Otherwise calculate the prediction time.
			_prediction = _distance / _speed
			# send signal
			_send_signal("pursue_evade_2", "off_max_prediction", last_on_off_max_prediction)
		# Put the target together.
		arrive_leave.target.position = target.position
		arrive_leave.target.velocity = target.velocity
		arrive_leave.target.position += target.velocity * _prediction
		# 2. Delegate to seek.
		return arrive_leave.get_steering()


class Face extends SteeringBehavior:
	@warning_ignore("unused_signal")
	signal face(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	var target: Kinematic
	# delegation
	# signal align
	var align: Align
	# private variables
	var _direction
	
	func _set_character(value):
		super(value)
		# override delegate's character
		align.character = value
	
	func _set_name(value):
		super(value)
		align.name = name
	
	func setup(max_angular_acceleration_delegate, 
			max_rotation_delegate, target_radius_delegate,
			slow_radius_delegate, time_to_target_delegate, 
			p_damping = 0.0, p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.FACE
		dims = p_dims
		# delegation setup
		align = Align.new()
		align.setup(max_angular_acceleration_delegate, max_rotation_delegate, 
				target_radius_delegate, slow_radius_delegate, 
				time_to_target_delegate, p_damping, dims)
		align.name = name
		#
		character = Kinematic.new(dims)
		character.damping = p_damping
		target = Kinematic.new(dims)
		# initialize private variables
		if dims == AI_TOOLS.Dimensions.THREE:
			_direction = Vector3.ZERO 
		else:
			_direction = Vector2.ZERO
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to align
		# Work out the distance to target.
		var last_direction = _direction
		_direction = target.position - character.position
		# Check for a zero direction, and make no change if so.
		if _direction == last_direction:
			# send signal
			_send_signal("face", "stop_steering", last_start_stop)
			return SteeringOutput.new(dims)
		# send signal
		_send_signal("face", "start_steering", last_start_stop)
		# Put the target together.
		if dims == AI_TOOLS.Dimensions.TWO:
			align.target.orientation = atan2(_direction.x, _direction.y)
		else:
			align.target.orientation = atan2(_direction.x, _direction.z)
		# 2. Delegate to seek.
		return align.get_steering()


class LookWhereYoureGoing extends SteeringBehavior:
	@warning_ignore("unused_signal")
	signal look_where_youre_going(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	var max_angular_acceleration: float
	var max_rotation: float
	# The radius for arriving at the target.
	var target_radius: float
	# The radius for beginning to slow down.
	var slow_radius: float
	# The time over which to achieve target speed.
	var time_to_target: float
	# delegation
	# signal align
	var align: Align
	
	func _set_character(value):
		super(value)
		# override delegate's character
		align.character = value
	
	func _set_name(value):
		super(value)
		align.name = name
	
	func setup(max_angular_acceleration_delegate, 
			max_rotation_delegate, target_radius_delegate,
			slow_radius_delegate, time_to_target_delegate, 
			p_damping = 0.0, p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.LOOK_WHERE_YOURE_GOING
		dims = p_dims
		# delegation setup
		align = Align.new()
		align.setup(max_angular_acceleration_delegate, max_rotation_delegate, 
				target_radius_delegate, slow_radius_delegate, 
				time_to_target_delegate, p_damping, dims)
		align.name = name
		#
		character = Kinematic.new(dims)
		character.damping = p_damping
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to align
		# Check for a zero direction, and make no change if so.
		var velocity = character.velocity
		if velocity.length() == 0.0:
			# send signal
			_send_signal("look_where_youre_going", "stop_steering", last_start_stop)
			return SteeringOutput.new(dims)
		# send signal
		_send_signal("face", "start_steering", last_start_stop)
		# Otherwise set the target based on the velocity.
		align.target.orientation = atan2(velocity.x, velocity.z)
		# 2. Delegate to seek.
		return align.get_steering()


class Wander extends SteeringBehavior:
	# The radius and forward offset of the wander circle.
	var wander_offset: float
	var wander_radius: float
	# The maximum rate at which the wander orientation can change.
	var wander_rate: float
	# The current orientation of the wander target.
	var wander_orientation: float
	# The maximum acceleration of the character.
	var max_acceleration: float
	# delegation
	# signal face, align
	var face: Face
	# private variables
	var _target_orientation
	var _target_orientation_as_vector
	var _character_orientation_as_vector
	
	func _set_character(value):
		super(value)
		# override delegate's character
		face.character = value
	
	func _set_name(value):
		super(value)
		face.name = name
	
	func setup(max_angular_acceleration_delegate,
			max_rotation_delegate, target_radius_delegate,
			slow_radius_delegate, time_to_target_delegate,
			p_wander_offset, p_wander_radius, p_wander_rate, 
			p_wander_orientation, p_max_acceleration, 
			p_damping = 0.0, _dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.WANDER
		dims = _dims
		# delegation setup
		face = Face.new()
		face.setup(max_angular_acceleration_delegate, max_rotation_delegate, 
				target_radius_delegate, slow_radius_delegate, 
				time_to_target_delegate, p_damping, dims)
		face.name = name
		#
		character = Kinematic.new(dims)
		character.damping = p_damping
		wander_offset = p_wander_offset
		wander_radius = p_wander_radius
		wander_rate = p_wander_rate
		wander_orientation = p_wander_orientation
		max_acceleration = p_max_acceleration
		# initialize private variables
		_target_orientation = 0.0
		if dims == AI_TOOLS.Dimensions.THREE:
			_target_orientation_as_vector = Vector3.ZERO 
		else:
			_target_orientation_as_vector = Vector2.ZERO
		_character_orientation_as_vector = _target_orientation_as_vector
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to face
		# Update the wander orientation.
		wander_orientation += AI_TOOLS.random_binomial() * wander_rate
		# Calculate the combined target orientation.
		_target_orientation = AI_TOOLS.map_to_range(wander_orientation + 
				character.orientation)

		# OPTIMIZATION!
		if dims == AI_TOOLS.Dimensions.TWO:
			_target_orientation_as_vector = AI_TOOLS.as_vector2(
					_target_orientation)
			_character_orientation_as_vector = AI_TOOLS.as_vector2(
					character.orientation)
		else:
			_target_orientation_as_vector = AI_TOOLS.as_vector3(
					_target_orientation)
			_character_orientation_as_vector = AI_TOOLS.as_vector3(
					character.orientation)
		
		# Calculate the center of the wander circle.
		face.target.position = (character.position + 
				_character_orientation_as_vector * wander_offset)
		# Calculate the target location.
		face.target.position += _target_orientation_as_vector * wander_radius
		# 2. Delegate to face.
		var result = face.get_steering()
		# 3. Now set the linear acceleration to be at full
		# acceleration in the direction of the orientation.
		result.linear = _character_orientation_as_vector * max_acceleration
		# Return it.
		return result


class PathAI extends RefCounted:
	# Note: path is supposed to lay in global space
	var path: Path3D
	var path_length: float
	var path_length_inv: float
	# coherence >0 (<0) -> param is monotonically increasing(decreasing)
	var coherence: float
	
	func _init(p_path):
		path = p_path
		path_length = path.curve.get_baked_length()
		path_length_inv = (1.0 / path.curve.get_baked_length())
	
	func get_param(position: Vector3, last_param: float) -> float:
		var param = path.curve.get_closest_offset(position) * path_length_inv
		if coherence > 0.0:
			if param < last_param:
				param = last_param
		if coherence < 0.0:
			if param > last_param:
				param = last_param
		return param
	
	func get_position(param: float) -> Vector3:
		return path.curve.interpolate_baked(param * path_length)


class FollowPath extends SteeringBehavior:
	# The path to follow
	var path: PathAI: set = _set_path
	# The distance along the path to generate the target. Can be
	# negative if the character is moving in the reverse direction.
	var path_offset: float: set = _set_path_offset
	# The time in the future to predict the character’s position.
	var predict_time: float
	# The current position on the path.
	var current_param: float
	# delegation
	var seek_flee: SeekFlee
	# private variables
	var _future_pos
	var _target_param
	
	func _set_character(value):
		super(value)
		# override delegate's character
		seek_flee.character = value
	
	func _set_name(value):
		super(value)
		seek_flee.name = name
	
	func _set_path(p_path:PathAI):
		path = p_path
		path.coherence = path_offset
	
	func _set_path_offset(p_path_offset:float):
		path_offset = clampf(p_path_offset, -1.0, 1.0)
		path.coherence = path_offset
	
	func setup(max_acceleration_delegate, p_path, p_path_offset,
			p_predict_time=0.1, p_damping = 0.0, 
			p_dims=AI_TOOLS.Dimensions.THREE, 
			p_type=AI_TOOLS.Behavior.FOLLOW_PATH):
		type = p_type
		dims = p_dims
		# delegation setup
		seek_flee = SeekFlee.new()
		seek_flee.setup(max_acceleration_delegate, p_damping, dims, 
				AI_TOOLS.Behavior.SEEK)
		seek_flee.name = name
		#
		character = Kinematic.new(dims)
		character.damping = p_damping
		path = p_path
		_set_path_offset(p_path_offset)
		current_param = 0.0 if path_offset >= 0.0 else 1.0
		predict_time = p_predict_time
		# initialize private variables
		if dims == AI_TOOLS.Dimensions.THREE:
			_future_pos = Vector3.ZERO 
		else:
			_future_pos = Vector2.ZERO
		_target_param = 0.0
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to seek.
		# Find the current position on the path.
		if dims == AI_TOOLS.Dimensions.TWO:
			if type == AI_TOOLS.Behavior.FOLLOW_PATH_PREDICTIVE:
				_future_pos = (Vector3(character.position.x, 0.0,
						character.position.y) +
						Vector3(character.velocity.x, 0.0,
						character.velocity.y) * predict_time)
			else:
				_future_pos = Vector3(character.position.x, 0.0,
					character.position.y)
			current_param = path.get_param(_future_pos, current_param)
		else:
			if type == AI_TOOLS.Behavior.FOLLOW_PATH_PREDICTIVE:
				_future_pos = (character.position +
						character.velocity * predict_time)
			else:
				_future_pos = character.position
			current_param = path.get_param(_future_pos, current_param)
		# Offset it.
		_target_param = current_param + path_offset
		# Get the target position.
		if dims == AI_TOOLS.Dimensions.TWO:
			var target_position = path.get_position(_target_param)
			seek_flee.target.position = Vector2(target_position.x,
					target_position.z)
		else:
			seek_flee.target.position = path.get_position(_target_param)
		# 2. Delegate to seek.
		return seek_flee.get_steering()


class SeparationAttraction extends SteeringBehavior:
	@warning_ignore("unused_signal")
	signal separation_attraction(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	# A list of potential targets.
	var targets: Array
	var max_acceleration: float
	# The (squared) threshold to take action.
	var threshold: float
	# The constant coefficient of decay for the inverse square law.
	var decay_coefficient: float
	# private variables
	var _steering_needed:bool
	
	func setup(p_max_acceleration, p_threshold, p_decay_coefficient,
			p_damping = 0.0, p_dims=AI_TOOLS.Dimensions.THREE,
			p_type=AI_TOOLS.Behavior.SEPARATION):
		type = p_type
		dims = p_dims
		character = Kinematic.new(dims)
		character.damping = p_damping
		targets = []
		max_acceleration = p_max_acceleration
		threshold = p_threshold
		decay_coefficient = p_decay_coefficient
		# initialize private variables
		_steering_needed = false
	
	func add_target(target):
		targets.append(target)
	
	func remove_target(target):
		targets.erase(target)
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Loop through each target.
		_steering_needed = false
		for target in targets:
			# Check if the target is close.
			var direction = target.position - character.position
			var distance = direction.length()
			if distance < threshold:
				_steering_needed = true
				# Calculate the strength of repulsion
				# (here using the inverse square law).
				var strength = min(decay_coefficient / (distance * distance), 
						max_acceleration)
				if type == AI_TOOLS.Behavior.ATTRACTION:
					strength = -strength
				# Add the acceleration.
				result.linear += strength * (-direction.normalized())
		result.angular = SteeringOutput.new(dims).angular
		if _steering_needed:
			# send signal
			_send_signal("separation_attraction", "start_steering", 
				last_start_stop)
		else:
			# send signal
			_send_signal("separation_attraction", "stop_steering", 
				last_start_stop)
		return result


class CollisionAvoidance extends SteeringBehavior:
	@warning_ignore("unused_signal")
	signal collision_avoidance(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	# A list of potential targets.
	var targets: Array
	var max_acceleration: float
	# The collision radius of a character (assuming all characters
	# have the same radius here).
	var radius: float
	var epsilon_2: float
	# private variables
	var _shortest_time
	var _first_target: Kinematic
	var _first_min_separation
	var _first_distance
	var _first_relative_pos
	var _first_relative_vel
	var _relative_pos
	
	func setup(p_max_acceleration, p_radius, p_epsilon = 0.1,
			p_damping = 0.0, p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.COLLISION_AVOIDANCE
		dims = p_dims
		character = Kinematic.new(dims)
		character.damping = p_damping
		targets = []
		max_acceleration = p_max_acceleration
		radius = p_radius
		epsilon_2 = p_epsilon ** 2
		# initialize private variables
		_shortest_time = 0.0
		_first_target = null
		_first_min_separation = 0.0
		_first_distance = 0.0
		if dims == AI_TOOLS.Dimensions.THREE:
			_first_relative_pos = Vector3.ZERO
		else:
			_first_relative_pos = Vector2.ZERO
		_first_relative_vel = _first_relative_pos
		_relative_pos = _first_relative_pos
	
	func add_target(target):
		targets.append(target)
	
	func remove_target(target):
		targets.erase(target)
	
	func get_steering() -> SteeringOutput:
		# 1. Find the target that’s closest to collision
		# Store the first collision time.
		_shortest_time = INF
		# Store the target that collides then, and other data that we
		# will need and can avoid recalculating.
		_first_target = null
		# Loop through each target.
		for target in targets:
			# Calculate the time to collision.
			var relative_pos = target.position - character.position
			var relative_vel = target.velocity - character.velocity
			var relative_speed_2 = relative_vel.dot(relative_vel)
			if abs(relative_speed_2) < epsilon_2:
				# corner case: both character and target are stopped
				# or they are moving with the same velocity
				continue
			var time_to_collision = -(relative_pos.dot(relative_vel) /
					relative_speed_2)
			# Check if it is going to be a collision at all.
			var min_separation = (relative_pos + 
					relative_vel * time_to_collision).length()
			if min_separation > 2 * radius:
				continue
			# Check if it is the shortest.
			if (time_to_collision > 0) and (time_to_collision < _shortest_time):
				# Store the time, target and other data.
				_shortest_time = time_to_collision
				_first_target = target
				_first_min_separation = min_separation
				_first_distance = relative_pos.length()
				_first_relative_pos = relative_pos
				_first_relative_vel = relative_vel
		# 2. Calculate the steering
		var result = SteeringOutput.new(dims)
		# If we have no target, then exit.
		if not _first_target:
			# send signal
			_send_signal("collision_avoidance", "stop_steering", 
					last_start_stop)
			return result
		# send signal
		_send_signal("collision_avoidance", "start_steering", 
				last_start_stop)
		# If we’re going to hit exactly, or if we’re already
		# colliding, then do the steering based on current position.
		if (_first_min_separation <= 0) or (_first_distance < 2 * radius):
			_relative_pos = _first_target.position - character.position
		# Otherwise calculate the future relative position.
		else:
			_relative_pos = (_first_relative_pos + 
					_first_relative_vel * _shortest_time)
		# Avoid the target.
		result.linear = (-_relative_pos.normalized()) * max_acceleration
		result.angular = SteeringOutput.new(dims).angular
		return result


class Collision extends RefCounted:
	var position: Vector3
	var normal: Vector3
	var body: CollisionObject3D
	
	func _init(p, n, b):
		position = p
		normal = n
		body = b


class CollisionDetector extends RefCounted:
	# NOTE: detector should be inside the scene tree
	var detector :Node3D
	var collision_mask: int: set = _set_collision_mask
	# private variables
	var _params: PhysicsRayQueryParameters3D
	var _result
	
	func _init(p_detector:Node3D):
		detector = p_detector
		# initialize private variables
		_params = PhysicsRayQueryParameters3D.new()
		_params.collision_mask = 0x7FFFFFFF # 0b01111111111111111111111111111111
		_result = {}
	
	func _set_collision_mask(value):
		collision_mask = value
		_params.collision_mask = value
	
	func get_collision(position: Vector3, move_amount: Vector3) -> Collision:
		# NOTE: should be during physics' tick (i.e. _physics_process)
		var space_state = detector.get_world_3d().direct_space_state
		# use global coordinates, not local to node
		_params.from = position
		_params.to = position + move_amount
		_result = space_state.intersect_ray(_params)
		if not _result:
			return null
		return Collision.new(_result["position"], _result["normal"], _result["collider"])


class ObstacleAvoidance extends SteeringBehavior:
	@warning_ignore("unused_signal")
	signal obstacle_avoidance(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	var detector: CollisionDetector
	var num_rays: int
	# The minimum distance to a wall (i.e., how far to avoid
	# collision) should be greater than the radius of the character.
	var avoid_distance: float
	# The distance to look ahead for a collision
	# (i.e., the length of the collision ray).
	var lookahead: float
	# whiskers' angle
	var whiskers_angle: float
	# delegation
	var seek_flee: SeekFlee
	# private variables
	var _ray
	var _ray1
	var _collision
	var _collision1
	
	func _set_character(value):
		super(value)
		# override delegate's character
		seek_flee.character = value
	
	func _set_name(value):
		super(value)
		seek_flee.name = name
	
	func setup(max_acceleration_delegate, p_detector, 
			p_avoid_distance, p_lookahead, p_num_rays=1, 
			p_whiskers_angle=deg_to_rad(15), p_damping = 0.0, 
			p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.OBSTACLE_AVOIDANCE
		dims = p_dims
		# delegation setup
		seek_flee = SeekFlee.new()
		seek_flee.setup(max_acceleration_delegate, p_damping, dims,
				AI_TOOLS.Behavior.SEEK)
		seek_flee.name = name
		#
		character = Kinematic.new(dims)
		character.damping = p_damping
		detector = p_detector
		avoid_distance = p_avoid_distance
		lookahead = p_lookahead
		num_rays = p_num_rays
		whiskers_angle = p_whiskers_angle
		# initialize private variables
		if dims == AI_TOOLS.Dimensions.THREE:
			_ray = Vector3.ZERO
		else:
			_ray = Vector2.ZERO
		_ray1 = _ray
		_collision = Collision.new(Vector3.ZERO, Vector3.ZERO, null)
		_collision1 = _collision
	
	func get_steering() -> SteeringOutput:
		if num_rays == 2:
			# 1. Calculate the target to delegate to seek
			# Calculate the collision ray vector.
			var _y
			var _z
			if (dims == AI_TOOLS.Dimensions.TWO):
				var velocity = Vector3(character.velocity.x, 0, 
						character.velocity.y)
				_z = velocity.normalized() * lookahead + velocity
				_y = Vector3.UP
			else:
				var velocity = character.velocity
				_z = velocity.normalized() * lookahead + velocity
				var _x = _z.cross(Vector3.UP)
				_y = _x.cross(_z).normalized()
			_ray = Basis(_y, -whiskers_angle) * (_z)
			_ray1 = Basis(_y, whiskers_angle) * (_z)
			# Find the collision.
			if dims == AI_TOOLS.Dimensions.TWO:
				_collision = detector.get_collision(
						Vector3(character.position.x, 0,character.position.y), 
						_ray)
				_collision1 = detector.get_collision(
						Vector3(character.position.x, 0,character.position.y), 
						_ray1)
			else:
				_collision = detector.get_collision(character.position, _ray)
				_collision1 = detector.get_collision(character.position, _ray1)
			# If have no collision, do nothing.
			if not (_collision or _collision1):
				# send signal
				_send_signal("obstacle_avoidance", "stop_steering", 
						last_start_stop)
				return SteeringOutput.new(dims)
			# 2. Otherwise create a target and delegate to seek.
			# send signal
			_send_signal("obstacle_avoidance", "start_steering", 
					last_start_stop)
			var position_r
			var normal_r
			var position_l
			var normal_l
			var position
			var normal
			if dims == AI_TOOLS.Dimensions.TWO:
				if _collision:
					position_r = Vector2(_collision.position.x, _collision.position.z)
					normal_r = Vector2(_collision.normal.x, _collision.normal.z)
				else:
					position_r = Vector2.ZERO
					normal_r = Vector2.ZERO
				if _collision1:
					position_l = Vector2(_collision1.position.x, _collision1.position.z)
					normal_l = Vector2(_collision1.normal.x, _collision1.normal.z)
				else:
					position_l = Vector2.ZERO
					normal_l = Vector2.ZERO
			else:
				if _collision:
					position_r = _collision.position
					normal_r = _collision.normal
				else:
					position_r = Vector3.ZERO
					normal_r = Vector3.ZERO
				if _collision1:
					position_l = _collision1.position
					normal_l = _collision1.normal
				else:
					position_l = Vector3.ZERO
					normal_l = Vector3.ZERO
			position = (position_r + position_l) * 0.5
			#normal = (normal_r + normal_l).normalized()
			normal = normal_r + normal_l # optimization
			seek_flee.target.position = (position + normal * avoid_distance)
		else:
			# only 1 ray by default
			if (dims == AI_TOOLS.Dimensions.TWO):
				var velocity = Vector3(character.velocity.x, 0, character.velocity.y)
				_ray = velocity.normalized() * lookahead + velocity
			else:
				var velocity = character.velocity
				_ray = velocity.normalized() * lookahead + velocity
			_ray1 = _ray
			if dims == AI_TOOLS.Dimensions.TWO:
				_collision = detector.get_collision(
						Vector3(character.position.x, 0,character.position.y), 
						_ray)
			else:
				# Find the collision.
				_collision = detector.get_collision(character.position, _ray)
			_collision1 = _collision
			# If have no collision, do nothing.
			if not _collision:
				# send signal
				_send_signal("obstacle_avoidance", "stop_steering", 
						last_start_stop)
				return SteeringOutput.new(dims)
			# 2. Otherwise create a target and delegate to seek.
			# send signal
			_send_signal("obstacle_avoidance", "start_steering", 
					last_start_stop)
			if dims == AI_TOOLS.Dimensions.TWO:
				seek_flee.target.position = (
						Vector2(_collision.position.x, _collision.position.z) + 
						Vector2(_collision.normal.x, _collision.normal.z) * 
								avoid_distance)
			else:
				seek_flee.target.position = (_collision.position + 
						_collision.normal * avoid_distance)
		#
		return seek_flee.get_steering()


class BlendedSteering extends SteeringBehavior:
	class BehaviorAndWeight extends RefCounted:
		var behavior: SteeringBehavior
		var weight: float
	
	var behaviors: Array = []: set = _set_behaviors
	# The overall maximum acceleration and rotation.
	var max_acceleration: float
	var max_rotation: float
	
	func _set_character(value):
		super(value)
		# override behaviors' characters
		_set_behaviors(behaviors)
	
	func _set_behaviors(p_behaviors:Array):
		behaviors = p_behaviors
		for bhv_wgth in p_behaviors:
			# override behavior's character
			bhv_wgth.behavior.character = character
	
	func setup(p_behaviors, p_max_acceleration, p_max_rotation,
			p_damping = 0.0, p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.BLENDED_STEERING
		dims = p_dims
		character = Kinematic.new(dims)
		character.damping = p_damping
		_set_behaviors(p_behaviors)
		max_acceleration = p_max_acceleration
		max_rotation = p_max_rotation
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Accumulate all accelerations.
		for b in behaviors:
			# result += b.weight * b.behavior.get_steering()
			result._iadd(b.behavior.get_steering()._mul(b.weight))
		# Crop the result and return.
		var linear_size = result.linear.length()
		result.linear = (min(linear_size, max_acceleration) * 
				result.linear.normalized())
		result.angular = clampf(result.angular, -max_rotation, max_rotation)
		return result


class PrioritySteering extends SteeringBehavior:
	# Holds a priority ordered list of BlendedSteering instances, which
	# in turn contain sets of behaviors with their blending weights.
	var groups: Array = []: set = _set_groups
	# A small value, effectively zero.
	var epsilon: float
	
	func _set_character(value):
		super(value)
		# override groups' characters
		_set_groups(groups)
	
	func _set_groups(value:Array):
		groups = value
		for group in value:
			# override group's character
			group.character = character
	
	func setup(p_groups, p_epsilon, p_damping = 0.0, 
			p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.PRIORITY_STEERING
		dims = p_dims
		character = Kinematic.new(dims)
		character.damping = p_damping
		_set_groups(p_groups)
		epsilon = p_epsilon
	
	func get_steering() -> SteeringOutput:
		var steering = SteeringOutput.new(dims)
		for group in groups:
			# Create the steering structure for accumulation.
			steering = group.get_steering()
			# Check if we’re above the threshold, if so return.
			if (steering.linear.length() > epsilon or
					abs(steering.angular) > epsilon):
				return steering
		# If we get here, it means that no group had a large enough
		# acceleration, so return the small acceleration from the
		# final group.
		return steering


class NavMapPath extends RefCounted:
	var path: PackedVector3Array
	var size: int
	
	func _init(_path:PackedVector3Array=PackedVector3Array()):
		path = _path
		size = _path.size()
	
	func get_point(idx:int) -> Vector3:
		var _idx = clampi(idx, 0, size - 1)
		return path[_idx]
	
	func get_segment(idx:int) -> AI_TOOLS.Segment:
		var _idx = clampi(idx, 0, size - 2)
		return AI_TOOLS.Segment.new(path[_idx], path[_idx + 1])


class Goal extends RefCounted:
	var dims = AI_TOOLS.Dimensions.THREE
	
	# Flags to indicate if each channel is to be used.
	var has_position: bool = false
	var has_orientation: bool = false
	var has_velocity: bool = false
	var has_rotation: bool = false
	
	# Data for each channel.
	var position
	var orientation: float
	var velocity
	var rotation: float
	
	func _init(p_dims=AI_TOOLS.Dimensions.THREE):
		dims = p_dims
		if dims == AI_TOOLS.Dimensions.TWO:
			position = Vector2.ZERO
			velocity = Vector2.ZERO
		else:
			position = Vector3.ZERO
			velocity = Vector3.ZERO
	
	func copy(other:Goal):
		has_position = other.has_position
		has_orientation = other.has_orientation
		has_velocity = other.has_velocity
		has_rotation = other.has_rotation
		position = other.position
		orientation = other.orientation
		velocity = other.velocity
		rotation = other.rotation
	
	# Updates this goal.
	func update_channels(other: Goal):
		if other.has_position:
			position = other.position
			has_position = true
		if other.has_orientation:
			orientation = other.orientation
			has_orientation = true
		if other.has_velocity:
			velocity = other.velocity
			has_velocity = true
		if other.has_rotation:
			rotation = other.rotation
			has_rotation = true


class Targeter extends RefCounted:
	var type_name: StringName
	var steering_pipeline: WeakRef
	# possible channels:
	# - position
	# - orientation
	# - velocity
	# - rotation
	var channel: String
	
	func _init(p_object:SteeringPipeline,
			p_channel:String):
		type_name = "Targeter"
		steering_pipeline = weakref(p_object)
		channel = p_channel
	
	func get_goal(_character:Kinematic) -> Goal:
		push_warning("Unimplemented get_goal")
		return Goal.new()


class Decomposer extends RefCounted:
	var type_name: StringName
	var steering_pipeline: WeakRef
	
	func _init(p_object:SteeringPipeline):
		type_name = "Decomposer"
		steering_pipeline = weakref(p_object)
	
	func decompose(_character:Kinematic, _goal:Goal) -> Goal:
		push_warning("Unimplemented decompose")
		return Goal.new()


class Constraint extends RefCounted:
	var type_name: StringName
	var steering_pipeline: WeakRef
	
	func _init(p_object:SteeringPipeline):
		type_name = "Constraint"
		steering_pipeline = weakref(p_object)
	
	func will_violate(_path:NavMapPath) -> bool:
		push_warning("Unimplemented will_violate")
		return false
	
	func suggest(_character:Kinematic, _path:NavMapPath, _goal:Goal) -> Goal:
		push_warning("Unimplemented suggest")
		return Goal.new()


class Actuator extends RefCounted:
	var type_name: StringName
	var steering_pipeline: WeakRef
	
	func _init(p_object:SteeringPipeline):
		type_name = "Actuator"
		steering_pipeline = weakref(p_object)
	
	func get_path(_character:Kinematic, _goal:Goal) -> NavMapPath:
		push_warning("Unimplemented get_path")
		return NavMapPath.new()
	
	func output(_character:Kinematic, _path:NavMapPath, _goal:Goal) -> SteeringOutput:
		push_warning("Unimplemented output")
		return SteeringOutput.new()


class SteeringPipeline extends SteeringBehavior:
	# Lists of components at each stage of the pipe.
	var targeters: Array = []
	var decomposers: Array = [] 
	var constraints: Array = []
	var actuator #: Actuator (cannot set type due to cyclic reference)
	# The number of attempts the algorithm will make to find an
	# unconstrained route.
	var constraint_steps: int
	# The deadlock steering behavior.
	var deadlock: SteeringBehavior: set = _set_deadlock
	# private variables
	var _goal
	var _original_goal
	var _path_is_valid
	var _last_constraint_violated
	
	func _set_character(value):
		super(value)
		# override deadlock's character
		deadlock.character = value
	
	func _set_deadlock(value:SteeringBehavior):
		deadlock = value
		deadlock.character = character
	
	func setup(p_constraint_steps, p_deadlock, p_damping = 0.0, 
			p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.STEERING_PIPELINE
		dims = p_dims
		constraint_steps = p_constraint_steps
		_set_deadlock(p_deadlock)
		character = Kinematic.new(dims)
		character.damping = p_damping
		# initialize private variables
		_goal = Goal.new(dims)
		_original_goal = Goal.new(dims)
		_path_is_valid = false
		_last_constraint_violated = null
	
	func get_steering() -> SteeringOutput:
		# Firstly we get the top level goal.
		for targeter in targeters:
			var targeter_goal = targeter.get_goal(character)
			_goal.update_channels(targeter_goal)
		_original_goal.copy(_goal)
		
		# Now we decompose it.
		for decomposer in decomposers:
			_goal = decomposer.decompose(character, _goal)
		
		# Now we loop through the actuation and constraint process.
		var actual_constraint_steps = max(constraints.size() + 1,
				constraint_steps)
		var violated_constraint_idx = -1
		_last_constraint_violated = null
		for i in actual_constraint_steps:
			# Get the path from the actuator.
			var path = actuator.get_path(character, _goal)
			# Check for constraint violation.
			var contraint_violated: bool = false
			for constraint_idx in constraints.size():
				# check if this constraint was violated in the previous step
				if constraint_idx == violated_constraint_idx:
					# This constraint was violated in the last step: no need
					# to check it, so move on to the next constraint
					continue
				var constraint = constraints[constraint_idx]
				# If we find a violation, get a suggestion.
				if constraint.will_violate(path):
					violated_constraint_idx = constraint_idx
					_last_constraint_violated = constraint
					_goal = constraint.suggest(character, path, _goal)
					
					# Go to the next iteration of the ’for i in ...’,
					# that is the next step and loop to try for the new goal.
					contraint_violated = true
					break
			if contraint_violated:
				continue
				
			# If we’re here it is because we found a valid path.
			_path_is_valid = true
			return actuator.output(character, path, _goal)
			
		# We arrive here if we ran out of constraint steps:
		# we don't have a valid path
		_path_is_valid = false
		# We delegate to the deadlock behavior.
		return deadlock.get_steering()


static func _behavior_factory(type:String, setup_args:Array, 
		kinematic=false) -> SteeringBehavior:
	var bhv: SteeringBehavior = null
	var type_name = type.to_upper()
	match type_name:
		"SEEK":
			if !kinematic:
				bhv = SeekFlee.new()
			else:
				bhv = KinematicSeekFlee.new()
			setup_args.append(AI_TOOLS.Behavior.SEEK)
		"FLEE":
			if !kinematic:
				bhv = SeekFlee.new()
			else:
				bhv = KinematicSeekFlee.new()
			setup_args.append(AI_TOOLS.Behavior.FLEE)
		"ARRIVE":
			if !kinematic:
				bhv = ArriveLeave.new()
			else:
				bhv = KinematicArriveLeave.new()
			setup_args.append(AI_TOOLS.Behavior.ARRIVE)
		"LEAVE":
			if !kinematic:
				bhv = ArriveLeave.new()
			else:
				bhv = KinematicArriveLeave.new()
			setup_args.append(AI_TOOLS.Behavior.LEAVE)
		"WANDER":
			if !kinematic:
				bhv = Wander.new()
			else:
				bhv = KinematicWander.new()
		"ALIGN":
			bhv = Align.new()
		"VELOCITY_MATCH":
			bhv = VelocityMatch.new()
		"PURSUE":
			bhv = PursueEvade.new()
			setup_args.append(AI_TOOLS.Behavior.PURSUE)
		"PURSUE2":
			bhv = PursueEvade2.new()
			setup_args.append(AI_TOOLS.Behavior.PURSUE2)
		"EVADE":
			bhv = PursueEvade.new()
			setup_args.append(AI_TOOLS.Behavior.EVADE)
		"EVADE2":
			bhv = PursueEvade2.new()
			setup_args.append(AI_TOOLS.Behavior.EVADE2)
		"FACE":
			bhv = Face.new()
		"LOOK_WHERE_YOURE_GOING":
			bhv = LookWhereYoureGoing.new()
		"FOLLOW_PATH":
			bhv = FollowPath.new()
			setup_args.append(AI_TOOLS.Behavior.FOLLOW_PATH)
		"FOLLOW_PATH_PREDICTIVE":
			bhv = FollowPath.new()
			setup_args.append(AI_TOOLS.Behavior.FOLLOW_PATH_PREDICTIVE)
		"SEPARATION":
			bhv = SeparationAttraction.new()
			setup_args.append(AI_TOOLS.Behavior.SEPARATION)
		"ATTRACTION":
			bhv = SeparationAttraction.new()
			setup_args.append(AI_TOOLS.Behavior.ATTRACTION)
		"COLLISION_AVOIDANCE":
			bhv = CollisionAvoidance.new()
		"OBSTACLE_AVOIDANCE":
			bhv = ObstacleAvoidance.new()
		_:
			bhv = null
	if bhv:
		# call setup
		bhv.callv("setup", setup_args)
	return bhv


class CompositeBehaviorFactory extends Resource:
	# object referenced in "pre update" functions
	var object:Node3D
	@warning_ignore("enum_variable_without_default")
	var dims:AI_TOOLS.Dimensions
	# member variables
	var behaviors_pre_update: Dictionary = {}
	
	func _init(p_object: Node3D, p_dims:AI_TOOLS.Dimensions):
		object = p_object
		dims = p_dims
	
	func _pre_update_pos(bhv, _delta, target:Node3D):
		# steering depends on: target.position
		# ARRIVE LEAVE SEEK FLEE
		if target:
			var p_position = target.global_transform.origin
			if dims == AI_TOOLS.Dimensions.TWO:
				bhv.target.position = Vector2(p_position.x, p_position.z)
			else:
				bhv.target.position = p_position
	
	func _pre_update_ori(bhv, _delta, target:Node3D):
		# steering depends on: target.orientation
		# ALIGN
		if target:
			bhv.target.orientation = AI_TOOLS.map_to_range(
					target.global_transform.basis.get_euler().y)
	
	func _pre_update_vel(bhv, _delta, target:Node3D):
		# steering depends on: target.velocity
		# VELOCITY_MATCH
		if target:
			var velocity = target.velocity
			if dims == AI_TOOLS.Dimensions.TWO:
				bhv.target.velocity = Vector2(velocity.x, velocity.z)
			else:
				bhv.target.velocity = velocity
	
	func _pre_update_char_vel(bhv, _delta, _target:Node3D):
		# steering depends on: character.velocity
		# LOOK_WHERE_YOURE_GOING
		bhv.character.velocity = object.velocity
	
	func _pre_update_pos_char_pos(bhv, _delta, target:Node3D):
		# steering depends on: target.position, character.position
		# FACE
		if target:
			var p_position = target.global_transform.origin
			var character_position = object.global_transform.origin
			if dims == AI_TOOLS.Dimensions.TWO:
				bhv.target.position = Vector2(p_position.x, p_position.z)
				bhv.character.position = Vector2(character_position.x,
						character_position.z)
			else:
				bhv.target.position = p_position
				bhv.character.position = character_position
	
	func _pre_update_pos_vel(bhv, _delta, target:Node3D):
		# steering depends on: target.position, target.velocity
		# PURSUE EVADE PURSUE2 EVADE2
		if target:
			var p_position = target.global_transform.origin
			var velocity = target.velocity
			if dims == AI_TOOLS.Dimensions.TWO:
				bhv.target.position = Vector2(p_position.x, p_position.z)
				bhv.target.velocity = Vector2(velocity.x, velocity.z)
			else:
				bhv.target.position = p_position
				bhv.target.velocity = velocity
	
	func _pre_update_pass(_bhv, _delta, _target:Node3D):
		# steering depends on: nothing
		# COLLISION_AVOIDANCE OBSTACLE_AVOIDANCE
		# FOLLOW_PATH FOLLOW_PATH_PREDICTIVE
		# SEPARATION ATTRACTION WANDER
		pass


class PrioritySteeringFactory extends CompositeBehaviorFactory:
	# member variables
	var behaviors_dict: Dictionary = {}
	var behavior_groups_dict: Dictionary = {}
	
	func _load_behaviors(json_file):
		return globals._load_and_parse_json(json_file)
	
	func _behavior_group_setup(group_name:String, behavior_names:Array,
			max_acceleration1, max_rotation1, damping1, dims1):
		print(group_name + " group construction...")
		var group = AI.BlendedSteering.new()
		var group_behaviors: Array = []
		for bhv_name in behavior_names:
			var bhv_name_lower = bhv_name.to_lower()
			#
			print("... " + bhv_name + " behavior construction...")
			var setup_args = []
			for arg in behaviors_dict[bhv_name_lower]["setup_args"]:
				var value = arg.values()[0]
				if (value is String) and (value == "null"):
					value = null
				setup_args.append(value)
			setup_args[-2] = damping1
			setup_args[-1] = dims1
			var behavior = AI._behavior_factory(bhv_name, setup_args)
			#
			print("... " + bhv_name + " setting name...")
			behavior.name = behaviors_dict[bhv_name_lower]["name"]
			#
			print("... " + bhv_name + " BehaviorAndWeight construction...")
			var bhv_wght = AI.BlendedSteering.BehaviorAndWeight.new()
			bhv_wght.behavior = behavior
			bhv_wght.weight = behaviors_dict[bhv_name_lower]["weight"]
			group_behaviors.append(bhv_wght)
			#
			print("... " + bhv_name + " pre_update function setting...")
			var pre_update_func:String = behaviors_dict[bhv_name_lower]["_pre_update"]
			behaviors_pre_update[bhv_wght] = Callable(self, pre_update_func)
			assert(behaviors_pre_update[bhv_wght] is Callable)
			#
			print("... " + bhv_name + " setting signals...")
			var _signal = behaviors_dict[bhv_name_lower]["signal"]
			if (_signal is String) and (not _signal.is_empty()):
				behavior.signal_enable = true
				behavior.connect(_signal, Callable(self, "_on_steering_behavior_signal"))
			else:
				behavior.signal_enable = false
		#
		print(group_name + " group final setup")
		group.setup(group_behaviors, max_acceleration1, max_rotation1, damping1, dims1)
		return group
	
	func create_and_setup(behaviors_file:String, behavior_groups_file:String,
			max_acceleration, max_rotation, damping, epsilon):
		behaviors_dict = _load_behaviors(behaviors_file)
		behavior_groups_dict = _load_behaviors(behavior_groups_file)
		# "behaviors pre update" dictionary is modified with each
		# creation and therefore must be cleared beforehand
		behaviors_pre_update.clear()
		# setup each behavior group in order
		var priority_group_list = []
		var behavior_groups = []
		for group_name in behavior_groups_dict:
			var behavior_names = behavior_groups_dict[group_name]["behavior_set"]
			var behavior_group = _behavior_group_setup(group_name, behavior_names,
					max_acceleration, max_rotation, damping, dims)
			# insert the behavior group into priority list
			var priority = behavior_groups_dict[group_name]["priority"]
			var pos = 0
			while pos < priority_group_list.size():
				if priority <= priority_group_list[pos]:
					break
				pos += 1
			priority_group_list.insert(pos, priority)
			behavior_groups.insert(pos, behavior_group)
		# create and setup
		var priority_steering = AI.PrioritySteering.new()
		priority_steering.setup(behavior_groups, epsilon, damping, dims)
		return priority_steering
