class_name AI_TOOLS
extends RefCounted


enum Dimensions {
	TWO = 2,
	THREE = 3,
}

enum Behavior {
	SEEK, #0
	FLEE, #1
	ARRIVE, #2
	LEAVE, #3
	WANDER, #4
	ALIGN, #5
	VELOCITY_MATCH, #6
	PURSUE, #7
	PURSUE2, #8
	EVADE, #9
	EVADE2, #10
	FACE, #11
	LOOK_WHERE_YOURE_GOING, #12
	FOLLOW_PATH, #13
	FOLLOW_PATH_PREDICTIVE, #14
	SEPARATION, #15
	ATTRACTION, #16
	COLLISION_AVOIDANCE, #17
	OBSTACLE_AVOIDANCE, #18
	BLENDED_STEERING, #19
	PRIORITY_STEERING, #20
	STEERING_PIPELINE, #21
	CUSTOM = 999, #999
}

const PI_2 = 2.0 * PI


static func new_orientation(current:float, velocity, dims=Dimensions.THREE) -> float:
	# Make sure we have a velocity.
	if velocity.length() > 0:
		# Calculate orientation from the velocity.
		if dims == Dimensions.TWO:
			return atan2(-velocity.x, velocity.y)
		else:
			return atan2(-velocity.x, velocity.z)
	# Otherwise use the current orientation.
	else:
		return current


static func as_vector3(orientation:float) -> Vector3:
	return Vector3(sin(orientation), 0.0, cos(orientation))


static func as_vector2(orientation:float) -> Vector2:
	return Vector2(sin(orientation), cos(orientation))


static func random_binomial() -> float:
	return randf() - randf()


static func map_to_range(radians) -> float:
	# map radians to [-PI, PI] range
	var result = fposmod(radians, PI_2)
	if result > PI:
		result = -PI_2 + result
	return result


# Geometric Tools
#Point = Vector3

class Line extends Resource:
	# Line = origin + t * direction
	var origin: Vector3
	var direction: Vector3


class Segment extends Line:
	# Segment = origin + t * (end - origin), 0 <= t <= 1
	var end: Vector3
	
	func _init(P0, P1):
		origin = P0
		end = P1
		direction = end - origin


static func point_line_distance_squared(q:Vector3, l:Line, 
		normalized:bool=false, t=[0.0]) -> float:
	var distance_squared: float
	t[0] = l.direction.dot(q - l.origin)
	if not normalized:
		t[0] = t[0]/l.direction.dot(l.direction)
	var q_prime = l.origin + t[0] * l.direction;
	var vec = q - q_prime
	distance_squared = vec.dot(vec)
	return distance_squared


static func point_segment_distance_squared(q:Vector3, s:Segment, 
		normalized:bool=false, t=[0.0]) -> float:
	var distance_squared: float
	# Get distance to line - may have t < 0 or t > 1
	distance_squared = point_line_distance_squared(q, s, normalized, t)
	if t[0] < 0.0:
		t[0] = 0.0
		# Get distance to segment origin instead
		var vec = q - s.origin
		distance_squared = vec.dot(vec)
	elif t[0] > 1.0:
		t[0] = 1.0
		# Get distance to segment terminus instead
		var vec = q - s.end
		distance_squared = vec.dot(vec)
	# return distance squared
	return distance_squared

static func point_segment_closest_point(s:Segment, q:Vector3, 
		normalized:bool=false) -> Vector3:
	# return the point on s closest to q
	var t = [0.0]
	point_segment_distance_squared(q, s, normalized, t)
	return s.origin + t[0] * s.direction
