extends Node3D


var dbg_lib: Script = preload("res://ai/debug_draw.gd")

@onready var drawable := ImmediateMesh.new()
@onready var material := StandardMaterial3D.new()
var from = Vector3.ZERO
var toy = from + Vector3(0, 10, 0)
var tox = from + Vector3(10, 0, 0)
var toz = from + Vector3(0, 0, 10)


func _ready():
	material.albedo_color = Color.BLUE
	var mesh_inst_tmp: MeshInstance3D = null
	mesh_inst_tmp = MeshInstance3D.new()
	mesh_inst_tmp.mesh = drawable
	add_child(mesh_inst_tmp)


func _process(delta):
	dbg_lib.debug_draw_strip(drawable, from, tox, 1.0, material)
	dbg_lib.debug_draw_strip(drawable, from, toy, 1.0, material)
	dbg_lib.debug_draw_strip(drawable, from, toz, 1.0, material)
