extends WorldEnvironment


@export var random_sky: bool = true

@onready var sky_material:PanoramaSkyMaterial = environment.sky.sky_material


func _ready():
	# set sky
	if random_sky:
		var skies = globals.GAME_DATA["skies"]
		var panorama_image = skies[skies.keys().pick_random()]
		sky_material.panorama = load(panorama_image)
