extends SubViewportContainer


@export var camera_size: float = 100.0: set = _set_camera_size
@export var camera_position: Vector2 = Vector2(0.0, 0.0)
@export var camera_rotation_y: float = 0.0
const MAX_COLOR_RECT_SIZE = Vector2(180.0, 180.0)
@export var color_rect_size: Vector2 = MAX_COLOR_RECT_SIZE


func _ready():
	# HACK: call setters with (potential) side effects
	camera_size = camera_size
	#
	$SubViewport/Camera3D.size = camera_size
	$SubViewport/Camera3D.position.x = camera_position.x
	$SubViewport/Camera3D.position.z = camera_position.y
	$SubViewport/Camera3D.rotation_degrees.y = camera_rotation_y
	$SubViewport/ColorRect.set_deferred("size", color_rect_size)
	#$Viewport/ColorRect.rect_position += (MAX_COLOR_RECT_SIZE - color_rect_size) / 2.0


func _set_camera_size(value):
	camera_size = value


func _physics_process(_delta):
	var vehicle_body = globals.current_vehicle_body
	if vehicle_body:
		# NOTE: here we're supposing that, for both camera and vehicle's body,
		# local and global transforms are identical
		$SubViewport/Camera3D.global_transform.origin.x = vehicle_body.global_transform.origin.x
		$SubViewport/Camera3D.global_transform.origin.z = vehicle_body.global_transform.origin.z
		$SubViewport/Camera3D.rotation.y = vehicle_body.rotation.y + camera_rotation_y
