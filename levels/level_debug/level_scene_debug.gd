class_name LevelSceneDebug
extends LevelScene


@export var city_textures: Array[Array] = []
@export var landscape_textures: Array[Array] = []

var environments: Array[String] = ["City", "Landscape", "Mixed"]


func _ready():
	prepare_debug_environment(weakref(self),
			globals.current_game_session % environments.size())
	super()


static func prepare_debug_environment(SELF:WeakRef, idx:int):
	@warning_ignore("shadowed_variable")
	var environments:Array = SELF.get_ref().environments
	@warning_ignore("shadowed_variable")
	var city_textures:Array = SELF.get_ref().city_textures
	@warning_ignore("shadowed_variable")
	var landscape_textures:Array = SELF.get_ref().landscape_textures
	# choose environment type
	var environment_type = environments[idx]
	# prepare environment
	var landscape:MeshInstance3D = SELF.get_ref().find_child("landscape_debug", true)
	var landscape_material:ShaderMaterial = landscape.get_surface_override_material(0)
	var params
	if environment_type == "City":
		city_textures.shuffle()
		params = [city_textures[0], city_textures[1], city_textures[2]]
		# cleanup environment
		for landscape_node in SELF.get_ref().get_tree().get_nodes_in_group("environment_landscape"):
			for child in landscape_node.get_children():
				child.queue_free()
	if environment_type == "Landscape":
		landscape_textures.shuffle()
		params = [landscape_textures[0], landscape_textures[1], landscape_textures[2]]
		# cleanup environment
		for city_node in SELF.get_ref().get_tree().get_nodes_in_group("environment_city"):
			for child in city_node.get_children():
				child.queue_free()
	if environment_type != "Mixed":
		# set landscape's textures
		landscape_material.set_shader_parameter("texture_albedo1", params[0][0])
		landscape_material.set_shader_parameter("texture_normal1", params[0][1])
		landscape_material.set_shader_parameter("texture_albedo2", params[1][0])
		landscape_material.set_shader_parameter("texture_normal2", params[1][1])
		landscape_material.set_shader_parameter("texture_albedo3", params[2][0])
		landscape_material.set_shader_parameter("texture_normal3", params[2][1])


func _get_class():
	return "LevelSceneDebug"


func _is_class(value):
	return "LevelSceneDebug" == value
