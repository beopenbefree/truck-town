#@tool
class_name MultimeshStaticObjectsPlacer
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export var object_total_num: int = 100

@export var object_types: Array[PackedScene] = []
@export var visual_only: bool = false
@export_flags_3d_physics var collision_layer: int = 0b1
@export_flags_3d_physics var collision_mask: int = 0b1
@export var object_collision_shape_scales: Array[Vector3] = []
@export_range(0, 1, 0.05) var object_max_delta_scale: float = 0.1
@export var physics_material: PhysicsMaterial = null

@export var placement_space_center: Vector3 = Vector3.ZERO
@export var placement_space_size: Vector3 = Vector3(400, 200, 400)

@export var placement_objects_names: Array[String] = []

# when specified only rays hitting this object will be considered
# for placements; its collision layer should be different
# from that of the static bodies of the placement objects.
# WARNING: vertical projection (along z-axis) of this mask object
# and those of the placement objects should overlap! (otherwise
# there will be an infinite loop of placement attempts)
@export var placement_mask_object_name: String = String()
@export_flags_3d_physics var placement_mask_object_collision_layer: int = 0b01000000000000000000000000000000

@export var save_scene: bool = false
@export_dir var save_scene_path: String = String()
@export var save_scene_name: String = String()

var ray_cast: RayCast3D = null

@onready var placements_finished: bool = false


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	if not enabled:
		return
	
	if object_types.size() < 1:
		print("Set the 'object_types' option!")
		return
	if placement_objects_names.size() <= 0:
		print("Set the 'placement_objects_names' option!")
		return
		
	# make sure placement_space_size has no negative component
	placement_space_size = placement_space_size.abs()
	if object_collision_shape_scales.size() < object_types.size():
		var delta_size = object_types.size() - object_collision_shape_scales.size()
		for i in delta_size:
			object_collision_shape_scales.append(Vector3.ONE)
	print(get_child_count())
	ray_cast = RayCast3D.new()
	ray_cast.collision_mask = 0xffffffff
	add_child(ray_cast)
	# reduce number of objects on "low end device"
	if globals.current_configuration_options["low_end_device"][0]:
		object_total_num = int(object_total_num * 
				globals.LOW_END_DEVICE_REDUCTION_FACTOR)
	# create MeshInstance3D children
	if object_total_num > 0:
		_create_additional_children(object_total_num)
	else:
		return
	
	# create multimesh objects from children by material
	ScriptLibrary.create_multimesh_objects_from_children_by_material(
		self, object_types, visual_only, object_max_delta_scale,
		collision_layer, collision_mask, object_collision_shape_scales,
		physics_material, ray_cast, 10.0 * placement_space_size.y,
		save_scene, save_scene_name, save_scene_path)
	
	# cleanup
	ray_cast.queue_free()
	# set ready is finished
	placements_finished = true


func _create_additional_children(children_num):
	var total_attempts = 0
	var child_count = 0
	var dummy_meshes = []
	for i in object_types.size():
		dummy_meshes.append(_create_dummy_mesh())
	
	# check if mask object is specified
	var ray_cast_object_mask: RayCast3D = null
	if not placement_mask_object_name.is_empty():
		ray_cast_object_mask = RayCast3D.new()
		add_child(ray_cast_object_mask)
		# prepare a ray that hits only the mask_object
		ray_cast_object_mask.collision_mask = placement_mask_object_collision_layer
	
	# start placements
	var space_half_size = placement_space_size / 2.0
	var space_center = placement_space_center
	while child_count < children_num:
		total_attempts += 1
		var pos_x = randf_range(space_center.x - space_half_size.x,
				space_center.x + space_half_size.x)
		var pos_y = space_half_size.y + space_center.y
		var pos_z = randf_range(space_center.z - space_half_size.z,
				space_center.z + space_half_size.z)
		
		# check if mask object is specified
		if ray_cast_object_mask:
			# cast ray vertically to hit mask object
			ray_cast_object_mask.global_transform.origin = Vector3(pos_x, pos_y, pos_z)
			ray_cast_object_mask.target_position = Vector3(0.0, -placement_space_size.y, 0.0)
			ray_cast_object_mask.force_raycast_update()
			if not ray_cast_object_mask.is_colliding():
				# mask object not hit go to next attempt
				continue
		
		# cast ray vertically
		ray_cast.global_transform.origin = Vector3(pos_x, pos_y, pos_z)
		ray_cast.target_position = Vector3(0.0, -placement_space_size.y, 0.0)
		ray_cast.force_raycast_update()
		if ray_cast.is_colliding():
			# get the mesh instance associated to the collider:
			# it is the parent or one of the child of it
			var collider = ray_cast.get_collider()
			var hit_object_name = String()
			var parent = collider.get_parent()
			if parent is MeshInstance3D:
				hit_object_name = parent.name
			else:
				var child = ScriptLibrary.find_nodes_by_type(MeshInstance3D,
						collider, false)[0]
				if child:
					hit_object_name = child.name
				#else:
					#print("Collider '%s' has no associated MeshInstance3D!" %
							#ray_cast.get_collider().name)
			if hit_object_name in placement_objects_names:
				var mesh_instance:MeshInstance3D = MeshInstance3D.new() 
				var dummy_mesh = dummy_meshes[randi() % object_types.size()]
				mesh_instance.mesh = dummy_mesh
				add_child(mesh_instance)
				var b = Basis.IDENTITY
				var o = ray_cast.get_collision_point()
				# set object transform
				mesh_instance.set_global_transform(Transform3D(b, o))
				child_count += 1
	#
	print("placements/attempts = ", children_num, "/", total_attempts, 
			" (", float(children_num)/float(total_attempts), ")" )
	# cleanup
	if ray_cast_object_mask:
		ray_cast_object_mask.queue_free()


func _create_dummy_mesh():
	var vertices = PackedVector3Array()
	var UVs = PackedVector2Array()
	var mat = StandardMaterial3D.new()
	var color = Color(0.9, 0.1, 0.1)
	vertices.push_back(Vector3(0,0,1))
	vertices.push_back(Vector3(0,0,0))
	vertices.push_back(Vector3(1,0,1))
	vertices.push_back(Vector3(1,0,0))
	UVs.push_back(Vector2(0,0))
	UVs.push_back(Vector2(0,1))
	UVs.push_back(Vector2(1,1))
	UVs.push_back(Vector2(1,0))
	mat.albedo_color = color
	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
	st.set_material(mat)
	for v in vertices.size(): 
		st.set_color(color)
		st.set_uv(UVs[v])
		st.add_vertex(vertices[v])
	return st.commit()


func _set_enabled(value):
	enabled = value
