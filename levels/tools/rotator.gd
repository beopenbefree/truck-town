class_name Rotator
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export var use_pitch_curve_sets: bool = false
@export_file("*.json") var pitch_curve_sets_file: String = String()
@export var initial_curve_set_name: String = String()

# yaw (left/right) 
@export_range(0.0, 90.0, 1.0, "radians_as_degrees") var max_right_yaw: float = deg_to_rad(30.0)
@export_range(0.0, 90.0, 1.0, "radians_as_degrees") var max_left_yaw: float = deg_to_rad(30.0)
@export_range(0.05, 10.0, 0.05) var yaw_speed: float = 1.5
# pitch (up/down)
@export var max_up_pitch_right_curve: Curve = null
@export var max_up_pitch_left_curve: Curve = null
@export var max_down_pitch_right_curve: Curve = null
@export var max_down_pitch_left_curve: Curve = null

@export_range(0.05, 10.0, 0.05) var pitch_speed: float = 1.5
#
@export_range(0.01, 5.0, 0.01) var mouse_sensitivity: float = 0.8: 
	set = _set_mouse_sensitivity

var pitch_curves_dict: Dictionary = {}

const PI_2 = PI * 0.5
var touch_rotating_direction: Vector2 = Vector2.ZERO
var touch_touching: bool = false

var mouse_delta:Vector2 = Vector2.ZERO


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	mouse_sensitivity = mouse_sensitivity
	# create the rotator pitch curves dict
	if use_pitch_curve_sets:
		_create_pitch_curve_set_dict()
		set_pitch_curve_set(initial_curve_set_name)


func _physics_process(delta):
	process_rotate(delta)


func _input(event):
	if event is InputEventMouseMotion:
		mouse_delta = event.relative
		get_viewport().set_input_as_handled()


func process_rotate(delta):
	var left = Input.is_action_pressed("fp_camera_turn_left")
	var right = Input.is_action_pressed("fp_camera_turn_right")
	var up = Input.is_action_pressed("fp_camera_turn_up")
	var down = Input.is_action_pressed("fp_camera_turn_down")
	# check if aiming is requested
	var yaw:float = 0.0
	var pitch:float = 0.0
	if left or right or up or down:
		# keyboard & joystick
		yaw += Input.get_action_strength("fp_camera_turn_left") - Input.get_action_strength("fp_camera_turn_right")
		pitch += Input.get_action_strength("fp_camera_turn_down") - Input.get_action_strength("fp_camera_turn_up")
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		# mouse
		yaw += -mouse_delta.x * mouse_sensitivity
		pitch += mouse_delta.y * mouse_sensitivity
		mouse_delta = Vector2.ZERO
	if touch_touching:
		# virtual analog & mouse
		yaw += -touch_rotating_direction.x
		pitch += touch_rotating_direction.y
	yaw *= yaw_speed * delta
	pitch *= pitch_speed * delta
	# clamp and set the current yaw (left/right) value
	var curr_yaw = clampf(rotation.y + yaw, -max_right_yaw, max_left_yaw)
	# get the max pitch (up/down) rotation based on the yaw
	# (rotation.y) value and clamp the current pitch value
	var max_up_pitch:float
	var max_down_pitch:float
	if curr_yaw <= 0.0:
		var yaw_factor = curr_yaw / -max_right_yaw
		# right
		max_up_pitch = max_up_pitch_right_curve.sample(yaw_factor) * PI_2
		max_down_pitch = max_down_pitch_right_curve.sample(yaw_factor) * PI_2
	else:
		var yaw_factor = curr_yaw / max_left_yaw
		# left
		max_up_pitch = max_up_pitch_left_curve.sample(yaw_factor) * PI_2
		max_down_pitch = max_down_pitch_left_curve.sample(yaw_factor) * PI_2
	var curr_pitch = clampf(rotation.x + pitch, -max_up_pitch,
			max_down_pitch)
	# finally update the rotation
	rotation.y = curr_yaw
	rotation.x = curr_pitch


func set_pitch_curve_set(curve_set_name:String):
	if use_pitch_curve_sets:
		var curve_set = pitch_curves_dict[curve_set_name]
		for curve_name in curve_set:
			var curve:Curve = curve_set[curve_name]
			set(curve_name, curve)


func _create_pitch_curve_set_dict():
	if use_pitch_curve_sets:
		var loaded_dict = globals._load_and_parse_json(
				pitch_curve_sets_file)
		# load the defined curves
		var curves = []
		for curve_file in loaded_dict["CURVES"]:
			var curve:Curve = load(curve_file)
			curves.append(curve)
		pitch_curves_dict["CURVES"] = curves
		# associate the curves
		for curve_set_name in loaded_dict:
			if curve_set_name == "CURVES":
				continue
			var curve_set = {}
			for curve_name in loaded_dict[curve_set_name]:
				# get the curve resource
				var curve_idx = loaded_dict[curve_set_name][curve_name]
				var curve:Curve = pitch_curves_dict["CURVES"][curve_idx]
				curve_set[curve_name] = curve
			pitch_curves_dict[curve_set_name] = curve_set


func _set_mouse_sensitivity(value):
	mouse_sensitivity = value * 0.1


func _get_class():
	return "Rotator"


func _is_class(value):
	return "Rotator" == value


func _set_enabled(value):
	enabled = value
	set_process_input(value)
	set_physics_process(value)
