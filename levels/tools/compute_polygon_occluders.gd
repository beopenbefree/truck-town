#@tool
extends Node3D


@export_dir var scene_dir_path: String = String()
@export var normals: Array = [
	Vector3.RIGHT,
	-Vector3.RIGHT,
	Vector3.UP,
	-Vector3.UP,
	Vector3.FORWARD,
	-Vector3.FORWARD,
]
@export var scaling: Vector3 = Vector3.ONE

@onready var scene_name: String = scene_dir_path + "/" + name + "_occluders_node.tscn"


func _ready():
	var occluder_node = Node3D.new()
	add_child(occluder_node)
	occluder_node.name = name + "Occluders"
	for child in get_children():			
		for o in ScriptLibrary.occluders_get(child, normals, scaling):
			occluder_node.add_child(o)
			o.set_owner(occluder_node)
	var packed_scene = PackedScene.new()
	packed_scene.pack(occluder_node)
	ResourceSaver.save(packed_scene, scene_name)
	occluder_node.queue_free()
