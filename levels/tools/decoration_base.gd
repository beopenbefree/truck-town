class_name DecorationBase
extends Decal


@export var use_visual_mask: bool = true

var parent_visual_instance: bool = false
var visual_mask_first_addition: bool = false

@onready var parent: Node3D = get_parent()


func _ready():
	# decoration works with the visual mask only when its
	# parent is a VisualInstance3D (and when it is enabled)
	parent_visual_instance = (parent is VisualInstance3D)
	if use_visual_mask and parent_visual_instance:
		# apply visual mask only on visual instances
		var visual_instance_layers = parent.layers
		if not (visual_instance_layers & cull_mask):
			# no one added the visual mask: let's add it first
			visual_instance_layers |= cull_mask
			parent.layers = visual_instance_layers
			visual_mask_first_addition = true
	else:
		# visual mask are not used so this decoration
		# should be visible on all layers
		cull_mask = 0xffffffff


func _exit_tree():
	if (use_visual_mask and parent_visual_instance and 
			is_instance_valid(parent) and visual_mask_first_addition):
		# we were the first to add the visual mask: let's remove it
		var visual_instance_layers = parent.layers
		visual_instance_layers &= ~cull_mask
		parent.layers = visual_instance_layers


func _get_class():
	return "DecorationBase"


func _is_class(value):
	return "DecorationBase" == value
