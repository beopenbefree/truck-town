#@tool
class_name MultimeshLandscapeActorsPlacer
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export var object_types: Array[PackedScene] = []
@export var visual_only: bool = false
@export var object_collision_shape_scales: Array[Vector3] = []
@export_flags_3d_physics var collision_layer: int = 0b1
@export_flags_3d_physics var collision_mask: int = 0b1
@export_range(0,1,0.05) var object_max_delta_scale: float = 0.5
@export var physics_material: PhysicsMaterial = null

@export var save_scene: bool = false
@export_dir var save_scene_path: String = String()
@export var save_scene_name: String = String()

var ray_cast: RayCast3D = null


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	if not enabled:
		return
	
	if object_types.size() < 1:
		return
	if object_collision_shape_scales.size() < object_types.size():
		var delta_size = object_types.size() - object_collision_shape_scales.size()
		for i in delta_size:
			object_collision_shape_scales.append(Vector3.ONE)
	print(get_child_count())
	ray_cast = RayCast3D.new()
	add_child(ray_cast)
	
	# create multimesh objects from children by material
	ScriptLibrary.create_multimesh_objects_from_children_by_material(
		self, object_types, visual_only, object_max_delta_scale,
		collision_layer, collision_mask, object_collision_shape_scales, 
		physics_material, ray_cast, 10000,
		save_scene, save_scene_name, save_scene_path)


func _set_enabled(value):
	enabled = value
