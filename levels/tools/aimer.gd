class_name Aimer
extends Node3D


@export var enabled: bool = true: set = _set_enabled

enum GeometryOrientation {FORWARD, BACKWARD}
@export var geometry_orientation: GeometryOrientation = GeometryOrientation.FORWARD

@export_range(0.0, 20.0, 0.001, "or_greater") var speed_factor: float = 1.0
# HACK: to slow down extremely, lower this value 
@export_range(0.0, 1.0, 0.001) var min_weight: float = 0.05

@export_enum("ORBITING_QUAT", "ORBITING_BASIS", "FREEFLY") var aimer_type: String = "ORBITING_QUAT"
@export_range(0.0, 180.0, 1.0) var rotating_max_yaw_angle: float = 180.0

# the scene's root node containing the aimer
@export_node_path("Node3D") var root_node_path = NodePath(".")

var target_node:Node3D = null
# HACK: to avoid floating point precision errors
const LIMIT_YAW_ANGLE: float = deg_to_rad(179.0)
var yaw_rotation_corrected: bool = false

@onready var root_node: Node3D = get_node(root_node_path)
@onready var global_scale_basis := Basis.from_scale(
		global_transform.basis.get_scale())
@onready var rotating_max_yaw_angle_rad: float = deg_to_rad(rotating_max_yaw_angle)
@onready var rotating_angle_min = (
		rotation.y - rotating_max_yaw_angle_rad)
@onready var rotating_angle_max = (
		rotation.y + rotating_max_yaw_angle_rad)
# HACK: enable quaternion/basis normalization if needed
@onready var normalize: bool = true if min_weight < 0.05 else false


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled


func _physics_process(delta):
	# NOTE: all computations are in global space;
	# NOTE: Node3d's quaternion property are (only) in local space
	if target_node:
		# get the target dir
		var target_dir:Vector3 = (target_node.global_transform.origin - 
				global_transform.origin).normalized()
		if aimer_type == "ORBITING_QUAT":
			# Vector3.UP preferred UP direction
			_compute_orbiting_quat(delta, target_dir,
					geometry_orientation)
		elif aimer_type == "ORBITING_BASIS":
			# Vector3.UP preferred UP direction
			_compute_orbiting_basis(delta, target_dir,
					geometry_orientation)
		elif aimer_type == "FREEFLY":
			# No preferred UP direction
			_compute_freefly(delta, target_dir,
					geometry_orientation)


func _compute_freefly(delta, target_dir,
		geom_orient:=GeometryOrientation.FORWARD):
	var curr_f_dir
	if geom_orient == GeometryOrientation.FORWARD:
		#var curr_f_dir = -global_transform.basis.z
		curr_f_dir = Vector3.FORWARD
	else:
		# geom_orient == GeometryOrientation.BACKWARD:
		#var curr_f_dir = global_transform.basis.z
		curr_f_dir = Vector3.BACK
	# compute normalized (ie between versors) dot product
	var target_dot = curr_f_dir.dot(target_dir)
	# check if versors are not (approximately) parallel
	if not is_equal_approx(target_dot, 1.0):
		# versors are not parallel: compute angle & axis of rotation
		var target_axis:Vector3 
		var target_angle = acos(target_dot)
		if geom_orient==GeometryOrientation.FORWARD:
				# NOTE: geometry oriented toward FORWARD (-Z axis)
				target_axis = Vector3.FORWARD.cross(target_dir).normalized()
		else: 
			# NOTE: geometry oriented toward BAKWARD (Z axis)
			target_axis = Vector3.BACK.cross(target_dir).normalized()
		# compute new rotation
		# get current global quaternion
		var curr_quat = global_transform.basis.get_rotation_quaternion()
		# update current quaternion
		var target_quat = Quaternion(target_axis, target_angle)
#		var weight = abs(PI - target_angle) / PI * delta * speed_factor
		var weight = (1.0 - target_dot) * delta * speed_factor
		weight = clampf(weight, min_weight, 1.0)
		# interpolate current wrt target quaternion
		if normalize:
			curr_quat = curr_quat.normalized()
		curr_quat = curr_quat.slerp(target_quat, weight)
		global_transform.basis = global_scale_basis * Basis(curr_quat)
		_check_max_yaw_rotation_angle()


func _compute_orbiting_basis(delta, target_dir,
		geom_orient:=GeometryOrientation.FORWARD):
	var curr_f_dir
	if geom_orient == GeometryOrientation.FORWARD:
		curr_f_dir = -global_transform.basis.z
	else:
		# geom_orient == GeometryOrientation.BACKWARD:
		curr_f_dir = global_transform.basis.z
	# compute normalized (ie between versors) dot product
	var target_dot = target_dir.dot(curr_f_dir)
	# check if versors are not (approximately) parallel
	if not is_equal_approx(target_dot, 1.0):
		# versors are not parallel: compute angle & axis of rotation
		var f_dir
		if geom_orient == GeometryOrientation.FORWARD:
			# NOTE: geometry oriented toward FORWARD (-Z axis)
			f_dir = -target_dir
		else: 
			# NOTE: geometry oriented toward BACKWARD (Z axis)
			f_dir = target_dir
		var r_dir = Vector3.UP.cross(f_dir).normalized()
		var u_dir = f_dir.cross(r_dir)#.normalized()
		# compute new rotation
		# get current global pure rotational basis
		var curr_basis = Basis(
				global_transform.basis.get_rotation_quaternion())
		# update current basis
		var target_basis = Basis(r_dir, u_dir, f_dir)
		var weight = (1.0 - target_dot) * delta * speed_factor
		weight = clampf(weight, min_weight, 1.0)
		# interpolate current wrt target basis
		if normalize:
			curr_basis = curr_basis.orthonormalized()
		curr_basis = curr_basis.slerp(target_basis, weight)
		global_transform.basis = global_scale_basis * curr_basis
		_check_max_yaw_rotation_angle()


func _compute_orbiting_quat(delta, target_dir,
		geom_orient:=GeometryOrientation.FORWARD):
	var curr_f_dir
	if geom_orient == GeometryOrientation.FORWARD:
		curr_f_dir = -global_transform.basis.z
	else:
		# geom_orient == GeometryOrientation.BACKWARD:
		curr_f_dir = global_transform.basis.z
	# compute normalized (ie between versors) dot product
	var target_dot = target_dir.dot(curr_f_dir)
	# check if versors are not (approximately) parallel
	if not is_equal_approx(target_dot, 1.0):
		# versors are not parallel: compute angle & axis of rotation
		var f_dir
		if geom_orient == GeometryOrientation.FORWARD:
			# NOTE: geometry oriented toward FORWARD (-Z axis)
			f_dir = -target_dir
		else: 
			# NOTE: geometry oriented toward BACKWARD (Z axis)
			f_dir = target_dir
		var r_dir = Vector3.UP.cross(f_dir).normalized()
		var u_dir = f_dir.cross(r_dir)#.normalized()
		# compute new rotation
		# get current global quaternion
		var curr_quat = global_transform.basis.get_rotation_quaternion()
		# update current quaternion
		var target_quat = Basis(r_dir, u_dir, f_dir).get_rotation_quaternion()
		var weight = (1.0 - target_dot) * delta * speed_factor
		weight = clampf(weight, min_weight, 1.0)
		# interpolate current wrt target basis
		if normalize:
			curr_quat = curr_quat.normalized()
		curr_quat = curr_quat.slerp(target_quat, weight)
		global_transform.basis = global_scale_basis * Basis(curr_quat)
		_check_max_yaw_rotation_angle()


func _check_max_yaw_rotation_angle():
	# HACK: to avid floating point precision errors
	# perform the check only when max yaw angle < PI
	if rotating_max_yaw_angle_rad <= LIMIT_YAW_ANGLE:
		yaw_rotation_corrected = false
		if rotation.y < rotating_angle_min:
			# revert rotation yaw
			rotation.y = rotating_angle_min
			yaw_rotation_corrected = true
		elif rotation.y > rotating_angle_max:
			# revert rotation yaw
			rotation.y = rotating_angle_max
			yaw_rotation_corrected = true


func _get_class():
	return "Aimer"


func _is_class(value):
	return "Aimer" == value


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)
