extends MeshInstance3D

signal fading_done


@export_range(0.1, 15.0) var fade_time: float = 5.0
@export_range(0.1, 10.0) var fade_flickering_frequency: float = 4.0

const FADE_ALBEDO = Color.WHITE
var fade_alpha: float = 1.0
var fade_elapsed_time: float = 0.0

@onready var fade_speed: float = 1.0 / fade_time
@onready var fade_phase_speed: float = 2 * PI * fade_flickering_frequency


func _ready():
	set_physics_process(false)


func fade():
	_clone_surface_materials()
	set_physics_process(true)


func _physics_process(delta):
	fade_elapsed_time += delta
	fade_alpha -= fade_speed * delta
	if fade_alpha < 0.0:
		set_physics_process(false)
		fading_done.emit()
	# perform fade logic
	var albedo = FADE_ALBEDO * (1.0 + sin(fade_phase_speed * fade_elapsed_time))
	albedo.a = fade_alpha * fade_alpha
	for i in get_surface_override_material_count():
		get_surface_override_material(i).albedo_color = albedo


func _clone_surface_materials():
	for i in mesh.get_surface_count():
		var material_orig:BaseMaterial3D = mesh.surface_get_material(i)
		if material_orig:
			var material:BaseMaterial3D = material_orig.duplicate(true)
			material.transparency = BaseMaterial3D.TRANSPARENCY_ALPHA
			material.vertex_color_use_as_albedo = false
			set_surface_override_material(i, material)
