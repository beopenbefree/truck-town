extends NavigationRegion3D


signal placements_finished_signal

var dynamic_placement_nodes: Array = []

@onready var placements_finished: bool = false


func _ready():
	dynamic_placement_nodes = ScriptLibrary.find_nodes_by_type(
			MultimeshStaticObjectsPlacer, self, true)
	dynamic_placement_nodes += ScriptLibrary.find_nodes_by_type(
			ObjectsPlacer, self, true)
	set_physics_process(true)


func _physics_process(_delta):
	for dpn in dynamic_placement_nodes:
		if is_instance_valid(dpn) and dpn.enabled:
			if not dpn.placements_finished:
				return
	# all placements are finished
	placements_finished = true
	placements_finished_signal.emit()
	set_physics_process(false)
