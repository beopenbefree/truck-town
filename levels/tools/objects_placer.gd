#@tool
class_name ObjectsPlacer
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export var objects_number: int = 10

@export var object_scene: PackedScene = null

@export_node_path("Node3D") var container_node_path = NodePath()

@export var placement_space_center: Vector3 = Vector3.ZERO
@export var placement_space_size: Vector3 = Vector3(400, 200, 400)

@export var placement_objects_names: Array[String] = [] 

# object that acts as a placement mask: when specified only rays
# hitting this object (and the placement_objects) will be
# considered for placements; its collision layer should be
# different from that of the static bodies of the placement objects.
# WARNING: vertical projection (along z-axis) of this mask object
# and those of the placement objects should overlap! (otherwise
# there will be an infinite loop of placement attempts)
@export var placement_mask_object_name: String = String()
# by default the collisione layer is the 31st
@export_flags_3d_physics var placement_mask_object_collision_layer: int = 0b01000000000000000000000000000000

@export var delta_pos: Vector3 = Vector3.ZERO
@export_range(0, 1, 0.01) var max_delta_scale: float = 0.0

@export var save_scene: bool = false
@export_dir var save_scene_path: String = String()
@export var save_scene_name: String = String()

@onready var placements_finished: bool = false


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	if not enabled:
		return
	
	if placement_objects_names.size() <= 0:
		print("Set the 'placement_objects_names' option!")
		return
	#
	var ray_cast = RayCast3D.new()
	ray_cast.collision_mask = 0xffffffff
	add_child(ray_cast)
	var container_node
	if container_node_path.is_empty():
		container_node = self
	else:
		container_node = get_node(container_node_path)
	var total_attempts = 0
	var object_count = 0
	
	# check if mask object is specified
	var ray_cast_object_mask: RayCast3D = null
	if not placement_mask_object_name.is_empty():
		ray_cast_object_mask = RayCast3D.new()
		add_child(ray_cast_object_mask)
		# prepare a ray that hits only the mask_object
		ray_cast_object_mask.collision_mask = placement_mask_object_collision_layer
	
	# start placements
	var space_half_size = placement_space_size / 2.0
	var space_center = placement_space_center
	while object_count < objects_number:
		total_attempts += 1
		var pos_x = randf_range(space_center.x - space_half_size.x, 
				space_center.x + space_half_size.x)
		var pos_y = space_half_size.y + space_center.y
		var pos_z = randf_range(space_center.z - space_half_size.z,
				space_center.z + space_half_size.z)
		
		# check if mask object is specified
		if ray_cast_object_mask:
			# cast ray vertically to hit mask object
			ray_cast_object_mask.global_transform.origin = Vector3(pos_x, pos_y, pos_z)
			ray_cast_object_mask.target_position = Vector3(0.0, -placement_space_size.y, 0.0)
			ray_cast_object_mask.force_raycast_update()
			if not ray_cast_object_mask.is_colliding():
				# mask object not hit go to next attempt
				continue
		
		# cast ray vertically
		ray_cast.global_transform.origin = Vector3(pos_x, pos_y, pos_z)
		ray_cast.target_position = Vector3(0.0, -placement_space_size.y, 0.0)
		ray_cast.force_raycast_update()
		if ray_cast.is_colliding():
			var placement_physics_object = ray_cast.get_collider()
			var hit_object_name = placement_physics_object.get_parent().name
			if hit_object_name in placement_objects_names:
				var object_node = object_scene.instantiate()
				# some object want to know what is the placement physics object
				if "placement_physics_object" in object_node:
					object_node.placement_physics_object = placement_physics_object
				container_node.add_child(object_node)
				var b = Basis.IDENTITY
				var scale_deviation = (1 + randf_range(-max_delta_scale, max_delta_scale))
				b = b.scaled(Vector3.ONE * scale_deviation)
				b = b.rotated(Vector3.UP, deg_to_rad(randf_range(-180.0, 180.0)))
				var o = ray_cast.get_collision_point() + delta_pos
				if "center_height" in object_node:
					o.y += object_node.center_height
				# set object transform
				object_node.set_global_transform(Transform3D(b, o))
				object_count += 1
	#
	var inst_tmp = object_scene.instantiate()
	print(inst_tmp.name, ": placements/attempts = ", objects_number, "/", total_attempts, 
			" (", float(objects_number)/float(total_attempts), ")" )
	
	# cleanup
	inst_tmp.free()
	ray_cast.queue_free()
	if ray_cast_object_mask:
		ray_cast_object_mask.queue_free()
	
	# save scene if requested
	if save_scene:
		# WARNING: don't use await in _ready()
		var ready_timer = Timer.new()
		ready_timer.timeout.connect(_do_save_scene)
		ready_timer.wait_time = 2.0
		ready_timer.one_shot = true
		add_child(ready_timer)
		ready_timer.start()
	
	# set ready is finished
	placements_finished = true


func _do_save_scene(p_container_node):
	# set children's owner (recursively): so they will get saved with the scene
	var all_childred_but_container = ScriptLibrary.get_all_children(p_container_node)
	all_childred_but_container.pop_front()
	for object_node in all_childred_but_container:
		object_node.set_owner(p_container_node)
	#
	var current_scene = p_container_node
	var packed_scene = PackedScene.new()
	packed_scene.pack(current_scene)
	var scene_name = save_scene_path + "/"
	if not save_scene_name.is_empty():
		scene_name += save_scene_name + "_saved.tscn"
	else:
		scene_name += current_scene.name + "_saved.tscn"
	ResourceSaver.save(scene_name, packed_scene)
	var saved_label = Label.new()
	saved_label.text = "SAVED!"
	add_child(saved_label)


func _set_enabled(value):
	enabled = value
