class_name Platform
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export_range(0.1, 15.0, 0.05, "or_greater") var animation_speed: float = 1.0
@export_range(4.0, 15.0, 0.5, "or_greater") var reload_time: float = 8.0
@export_range(10.0, 30.0, 0.5, "or_greater") var dissolve_max_time: float = 20.0
@export_range(0.0, 10.0, 0.5, "or_greater") var dissolve_max_delta_time: float = 5.0

var platform_objects: PackedScene = preload("res://levels/elements/platform_objects.tscn")
var platform_objects_node: Node3D = null


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	$ReloadTimer.timeout.connect(_reload_timeout)
	$ReloadTimer.wait_time = reload_time
	$ReloadTimer.stop()
	#
	$AnimationPlayer.play("RESET")


func _trigger_body_entered(_body):
	if enabled:
		_create_platform_objects()
		await get_tree().create_timer(0.1).timeout
		$AnimationPlayer.play("operate", -1, animation_speed)
		enabled = false
		$ReloadTimer.start()


func _reload_timeout():
	$AnimationPlayer.play("rewind", -1, animation_speed)


func _create_platform_objects():
	if is_instance_valid(platform_objects_node):
		platform_objects_node.queue_free()
	var current_scene = globals.get_current_scene()
	var plane_global_transform = ($Plane.global_transform.translated(
			Vector3(0.0, 0.1, 0.0)))
	platform_objects_node = platform_objects.instantiate()
	for body:PhysicsBody3D in platform_objects_node.get_children():
		body.set_owner(null)
		body.reparent(current_scene, false)
		body.set_owner(current_scene)
		body.global_transform = plane_global_transform * body.transform
		if "dissolve" in body:
			body.dissolve_time = abs(randf_range(
				dissolve_max_time - dissolve_max_delta_time, 
				dissolve_max_time + dissolve_max_delta_time))
			body.dissolve(true)
	platform_objects_node.queue_free()


func _set_enabled(value):
	enabled = value
	if enabled:
		$TriggerArea.body_entered.connect(_trigger_body_entered)
	else:
		$TriggerArea.body_entered.disconnect(_trigger_body_entered)
