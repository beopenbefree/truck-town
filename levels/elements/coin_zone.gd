class_name CoinZone
extends Area3D


@export var enabled: bool = true: set = _set_enabled

@export var coin_scene: PackedScene = null
@export var coin_score: int = 1

@export_group("Text Explosion")
@export var te_scene: PackedScene = null
@export var te_delta_z: float = 0.5
@export var te_speed_factor: float = 0.3
@export var te_amount: int = 8
@export var te_text: String = String()
@export var te_font_size: int = 32
@export var te_want_color: bool = false
@export var te_color: Color = Color.WHITE
@export_group("")

# AI: target priority
@export var target_priority: int = 0

var coin: MeshInstance3D = null

# AI: when used as target
var velocity:Vector3 = Vector3.ZERO


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	coin = coin_scene.instantiate()
	coin.score = coin_score
	add_child(coin)
	# set the scale of children
	coin.scale = scale
	$CollisionShape3D.scale = scale
	body_entered.connect(_trigger_body_entered)


func _trigger_body_entered(body):
	if enabled: # (body is Vehicle) or (body is VehicleAI)
		var grabbed = false
		if body is Vehicle:
			grabbed = true
			_update_score(coin.score)
		# AI
		if body is VehicleAI:
			grabbed = true
		if grabbed:
			# play some sound
			globals.play_sound("coin_grabbed", null, true, false, 
					global_transform.origin)
			# emit text explosion (if requested)
			if te_scene:
				var text_explosion: TextExplosion = te_scene.instantiate()
				text_explosion.want_color = te_want_color
				text_explosion.color = te_color
				var delta_z = (abs(te_delta_z) + 
						body.velocity.length() * abs(te_speed_factor))
				var origin = Vector3(0.0, 0.0, delta_z)
				text_explosion.emit(te_amount, te_text,
						te_font_size, origin, body, coin)
			queue_free()


func _update_score(score):
	globals.score_current_level += score
	globals.score += score
	globals.enable_end_zones()


func _get_class():
	return "CoinZone"


func _is_class(value):
	return "CoinZone" == value


func _set_enabled(value):
	enabled = value
