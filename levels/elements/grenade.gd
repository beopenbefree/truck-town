class_name Grenade
extends RigidBody3D


@export_range(0, 100, 1, "or_greater") var DAMAGE: int = 60

@export_range(0.0, 10.0, 0.01, "or_greater") var GRENADE_TIME: float = 2.0

@export_range(0.0, 10.0, 0.01, "or_greater") var EXPLOSION_WAIT_TIME: float = 0.48

var grenade_timer: float = 0
var explosion_wait_timer: float = 0

var entered_objects: Dictionary = {}

@onready var collision_shape: CollisionShape3D = ScriptLibrary.find_nodes_by_type(
		CollisionShape3D, self)[0]
@onready var mesh_instance: MeshInstance3D = ScriptLibrary.find_nodes_by_type(
		MeshInstance3D, self)[0]
@onready var blast_area: Area3D = ScriptLibrary.find_nodes_by_type(
		Area3D, self)[0]
@onready var explosion_holder: ExplosionHolder = ScriptLibrary.find_nodes_by_type(
		ExplosionHolder, self)[0]


func _ready():
	# connect signals
	body_entered.connect(_on_body_entered)
	body_exited.connect(_on_body_exited)
	# 
	explosion_holder.emit(false)


func _physics_process(delta):
	
	if grenade_timer < GRENADE_TIME:
		grenade_timer += delta
		return
	else:
		if explosion_wait_timer <= 0:
			mesh_instance.visible = false
			freeze_mode = RigidBody3D.FREEZE_MODE_STATIC
			freeze = true
			
			var bodies = blast_area.get_overlapping_bodies()
			for body in bodies:
				if body.has_method("bullet_hit"):
					var bullet_transform:Transform3D = Transform3D.IDENTITY
					bullet_transform.basis.z = -(body.global_position - global_position).normalized()
					bullet_transform.basis.x = Vector3.ZERO
					bullet_transform.basis.y = Vector3.ZERO
					bullet_transform.origin = body.global_position
					body.bullet_hit(DAMAGE, bullet_transform, false, true)
			# disable collision shape
			collision_shape.disabled = true
			# pass decoration parent information to explosion holder
			var decoration_parent = null
			# HACK: a physics body should have a parent or
			# a child that is a VisualInstance3D
			if not entered_objects.is_empty():
				var entered_object = entered_objects.get(entered_objects.keys()[0], null)
				if (entered_object.get_parent() is VisualInstance3D):
					decoration_parent = entered_object.get_parent()
				else:
					decoration_parent = ScriptLibrary.find_nodes_by_type(
							VisualInstance3D, entered_object, false)[0]
			explosion_holder.decoration_parent = decoration_parent
			# finally ask explosion holder to emit the explosion effect
			explosion_holder.emit(true)
		
		# See if we have waited long enough to destroy the grenade
		if explosion_wait_timer < EXPLOSION_WAIT_TIME:
			explosion_wait_timer += delta
			
			# If we have waited long enough, then free/destroy the grenade
			if explosion_wait_timer >= EXPLOSION_WAIT_TIME:
				queue_free()


func _on_body_entered(body):
	entered_objects[body] = body


func _on_body_exited(body):
	entered_objects.erase(body)


func _get_class():
	return "Grenade"


func _is_class(value):
	return "Grenade" == value
