class_name EffectBase
extends Node3D


@export var scaling: float = 1.0

@export var sound_name: String = String()

var effects: Array[GPUParticles3D] = []

# HACK: many effects play some sort of sounds, so due to asynchronicity
# it is not certain that the sound emitted will be correctly cleared at
# exit, therefore a refernce to the sound emitted should be taken and
# it will be correctly cleared
var sound_emitted: Node3D = null


func _ready():
	# make the resources unique
	_make_resources_unique()
	# tweak resources
	set_effects_scale(sqrt(abs(scaling)))


func _exit_tree():
	# HACK: see above
	if is_instance_valid(sound_emitted):
		sound_emitted.clear_sound()


func emit_effects(enable:bool):
	for effect in effects:
		effect.set_deferred("emitting", enable)


func is_emitting():
	var emitting = true
	for effect in effects:
		if not effect.emitting:
			emitting = false
			break
	return emitting


func destroy_on_finished():
	# HACK: see above
	if is_instance_valid(sound_emitted):
		sound_emitted.clear_sound()
	queue_free()


func set_effects_scale(value:float):
	for effect in effects:
		effect.visibility_aabb.size *= value
		effect.draw_pass_1.size *= value


func _make_resources_unique():
	for effect in effects:
		effect.process_material = effect.process_material.duplicate(true)
		effect.draw_pass_1 = effect.draw_pass_1.duplicate(true)


func _get_class():
	return "EffectBase"


func _is_class(value):
	return "EffectBase" == value
