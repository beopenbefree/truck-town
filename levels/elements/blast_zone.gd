class_name BlastZone
extends Area3D


@export var enabled: bool = true: set = _set_enabled

@export var MAX_BLAST_DELAY: float = 2.0

@export var MIN_BLAST_MAGNITUDE: float = 5.0
@export var MAX_BLAST_MAGNITUDE: float = 10.0

var blast_timer: Timer

var vehicles: Array = []

var placement_physics_object: PhysicsBody3D = null

var new_collision_shape: CollisionShape3D = null
@onready var explosion_holder: ExplosionHolder = $ExplosionHolder


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	body_entered.connect(_on_BlastZone_body_entered)
	# re-add collision shape taking into account any scaling effect
	var scaled_aabb = $DecalTexture.get_aabb()
	new_collision_shape = ScriptLibrary.compute_collision_shape(
			ScriptLibrary.ShapeType.BOX, scaled_aabb)
	add_child(new_collision_shape)
	$CollisionShape3D.queue_free()
	#
	if MIN_BLAST_MAGNITUDE > MAX_BLAST_MAGNITUDE:
		MIN_BLAST_MAGNITUDE = MAX_BLAST_MAGNITUDE
	# initialize timers
	blast_timer = Timer.new()
	blast_timer.one_shot = true
	add_child(blast_timer)
	# HACK: optimization
	set_physics_process(false)


func _physics_process(_delta):
	if blast_timer.is_stopped():
		for vehicle:VehicleBase in vehicles:
			vehicle.apply_blast(randf_range(MIN_BLAST_MAGNITUDE, 
					MAX_BLAST_MAGNITUDE))
			# explosion effects
			explosion_holder.global_transform.origin = vehicle.global_transform.origin
			# pass decoration parent information to explosion holder
			var decoration_parent = null
			# HACK: a physics body should have a parent or
			# a child that is a VisualInstance3D
			if placement_physics_object:
				if placement_physics_object.get_parent() is VisualInstance3D:
					decoration_parent = placement_physics_object.get_parent()
				else:
					decoration_parent = ScriptLibrary.find_nodes_by_type(
							VisualInstance3D, placement_physics_object, false)[0]
			explosion_holder.decoration_parent = decoration_parent
			# finally ask explosion holder to emit the explosion effect
			explosion_holder.emit(true)
		vehicles.clear()
		set_physics_process(false)


func _on_BlastZone_body_entered(body):
	if (not enabled) or (not blast_timer.is_stopped()):
		return
	
	if body.has_method("apply_blast"):
		vehicles.push_back(body)
		blast_timer.wait_time = randf_range(0.0, MAX_BLAST_DELAY)
		blast_timer.start()
		set_physics_process(true)


func _get_class():
	return "BlastZone"


func _is_class(value):
	return "BlastZone" == value


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)
