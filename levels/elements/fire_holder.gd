class_name FireHolder
extends EffectHolderBase


# mandatory method
func emit(enable:bool):
	# HACK: wait one frame as the holder may have changed
	# its transform in this frame and will therefore apply
	# it on the held effect, during the next frame
	# WARNING: don't use await
	_deferred_emit.call_deferred(enable)


func _deferred_emit(enable:bool):
	if is_instance_valid(effect_vfx) and effect_vfx.is_inside_tree():
		(effect_vfx as Fire).emit(enable, self)


func _get_class():
	return "FireHolder"


func _is_class(value):
	return "FireHolder" == value
