class_name LaserBeam
extends EffectBase


# HACK: amount of particles of $BeamSparks
const BEAM_SPARKS_AMOUNT: int = 50

@onready var beam_mesh: MeshInstance3D = $BeamMesh
@onready var beam_sparks: GPUParticles3D = $BeamSparks
@onready var hit_point_sparks: GPUParticles3D = $HitPointSparks


func _ready():
	effects = [beam_sparks, hit_point_sparks]
	super()
	emit(false)


func emit(enable:bool, signed_length:float=0.0, sound_root:Node3D=self):
	# adjust beam mesh
	beam_mesh.mesh.height = abs(signed_length)
	beam_mesh.position.z = signed_length * 0.5
	# adjust beam sparks
	var particle_amount = snapped(abs(signed_length) * BEAM_SPARKS_AMOUNT, 1)
	if particle_amount > 1:
		beam_sparks.amount = particle_amount
	else:
		beam_sparks.amount = 1
	beam_sparks.process_material.emission_box_extents = (
			Vector3(beam_mesh.mesh.top_radius,
					beam_mesh.mesh.top_radius,
					abs(signed_length) / 2.0) * 
			sqrt(abs(scaling)))
	beam_sparks.position.z = signed_length * 0.5
	# adjust hit point's sparks
	hit_point_sparks.position.z = signed_length
	# emit vfx and sound
	if enable and (not is_emitting()):
		emit_effects(enable)
		beam_mesh.set_deferred("visible", enable)
		# emit sound
		# HACK: see EffectBase
		sound_emitted = globals.play_sound(sound_name, sound_root, true, true)
	elif (not enable) and is_emitting():
		emit_effects(enable)
		beam_mesh.set_deferred("visible", enable)
		# emit sound
		globals.clear_sound(sound_name, sound_root)


func _get_class():
	return "LaserBeam"


func _is_class(value):
	return "LaserBeam" == value
