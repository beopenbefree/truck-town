class_name ExplosiveObject
extends Grenade


@export var auto_shape_build: bool = true

@export var shape_type: ScriptLibrary.ShapeType = ScriptLibrary.ShapeType.SPHERE
# Center Of Mass inside the (center of) mesh
@export var com_inside_mesh: bool = true


func _ready():
	super()
	if auto_shape_build:
		@warning_ignore("standalone_expression")
		ScriptLibrary.set_collision_shape(self, shape_type, com_inside_mesh)[0]


func bullet_hit(_damage, _bullet_global_transform,
		raycast=false, with_hit=false):
	if (((self as CollisionObject3D) is RigidBody3D) and 
			(not self.freeze) and (raycast or with_hit)):
		_do_explode()


func rocket_hit(_damage, _rocket_global_transform):
	if ((self as CollisionObject3D) is RigidBody3D) and (not self.freeze):
		_do_explode()


func _do_explode():
	grenade_timer = GRENADE_TIME


func _get_class():
	return "ExplosiveObject"


func _is_class(value):
	return "ExplosiveObject" == value
