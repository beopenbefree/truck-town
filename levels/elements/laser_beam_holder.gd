class_name LaserBeamHolder
extends EffectHolderBase


# mandatory method
func emit(enable:bool, length:float=0.0):
	# HACK: wait one frame as the holder may have changed
	# its transform in this frame and will therefore apply
	# it on the held effect, during the next frame
	# WARNING: don't use await
	_deferred_emit.call_deferred(enable, length)


func _deferred_emit(enable:bool, length:float):
	if is_instance_valid(effect_vfx) and effect_vfx.is_inside_tree():
		(effect_vfx as LaserBeam).emit(enable, length, self)


func _get_class():
	return "LaserBeamHolder"


func _is_class(value):
	return "LaserBeamHolder" == value
