class_name BoostZone
extends Area3D


@export var enabled: bool = true: set = _set_enabled

@export var BOOST_FACTOR: float = 3.0
@export var BOOST_DURATION: float = 5.0

# to make only one vehicle of a multibody vehicle to be detected
var entered_timer: Timer
const ENTERED_DURATION = 0.5

var new_collision_shape: CollisionShape3D = null


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	body_entered.connect(_on_BoostZone_body_entered)
	# re-add collision shape taking into account any scaling effect
	var scaled_aabb = $DecalTexture.get_aabb()
	new_collision_shape = ScriptLibrary.compute_collision_shape(
			ScriptLibrary.ShapeType.BOX, scaled_aabb)
	add_child(new_collision_shape)
	$CollisionShape3D.queue_free()
	# initialize timers
	entered_timer = Timer.new()
	entered_timer.wait_time = ENTERED_DURATION
	entered_timer.one_shot = true
	add_child(entered_timer)


func _on_BoostZone_body_entered(body):
	if (not enabled) or (not entered_timer.is_stopped()):
		return
	
	if body.has_method("apply_boost"):
		body.apply_boost(BOOST_FACTOR, BOOST_DURATION)
		entered_timer.start()


func _get_class():
	return "BoostZone"


func _is_class(value):
	return "BoostZone" == value


func _set_enabled(value):
	enabled = value
