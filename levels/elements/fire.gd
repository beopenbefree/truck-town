class_name Fire
extends EffectBase


@onready var flames: GPUParticles3D = $Flames
@onready var smoke: GPUParticles3D = $Smoke
@onready var sparks: GPUParticles3D = $Sparks


func _ready():
	effects = [flames, smoke, sparks]
	super()
	emit(false)


func emit(enable:bool, sound_root:Node3D=self):
	if enable and (not is_emitting()):
		emit_effects(enable)
		# emit sound
		# HACK: see EffectBase
		sound_emitted = globals.play_sound(sound_name, sound_root, true, true)
	elif (not enable) and is_emitting():
		emit_effects(enable)
		# clear sound
		globals.clear_sound(sound_name, sound_root)


func set_effects_scale(value:float):
	super(value)
	for effect in effects:
		effect.process_material.emission_sphere_radius *= value


func _get_class():
	return "Fire"


func _is_class(value):
	return "Fire" == value
