class_name EndZone
extends Area3D


@export var enabled: bool = false: set = _set_enabled

@export var amount: int = 24
@export var lifetime: float = 24.0

@export_group("Text Explosion")
@export var te_scene: PackedScene = null
@export var te_delta_z: float = 0.5
@export var te_speed_factor: float = 0.3
@export var te_amount: int = 8
@export var te_text: String = String()
@export var te_font_size: int = 32
@export var te_want_color: bool = false
@export var te_color: Color = Color.WHITE


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	body_entered.connect(_on_EndZone_body_entered)
	$Particles.lifetime = self.amount
	$Particles.lifetime = self.lifetime
	$Particles.visibility_aabb.size.y = self.lifetime * 2.0
	enabled = false
	# register in current end zones
	globals.current_end_zones.append(self)


func _on_EndZone_body_entered(body):
	if enabled and (body is Vehicle):
		# emit text explosion (if requested)
		if te_scene:
			var parent = (body as Node3D).find_child("Shooter")
			if not parent:
				parent = body
			var text_explosion: TextExplosion = te_scene.instantiate()
			text_explosion.want_color = te_want_color
			text_explosion.color = te_color
			var delta_z = (abs(te_delta_z) + 
					body.velocity.length() * abs(te_speed_factor))
			var origin = Vector3(0.0, 0.0, delta_z)
			# override text
			te_text = str(globals.current_score_level_bonus())
			text_explosion.emit(te_amount, te_text, te_font_size,
					origin, parent, $Base)
		
		# update level
		globals.update_level(body, true)
		
		# play some sound
		globals.play_sound("session_end", null, true, false, 
				global_transform.origin)


func _get_class():
	return "EndZone"


func _is_class(value):
	return "EndZone" == value


func _set_enabled(value):
	enabled = value
	$Particles.emitting = enabled
