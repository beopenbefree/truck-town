extends StaticBody3D


@export_node_path("Node3D") var turret_root_path = NodePath()

@onready var turret_root:Node = get_node_or_null(turret_root_path)


func bullet_hit(damage, bullet_global_transform,
		_raycast=false, _with_hit=false):
	if turret_root != null:
		turret_root.bullet_hit(damage, bullet_global_transform)


func rocket_hit(damage, rocket_global_transform):
	if turret_root != null:
		turret_root.rocket_hit(damage, rocket_global_transform)


func raycast_hit(damage, hit_position):
	if turret_root != null:
		turret_root.raycast_hit(damage, hit_position)
