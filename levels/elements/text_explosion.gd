class_name TextExplosion
extends GPUParticles3D


@export var want_color: bool = false
@export var color: Color = Color.WHITE


func emit(p_amount:int, text:String, font_size:int,
		origin:Vector3, object_parent:Node3D,
		object_for_material:MeshInstance3D=null):
	# set text explosion parameters
	amount = p_amount
	# make unique draw_pass_1 (Mesh)
	draw_pass_1 = draw_pass_1.duplicate()
	draw_pass_1.font_size = font_size
	draw_pass_1.text = text
	var material:BaseMaterial3D = null
	if object_for_material:
		if object_for_material.get_surface_override_material_count() > 0:
			material = object_for_material.get_surface_override_material(0).duplicate(true)
		elif object_for_material.mesh.get_surface_count() > 0:
			material = object_for_material.mesh.surface_get_material(0).duplicate(true)
	if material:
		if want_color:
			material.albedo_color = color
		draw_pass_1.material = material
		material = null
	object_parent.add_child(self)
	position = origin
	emitting = true
	# WARNING: don't use await
	self.finished.connect(_on_finished)


func _on_finished():
	queue_free()


func _get_class():
	return "TextExplosion"


func _is_class(value):
	return "TextExplosion" == value
