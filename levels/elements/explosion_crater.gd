class_name ExplosionCrater
extends DecorationBase


@export var dissolve_time: float = 16.0


func _physics_process(delta):
	if dissolve_time >= 0.0:
		dissolve_time -= delta
	else:
		queue_free()


func _get_class():
	return "ExplosionCrater"


func _is_class(value):
	return "ExplosionCrater" == value
