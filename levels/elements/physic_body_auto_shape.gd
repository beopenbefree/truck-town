#@tool
extends CollisionObject3D


@export var auto_shape_build: bool = true

@export var shape_type: ScriptLibrary.ShapeType = ScriptLibrary.ShapeType.SPHERE
# Center Of Mass inside the (center of) mesh
@export var com_inside_mesh: bool = true

@export_node_path("MeshInstance3D") var dissolve_mesh_instance_path = NodePath()
@export var dissolve_time: float = 16.0

enum Axis {X, Y, Z}

var center_height:float = 0.0

var do_hit:bool = false
var hit_impulse:Vector3 = Vector3()
var hit_impulse_position:Vector3 = Vector3()

var do_dissolve:bool = false


func _ready():
	if auto_shape_build:
		ScriptLibrary.set_collision_shape(self, shape_type, com_inside_mesh)


func _physics_process(delta):
	if do_hit:
		if (((self as CollisionObject3D) is RigidBody3D) and 
				(PhysicsServer3D.body_get_mode(get_rid()) == PhysicsServer3D.BODY_MODE_RIGID)):
			(self as CollisionObject3D).apply_impulse(hit_impulse, hit_impulse_position)
			do_hit = false
	if do_dissolve: 
		if dissolve_time >= 0.0:
			dissolve_time -= delta
		else:
			var dissolve_mesh_instance = get_node(dissolve_mesh_instance_path)
			if dissolve_mesh_instance:
				dissolve_mesh_instance.fading_done.connect(queue_free)
				dissolve_mesh_instance.fade()
				set_physics_process(false)
			else:
				queue_free()


func bullet_hit(damage, bullet_global_transform,
		raycast=false, with_hit=false):
	if (((self as CollisionObject3D) is RigidBody3D) and 
			(not self.freeze) and (raycast or with_hit)):
		# hit direction and position
		hit_impulse = -bullet_global_transform.basis.z * damage
		hit_impulse_position = bullet_global_transform.origin - global_transform.origin
		do_hit = true


func rocket_hit(damage, rocket_global_transform):
	if ((self as CollisionObject3D) is RigidBody3D) and (not self.freeze):
		#return
	#else:
		## NOTE: enhance physic collision with supplementary impulse
		# hit direction and position
		hit_impulse = rocket_global_transform.basis.z * damage
		hit_impulse_position = rocket_global_transform.origin - global_transform.origin
		do_hit = true


func dissolve(enable):
	await get_tree().create_timer(randf() * 1.5).timeout
	do_dissolve = enable
