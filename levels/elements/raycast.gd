class_name ShootRayCast
extends RayCast3D


@export var MAX_DIST: float = 200.0
@export var DAMAGE: int = 10

@export var laser_beam_duration: float = 0.125

@onready var laser_beam_holder: LaserBeamHolder = $LaserBeamHolder


func _ready():
	laser_beam_holder.emit(false)


func shoot(target):
	look_at(target.global_transform.origin, Vector3(0, 1, 0))
	var to_point = global_transform.origin - global_transform.basis.z * MAX_DIST
	force_raycast_update()
	if is_colliding():
		var body = get_collider()
		to_point = get_collision_point()
		if body.has_method("raycast_hit"):
			body.raycast_hit(DAMAGE, to_point)
	
	# emit laser beam
	emit_laser_beam(to_point)


func emit_laser_beam(to_point):
	var cast_point = to_local(to_point)
	# emit
	laser_beam_holder.emit(true, cast_point.z)
	await get_tree().create_timer(laser_beam_duration).timeout
	laser_beam_holder.emit(false)


func _get_class():
	return "ShootRayCast"


func _is_class(value):
	return "ShootRayCast" == value
