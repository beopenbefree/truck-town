class_name DeadZone
extends Area3D


@export var enabled: bool = true: set = _set_enabled
@export var COUNTDOWN_DURATION: float = 3.0

var count_down_timer: Timer

var current_vehicle: Vehicle = null

var new_collision_shape: CollisionShape3D = null


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	body_entered.connect(_on_DeadZone_body_entered)
	# re-add collision shape taking into account any scaling effect
	var scaled_aabb = $DebugMesh.get_aabb()
	new_collision_shape = ScriptLibrary.compute_collision_shape(
			ScriptLibrary.ShapeType.BOX, scaled_aabb)
	add_child(new_collision_shape)
	$CollisionShape3D.queue_free()
	# initialize timers
	count_down_timer = Timer.new()
	count_down_timer.wait_time = COUNTDOWN_DURATION
	count_down_timer.one_shot = true
	count_down_timer.timeout.connect(_complete_exit)
	add_child(count_down_timer)


func _on_DeadZone_body_entered(body):
	if enabled and (body is Vehicle):
		current_vehicle = body
		count_down_timer.start()


func _complete_exit():
	# disable physics temporarily for current truck
	current_vehicle.lift_up = true
	# remove all vehicle sounds
	globals.clear_sound("", current_vehicle)
	if "current_sound" in current_vehicle:
		current_vehicle.current_sound = null
	
	# play fade animation if any
	var fade_animation:AnimationPlayer = globals.get_current_scene().get_node("FadeAnimation")
	if fade_animation:
		fade_animation.play("fade")
		await fade_animation.animation_finished
	
	# handle score/trucks
	globals.vehicle_number -= 1
	if globals.is_game_over:
		globals.load_new_scene(globals.GAME_OVER_SCENE)
	else:
		# continue game
		globals.load_new_scene(globals.current_level_path)


func _get_class():
	return "DeadZone"


func _is_class(value):
	return "DeadZone" == value


func _set_enabled(value):
	enabled = value
