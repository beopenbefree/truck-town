class_name HealthPickup
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export_enum("full size:0", "small:1") var kit_size: int = 0: set = _set_kit_size

# 0 = full size pickup, 1 = small pickup
@export var HEALTH_AMOUNTS: Array = [10, 5]

@export var RESPAWN_HEALTH: float = 15.0

@export_group("Text Explosion")
@export var te_scene: PackedScene = null
@export var te_delta_z: float = 0.5
@export var te_speed_factor: float = 0.3
@export var te_amount: int = 8
@export var te_text: String = String()
@export var te_font_size: int = 32
@export var te_want_color: bool = false
@export var te_color: Color = Color.WHITE
@export_group("")

# AI: target priority
@export var target_priority: int = 0

const holder_scene: PackedScene = preload("res://levels/elements/health_holder.tscn")

var holder: Node3D = null
var trigger: Area3D = null
var respawn_timer: float = 0.0

var is_ready: bool = false
var body_exited: bool = false


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	kit_size = kit_size
	# add very first holder
	_add_holder()


func _physics_process(delta):
	if respawn_timer > 0:
		respawn_timer -= delta
	
	if (respawn_timer <= 0) and body_exited:
		_add_holder()


func _add_holder():
	holder = holder_scene.instantiate()
	trigger = holder.get_node("HealthPickupTrigger")
	trigger.body_entered.connect(_trigger_body_entered)
	trigger.body_exited.connect(_trigger_body_exited)
	trigger.target_priority = target_priority
	add_child(holder)
	is_ready = true
	# hide all of the possible kit sizes
	_set_kit_size_values(0, false)
	_set_kit_size_values(1, false)
	# show only selected kit size
	_set_kit_size_values(kit_size, true)
	# optimization
	set_physics_process(false)


func _remove_holder():
	_set_kit_size_values(kit_size, false)
	set_physics_process(true)
	holder.queue_free()
	is_ready = false


func _trigger_body_entered(body):
	if enabled: # (body is Vehicle or VehicleAI)
		# check first if respawn_timer has expired
		if (respawn_timer <= 0) and body.has_method("add_health"):
			body.add_health(HEALTH_AMOUNTS[kit_size])
			respawn_timer = RESPAWN_HEALTH
			body_exited = false
			
			# emit text explosion (if requested)
			if te_scene:
				var text_explosion: TextExplosion = te_scene.instantiate()
				text_explosion.want_color = te_want_color
				text_explosion.color = te_color
				var delta_z = (abs(te_delta_z) + 
						body.velocity.length() * abs(te_speed_factor))
				var origin = Vector3(0.0, 0.0, delta_z)
				text_explosion.emit(te_amount, 
						te_text, te_font_size,
						origin, body, holder.get_node("HealthKit/HealthBox"))
			# play some sound
			globals.play_sound("health_pickup", null, true, false, 
					global_transform.origin)
			
			# remove holder
			_remove_holder()


func _trigger_body_exited(_body):
	if enabled: # (body is Vehicle)
		body_exited = true


func _set_kit_size(value):
	if enabled:
		if is_ready:
			_set_kit_size_values(kit_size, false)
			kit_size = value
			
			_set_kit_size_values(kit_size, true)
		else:
			kit_size = value


func _set_kit_size_values(size, enable):
	if enabled:
		if size == 0:
			trigger.get_node("ShapeKit").set_deferred("disabled", !enable)
			holder.get_node("HealthKit").set_deferred("visible", enable)
		elif size == 1:
			trigger.get_node("ShapeKitSmall").set_deferred("disabled", !enable)
			holder.get_node("HealthKitSmall").set_deferred("visible", enable)


func _get_class():
	return "HealthPickup"


func _is_class(value):
	return "HealthPickup" == value


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)
