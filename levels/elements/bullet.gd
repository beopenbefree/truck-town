class_name Bullet
extends RigidBody3D


@export_range(0, 100, 1) var DAMAGE: int = 10
@export var scaling: Vector3 = Vector3.ONE: set = _set_scaling
@export var add_effects: bool = true

var impulse_size: float = 0.0
var direction := Vector3.ZERO
var firing: bool = false
# HACK: rocket start position correction
var firing_just_started: bool = false

const LIFE_TIME: float = 4.0
var timer: float = 0
var hit_something: bool = false

var bullet_hole_scene: PackedScene = preload("res://levels/elements/bullet_hole.tscn")


func _ready():
	# HACK: call setters with (potential) side effects
	scaling = scaling
	# connect body_entered signal
	body_entered.connect(_on_Body_body_entered)


func _physics_process(delta):
	if firing:
		apply_impulse(impulse_size * direction, Vector3(0.0, 0.0, 0.0))
		firing = false
	# anyway free itself after life time
	timer += delta
	if timer > LIFE_TIME:
		queue_free()


# HACK: rocket start position correction due to vehicle movement
func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	if firing_just_started:
		global_position += state.linear_velocity * state.step
		firing_just_started = false


func fire(p_impulse_size:float, p_direction:Vector3):
	impulse_size = p_impulse_size
	direction = p_direction.normalized()
	firing = true
	# HACK: bullet start position correction
	firing_just_started = true


func _on_Body_body_entered(body:PhysicsBody3D):
	if hit_something == false:
		hit_something = true
		if body.has_method("bullet_hit"):
			body.bullet_hit(DAMAGE, global_transform)
		
		elif (body.collision_layer & 0b1):
			# collision with environment: play sound
			globals.play_sound("environment_collision_dynamic", null, true, false, 
					global_transform.origin)
		
		if add_effects:
			# add bullet hole decal
			var bullet_hole = bullet_hole_scene.instantiate()
			
			# get decoration parent = body
			var decoration_parent = body
			
			# NOTE: for performance reason we don't use cull mask
			# for bullet hole decoration (decal)
			# HACK: a physics body should have a parent or
			# a child that is a VisualInstance3D
			#if (body.get_parent() is VisualInstance3D):
				#decoration_parent = body.get_parent()
			#else:
				#decoration_parent = ScriptLibrary.find_nodes_by_type(
						#VisualInstance3D, body, false)[0]
			
			decoration_parent.add_child(bullet_hole)
			bullet_hole.global_transform = global_transform * Transform3D(
				Basis(Vector3.RIGHT, deg_to_rad(90)), Vector3.ZERO)
			# emit some dust
			bullet_hole.emit_dust()
			# bullet disapper into hole
			queue_free()
		
		if not (body.collision_layer & 0b1):
			# no collision with environment
			queue_free()
		
		# Alternartive (more precise) way FIXME
#		var delta_dist = 20.0
#		var forward_dir = -global_transform.basis.z
#		var bullet_pos = global_transform.origin
#		var from = bullet_pos - forward_dir * delta_dist
#		var to = from + forward_dir * delta_dist * 2.0
#		var raycast_params = PhysicsRayQueryParameters3D.create(from, to)
#		var space_state = get_world_3d().direct_space_state
#		var result = space_state.intersect_ray(raycast_params)
#		if not result:
#			return
#		var hole_pos = result["position"]
#		var hole_lookat = hole_pos + result["normal"] * delta_dist
#		var hole_transform = Transform3D(Basis.IDENTITY, hole_pos)
#		hole_transform = hole_transform.looking_at(hole_lookat, 
#				Vector3.UP, true)
#		hole_transform = hole_transform * Transform3D(
#			Basis(Vector3.RIGHT, deg_to_rad(90)), Vector3.ZERO)
#		bullet_hole.global_transform = hole_transform


func _set_scaling(value):
	scaling = value
	$MeshInstance3D.scale = value
	$CollisionShape3D.scale = value


func _get_class():
	return "Bullet"


func _is_class(value):
	return "Bullet" == value
