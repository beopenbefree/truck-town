class_name StickyGrenade
extends RigidBody3D


@export_range(0, 100, 1, "or_greater") var DAMAGE: int = 60

@export_range(0.0, 10.0, 0.01, "or_greater") var GRENADE_TIME: float = 2.0

@export_range(0.0, 10.0, 0.01, "or_greater") var EXPLOSION_WAIT_TIME: float = 0.48

var grenade_timer: float = 0
var explosion_wait_timer: float = 0

var attached: bool = false
var attach_point: Node3D = null
var attach_body: PhysicsBody3D = null

var player_body: Node3D = null

@onready var collision_shape: CollisionShape3D = ScriptLibrary.find_nodes_by_type(
		CollisionShape3D, self)[0]
@onready var mesh_instance: MeshInstance3D = ScriptLibrary.find_nodes_by_type(
		MeshInstance3D, self)[0]
@onready var blast_area: Area3D = ScriptLibrary.find_nodes_by_type(
		Area3D, self)[0]
@onready var explosion_holder: ExplosionHolder = ScriptLibrary.find_nodes_by_type(
		ExplosionHolder, self)[0]


func _ready():
	explosion_holder.emit(false)
	
	$StickyArea.body_entered.connect(collided_with_body)


func collided_with_body(body):
	if body == self:
		return
	
	if player_body != null:
		if body == player_body:
			return
	
	if attached == false:
		attached = true
		attach_point = Node3D.new()
		attach_body = body
		body.add_child(attach_point)
		attach_body = body
		attach_point.global_transform.origin = global_transform.origin
		
		collision_shape.set_deferred("disabled", true)
		
		freeze_mode = RigidBody3D.FREEZE_MODE_STATIC
		freeze = true


func _physics_process(delta):
	if attached == true:
		if attach_point != null:
			global_transform.origin = attach_point.global_transform.origin
	
	if grenade_timer < GRENADE_TIME:
		grenade_timer += delta
		return
	else:
		if explosion_wait_timer <= 0:
			mesh_instance.visible = false
			
			var bodies = blast_area.get_overlapping_bodies()
			for body in bodies:
				if body.has_method("bullet_hit"):
					var bullet_transform:Transform3D = Transform3D.IDENTITY
					bullet_transform.basis.z = -(body.global_position - global_position).normalized()
					bullet_transform.basis.x = Vector3.ZERO
					bullet_transform.basis.y = Vector3.ZERO
					bullet_transform.origin = body.global_position
					body.bullet_hit(DAMAGE, bullet_transform, false, true)
			# pass decoration parent information to explosion holder
			var decoration_parent = null
			if is_instance_valid(attach_body):
				# HACK: a physics body should have a parent or
				# a child that is a VisualInstance3D
				if (attach_body.get_parent() is VisualInstance3D):
					decoration_parent = attach_body.get_parent()
				else:
					decoration_parent = ScriptLibrary.find_nodes_by_type(
							VisualInstance3D, attach_body, false)[0]
			explosion_holder.decoration_parent = decoration_parent
			# finally ask explosion holder to emit the explosion effect
			explosion_holder.emit(true)
		
		if explosion_wait_timer < EXPLOSION_WAIT_TIME:
			explosion_wait_timer += delta
			
			if explosion_wait_timer >= EXPLOSION_WAIT_TIME:
				if attach_point != null:
					attach_point.queue_free()
				queue_free()


func _get_class():
	return "StickyGrenade"


func _is_class(value):
	return "StickyGrenade" == value
