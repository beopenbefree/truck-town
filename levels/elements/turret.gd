class_name Turret
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export_group("Shooting properties")
@export var fire_enabled: bool = true: set = _set_fire_enabled

@export var use_raycast: bool = false

@export_range(10, 100, 0.1) var vision_area_radius: float = 36.0: set = _set_vision_area_radius

@export var DAMAGE_BULLET: int = 20
@export var DAMAGE_RAYCAST: int = 5

@export var BULLET_IMPULSE_RANGE: Array = [50.0, 150.0]
@export var BULLET_MASS_RANGE: Array = [1.0, 10.0]

@export var MAX_TURRET_HEALTH: int = 60
@export var DESTROYED_TIME: int = 20

@export_group("Pointing modes")
# Aimer
@export var aimer_use: bool = false

#<AI
@export var ai_use: bool = false
@export_subgroup("AI properties")
@export var ai_dims: AI_TOOLS.Dimensions = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)
@export_range(0.0, 1.0, 0.01) var ai_damping: float = 0.0
@export var ai_max_angular_acceleration: float = 5.0
@export var ai_max_rotation: float = 5.0
@export var ai_target_radius: float = 5.0 # degrees
@export var ai_slow_radius: float = 45.0 # degrees
@export var ai_time_to_target: float = 0.1

var ai_face: AI.Face
var start_basis: Basis = Basis(Vector3.UP, deg_to_rad(180.0))
#AI>

const FLASH_TIME: float = 0.1
var flash_timer: float = 0

const FIRE_TIME: float = 0.8
var fire_timer: float = 0

var ammo_in_turret: int = 20
const AMMO_IN_FULL_TURRET: int = 20
const AMMO_RELOAD_TIME: int = 4
var ammo_reload_timer: float = 0

var is_active: bool = false

var current_target: Node = null
var visibility_check: RayCast3D = null
var visibility_check_position: Vector3
var visible_target: Marker3D = null

var bullet_scene: PackedScene = preload("res://levels/elements/bullet.tscn")

@onready var node_turret_head: Node3D = $Head
@onready var node_raycast: Node3D = $Head/RayCast3D
@onready var node_flash_one: Node3D = $Head/Flash
@onready var node_flash_two: Node3D = $Head/Flash_2
@onready var turret_health: int = MAX_TURRET_HEALTH
@onready var destroyed_timer: float = 0
@onready var fire_holder: Node3D = $FireHolder
@onready var aimer: Node3D = $Head


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	fire_enabled = fire_enabled
	vision_area_radius = vision_area_radius
	
	$VisionArea.body_entered.connect(_body_entered_vision)
	$VisionArea.body_exited.connect(_body_exited_vision)
	
	node_raycast.add_exception($Base/StaticBody3D)
	node_raycast.add_exception($Head/StaticBody3D)
	node_raycast.add_exception($VisionArea)
	node_raycast.target_position = Vector3(0, 0, -vision_area_radius)
	
	node_flash_one.visible = false
	node_flash_two.visible = false
	
	# HACK: re-add vision collision shape 
	# taking into account any scaling effect
	var sphere_shape = SphereShape3D.new()
	sphere_shape.radius = vision_area_radius
	var new_collision_shape = CollisionShape3D.new()
	new_collision_shape.shape = sphere_shape
	$VisionArea.add_child(new_collision_shape)
	$VisionArea/CollisionShape3D.queue_free()
	
	# Aimer: disable aiming anyway
	aimer.enabled = false
	
	#<AI
	if ai_use:
		ai_face = AI.Face.new()
		ai_face.setup(ai_max_angular_acceleration, ai_max_rotation, 
				deg_to_rad(ai_target_radius), deg_to_rad(ai_slow_radius),
				ai_time_to_target, ai_damping, ai_dims)
		_ai_initialize()
	#AI>
	
	# set fire related stuff
	fire_holder.emit(false)
	
	# HACK: optimization
	set_physics_process(false)


func _physics_process(delta):
	if flash_timer > 0:
		flash_timer -= delta
		if flash_timer <= 0:
			node_flash_one.visible = false
			node_flash_two.visible = false
	
	if turret_health <= 0.0:
		if destroyed_timer > 0:
			# disable
			destroyed_timer -= delta
			aimer.enabled = false
			return
		else:
			# enable
			turret_health = MAX_TURRET_HEALTH
			aimer.enabled = true
			fire_holder.emit(false)
	
	if (current_target != null) and (visible_target != null):
		# check if current_target is really visible
		visibility_check.target_position = visible_target.global_transform.origin - visibility_check_position
		if (visibility_check.is_colliding()):
			
			if not (visibility_check.get_collider() == current_target):
				return
			
			if aimer_use:
				aimer.target_node = visible_target
			#<AI
			elif ai_use:
				_ai_pre_update(delta)
				_ai_update(delta)
				_ai_post_update(delta)
			else:
			#AI>
				node_turret_head.look_at(visible_target.global_transform.origin, Vector3.UP)
			
			if fire_enabled:
				if turret_health> 0:
					if ammo_in_turret > 0:
						if fire_timer > 0:
							fire_timer -= delta
						else:
							# HACK: When using the aimer and a max
							# yaw rotation is set (< PI), when the
							# target is out of the view angle,
							# the aimer will correct the yaw
							# rotation, so we could use this
							# information (set in the previous
							# physical frame) to fire or not
							if aimer_use:
								if not aimer.yaw_rotation_corrected:
									fire()
							else:
								fire()
					else:
						if ammo_reload_timer > 0:
							ammo_reload_timer -= delta
						else:
							ammo_in_turret = AMMO_IN_FULL_TURRET


func fire():
	if use_raycast == true:
		node_raycast.DAMAGE = DAMAGE_RAYCAST
		node_raycast.shoot($Head/BarrelEnd)
		#
		ammo_in_turret -= 1
	else:
		var bullet = bullet_scene.instantiate()
		bullet.DAMAGE = DAMAGE_BULLET
		bullet.scaling *= 2.0
		bullet.collision_layer = 0b100000000
		bullet.collision_mask = 0b111
		# avoid collisions between the bullet and the body of this turret
		bullet.add_collision_exception_with($Head/StaticBody3D)
		#var scene_root = get_tree().root.get_children()[0]
		globals.get_current_scene().add_child(bullet)
		bullet.mass = randf_range(BULLET_MASS_RANGE[0], BULLET_MASS_RANGE[1])
		
		bullet.global_transform = $Head/BarrelEnd.global_transform
		var impulse_size = randf_range(BULLET_IMPULSE_RANGE[0], BULLET_IMPULSE_RANGE[1])
		bullet.fire(impulse_size, bullet.global_transform.basis.z)
		# emit sound
		globals.play_sound("cannon_shot", null, true, false, bullet.global_transform.origin)
		
		ammo_in_turret -= 1
		
	node_flash_one.visible = true
	node_flash_two.visible = true
	
	flash_timer = FLASH_TIME
	fire_timer = FIRE_TIME
	
	if ammo_in_turret <= 0:
		ammo_reload_timer = AMMO_RELOAD_TIME


func bullet_hit(damage, bullet_global_transform,
		_raycast=false, _with_hit=false):
	if enabled and (turret_health > 0):
		turret_health -= damage
		if turret_health <= 0:
			fire_holder.emit(true)
			destroyed_timer = DESTROYED_TIME
		
	# play sound
	globals.play_sound("vehicle_collision_static", null, true, false, 
			bullet_global_transform.origin)


func rocket_hit(damage, rocket_global_transform):
	if enabled and (turret_health > 0):
		turret_health -= damage
		if turret_health <= 0:
			fire_holder.emit(true)
			destroyed_timer = DESTROYED_TIME
		
	# play sound
	globals.play_sound("vehicle_collision_static", null, true, false, 
			rocket_global_transform.origin)


func raycast_hit(damage, hit_position):
	if enabled and (turret_health > 0):
		turret_health -= damage
		if turret_health <= 0:
			fire_holder.emit(true)
			destroyed_timer = DESTROYED_TIME
		
	# play sound
	globals.play_sound("vehicle_collision_static", null, true, false, 
			hit_position)


func _body_entered_vision(body):
	if enabled:
		if current_target == null:
			if (body is RigidBody3D) and (body.collision_layer & 0b110):
				# current target is Vehicle or its trailer (if any)
				current_target = body
				set_physics_process(true)
				if aimer_use:
					aimer.enabled = true
				# add a visibility check and place it at turret's lookout global position
				visibility_check = RayCast3D.new()
				globals.get_current_scene().add_child(visibility_check)
				
				# remove any turret's scaling effect
				var pos = $Head/BarrelEnd/LookOut.global_transform.origin
				var p_scale = $Head/BarrelEnd/LookOut.global_transform.basis.get_scale()
				pos = Vector3(pos.x / p_scale.x, pos.y / p_scale.y, pos.z / p_scale.z)
				visibility_check.global_transform.origin = pos
				visibility_check_position = pos
				
				# get visible target
				visible_target = current_target.visible_target
				# visibility check against Vehicle, its trailer
				# (if any) and environment
				visibility_check.collision_mask = 0b111
				visibility_check.enabled = true


func _body_exited_vision(body):
	if enabled:
		if current_target != null:
			if body == current_target:
				current_target = null
				set_physics_process(false)
				if aimer_use:
					aimer.enabled = false
				
				flash_timer = 0
				fire_timer = 0
				node_flash_one.visible = false
				node_flash_two.visible = false
				#
				visibility_check.enabled = false
				visible_target = null
				visibility_check.queue_free()


#<AI
func _ai_initialize():
	var character_position = node_turret_head.global_transform.origin
	if ai_dims == AI_TOOLS.Dimensions.TWO:
		ai_face.character.position = Vector2(character_position.x,
				character_position.z)
	else:
		ai_face.character.position = character_position
	ai_face.character.orientation = AI_TOOLS.map_to_range(
			node_turret_head.global_transform.basis.get_euler().y)


func _ai_pre_update(_delta):
	# steering depends on: target.position, character.position
	var p_position = visible_target.global_transform.origin
	var character_position = node_turret_head.global_transform.origin
	if ai_dims == AI_TOOLS.Dimensions.TWO:
		ai_face.target.position = Vector2(p_position.x, p_position.z)
		ai_face.character.position = Vector2(character_position.x,
				character_position.z)
	else:
		ai_face.target.position = p_position
		ai_face.character.position = character_position


func _ai_update(delta):
	var output:AI.SteeringOutput = ai_face.get_steering()
	ai_face.character.update(output, delta)


func _ai_post_update(_delta):
	node_turret_head.global_transform.basis = start_basis.rotated(Vector3.UP, 
			ai_face.character.orientation)
#AI>


func _set_fire_enabled(value):
	fire_enabled = value


func _set_vision_area_radius(value):
	vision_area_radius = value


func _get_class():
	return "Turret"


func _is_class(value):
	return "Turret" == value


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)
