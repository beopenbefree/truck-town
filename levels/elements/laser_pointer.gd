class_name LaserPointer
extends RayCast3D


@export_range(0.0, 100.0, 1.0, "or_greater") var beam_max_length: float = 50.0
@export_range(0.0, 2.0, 0.005, "or_greater") var beam_thickness: float = 0.2
@export var draw_beam: bool = true
@export var fast_compute: bool = true

@onready var spot: Decal = $Spot
@onready var beam := MeshInstance3D.new()
@onready var beam_mesh := ImmediateMesh.new()
@onready var beam_material := StandardMaterial3D.new()
@onready var beam_start := Vector3.ZERO
@onready var beam_end := Vector3.ZERO
@onready var beam_length: float = 0.0 # option 1

@onready var beam_thickness_start: float = beam_thickness * 0.05
@onready var beam_thickness_end: float = beam_thickness * 0.2
const cos_210 = cos(deg_to_rad(210.0))
const sin_210 = sin(deg_to_rad(210.0))
const cos_330 = cos(deg_to_rad(330.0))
const sin_330 = sin(deg_to_rad(330.0))


func _ready():
	target_position = Vector3(0.0, beam_max_length, 0.0)
	beam_start = global_position
	beam_end = to_global(target_position)
	beam_length = (beam_end - beam_start).length() # option 1
	# drawing stuff
	beam.mesh = beam_mesh
	beam.top_level = true
	beam.cast_shadow = GeometryInstance3D.SHADOW_CASTING_SETTING_OFF
	add_child(beam)
	beam_material.transparency = BaseMaterial3D.TRANSPARENCY_ALPHA
	beam_material.shading_mode = BaseMaterial3D.SHADING_MODE_PER_VERTEX
	beam_material.cull_mode = BaseMaterial3D.CULL_DISABLED
	beam_material.albedo_color = Color.hex(0xff00007f)


func _physics_process(_delta):
	beam_start = global_position
	if is_colliding():
		beam_end = get_collision_point()
		# option 1
		beam_length = (beam_end - beam_start).length()
		# compute spot transform
		var spot_transform = Transform3D.IDENTITY
		spot_transform.origin = beam_end
		# compute spot tranform
		if fast_compute:
			# fast computation
			spot_transform.basis = global_basis
		else:
			# slower, more accurate, computation
			var up = get_collision_normal()
			var right
			var forward
			if abs(up.y) <= max(abs(up.x), abs(up.z)):
				# up ~ RIGHT or up ~ FORWARD
				right = up.cross(Vector3.UP)
				forward = up.cross(right)
			else:
				# up ~ UP
				right = Vector3.FORWARD.cross(up)
				forward = up.cross(right)
			spot_transform.basis.y = up
			spot_transform.basis.x = right
			spot_transform.basis.z = -forward
		#
		spot.global_transform = spot_transform
		spot.visible = true
	else:
		spot.visible = false
		# option 1
		beam_end = beam_start + global_basis.y.normalized() * beam_length
		# option 2
		#beam_end = beam_start + global_basis.y.normalized() * beam_max_length
	# draw the beam mesh
	if draw_beam:
		draw_beam_mesh(beam_start, beam_end)


func draw_beam_mesh(start:Vector3, end:Vector3):
		# HACK: rotated -90 along z, so up direction is -x
		#var f = global_basis.y
		var r = global_basis.z
		var u = -global_basis.x
		var p1s = start + u * beam_thickness_start
		var p1e = end + u * beam_thickness_end
		var p2s = start + (r * cos_210 + u * sin_210) * beam_thickness_start
		var p2e = end + (r * cos_210 + u * sin_210) * beam_thickness_end
		var p3s = start + (r * cos_330 + u * sin_330) * beam_thickness_start
		var p3e = end + (r * cos_330 + u * sin_330) * beam_thickness_end
		beam_mesh.clear_surfaces()
		beam_mesh.surface_begin(Mesh.PRIMITIVE_TRIANGLE_STRIP,
				beam_material)
		# strip
		beam_mesh.surface_add_vertex(p1s)
		beam_mesh.surface_add_vertex(p1e)
		beam_mesh.surface_add_vertex(p3s)
		beam_mesh.surface_add_vertex(p3e)
		beam_mesh.surface_add_vertex(p2s)
		beam_mesh.surface_add_vertex(p2e)
		beam_mesh.surface_add_vertex(p1s)
		beam_mesh.surface_add_vertex(p1e)
		beam_mesh.surface_end()


func _get_class():
	return "LaserPointer"


func _is_class(value):
	return "LaserPointer" == value
