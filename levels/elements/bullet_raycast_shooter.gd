class_name BulletRayCastShooter
extends Node3D


@export_flags_3d_physics var collisions: int = 0b0

@export_range(0, 100, 1) var DAMAGE: int = 10

@export var with_hole: bool = true
@export var with_dust: bool = true
@export var hole_dissolve_time: float = 7.0

var bullet_hole_scene: PackedScene = preload("res://levels/elements/bullet_hole.tscn")

var collision_detector: AI.CollisionDetector = null


func _ready():
	collision_detector = AI.CollisionDetector.new(self)
	collision_detector.collision_mask = collisions


func shoot(from:Vector3, to:Vector3):
		var collision = collision_detector.get_collision(from, to - from)
		if collision:
			var body = collision_detector._result["collider"]
			#
			if body.has_method("bullet_hit"):
				# compute global collision transform: +z = -normal
				var z_axis = collision.normal
				var y_axis = -Vector3.FORWARD.cross(z_axis)
				var x_axis = y_axis.cross(z_axis)
				var collision_transform = Transform3D(
						Basis(x_axis, y_axis, z_axis), collision.position)
				body.bullet_hit(DAMAGE, collision_transform, true)
			elif (body.collision_layer & 0b1):
				# collided with environment: play sound
				globals.play_sound("environment_collision_dynamic", null, true, false, 
						collision.position)
			if with_hole:
				# instantiate bullet hole
				var bullet_hole = bullet_hole_scene.instantiate()
				bullet_hole.dissolve_time = hole_dissolve_time
				body.add_child(bullet_hole)
				
				# compute global hole transform: +y = normal
				var y_axis = collision.normal
				var z_axis = Vector3.UP.cross(y_axis)
				var x_axis = y_axis.cross(z_axis)
				bullet_hole.global_transform = Transform3D(
						Basis(x_axis, y_axis, z_axis), collision.position)
				
				if with_dust:
					# emit some dust
					bullet_hole.emit_dust()


func _get_class():
	return "BulletRayCastShooter"


func _is_class(value):
	return "BulletRayCastShooter" == value
