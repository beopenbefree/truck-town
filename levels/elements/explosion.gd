class_name Explosion
extends EffectBase


@export var add_effects: bool = true
@export var explosion_crater_duration: float = 10.0

var explosion_crater_scene: PackedScene = preload("res://levels/elements/explosion_crater.tscn")

@onready var flares_smoke: GPUParticles3D = $FlaresSmoke


func _ready():
	effects = [flares_smoke]
	super()
	emit(false)


func emit(enable:bool, sound_root:Node3D=self,
		decoration_parent:VisualInstance3D=null):
	if enable and (not is_emitting()):
		flares_smoke.set_deferred("emitting", enable)
		
		if add_effects:
			# add explosion crater decoration (decal)
			var explosion_crater = explosion_crater_scene.instantiate()
			explosion_crater.dissolve_time = explosion_crater_duration
			explosion_crater.size *= sqrt(abs(scaling))
			if decoration_parent:
				# decoration parent is a VisualInstance3D: add decoration to it
				decoration_parent.add_child(explosion_crater)
			else:
				# null decoration parent: add decoration to current scene
				globals.get_current_scene().add_child(explosion_crater)
			explosion_crater.global_transform.origin = global_transform.origin
		
		# emit sound
		# HACK: see EffectBase
		sound_emitted = globals.play_sound(sound_name, sound_root)
	elif (not enable) and is_emitting():
		flares_smoke.set_deferred("emitting", enable)
		# clear sound
		globals.clear_sound(sound_name, sound_root)


func set_effects_scale(value:float):
	super(value)
	for effect in effects:
		effect.process_material.emission_sphere_radius *= value


func _get_class():
	return "Explosion"


func _is_class(value):
	return "Explosion" == value
