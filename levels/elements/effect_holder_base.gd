class_name EffectHolderBase
extends RemoteTransform3D


@export var effect_scene: PackedScene = PackedScene.new()

@export var effect_sound: String = String()
@export var effect_scaling: float = 1.0

@onready var effect_vfx: EffectBase = effect_scene.instantiate()


func _ready():
	call_deferred("_ready_deferred")


func _exit_tree():
	if is_instance_valid(effect_vfx):
		effect_vfx.destroy_on_finished()


func is_emitting():
	var result = false
	if effect_vfx.is_inside_tree():
		result = effect_vfx.is_emitting()
	return result


func _ready_deferred():
	# set effect related stuff
	effect_vfx.sound_name = effect_sound
	effect_vfx.scaling = effect_scaling
	globals.get_current_scene().add_child(effect_vfx)
	remote_path = effect_vfx.get_path()
	effect_vfx.emit(false)


func _get_class():
	return "EffectHolderBase"


func _is_class(value):
	return "EffectHolderBase" == value
