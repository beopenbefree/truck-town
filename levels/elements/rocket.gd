class_name Rocket
extends RigidBody3D


@export_range(0.0, 1.0, 0.01) var impulse_decreasing_factor: float = 0.8
@export_range(0, 200, 1) var DAMAGE: int = 80
@export var scaling: Vector3 = Vector3.ONE: set = _set_scaling
@export var add_effects: bool = true

@export var rocket_sound_name: String = String()

var initial_impulse_size: float = 0.0
var current_impulse_size: float = 0.0
var direction := Vector3.ZERO
var firing: bool = false
# HACK: rocket start position correction
var firing_just_started: bool = false

var LIFE_TIME: float = 4.0
var timer: float = 0
var hit_something: bool = false
var exploded: bool = false

const EXPLOSION_WAIT_TIME: float = 0.48

var explosion_wait_timer: float = 0

var rocket_hole_scene: PackedScene = preload("res://levels/elements/rocket_hole.tscn")

@onready var rigid_shape1: CollisionShape3D = $CollisionShape1
@onready var rigid_shape2: CollisionShape3D = $CollisionShape2
@onready var rocket_mesh: MeshInstance3D = $RocketMesh
@onready var blast_area: Area3D = $BlastArea
@onready var explosion_holder: ExplosionHolder = $ExplosionHolder
@onready var trail: GPUParticles3D = $Trail


func _ready():
	# HACK: call setters with (potential) side effects
	scaling = scaling
	#
	explosion_holder.emit(false)
	(explosion_holder.effect_vfx as Explosion).add_effects = false
	# adjust life time
	LIFE_TIME = max(LIFE_TIME, EXPLOSION_WAIT_TIME)
	# connect body_entered signal
	body_entered.connect(_on_Body_body_entered)


func _physics_process(delta):
	if hit_something:
		# stop hitting and firing
		hit_something = false
		firing = false
		
		# stop emitting trail
		trail.emitting = false
		# stop emitting sound
		globals.clear_sound(rocket_sound_name, self)
		
		# rise explosion
		if not exploded:
			exploded = true
			explosion_holder.emit(true)
			
			rocket_mesh.visible = false
			freeze_mode = RigidBody3D.FREEZE_MODE_STATIC
			freeze = true
			
			var bodies = blast_area.get_overlapping_bodies()
			for body in bodies:
				if body.has_method("rocket_hit"):
					body.rocket_hit(DAMAGE, 
							body.global_transform.looking_at(
									global_transform.origin, Vector3(0,1,0)) )
		else:
			# wait for explosion completion
			if explosion_wait_timer < EXPLOSION_WAIT_TIME:
				explosion_wait_timer += delta
				if explosion_wait_timer >= EXPLOSION_WAIT_TIME:
					queue_free()
	if firing:
		# apply impulse/force continuously and decreasingly
		#apply_impulse(current_impulse_size * direction, Vector3.ZERO)
		apply_force(current_impulse_size * direction, Vector3.ZERO)
		current_impulse_size *= impulse_decreasing_factor
		direction = -global_transform.basis.z
		if current_impulse_size < initial_impulse_size * 0.01:
			firing = false
	# anyway free itself after life time
	timer += delta
	if timer > LIFE_TIME:
		queue_free()


# HACK: rocket start position correction due to vehicle movement
func _integrate_forces(state: PhysicsDirectBodyState3D) -> void:
	if firing_just_started:
		global_position += state.linear_velocity * state.step
		firing_just_started = false


func fire(impulse_size:float, p_direction:Vector3):
	initial_impulse_size = impulse_size
	current_impulse_size = impulse_size
	direction = p_direction.normalized()
	firing = true
	# HACK: rocket start position correction
	firing_just_started = true
	
	# emit sound
	globals.play_sound(rocket_sound_name, self)
	# emit trail
	trail.emitting = true


func set_collision(layer, mask):
	collision_layer = layer
	collision_mask = mask
	blast_area.collision_layer = layer
	blast_area.collision_mask = mask


func _on_Body_body_entered(body:PhysicsBody3D):
	if hit_something == false:
		hit_something = true
		if add_effects:
			# add rocket hole decal
			var rocket_hole = rocket_hole_scene.instantiate()
			# get decoration parent
			var decoration_parent = null
			# HACK: a physics body should have a parent or
			# a child that is a VisualInstance3D
			if (body.get_parent() is VisualInstance3D):
				decoration_parent = body.get_parent()
			else:
				decoration_parent = ScriptLibrary.find_nodes_by_type(
						VisualInstance3D, body, false)[0]
			decoration_parent.add_child(rocket_hole)
			rocket_hole.global_transform = global_transform * Transform3D(
				Basis(Vector3.RIGHT, deg_to_rad(90)), Vector3.ZERO)
		
		# Alternartive (more precise) way FIXME
		#var delta_dist = 20.0
		#var forward_dir = -global_transform.basis.z
		#var rocket_pos = global_transform.origin
		#var from = rocket_pos - forward_dir * delta_dist
		#var to = from + forward_dir * delta_dist * 2.0
		#var raycast_params = PhysicsRayQueryParameters3D.create(from, to)
		#var space_state = get_world_3d().direct_space_state
		#var result = space_state.intersect_ray(raycast_params)
		#if not result:
			#return
		#var hole_pos = result["position"]
		#var hole_lookat = hole_pos + result["normal"] * delta_dist
		#var hole_transform = Transform3D(Basis.IDENTITY, hole_pos)
		#hole_transform = hole_transform.looking_at(hole_lookat, 
				#Vector3.UP, true)
		#hole_transform = hole_transform * Transform3D(
			#Basis(Vector3.RIGHT, deg_to_rad(90)), Vector3.ZERO)
		#rocket_hole.global_transform = hole_transform


func _set_scaling(value):
	scaling = value
	$RocketMesh.scale = value
	$CollisionShape1.scale = value
	$CollisionShape2.scale = value


func _get_class():
	return "Rocket"


func _is_class(value):
	return "Rocket" == value
