class_name ExplosionHolder
extends EffectHolderBase


@export_range(0.0, 10.0, 0.01, "or_greater") var explosion_lifetime: float = 1.0

var decoration_parent:VisualInstance3D = null


# mandatory method
func emit(enable:bool):
	# HACK: wait one frame as the holder may have changed
	# its transform in this frame and will therefore apply
	# it on the held effect, during the next frame
	# WARNING: don't use await
	_deferred_emit.call_deferred(enable)


func _ready_deferred():
	super()
	(effect_vfx as Explosion).effects[0].lifetime = explosion_lifetime


func _deferred_emit(enable:bool):
	if is_instance_valid(effect_vfx) and effect_vfx.is_inside_tree():
		(effect_vfx as Explosion).emit(enable, self, decoration_parent)


func _get_class():
	return "ExplosionHolder"


func _is_class(value):
	return "ExplosionHolder" == value
