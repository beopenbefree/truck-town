class_name LevelScene
extends Node3D


# items whose number depends on current session
@export var session_gameplay_elements: Array[PackedScene] = []
@export var session_scene_elements: Array[PackedScene] = []
@export var session_vehicle_ai_drivers: Array[PackedScene] = []
@export_range(-1, 10, 1, "or_greater") var fixed_num_vehicles_ai: int = -1
# navigation mesh data
@export var navmesh_agent_radius: float = 2.0

@export_range(0.0, 10.0, 1.0, "or_greater") var aggression_variation_percent: float = 5.0
@export_range(0.0, 10.0, 1.0, "or_greater") var aggression_variation_percent_max: float = 20.0

# OPTIMIZATION: objects whose collision shapes can and will be disabled after 
# the initialization procedure. EXAMPLE: objects used exclusively as mask
# objects for element placement purposes
@export var post_init_disabled_object_paths: Array[NodePath] = []


var total_count_down: float
var total_count_down_inv: float
var current_count: float
const DEFAULT_WORLD_ENVIRONMENT: String = "res://levels/elements_scene/world_environment.tscn"
const DEFAULT_DIRECTIONAL_LIGHT: String = "res://levels/elements_scene/directional_light.tscn"

const vehicle_vanish_effect_scene: PackedScene = preload("res://levels/elements/smoke_cloud.tscn") 

@onready var starting: bool = true
@onready var updating_level: bool = false
@onready var toggle_mute_sounds = $HUD/MarginContainer/VBoxContainer/HBoxContainer3/ToggleMuteSounds
@onready var time_counter = $HUD/MarginContainer/VBoxContainer/HBoxContainer/TimeCounter
@onready var speedometer = $HUD/MarginContainer/VBoxContainer/HBoxContainer2/Dashboard/Speedometer
@onready var vehicles_ai_drivers_parent = $VehicleDriversAI
@onready var gameplay_elements_parent = $GameplayElements
@onready var all_vehicle_ai_drivers: Array[VehicleAIDriver] = []
@onready var vehicle_camera_idx: int = 0
@onready var vehicle_labels_show: bool = true
# navigation region
@onready var navigation_region: NavigationRegion3D = ScriptLibrary.find_nodes_by_type(
		NavigationRegion3D, self, false)[0]
@onready var nav_mesh_backing_time: float = 0.0


func _ready():
	starting = true
	updating_level = false
	set_process(true)
	_setup_environment()
	# play fade anim in reverse
	$FadeAnimation.animation_finished.connect(_on_fade_animation_finished)
	$FadeAnimation.play_backwards("fade")
	_set_hud_visible(false)
	total_count_down = globals.LEVELS[globals.current_level_idx]["total_count_down"]
	total_count_down_inv = 1.0 / total_count_down
	current_count = total_count_down
	# set the dynamic number scene elements: they contribute to navigation region 
	_set_session_items(session_scene_elements, navigation_region)
	# set the dynamic number gameplay elements: they do not contribute to navigation region
	_set_session_items(session_gameplay_elements, gameplay_elements_parent)
	# bake the NavigationMeshInstance
	if navigation_region:
		navigation_region.placements_finished_signal.connect(
			 _bake_navigation_mesh_instance)
	else:
		# OPTIMIZATION
		_post_init_disable_objects()
	# by default at level start aim with mouse
	if Input.mouse_mode == Input.MOUSE_MODE_VISIBLE:
		globals._toggle_aim_with_mouse()


func _unhandled_input(event):
	var handled_input:bool = false
	# HACK: use only SHOW GAME MENU
	if event.is_action_pressed("show_game_menu"):
		handled_input = true
		globals._show_game_menu_popup()
	if event.is_action_pressed("want_quit"):
		handled_input = true
		globals._show_want_quit_popup()
	if event.is_action_pressed("pause"):
		handled_input = true
		globals._show_game_pause_popup()
	#
	if event.is_action_pressed("unlock_vehicle"):
		handled_input = true
		globals._unlock_vehicle()
	if event.is_action_pressed("aim_with_mouse"):
		handled_input = true
		globals._toggle_aim_with_mouse()
	if event.is_action_pressed("toggle_mute"):
		handled_input = true
		globals._enable_mute_all_sounds(toggle_mute_sounds)
	if event.is_action_pressed("music_up", true):
		handled_input = true
		var music_name = globals.get_song_name(globals.music_track_index)
		var volume_db = globals.get_song_volume(music_name)
		if volume_db:
			volume_db += 3.0
			globals.set_song_volume(music_name, volume_db)
	if event.is_action_pressed("music_down", true):
		handled_input = true
		var music_name = globals.get_song_name(globals.music_track_index)
		var volume_db = globals.get_song_volume(music_name) - 3.0
		globals.set_song_volume(music_name, volume_db)
	# HACK: zoom
	if event.is_action_pressed("zoom") or event.is_action_released("zoom"):
		handled_input = true
		var real_camera = get_tree().root.get_camera_3d()
		if real_camera is RealCamera:
			var enable = true if event.is_action_pressed("zoom") else false
			real_camera.camera_base.zoom(enable)
	#  
	if is_instance_valid(globals.current_vehicle):
		if event.is_action_pressed("swap_camera"):
			handled_input = true
			globals.swap_camera()
			# HACK: 1
			var current_camera = globals.get_current_camera()
			var vehicle_body = globals.current_vehicle_body
			var shooter = vehicle_body.shooter_node
			if "hit_point" in current_camera:
				shooter.aimer_use = true
				shooter.set_aimer_target(current_camera.hit_point)
			else:
				shooter.aimer_use = false
				shooter.set_aimer_target(null)
				shooter.aimer.transform = Transform3D.IDENTITY
			# HACK: 2
			var camera_group = vehicle_body.camera_group_node
			camera_group.set_current_camera_rotator_pitch_curve(
					shooter.current_weapon_name)
		if event.is_action_pressed("aim") or event.is_action_released("aim"):
			handled_input = true
			var shooter = globals.current_vehicle_body.shooter_node
			var weapon = shooter.weapons[shooter.current_weapon_name]
			if weapon and weapon.view_finder:
				var camera_group = globals.current_vehicle_body.camera_group_node
				var aim_on = event.is_action_pressed("aim")
				camera_group.get_current_camera().current = false if aim_on else true
				weapon.view_finder.current = true if aim_on else false
		if event.is_action_pressed("spy_on_enemies"):
			handled_input = true
			# consider all cameras
			var vehicles_num: int = all_vehicle_ai_drivers.size() + 1
			var delta: int = 1 if not event.shift_pressed else -1
			# get current vehicle camera
			var current_vehicle_camera:CameraBase = globals.get_current_camera()
			# select the camera index to set current and enabled
			vehicle_camera_idx += delta
			if vehicle_camera_idx >= vehicles_num:
				vehicle_camera_idx -= vehicles_num
			elif vehicle_camera_idx < 0:
				vehicle_camera_idx += vehicles_num
			if vehicle_camera_idx == 0:
				# enable current vehicle camera
				current_vehicle_camera.current = true
				current_vehicle_camera.enabled = true
				# disable all others
				for vehicle_ai_driver in all_vehicle_ai_drivers:
					var vehicle_ai_camera = ScriptLibrary.find_nodes_by_type(CameraBase,
						vehicle_ai_driver, false)[0]
					if vehicle_ai_camera:
						# vehicle ai camera exists
						vehicle_ai_camera.current = false
						vehicle_ai_camera.enabled = false
			else:
				# enable selected vehicle ai camera
				for i in all_vehicle_ai_drivers.size():
					var vehicle_ai_driver = all_vehicle_ai_drivers[i]
					var vehicle_ai_camera = ScriptLibrary.find_nodes_by_type(CameraBase,
							vehicle_ai_driver, false)[0]
					if vehicle_ai_camera:
						# vehicle ai camera exists
						if i == (vehicle_camera_idx - 1):
							# enable selected vehicle ai camera
							vehicle_ai_camera.current = true
							vehicle_ai_camera.enabled = true
							# disable current vehicle camera
							current_vehicle_camera.current = false
							current_vehicle_camera.enabled = false
						else:
							# disable all others
							vehicle_ai_camera.current = false
							vehicle_ai_camera.enabled = false
		if event.is_action_pressed("toggle_labels"):
			handled_input = true
			vehicle_labels_show = not vehicle_labels_show
			globals.current_vehicle.find_child("VehicleLabel", true).visible = vehicle_labels_show
			for driver_ai in all_vehicle_ai_drivers:
				driver_ai.find_child("VehicleLabel", true).visible = vehicle_labels_show
		if event.is_action_pressed("toggle_speed_unit"):
			handled_input = true
			speedometer.toggle_speed_unit()
	#
	if handled_input:
		get_viewport().set_input_as_handled()


func _physics_process(delta):
	if starting:
		return
	
	if current_count >= 0:
		update_counter(current_count)
	elif not updating_level:
		# time out: restart level (if possible)
		updating_level = true
		globals.update_level(globals.current_vehicle_body, false)
	
	current_count -= delta


func update_counter(count):
	# count normalized in [0, 1]
	time_counter.update_counter(count * total_count_down_inv, total_count_down)


func add_vehicle_ai_driver(vehicle_ai_driver_placer:ObjectsPlacer=null):
	if not vehicle_ai_driver_placer:
		# create a randomly selected vehicle ai driver placer from the list
		var random_indices: Array[int] = globals.shuffled_indices(
					session_vehicle_ai_drivers.size())
		vehicle_ai_driver_placer = (
				session_vehicle_ai_drivers[random_indices[0]].instantiate())
	
	# HACK: actual current camera (see Camera3D's reference documentation)
	var active_camera:RealCamera = (get_tree().root.get_camera_3d() as RealCamera)
	var current_camera:CameraBase = active_camera.camera_base
	
	# actual creation
	vehicle_ai_driver_placer.objects_number = 1
	# NOTE: vehicle ai driver is randomly placed when added to scene tree
	vehicles_ai_drivers_parent.add_child(vehicle_ai_driver_placer)
	# store the new vehicle ai driver into all_vehicle_ai_drivers
	var new_vehicle_ai_driver = ScriptLibrary.find_nodes_by_type(
			VehicleAIDriver, vehicle_ai_driver_placer, false)[0]
	if new_vehicle_ai_driver:
		all_vehicle_ai_drivers.append(new_vehicle_ai_driver)
	# post configuration of the new vehicle ai driver
	# started/finished repair signal connection
	new_vehicle_ai_driver.repair_started.connect(_on_vehicle_ai_driver_repair_started)
	new_vehicle_ai_driver.repair_finished.connect(_on_vehicle_ai_driver_repair_finished)
	
	# HACK: restore current camera
	current_camera.current = true
	current_camera.enabled = true
	
	# handle aggression of the newly added vehicle ai
	var new_vehicle_ai_model:VehicleAIModel = ScriptLibrary.find_nodes_by_type(
			VehicleAIModel, new_vehicle_ai_driver, false)[0]
	# aggression increases with each session up to a maximum
	var aggression_increment_percent = aggression_variation_percent * globals.current_game_session
	aggression_increment_percent = min(aggression_increment_percent, aggression_variation_percent_max)
	new_vehicle_ai_model.aggression.increment(aggression_increment_percent)
	for vehicle_ai_driver in all_vehicle_ai_drivers:
		# update targets for all the vehicle ai drivers
		vehicle_ai_driver.assign_targets_to_behaviors(all_vehicle_ai_drivers)


func remove_vehicle_ai_driver(vehicle_ai_driver:VehicleAIDriver):
	if is_instance_valid(vehicle_ai_driver):
		var vehicle_ai_body = vehicle_ai_driver.vehicle_body
		var vehicle_ai_material:BaseMaterial3D = vehicle_ai_body.chassis.get_surface_override_material(0)
		var vehicle_ai_body_position = vehicle_ai_body.get_node("VisibleTarget").global_position
		var vehicle_ai_color = vehicle_ai_material.albedo_color
		# free vehicle ai driver
		vehicle_ai_driver.queue_free()
		# remove vehicle ai driver from all_vehicle_ai_drivers
		all_vehicle_ai_drivers.erase(vehicle_ai_driver)
		# post configuration for the remaining vehicle ai drivers
		for rem_vehicle_ai_driver in all_vehicle_ai_drivers:
			# update targets for all the vehicle ai drivers
			rem_vehicle_ai_driver.assign_targets_to_behaviors(all_vehicle_ai_drivers)
		# create vanish effect
		if vehicle_vanish_effect_scene:
			var vehicle_vanish_effect = vehicle_vanish_effect_scene.instantiate()
			vehicles_ai_drivers_parent.add_child(vehicle_vanish_effect)
			var inner_effect:GPUParticles3D = vehicle_vanish_effect.effects[0]
			inner_effect.process_material.color = vehicle_ai_color * 1.5
			vehicle_vanish_effect.global_position = vehicle_ai_body_position
			vehicle_vanish_effect.emit_effects(true)
			# wait the effect end
			await inner_effect.finished
			# remove effect from the scene
			vehicle_vanish_effect.queue_free()


func _setup_environment():
	var conf_opts = globals.current_configuration_options
	if not conf_opts["low_end_device"][0]:
		# add a world environment (if not present)
		var world:WorldEnvironment = get_node_or_null("WorldEnvironment")
		if not world:
			world = load(DEFAULT_WORLD_ENVIRONMENT).instantiate()
			add_child(world)
		# add a directional light
		var light:Light3D = get_node_or_null("DirectionalLight3D")
		if not light:
			light = load(DEFAULT_DIRECTIONAL_LIGHT).instantiate()
			add_child(light)
		# set the effects up
		if world and light:
			for effect in globals.EFFECTS:
				var effect_enabled = conf_opts[effect][0]
				# try if effect is in world.environment
				if (world.environment) and (effect in world.environment):
					world.environment.set(effect, effect_enabled)
				# try if effect is in world.camera_attributes
				if (world.camera_attributes) and (effect in world.camera_attributes):
					world.camera_attributes.set(effect, effect_enabled)
				# try if effect is in light
				if effect in light:
					light.set(effect, effect_enabled)


func _on_fade_animation_finished(_anim):
	# clear current vehicle engine sound(s)
	globals.clear_sound(globals.current_vehicle_sound, 
			globals.current_vehicle_body)
	
	if starting:
		# play current truck engine sound
		globals.play_sound(globals.current_vehicle_sound, 
				globals.current_vehicle_body, false, true)
		var sound = globals.get_sound_first(globals.current_vehicle_sound, 
				globals.current_vehicle_body)
		if globals.current_vehicle:
			globals.current_vehicle.get_node("Body").current_sound = sound
		# start background music
		_start_background_music()
		# wait until audio reaches clip's loop start position (if any)
		if globals.audio_clips_loop_start.has(globals.current_vehicle_sound):
			await get_tree().create_timer(globals.audio_clips_loop_start[globals.current_vehicle_sound]).timeout
		
		# re-enable physics for current truck
		if globals.current_vehicle:
			globals.current_vehicle.get_node("Body").set_physics_process(true)
		starting = false
		_set_hud_visible(true)


func _set_hud_visible(enable):
	$HUD.visible = enable
	$HUD/MarginContainer/VBoxContainer.visible = enable
	# starts with the score text in red
	var score_label = $HUD/MarginContainer/VBoxContainer/HBoxContainer2/ScoreInfos
	score_label.set_score_label_color(Color(1.0, 0.0, 0.0))


func _start_background_music():
	# play random music track
	globals.play_random_song(true)


func _bake_navigation_mesh_instance():
	# In a multi-threaded environment to start baking the 
	# NavigationMeshInstance we need to wait that 
	# all placements are finished
	assert(navigation_region.placements_finished)
	# all placements are ready so start baking
	navigation_region.bake_finished.connect(_on_nav_mesh_ready)
	navigation_region.navigation_mesh.agent_radius = navmesh_agent_radius
	navigation_region.bake_navigation_mesh(true)
	nav_mesh_backing_time = Time. get_unix_time_from_system()
	# defer _set_session_vehicle_ai_drivers()


func _on_nav_mesh_ready():
	print("nav mesh backing time: ", 
			Time.get_unix_time_from_system() - nav_mesh_backing_time)
	await get_tree().process_frame
	# ai vehicles
	_set_session_vehicle_ai_drivers()
	
	# OPTIMIZATION
	_post_init_disable_objects()


func _set_session_items(element_list, parent_node):
	var current_level_increasing_factor = globals.current_level_increasing_factor()
	for element in element_list:
		var element_node = element.instantiate()
		# compute allowed objects for current game session
		var elements_number = int(element_node.objects_number * 
				current_level_increasing_factor)
		element_node.objects_number = elements_number
		# NOTE: elements are randomly placed when added to scene tree
		parent_node.add_child(element_node)


func _set_session_vehicle_ai_drivers():
	# compute the number of vehicles ai to create
	var vehicles_number: int
	if fixed_num_vehicles_ai >= 0:
		# set the fixed number of vehicles ai
		vehicles_number = fixed_num_vehicles_ai
	else:
		# default -> compute allowed vehicles ai for
		# current game session: at least two of them
		vehicles_number = clampi(floori(2.0 + globals.current_game_session * 0.5),
				1, globals.GAME_DATA["max_vehicle_ai_number"])
	var list_size = session_vehicle_ai_drivers.size()
	if list_size <= 0:
		return
	# clear vehicle ai drivers (if any)
	for child in vehicles_ai_drivers_parent.get_children():
		child.queue_free()
	all_vehicle_ai_drivers.clear()
	# to create, randomly select all the vehicles ai in the list
	# one at a time; repeat the procedure until you have completed
	# the required number of vehicles
	var random_indices: Array[int]
	for i in vehicles_number:
		var list_idx = i % list_size
		if list_idx == 0:
			# repeat the index shuffle for the next set of list_size vehicles ai
			random_indices = globals.shuffled_indices(list_size)
		var idx = random_indices[list_idx]
		var vehicle_ai_driver_placer:ObjectsPlacer = (
				session_vehicle_ai_drivers[idx].instantiate())
		vehicle_ai_driver_placer.objects_number = 1
		# NOTE: vehicle ai driver is randomly placed when added to scene tree
		vehicles_ai_drivers_parent.add_child(vehicle_ai_driver_placer)
		# store vehicle ai driver into all_vehicle_ai_drivers
		var vehicle_ai_driver = ScriptLibrary.find_nodes_by_type(
				VehicleAIDriver, vehicle_ai_driver_placer, false)[0]
		if vehicle_ai_driver:
			all_vehicle_ai_drivers.append(vehicle_ai_driver)
	
	# post configuration
	for vehicle_ai_driver in all_vehicle_ai_drivers:
		# started/finished repair signal connection
		vehicle_ai_driver.repair_started.connect(_on_vehicle_ai_driver_repair_started)
		vehicle_ai_driver.repair_finished.connect(_on_vehicle_ai_driver_repair_finished)
		# update targets for all the vehicle ai drivers
		vehicle_ai_driver.assign_targets_to_behaviors(all_vehicle_ai_drivers)
		# handle aggression of vehicle ai
		var vehicle_ai_model:VehicleAIModel = ScriptLibrary.find_nodes_by_type(
				VehicleAIModel, vehicle_ai_driver, false)[0]
		# aggression increases with each session up to a maximum
		var aggression_increment_percent = aggression_variation_percent * globals.current_game_session
		aggression_increment_percent = min(aggression_increment_percent, aggression_variation_percent_max)
		vehicle_ai_model.aggression.increment(aggression_increment_percent)


func _post_init_disable_objects():
	for object_path in post_init_disabled_object_paths:
		var object = get_node(object_path)
		var collision_shape: CollisionShape3D = ScriptLibrary.find_nodes_by_type(CollisionShape3D,
				object, false)[0]
		if collision_shape:
			collision_shape.disabled = true


func _on_vehicle_ai_driver_repair_started(_vehicle_ai_driver:VehicleAIDriver):
	add_vehicle_ai_driver()


func _on_vehicle_ai_driver_repair_finished(vehicle_ai_driver:VehicleAIDriver):
	remove_vehicle_ai_driver(vehicle_ai_driver)


func _get_class():
	return "LevelScene"


func _is_class(value):
	return "LevelScene" == value
