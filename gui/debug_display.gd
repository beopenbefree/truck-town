extends Panel


func _ready():
	$VBoxContainer/OSLabel.text = "OS: " + OS.get_name()
	$VBoxContainer/EngineLabel.text = "Godot version: " + Engine.get_version_info()["string"]


func _physics_process(_delta):
	$VBoxContainer/FPSLabel.text = "FPS: " + str(Engine.get_frames_per_second())
