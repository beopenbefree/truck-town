extends VBoxContainer


func set_score_label(score, score_to_end_zone_enabled):
	$Score.text = ("Score: " + str(score) + " (" + 
			str(score_to_end_zone_enabled) + ")")


func set_score_label_color(color):
	$Score.add_theme_color_override("font_color", color)


func set_weapon_ammo_label(weapon_name, weapons, 
		grenade_name, grenade_amounts):
	$WeaponAmmo.text = "Weapon: " + weapon_name
	if weapon_name != "UNARMED":
		var current_weapon = weapons[weapon_name]
		$WeaponAmmo.text += ("\nAMMO:" + 
				str(current_weapon.ammo_in_weapon) + "/" + 
				str(current_weapon.spare_ammo))
	$WeaponAmmo.text += ("\n" + grenade_name + ": " + 
			str(grenade_amounts[grenade_name]))


func set_weapon_ammo_label_color(color):
	$WeaponAmmo.add_theme_color_override("font_color", color)


func set_vehicle_number_label(vehicle_number):
	#var available_vehicles = max(
			#globals.GAME_DATA["starting_available_vehicle_number"], vehicle_number)
	#var vehicle_id = str(available_vehicles - vehicle_number + 1) + "/" + str(available_vehicles)
	#$VehicleNumber.text = "Vehicle: " + vehicle_id
	$VehicleNumber.text = "Vehicles: " + str(vehicle_number - 1)


func set_vehicle_number_label_color(color):
	$VehicleNumber.add_theme_color_override("font_color", color)
