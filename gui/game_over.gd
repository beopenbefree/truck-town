extends ColorRect


func _ready():
	$VBoxContainer/Score.text = "Score: " + str(globals.score)
	$VBoxContainer/Level.text = "Level: " + str(globals.levels_completed)
	# WARNING: don't use await in _ready()
	var ready_timer = Timer.new()
	ready_timer.timeout.connect(_on_ready_timer_timeout)
	ready_timer.wait_time = 5.0
	ready_timer.one_shot = true
	add_child(ready_timer)
	ready_timer.start()
	
	# play random interlude
	globals.play_random_song(false)


func _on_ready_timer_timeout():
	# free mouse
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	globals._show_game_over_popup()
