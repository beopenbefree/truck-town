extends TextureButton


var dummy_texture:Texture2D = preload("res://icon.png")


@export var checked_normal: Texture2D = null
@export var checked_disabled: Texture2D = null
@export var checked_focused: Texture2D = null
@export var checked_hover: Texture2D = null
@export var checked_pressed: Texture2D = null

@export var unchecked_normal: Texture2D = null
@export var unchecked_disabled: Texture2D = null
@export var unchecked_focused: Texture2D = null
@export var unchecked_hover: Texture2D = null
@export var unchecked_pressed: Texture2D = null

@export var CHECKED: bool = false


func _ready():
	if not checked_normal: checked_normal = dummy_texture
	if not checked_disabled: checked_disabled = dummy_texture
	if not checked_focused: checked_focused = dummy_texture
	if not checked_hover: checked_hover = dummy_texture
	if not checked_pressed: checked_pressed = dummy_texture
	if not unchecked_normal: unchecked_normal = dummy_texture
	if not unchecked_disabled: unchecked_disabled = dummy_texture
	if not unchecked_focused: unchecked_focused = dummy_texture
	if not unchecked_hover: unchecked_hover = dummy_texture
	if not unchecked_pressed: unchecked_pressed = dummy_texture
	_set_textures(CHECKED)


func toggle_checking(checked):
	CHECKED = not checked
	_set_textures(CHECKED)
	release_focus()


func _set_textures(checked):
	if checked:
		set_texture_normal(checked_normal)
		set_texture_disabled(checked_disabled)
		set_texture_focused(checked_focused)
		set_texture_hover(checked_hover)
		set_texture_pressed(checked_pressed)
	else:
		set_texture_normal(unchecked_normal)
		set_texture_disabled(unchecked_disabled)
		set_texture_focused(unchecked_focused)
		set_texture_hover(unchecked_hover)
		set_texture_pressed(unchecked_pressed)
