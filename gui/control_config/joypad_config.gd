extends HBoxContainer


const ConfigClass := preload("res://addons/control_config.gd")

@onready var mapping_string_src: TextEdit = $ScrollContainer/joypads/RemapWizard/MapWindow/Margin/VBoxContainer/TextEdit
@onready var save_mapping_string: Button = $ScrollContainer/joypads/RemapWizard/MapWindow/Margin/VBoxContainer/HBoxContainer/SaveMappingString
@onready var close_map_window: Button = $ScrollContainer/joypads/RemapWizard/MapWindow/Margin/VBoxContainer/HBoxContainer/CloseMapWindow
@onready var map_window: Window = $ScrollContainer/joypads/RemapWizard/MapWindow


func _ready():
	save_mapping_string.pressed.connect(_on_SaveMappingString_pressed)
	close_map_window.pressed.connect(_on_CloseMapWindow_pressed)


func _on_CloseMapWindow_pressed():
	map_window.hide()


func _on_SaveMappingString_pressed():
	var mapping_string = mapping_string_src.text
	if not mapping_string.is_empty():
		save_config(mapping_string)


func save_config(mapping_string):
	var config = ConfigClass.new()
	config.joypad_map = mapping_string
	var config_file = globals.GAME_DATA["persistent_files"]["save_joypad_map_game"]
	
	ResourceSaver.save(config, config_file)
