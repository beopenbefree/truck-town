extends Control


@export var unhandled_input:Callable = Callable()


func _unhandled_input(event) -> void:
	unhandled_input.call(event)
