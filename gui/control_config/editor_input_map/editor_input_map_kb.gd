extends Control


const ConfigClass := preload("res://addons/control_config.gd")
@export var ActionClass := preload("res://gui/control_config/editor_input_map/action.tscn")

@export var config_file_name := "input_map.tres"

@export var main_action_list := ["ui_accept", "ui_select", "ui_cancel", "ui_focus_next", "ui_focus_prev", "ui_left", "ui_right", "ui_up", "ui_down", "ui_page_up", "ui_page_down", "ui_home", "ui_end"]
@export var reserved_action_list := PackedStringArray()
@export var preferred_action_list := PackedStringArray()

@export var enable_main_actions := true
@export var enable_reserved_actions := true
@export var enable_preferred_actions := true
@export var enable_remaining_actions := false

@export var allow_add_actions := false
@export var allow_add_inputs := true
@export var allow_delete_actions := false
@export var allow_delete_inputs := true
@export var check_input_duplicate := false

var input_confirm_ok: Button
var requester: Control
var cur_input: InputEvent

var config: ConfigClass = null
var config_file:String = String()


func _enter_tree():
	config_file = globals.GAME_DATA["persistent_files"].get(
			"save_input_map_game", null)
	if not config_file:
		config_file = "user://" + config_file_name
	load_config()


func _ready() -> void:
	set_process_unhandled_input(false)
	$InputMap/Margin/VBox/Top/LineEdit.text_changed.connect(
			_on_LineEdit_text_changed)
	$InputMap/Margin/VBox/Top/LineEdit.text_submitted.connect(
			_on_LineEdit_text_entered)
	$InputMap/Margin/VBox/Top/Add.pressed.connect(add_action)
	$InputConfirm.confirmed.connect(_on_InputConfirm_confirmed)
	#
	$InputMap/Margin/VBox/Top.visible = allow_add_actions
	$InputConfirm.get_label().visible = false
	input_confirm_ok = $InputConfirm.get_ok_button()
	input_confirm_ok.focus_mode = FOCUS_CLICK
	$InputConfirm.get_cancel_button().focus_mode = FOCUS_CLICK
	#
	$InputMap/AlertDuplicateDialog.get_label().visible = false
	
	if enable_main_actions:
		for action in main_action_list:
			if InputMap.has_action(action):
				add_action_ready(action)
	if enable_preferred_actions:
		for action in preferred_action_list:
			if InputMap.has_action(action):
				add_action_ready(action)
	if enable_remaining_actions:
		for action in InputMap.get_actions():
			if not (action in main_action_list) and not (action in preferred_action_list):
				add_action_ready(action)


func unhandled_input_delegate(event: InputEvent) -> void:
	if event is InputEventKey:
		$InputConfirm/ListeningInput.text = ConfigClass.get_input_text(event)
		cur_input = event
		input_confirm_ok.disabled = false
	elif event is InputEventJoypadButton:
		cur_input = InputEventJoypadButton.new()
		cur_input.device = event.device
		cur_input.button_index = event.button_index
		$InputConfirm/ListeningInput.text = ConfigClass.get_input_text(cur_input)
		input_confirm_ok.disabled = false
	elif (event is InputEventJoypadMotion) and (abs(event.axis_value) > 0.5):
		cur_input = InputEventJoypadMotion.new()
		cur_input.device = event.device
		cur_input.axis = event.axis
		cur_input.axis_value = -1.0 if event.axis_value < 0.0 else 1.0
		$InputConfirm/ListeningInput.text = ConfigClass.get_input_text(cur_input)
		input_confirm_ok.disabled = false
	elif event is InputEventMouseButton:
		$InputConfirm/ListeningInput.text = ConfigClass.get_input_text(event)
		cur_input = event
		input_confirm_ok.disabled = false


func load_config():
	config = globals._load_resource_if_exists(config_file)
	if not config:
		config = ConfigClass.new()
	
	for action in config.input_map:
		if not InputMap.has_action(action):
			InputMap.add_action(action)
		InputMap.action_erase_events(action)
		for input_event in config.input_map[action]:
			InputMap.action_add_event(action, input_event)


func save_config():
	config.input_map.clear()
	for action in InputMap.get_actions():
		config.input_map[action] = InputMap.action_get_events(action)
	
	ResourceSaver.save(config, config_file)


func add_action() -> void:
	if InputMap.has_action($InputMap/Margin/VBox/Top/LineEdit.text):
		return
	InputMap.add_action($InputMap/Margin/VBox/Top/LineEdit.text)
	var a := ActionClass.instantiate()
	a.editor_input_map = self
	a.init($InputMap/Margin/VBox/Top/LineEdit.text, allow_add_inputs, allow_delete_actions, allow_delete_inputs)
	$InputMap/Margin/VBox/Panel/VBox/Scroll/Actions.add_child(a)
	a.input_requested.connect(on_input_requested.bind(a))
	$InputMap/Margin/VBox/Top/LineEdit.text = ""
	$InputMap/Margin/VBox/Top/Add.disabled = true
	# save input map
	save_config()


func add_action_ready(action: String) -> void:
	var a := ActionClass.instantiate()
	a.editor_input_map = self
	a.init(action, allow_add_inputs, allow_delete_actions, allow_delete_inputs)
	$InputMap/Margin/VBox/Panel/VBox/Scroll/Actions.add_child(a)
	a.input_requested.connect(on_input_requested.bind(a))
	for input in a.get_all_inputs():
		input.input_requested.connect(on_input_requested.bind(input))


func on_input_requested(id: int, r: Control) -> void:
	requester = r
	var action = requester.action.replace("_", " ")
	match id:
		0,1,2,3: # Input
			input_confirm_ok.disabled = true
			$InputConfirm.title = action
			$InputConfirm/ListeningInput.text = "Listening for input..."
			$InputConfirm.popup_centered()
			$InputConfirm/ListeningInput.set_process_unhandled_input(true)
			$InputConfirm/ListeningInput.unhandled_input = unhandled_input_delegate


func set_input() -> void:
	if check_input_duplicate:
		var is_duplicate = _is_input_duplicate(requester.action, cur_input)
		if is_duplicate[0]:
			_display_duplicate_used(is_duplicate[1], requester.action, 
					cur_input, is_duplicate[2])
			return
	#
	for input in InputMap.action_get_events(requester.action):
		if input is InputEventKey and cur_input is InputEventKey and input.keycode == cur_input.keycode:
			return
		if input is InputEventJoypadButton and cur_input is InputEventJoypadButton and input.button_index == cur_input.button_index:
			return
		if input is InputEventJoypadMotion and cur_input is InputEventJoypadMotion and input.axis == cur_input.axis and input.axis_value == cur_input.axis_value:
			return
		if input is InputEventMouseButton and cur_input is InputEventMouseButton and input.button_index == cur_input.button_index:
			return
	requester.set_input(cur_input)
	if requester is VBoxContainer: # Action
		requester.last_added_input.input_requested.connect(on_input_requested.bind(requester.last_added_input))
	# save input map
	save_config()


func _on_InputConfirm_confirmed() -> void:
	$InputConfirm/ListeningInput.set_process_unhandled_input(false)
	$InputConfirm/ListeningInput.unhandled_input = Callable()
	set_input()


func _on_LineEdit_text_changed(new_text: String) -> void:
	$InputMap/Margin/VBox/Top/Add.disabled = new_text == "" or InputMap.has_action(new_text)
	$InputMap/Margin/VBox/Top/Warning.visible = InputMap.has_action(new_text)
	if InputMap.has_action(new_text):
		$InputMap/Margin/VBox/Top/Warning.text = "An action with the name '%s' already exists." % new_text


func _on_LineEdit_text_entered(_new_text: String) -> void:
	add_action()


func _is_input_duplicate(curr_ac:String, curr_i:InputEvent) -> Array:
	for ac in InputMap.get_actions():
		if ac == curr_ac:
			continue
		
		# check for duplicates only in enabled action lists
		var reserverd_action = false
		if ac in main_action_list:
			if not enable_main_actions:
				continue
		elif ac in preferred_action_list:
			if not enable_preferred_actions:
				continue
		elif ac in reserved_action_list:
			reserverd_action = true
			if not enable_preferred_actions:
				continue
		else:
			if not enable_remaining_actions:
				continue
		# search for (the first found) duplicate
		for i in InputMap.action_get_events(ac):
			var i_text = ConfigClass.get_input_text(i)
			var curr_i_text = ConfigClass.get_input_text(curr_i)
			if i_text == curr_i_text:
				return [true, ac, reserverd_action]
	return [false]


func _display_duplicate_used(ac:String, curr_ac:String, i:InputEvent,
		reserved_action:bool) -> void:
	var alert_dialog = $InputMap/AlertDuplicateDialog
	var alert_container = $InputMap/AlertDuplicateDialog/VBoxContainer
	var ac_str = ac.replace("_", " ")
	var i_str = ConfigClass.get_input_text(i)
	alert_dialog.title = curr_ac
	alert_container.get_node("InputChosen").text = i_str
	var warning = "is already used by"
	var ac_input_str = ac_str
	if reserved_action:
		warning = "cannot be used (reserverd)!"
		ac_input_str = ""
	alert_container.get_node("Warning").text = warning
	alert_container.get_node("Input").text = ac_input_str
	alert_dialog.visible = true
	var msg = i_str + "\n is already used by " + ac_str + "!"
	print(msg)
