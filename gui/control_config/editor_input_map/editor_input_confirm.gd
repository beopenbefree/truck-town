extends ConfirmationDialog


@export var device_path := NodePath()
@export var index_path := NodePath()
@export_enum("JOY_BUTTON_TEXT", "JOY_AXIS_TEXT",
		"MOUSE_BUTTON_TEXT", "NONE",) var config_repo: String = "NONE"

const ConfigClass := preload("res://addons/control_config.gd")

@onready var device := get_node(device_path)
@onready var index := get_node(index_path)

var config_repos_dict = {
	"JOY_BUTTON_TEXT": ConfigClass.JOY_BUTTON_TEXT,
	"JOY_AXIS_TEXT": ConfigClass.JOY_AXIS_TEXT,
	"MOUSE_BUTTON_TEXT": ConfigClass.MOUSE_BUTTON_TEXT,
}

func _ready():
	# set devices
	var i = 0
	for item in ConfigClass.DEVICES:
		device.add_item(item, i)
		i += 1
	# set indexes
	i = 0
	for item in config_repos_dict[config_repo]:
		index.add_item(item, i)
		i += 1
