extends CheckButton


func set_icons(texture_path_on:String, texture_path_off:String):
	if texture_path_on and texture_path_off:
		add_theme_icon_override("checked", load(texture_path_on))
		add_theme_icon_override("unchecked", load(texture_path_off))
