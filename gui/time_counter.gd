extends Control


var count:float = 0


func update_counter(value, absolute_max_value=1):
	# value normalized in [0, 1]
	count = value
	$ProgressBar.value = count * 100
	$Label.text = str(int(count * absolute_max_value))


func get_count():
	return count
