extends Control


var start_menu
var vehicle_select_menu
var vehicle_control_menu
var vehicle_setting_menu
var configuration_menu

var check_button_image:Resource = preload("res://gui/check_button_image.tscn")
var list_item:Resource = preload("res://gui/list_item.tscn")


func _ready():
	start_menu = $StartMenu
	vehicle_select_menu = $VehicleSelectMenu
	vehicle_control_menu = $VehicleControlMenu
	vehicle_setting_menu = $VehicleSettingMenu
	configuration_menu = $ConfigurationMenu
	# StartMenu
	_initialize_start_menu()
	
	# VehicleSelectMenu
	_initialize_vehicle_select_menu()
	
	# VehicleControlMenu
	_initialize_vehicle_control_menu()
	
	# VehicleSettingMenu
	_initialize_vehicle_setting_menu()
	
	# ConfigurationMenu
	_initialize_configuration_menu()
	
	# load saved configurations (if any) at game startup
	if globals.game_startup:
		if globals.saved_file_good(
				globals.GAME_DATA["persistent_files"]["save_conf_opts_game"]):
			globals.load_configuration_options()
		if globals.saved_file_good(
				globals.GAME_DATA["persistent_files"]["save_vehicle_settings_game"]):
			globals.load_vehicle_settings()
		# reset anyway game startup flag
		globals.game_startup = false
	
	# apply configuration options
	_apply_configuration_options()
	
	# show mouse
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	# WARNING: don't use await in _ready()
	var ready_timer = Timer.new()
	ready_timer.timeout.connect(_on_ready_timer_timeout)
	ready_timer.wait_time = 1.0
	ready_timer.one_shot = true
	add_child(ready_timer)
	ready_timer.start()


func start_menu_button_pressed(button_name):
	if button_name == "user_name":
		globals._show_set_username_popup()
	elif button_name == "continue_saved":
		if (globals.saved_file_good(
				globals.GAME_DATA["persistent_files"]["save_game"]) and 
				globals.load_game()):
			# set persistent global configuration options
			for key in globals.current_configuration_options:
				# check if gui enabled
				if not globals.current_configuration_options[key][4]:
					continue
				configuration_menu_button_pressed(key, true)
			globals.load_new_scene(globals.current_level_path)
		else:
			vehicle_select_menu.visible = true
			start_menu.visible = false
	elif button_name == "configuration":
		configuration_menu.visible = true
		start_menu.visible = false
	elif button_name == "reset_configurations":
		globals._delete_saved_configurations()
		globals._initialize_configuration_options()
		globals._initialize_vehicle_settings()
		_start_menu_disable_needed_buttons()
		_apply_configuration_options()
	elif button_name == "high_scores":
		globals._show_high_scores_popup()
	elif button_name == "open_url":
		OS.shell_open(globals.GAME_DATA["WEBSITE_URL"])
	elif button_name == "quit":
		globals.end_game()
	elif button_name == "start_new":
		globals._reset_game_state()
		_display_vehicle_select()
		_display_vehicle_controls()
		_display_vehicle_settings()
		vehicle_select_menu.visible = true
		start_menu.visible = false


func configuration_menu_button_pressed(button_name, set_from_global = false):
	if button_name == "back":
		start_menu.visible = true
		configuration_menu.visible = false
	else:
		var container = $ConfigurationMenu/HBoxContainer/Panel/VBoxContainer/ScrollContainer/VBoxOthers
		var conf_opts = globals.current_configuration_options
		var set_key_ref = Callable(self, conf_opts[button_name][1])
		var button = container.get_node(button_name)
		if set_from_global:
			button.button_pressed = conf_opts[button_name][0]
		else:
			conf_opts[button_name][0] = button.button_pressed
		set_key_ref.call(conf_opts[button_name][0])


func vehicle_select_menu_button_pressed(button_name, button_type):
	if button_type == "system":
		if button_name == "back":
			start_menu.visible = true
			vehicle_select_menu.visible = false
			vehicle_control_menu.visible = false
			vehicle_setting_menu.visible = false
	elif button_type == "control":
		globals.current_control_type = load(globals.GAME_DATA["control_types"][button_name]["scene"])
		globals.current_control_type_name = button_name
		# display
		var display_controls = $VehicleControlMenu/HBoxContainer/Display/Controls
		for child in display_controls.get_children():
			child.visible = false
		display_controls.get_node(NodePath(button_name)).visible = true
	elif button_type == "vehicle":
		var vehicle
		for item in globals.GAME_DATA["vehicles"]:
			if item["name"] == button_name:
				vehicle = item
				break
		globals.current_vehicle_path = vehicle["path"]
		globals.current_vehicle_sound = vehicle["sound"]
		# 
		_display_vehicle_settings()
		# display
		var display_vehicle = $VehicleSelectMenu/HBoxContainer/Display/Vehicle
		display_vehicle.texture = load(vehicle["image"])


func _initialize_start_menu():
	var container = $StartMenu/HBoxContainer/Panel/VBoxContainer
	var display = $StartMenu/HBoxContainer/Display
	#
	container.get_node("ButtonUserName").pressed.connect(
		start_menu_button_pressed.bind("user_name"))
	container.get_node("ButtonContinueSavedGame").pressed.connect(
		start_menu_button_pressed.bind("continue_saved"))
	container.get_node("HBoxContainer/ButtonConfiguration").pressed.connect(
		start_menu_button_pressed.bind("configuration"))
	container.get_node("HBoxContainer/ButtonResetConfigurations").pressed.connect(
		start_menu_button_pressed.bind("reset_configurations"))
	container.get_node("ButtonHighScores").pressed.connect(
		start_menu_button_pressed.bind("high_scores"))
	container.get_node("ButtonOpenURL").pressed.connect(
		start_menu_button_pressed.bind("open_url"))
	container.get_node("ButtonQuit").pressed.connect(
		start_menu_button_pressed.bind("quit"))
	#
	display.get_node("ButtonNewGame").pressed.connect(
		start_menu_button_pressed.bind("start_new"))
	# disable the appropriate buttons if necessary
	_start_menu_disable_needed_buttons()


func _initialize_configuration_menu():
	var container = $ConfigurationMenu/HBoxContainer/Panel/VBoxContainer/ScrollContainer/VBoxOthers
	var display = $ConfigurationMenu/HBoxContainer/Display
	var conf_opts = globals.current_configuration_options
	for key in conf_opts:
		# check if gui enabled
		if not conf_opts[key][4]:
			continue
		var check_button = check_button_image.instantiate()
		check_button.name = key
		check_button.text = conf_opts[key][2]
		check_button.add_theme_color_override("font_color", Color(conf_opts[key][3]))
		check_button.add_theme_color_override("font_color_hover", Color(conf_opts[key][3]))
		check_button.add_theme_color_override("font_color_pressed", Color(conf_opts[key][3]))
		check_button.pressed.connect(configuration_menu_button_pressed.bind(key))
		container.add_child(check_button)
	display.get_node("ButtonBack").pressed.connect(
		configuration_menu_button_pressed.bind("back"))


func _initialize_vehicle_select_menu():
	var container = $VehicleSelectMenu/HBoxContainer/Panel/VBoxContainer
	# vehicle select/controls/settings postponed
	container.get_node("HBoxContainer/ButtonBack").pressed.connect(
		vehicle_select_menu_button_pressed.bind("back", "system"))


func _initialize_vehicle_control_menu():
	# vehicle select/controls/settings postponed
	pass


func _initialize_vehicle_setting_menu():
	# vehicle select/controls/settings postponed
	pass


func _on_ready_timer_timeout():
	# play random interlude
	globals.play_random_song(false)


func _start_menu_disable_needed_buttons():
	var container = $StartMenu/HBoxContainer/Panel/VBoxContainer
	var reset_configurations = container.get_node(
			"HBoxContainer/ButtonResetConfigurations")
	var continue_saved_button:TextureButton = container.get_node(
			"ButtonContinueSavedGame")
	var saved_configs = globals._get_saved_configurations()
	var save_game_found = false
	if saved_configs.is_empty():
		reset_configurations.disabled = true
		reset_configurations.focus_mode = Control.FOCUS_NONE
	else:
		for file_pair in saved_configs:
			if file_pair[0] == "save_game":
				save_game_found = true
				break
	if not save_game_found:
		continue_saved_button.disabled = true
		continue_saved_button.focus_mode = Control.FOCUS_NONE


func _apply_configuration_options():
	for key in globals.current_configuration_options:
		# check if gui enabled
		if not globals.current_configuration_options[key][4]:
			continue
		configuration_menu_button_pressed(key, true)


func _set_vsync(key_value):
	DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_ENABLED if (key_value) else DisplayServer.VSYNC_DISABLED)


func _set_full_screen(key_value):
	# HACK: due to Windows' awkward handling of full-screen mode
	if OS.has_feature("windows"):
		get_window().mode = Window.MODE_WINDOWED
		await get_tree().process_frame
	get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN if (key_value) else Window.MODE_WINDOWED


func _set_debug_display(key_value):
	globals.set_debug_display(key_value)


func _set_low_end_device(key_value):
	var container = $ConfigurationMenu/HBoxContainer/Panel/VBoxContainer/ScrollContainer/VBoxOthers
	var conf_opts = globals.current_configuration_options
	for effect_name in globals.EFFECTS:
		# check if gui enabled
		if not conf_opts[effect_name][4]:
			continue
		var button = container.get_node(effect_name)
		if key_value == true:
			button.button_pressed = false
			conf_opts[effect_name][0] = false
			button.disabled = true
		else:
			button.disabled = false


func _set_low_resolution(key_value):
	if key_value:
		get_window().size = globals.GAME_DATA["SCREEN_LOW_RES"]
#		RenderingServer.texture_set_shrink_all_x2_on_set_data(true) FIXME:unimplemented in 4.x
	else:
		get_window().size = globals.GAME_DATA["SCREEN_HIGH_RES"]
#		RenderingServer.texture_set_shrink_all_x2_on_set_data(false) FIXME:unimplemented in 4.x
	configuration_menu_button_pressed("back")


func _set_none(_key_value):
	pass


func _display_vehicle_select():
	var container = $VehicleSelectMenu/HBoxContainer/Panel/VBoxContainer/VBoxVehicleSelect/ScrollContainer
	var sub_container = container.get_node("HBoxContainer/VBoxContainer")
	# reset
	container.scroll_vertical = 0.0
	for node in sub_container.get_children():
		node.free()
	# (re)create
	var check_button_group = ButtonGroup.new()
	var vehicles = globals.GAME_DATA["vehicles"]
	for vehicle in vehicles:
		var check_button_instance = check_button_image.instantiate()
		check_button_instance.set_name(vehicle["name"])
		check_button_instance.button_group = check_button_group
		var icon_on = vehicle["icon"]
		var icon_off = vehicle["icon_off"]
		check_button_instance.set_icons(icon_on, icon_off)
		check_button_instance.pressed.connect(vehicle_select_menu_button_pressed.bind(
				vehicle["name"], "vehicle"))
		sub_container.add_child(check_button_instance)
	sub_container.get_child(0).button_pressed = true
	vehicle_select_menu_button_pressed(sub_container.get_child(0).name, "vehicle")


func _display_vehicle_controls():
	var container = $VehicleControlMenu/HBoxContainer/Panel/VBoxContainer/VBoxVehicleControls/ScrollContainer
	var sub_container = container.get_node("VBoxcontainer")
	var display_controls = $VehicleControlMenu/HBoxContainer/Display/Controls
	# reset
	container.scroll_vertical = 0.0
	for node in sub_container.get_children():
		node.free()
	for node in display_controls.get_children():
		node.free()
	# (re)create
	var check_button_group = ButtonGroup.new()
	var contro_types = globals.GAME_DATA["control_types"]
	for control_key in contro_types:
		var kind = contro_types[control_key]["kind"]
		if (OS.has_feature("mobile")) and (kind != "touch"):
			# mobile platforms use touch controls
			continue
		if (not OS.has_feature("mobile")) and (kind == "touch"):
			# not mobile platforms don't use touch controls
			continue
		var check_button_instance = check_button_image.instantiate()
		check_button_instance.set_name(control_key)
		check_button_instance.button_group = check_button_group
		var icon_on = contro_types[control_key]["icon_on"]
		var icon_off = contro_types[control_key]["icon_off"]
		check_button_instance.set_icons(icon_on, icon_off)
		check_button_instance.pressed.connect(vehicle_select_menu_button_pressed.bind(
				control_key, "control"))
		sub_container.add_child(check_button_instance)
		# prepare the display's controls
		var controls_child = load(globals.GAME_DATA["control_types"][control_key]["display"]).instantiate()
		controls_child.set_name(control_key)
		display_controls.add_child(controls_child)
	sub_container.get_child(0).button_pressed = true
	vehicle_select_menu_button_pressed(sub_container.get_child(0).name, "control")


func _display_vehicle_settings():
	var container = $VehicleSettingMenu/HBoxContainer/Panel/VBoxContainer/VBoxVehicleSettings/ScrollContainer
	var sub_container = container.get_node("HBoxContainer/VBoxContainer")
	# reset
	container.scroll_vertical = 0.0
	for node in sub_container.get_children():
		node.queue_free()
	# (re)create
	for item_key in globals.current_vehicle_settings:
		var item_value = globals.current_vehicle_settings[item_key]
		# check if gui enabled
		if item_value[4]:
			var item_instance = list_item.instantiate()
			var label_name = item_key.replace("_", " ")
			item_instance.get_node("HBoxContainer/Label").text = "%s" % label_name
			var slider:HSlider = item_instance.get_node("HBoxContainer/HSlider")
			slider.min_value = item_value[1]
			slider.max_value = item_value[2]
			slider.step = item_value[3]
			var value_gui_node = item_instance.get_node("HBoxContainer/Value")
			slider.value_changed.connect(_on_ListItem_value_changed.bind(
					globals.current_vehicle_settings, item_key, 0, value_gui_node))
			
			# initial update
			value_gui_node.text = str(slider.value)
			sub_container.add_child(item_instance)
			slider.value = item_value[0]


func _on_Start_pressed():
	globals.current_level_path = globals.LEVELS[globals.current_level_idx]["path"]
	globals.load_new_scene(globals.current_level_path)


func _on_BackStart_pressed(menu):
	vehicle_select_menu.visible = true
	get_node(menu).visible = false 


func _on_ButtonControl_pressed():
	vehicle_select_menu.visible = false
	vehicle_control_menu.visible = true


func _on_ButtonSetting_pressed():
	vehicle_select_menu.visible = false
	vehicle_setting_menu.visible = true


func _on_ListItem_value_changed(value, dictionary, key, idx, value_gui_node):
	dictionary[key][idx] = value
	value_gui_node.text = "%*.*f" % [4, 2, float(value)]
