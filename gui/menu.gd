extends Control


@export var initial_focus: NodePath = NodePath()

@onready var initial_focus_node:Control = get_node(initial_focus)


func _set(property, value):
	if property == "visible":
		if value:
			initial_focus_node.grab_focus()
		visible = value
