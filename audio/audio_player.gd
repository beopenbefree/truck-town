extends Node3D


@export var ephemeral: bool = true

var audio_node = null
var should_loop = false
var start_loop_position = 0.0
var sound_name: String


func _ready():
	audio_node = get_node("PlayerON")
	audio_node.finished.connect(_sound_finished)
	audio_node.stop()


func _exit_tree():
	if is_instance_valid(self):
		globals.created_audios.erase(self)


func play_sound(p_sound_name, audio_stream, p_position=null):
	if audio_stream == null:
		print ("No audio stream passed; cannot play sound")
		globals.created_audios.erase(self)
		queue_free()
		return
		
	audio_node.stream = audio_stream
	
	# If you are using an AudioStreamPlayer3D, then uncomment these lines to set the position.
	if (audio_node is AudioStreamPlayer3D) and (p_position != null):
		audio_node.global_transform.origin = p_position
	
	audio_node.play(0.0)
	globals.created_audios[self] = p_sound_name


func clear_sound():
	audio_node.stop()
	if ephemeral:
		queue_free()


func get_pitch_scale():
	if audio_node != null:
		return audio_node.pitch_scale


func set_pitch_scale(value):
	if audio_node != null:
		audio_node.pitch_scale = value


func set_volume_db(value):
	audio_node.volume_db = value


func get_volume_db():
	return audio_node.volume_db


func _sound_finished():
	if should_loop:
		audio_node.play(start_loop_position)
	else:
		clear_sound()

## All of the audio files.
## You will need to provide your own sound files.
#var audio_pistol_shot = preload("res://test_3D_fps_tutorial/assets/gun_revolver_pistol_shot_04.wav")
#var audio_gun_cock = preload("res://test_3D_fps_tutorial/assets/gun_semi_auto_rifle_cock_02.wav")
#var audio_rifle_shot = preload("res://test_3D_fps_tutorial/assets/gun_rifle_sniper_shot_01.wav")
#
#var audio_node = null
#
#
#func _ready():
#	audio_node = $Audio_Stream_Player
#	audio_node.finished.connect(destroy_self)
#	audio_node.stop()
#
#
#func play_sound(sound_name, position=null):
#	if audio_pistol_shot == null or audio_rifle_shot == null or audio_gun_cock == null:
#		print ("Audio not set!")
#		queue_free()
#		return
#
#	if sound_name == "Pistol_shot":
#		audio_node.stream = audio_pistol_shot
#	elif sound_name == "Rifle_shot":
#		audio_node.stream = audio_rifle_shot
#	elif sound_name == "Gun_cock":
#		audio_node.stream = audio_gun_cock
#	else:
#		print ("UNKNOWN STREAM")
#		queue_free()
#		return
#
#	# If you are using an AudioStreamPlayer3D, then uncomment these lines to set the position.
#	if audio_node is AudioStreamPlayer3D:
#		if position != null:
#			audio_node.global_transform.origin = position
#
#	audio_node.play()
#
#
#func destroy_self():
#	audio_node.stop()
#	queue_free()
