extends Node


const ControlConfigClass := preload("res://addons/control_config.gd")

var GAME_DATA:Dictionary = {
	"WEBSITE_URL": "https://gamejolt.com/games/fighty-driver/782518",
	"MAIN_MENU_PATH": "res://gui/main_menu.tscn",
	"GAME_MENU_POPUP": "res://gui/game_menu_popup.tscn",
	"GAME_PAUSE_POPUP": "res://gui/game_pause_popup.tscn",
	"GAME_OVER_SCENE": "res://gui/game_over.tscn",
	"GAME_OVER_POPUP": "res://gui/game_over_popup.tscn",
	"SET_USERNAME_POPUP": "res://gui/set_username_popup.tscn",
	"WANT_QUIT_POPUP": "res://gui/want_quit_popup.tscn",
	"HIGH_SCORES_POPUP": "res://gui/high_scores_popup.tscn",
	"HIGH_SCORES_LABEL": "res://gui/high_score_label.tscn",
	"SOUND_PLAYER_SCENE": "res://audio/sound_player.tscn",
	"MUSIC_PLAYER_SCENE": "res://audio/music_player.tscn",
	"progress_animation_res": "res://gui/progress_animation.tscn",
	"DEBUG_DISPLAY_SCENE": "res://gui/debug_display.tscn",
	"SCREEN_HIGH_RES": Vector2(1280, 720),
	"SCREEN_LOW_RES": Vector2(640, 360),
	# levels related
	"GAME_REPEAT": true,
	"GAME_CHANGE_LEVEL": true,
	"GAME_RANDOM_LEVEL": false,
	"LEVELS": [
		{
			"path": "res://levels/level_debug/debug_scene.tscn", # DEBUG
			"total_count_down": 481,
			"score_level_base": 100,
			"score_level_increasing_factor": 0.1,
			"score_level_bonus": 100,
			"shooter_weapons": {"UNARMED":null, "GUN":null, "RIFLE":null, "LAUNCHER":null,},
		},
		#{
			#"path": "res://levels/level_city01/city01_scene.tscn",
			#"total_count_down": 481,
			#"score_level_base": 100,
			#"score_level_increasing_factor": 0.1,
			#"score_level_bonus": 100,
			#"shooter_weapons": {"UNARMED":null, "GUN":null, "RIFLE":null, "LAUNCHER":null,},
		#},
		#{
			#"path": "res://levels/level_landscape01/landscape01_scene.tscn",
			#"total_count_down": 481,
			#"score_level_base": 100,
			#"score_level_increasing_factor": 0.1,
			#"score_level_bonus": 100,
			#"shooter_weapons": {"UNARMED":null, "GUN":null, "RIFLE":null, "LAUNCHER":null,},
		#},
	],
	# control types specified with the following syntax (from left to right):
	# inputs: FW=forward, BW=backward, BR=brake, ST=steer left/right
	# controls: VA=virtual analog, VB=virtual button, AC=accelerometer
	"control_types": {
		"KB-MOUSE": {
			"scene":"res://vehicles/controls/combinations/kb-mouse.tscn",
			"controls":"Forward/Backward/Brake: key | Steer: key", 
			"icon_on":"res://assets/images/icon_KB-MOUSE.png",
			"icon_off":"res://assets/images/icon_KB-MOUSE_off.png",
			"display":"res://gui/control_config/control_config_kb.tscn",
			"kind":"kb_mouse",
			},
		"JOYSTICK": {
			"scene":"res://vehicles/controls/combinations/joystick.tscn",
			"controls":"Forward/Backward/Brake: joystick | Steer: joystick", 
			"icon_on":"res://assets/images/icon_JOYSTICK.png",
			"icon_off":"res://assets/images/icon_JOYSTICK_off.png",
			"display":"res://gui/control_config/joypad_config.tscn",
			"kind":"joystick",
			},
		"FW_VB-BW_VB-BR_VB-ST_VA": {
			"scene":"res://vehicles/controls/combinations/fw_vb-bw_vb-br_vb-st_va.tscn",
			"controls":"Forward/Backward/Brake: button | Steer: joystick", 
			"icon_on":"res://assets/images/icon_FW_VB-BW_VB-BR_VB-ST_VA.png",
			"icon_off":"res://assets/images/icon_FW_VB-BW_VB-BR_VB-ST_VA_off.png",
			"display":"res://gui/control_config/image_scenes/image_fw_vb-bw_vb-br_vb-st_va.tscn",
			"kind":"touch",
			},
		"FW_VB-BW_VB-BR_VB-ST_AC": {
			"scene":"res://vehicles/controls/combinations/fw_vb-bw_vb-br_vb-st_ac.tscn",
			"controls":"Forward/Backward/Brake: button | Steer: accelerometer",
			"icon_on":"res://assets/images/icon_FW_VB-BW_VB-BR_VB-ST_AC.png",
			"icon_off":"res://assets/images/icon_FW_VB-BW_VB-BR_VB-ST_AC_off.png",
			"display":"res://gui/control_config/image_scenes/image_fw_vb-bw_vb-br_vb-st_ac.tscn",
			"kind":"touch",
			},
		"FW_VA-BW_VA-BR_VB-ST_VA": {
			"scene":"res://vehicles/controls/combinations/fw_va-bw_va-br_vb-st_va.tscn",
			"controls":"Forward/Backward: joystick | Brake: button | Steer: joystick",
			"icon_on":"res://assets/images/icon_FW_VA-BW_VA-BR_VB-ST_VA.png",
			"icon_off":"res://assets/images/icon_FW_VA-BW_VA-BR_VB-ST_VA_off.png",
			"display":"res://gui/control_config/image_scenes/image_fw_va-bw_va-br_vb-st_va.tscn",
			"kind":"touch",
			},
		"FW_VA-BW_VA-BR_VB-ST_AC": {
			"scene":"res://vehicles/controls/combinations/fw_va-bw_va-br_vb-st_ac.tscn",
			"controls":"Forward/Backward: joystick | Brake: button | Steer: accelerometer",
			"icon_on":"res://assets/images/icon_FW_VA-BW_VA-BR_VB-ST_AC.png",
			"icon_off":"res://assets/images/icon_FW_VA-BW_VA-BR_VB-ST_AC_off.png",
			"display":"res://gui/control_config/image_scenes/image_fw_va-bw_va-br_vb-st_ac.tscn",
			"kind":"touch",
			},
	},
	# audio related
	"audio_clips": {
		"vehicle_collision_static": "res://assets/sounds/qubodup-crash.ogg",
		"vehicle_collision_dynamic": "res://assets/sounds/cast iron clangs - Marker #10.ogg",
		"environment_collision_dynamic": "res://assets/sounds/qubodupImpactStone.ogg",
		"vehicle_engine_1": "res://assets/sounds/435656__broken-head-productions__ford-ltl-9000-engine-start-and-idle.ogg",
		"vehicle_engine_2": "res://assets/sounds/160442__henrique85n__start-loop-engine.ogg",
		"vehicle_engine_loop": "res://assets/sounds/qubodup-car-engine-loop.ogg",
		"squealing_tires": "res://assets/sounds/tires_squal_loop.ogg",
		"explosion": "res://assets/sounds/explosion.ogg",
		"coin_grabbed": "res://assets/sounds/Rise02.ogg",
		"health_pickup": "res://assets/sounds/Rise01.ogg",
		"ammo_pickup": "res://assets/sounds/Rise07.ogg",
		"cannon_shot": "res://assets/sounds/cannon_02.ogg",
		"laser_shot": "res://assets/sounds/sfx_wpn_laser5.ogg",
		"gun_shot": "res://assets/sounds/bang_06.ogg",
		"gun_cock": "res://assets/sounds/reload.ogg",
		"rifle_shot": "res://assets/sounds/bang_rifle.ogg",
		"rifle_cock": "res://assets/sounds/reload.ogg",
		"launcher_shot": "res://assets/sounds/bang_06.ogg",
		"launcher_cock": "res://assets/sounds/reload.ogg",
		"rocket_launch": "res://assets/sounds/rocket_launch.ogg",
		"fire": "res://assets/sounds/fire-1.ogg",
		"laser_beam": "res://assets/sounds/laserbeam.ogg",
		"level_score_reached": "res://assets/sounds/jingles_STEEL10.ogg",
		"session_end": "res://assets/sounds/round_end.ogg",
		"session_fail": "res://assets/sounds/Jingle_Lose_00.ogg",
		# music tracks
		"music_track0": "res://assets/music/determined_pursuit_loop.ogg",
		"music_track1": "res://assets/music/Dirby_day.ogg",
		"music_track2": "res://assets/music/Doom.ogg",
		"music_track3": "res://assets/music/race.ogg",
		"music_track4": "res://assets/music/Skirmish.ogg",
		# interludes
		"interlude0": "res://assets/music/Interlude Normal.ogg",
		"interlude1": "res://assets/music/Interlude Dark.ogg",
		"interlude2": "res://assets/music/Interlude Boss.ogg",
		"interlude3": "res://assets/music/Interlude Silly.ogg",
		"interlude4": "res://assets/music/Interlude Tension.ogg",
	},
	"audio_clips_loop_start": {
		"vehicle_engine_1": 5.0,
		"vehicle_engine_2": 2.5,
	},
	"skies": {
		"sky1": "res://assets/images/skies/panorama_miramar.png",
		"sky2": "res://assets/images/skies/panorama_interstellar.png",
		"sky3": "res://assets/images/skies/panorama_stormydays.png",
	},
	# vehicles related
	"vehicles": [
		{
			"name": "sedan",
			"path": "res://vehicles/models/sedan.tscn",
			"sound": "vehicle_engine_2",
			"image": "res://assets/images/choose-image-sedan.png",
			"icon": "res://assets/images/choose-icon-sedan.png",
			"icon_off": "res://assets/images/choose-icon-off-sedan.png",
		},
		{
			"name": "wagon",
			"path": "res://vehicles/models/wagon.tscn",
			"sound": "vehicle_engine_2",
			"image": "res://assets/images/choose-image-wagon.png",
			"icon": "res://assets/images/choose-icon-wagon.png",
			"icon_off": "res://assets/images/choose-icon-off-wagon.png",
		},
		{
			"name": "pickup",
			"path": "res://vehicles/models/pickup.tscn",
			"sound": "vehicle_engine_2",
			"image": "res://assets/images/choose-image-pickup.png",
			"icon": "res://assets/images/choose-icon-pickup.png",
			"icon_off": "res://assets/images/choose-icon-off-pickup.png",
		},
		{
			"name": "bus",
			"path": "res://vehicles/models/bus.tscn",
			"sound": "vehicle_engine_1",
			"image": "res://assets/images/choose-image-bus.png",
			"icon": "res://assets/images/choose-icon-bus.png",
			"icon_off": "res://assets/images/choose-icon-off-bus.png",
		},
		{
			"name": "car01",
			"path": "res://vehicles/models/car01.tscn",
			"sound": "vehicle_engine_2",
			"image": "res://assets/images/choose-image-car01.png",
			"icon": "res://assets/images/choose-icon-car01.png",
			"icon_off": "res://assets/images/choose-icon-off-car01.png",
		},
#		{
#			"name": "mini_van_test", # DEBUG
#			"path": "res://vehicles/models/car_base_test.tscn",
#			"sound": "vehicle_engine_2",
#			"image": "res://assets/images/choose_van_test.png",
#			"icon": "res://assets/images/icon_choose_van_test.png",
#			"icon_off": "res://assets/images/icon_choose_van_off_test.png",
#		},
#		{
#			"name": "mini_van",
#			"path": "res://vehicles/models/car_base.tscn",
#			"sound": "vehicle_engine_2",
#			"image": "res://assets/images/choose_van.png",
#			"icon": "res://assets/images/icon_choose_van.png",
#			"icon_off": "res://assets/images/icon_choose_van_off.png",
#		},
#		{
#			"name": "trailer_truck2",
#			"path": "res://vehicles/models/trailer_truck2.tscn",
#			"sound": "vehicle_engine_1",
#			"image": "res://assets/images/choose_trailer2.png",
#			"icon": "res://assets/images/icon_choose_trailer2.png",
#			"icon_off": "res://assets/images/icon_choose_trailer2_off.png",
#		},
#		{
#			"name": "tow_truck_impounded",
#			"path": "res://vehicles/models/tow_truck.tscn",
#			"sound": "vehicle_engine_2",
#			"image": "res://assets/images/choose_tow.png",
#			"icon": "res://assets/images/icon_choose_tow.png",
#			"icon_off": "res://assets/images/icon_choose_tow_off.png",
#		},
#		{
#			"name": "trailer_truck",
#			"path": "res://vehicles/models/trailer_truck.tscn",
#			"sound": "vehicle_engine_1",
#			"image": "res://assets/images/choose_trailer.png",
#			"icon": "res://assets/images/icon_choose_trailer.png",
#			"icon_off": "res://assets/images/icon_choose_trailer_off.png",
#		},
#		{
#			"name": "gaz-67",
#			"path": "res://vehicles/models/gaz-67.tscn",
#			"sound": "truck_engine_2",
#			"image": "res://assets/images/choose_gaz67.png",
#			"icon": "res://assets/images/icon_choose_gaz67.png",
#			"icon_off": "res://assets/images/icon_choose_gaz67_off.png",
#		},
	],
	"vehicle_settings": {
		# name: [value, gui_min_value, gui_max_value, gui_step, gui_enabled]
		"drag_force_factor": [20.0, 1.0, 120.0, 0.5, false],
		"steer_speed": [1.5, 0.1, 5.0, 0.1, true],
		"steer_limit": [0.4, 0.0, 1.0, 0.05, true],
		"tilt_angle_max": [25, 10.0, 60.0, 1.0, true],
		"tilt_engine_force_factor": [1.0, 1.0, 3.0, 0.1, true],
		"accel_steer_speed": [1.0, 0.5, 2.0, 0.05, true],
		"engine_force_value": [40, 2.0, 200.0, 1.0, false],
		"brake_size_factor": [0.0, 0.0, 100.0, 0.1, false],
		"lift_force_x": [0.0, 0.0, 500.0, 0.0, false],
		"lift_force_y": [1000.0, 0.0, 500.0, 0.0, false],
		"lift_force_z": [0.0, 0.0, 500.0, 0.0, false],
		"lift_force_position_x": [0.0, 0.0, 0.0, 0.0, false],
		"lift_force_position_y": [0.0, 0.0, 0.0, 0.0, false],
		"lift_force_position_z": [0.0, 0.0, 0.0, 0.0, false],
		"gear_max_speed1": [3.0, 1.0, 20.0, 0.25, false],
		"gear_max_speed2": [6.0, 1.0, 40.0, 0.25, false],
		"gear_max_speed3": [9.0, 1.0, 60.0, 0.5, false],
		"gear_max_speed4": [12.0, 1.0, 80.0, 0.5, false],
		"gear_speed_factor": [0.3, 0.1, 1.0, 0.05, false],
		"gear_engine_factor": [0.4, 0.01, 1.0, 0.01, false],
		"gear_max_low_factor": [0.9, 0.8, 1.0, 0.001, false],
		"steering_wheel_angle_max": [90, 30, 90.0, 1.0, false],
		"steering_wheel_scale_x": [1.0, 0.1, 10.0, 0.1, false],
		"steering_wheel_scale_y": [1.0, 0.1, 10.0, 0.1, false],
		"steering_wheel_scale_z": [1.0, 0.1, 10.0, 0.1, false],
		"MAX_NO_DAMAGE_SPEED": [5.0, 1.0, 10.0, 0.5, false],
		"DAMAGE_SPEED_FACTOR": [0.05, 0.0, 0.1, 0.01, false],
		"COLLISION_TIME_INTERVAL": [0.5, 0.0, 2.0, 0.01, false],
		"mouse_sensitivity": [0.8, 0.01, 5.0, 0.01, true],
		"camera_sensitivity": [1.5, 0.05, 10.0, 0.05, true],
	},
	# global configurations related
	"configuration_options": {
		# key: [value, set_func, button_name, font_color, gui_enabled]
		"vsync": [true, "_set_vsync", "V-Sync", "ffffff", true],
		"full_screen": [true, "_set_full_screen", "Full Screen", "ffffff", true],
		"debug_display": [false, "_set_debug_display", "Debug Display", "ffffff", true],
		"shadow_enabled": [true, "_set_none", "Shadows", "ffffff", true],
		"fog_enabled": [true, "_set_none", "Fog", "ffffff", true],
		"volumetric_fog_enabled": [false, "_set_none", "Volumetric Fog", "ffffff", false],
		"glow_enabled": [false, "_set_none", "Glow", "ffffff", false],
		"ssr_enabled": [false, "_set_none", "Screen-space Reflections", "ffffff", false],
		"ssao_enabled": [false, "_set_none", "Screen-space Ambient Occlusion", "ffffff", false],
		"ssil_enabled": [false, "_set_none", "Screen-space Indirect Lighting", "ffffff", false],
		"sdfgi_enabled": [false, "_set_none", "Signed Distance Field Global Illumination", "ffffff", false],
		"dof_blur_far_enabled": [false, "_set_none", "Depth-of-field Blur Far", "ffffff", false],
		"dof_blur_near_enabled": [false, "_set_none", "Depth-of-field Blur Near", "ffffff", false],
		"auto_exposure_enabled": [false, "_set_none", "Auto Exposure", "ffffff", false],
		"low_end_device": [false, "_set_low_end_device", "Low End Device", "ff0000", true], 
		"low_resolution": [false, "_set_low_resolution", "Low Resolution", "ff0000", true],
	},
	# score/vehicle_number/weapons related
	"GAME_ADD_COUNT_SCORE": true,
	"starting_score": 0,
	"starting_available_vehicle_number": 5,
	"score_gap_for_next_player_upgrade": 500,
	"max_vehicle_ai_number": 6,
	"high_scores_list_size": 10,
	# persistent files
	"persistent_files": {
		"save_game": "user://fighty-driver.save",
		"save_conf_opts_game": "user://fighty-driver.save.co",
		"save_vehicle_settings_game": "user://fighty-driver.save.vs",
		"save_high_scores_game": "user://fighty-driver.save.hs",
		"save_input_map_game": "user://fighty-input_map.tres",
		"save_joypad_map_game": "user://fighty-joypad_map.tres",
	}
}

# configuration options
var username := String()
var current_configuration_options:Dictionary = {}
const EFFECTS:Array = ["shadow_enabled", "fog_enabled", 
		"volumetric_fog_enabled", "glow_enabled", "ssr_enabled",
		"ssao_enabled", "ssil_enabled", "sdfgi_enabled", 
		"dof_blur_far_enabled", "dof_blur_near_enabled",
		"auto_exposure_enabled"]
const LOW_END_DEVICE_REDUCTION_FACTOR:float = 0.1

# current options
var current_control_type: Resource = null
var current_control_type_name:String = GAME_DATA["control_types"].keys()[0]

# GUI/UI-related variables
var canvas_layer:CanvasLayer = null

var MAIN_MENU_PATH:String = ""
var GAME_OVER_SCENE:String = ""
var GAME_OVER_POPUP:Resource = null
var GAME_PAUSE_POPUP:Resource = null
var GAME_MENU_POPUP:Resource = null
var SET_USERNAME_POPUP:Resource = null
var WANT_QUIT_POPUP:Resource = null
var HIGH_SCORES_POPUP:Resource = null
var HIGH_SCORES_LABEL:Resource = null

var DEBUG_DISPLAY_SCENE:Resource = _load_resource_if_exists(GAME_DATA["DEBUG_DISPLAY_SCENE"])
var debug_display:Control = null

# Levels-related variables
var LEVELS:Array = []
var level_debug_idx:int = 0
var current_level_idx: int = 0
var levels_completed: int = 0
var current_game_session: int = 0
var num_levels:int = 0
var current_level_path:String = ""
var current_vehicle_path:String = ""
var current_vehicle:Node = null: set = _set_current_vehicle
var current_vehicle_body:Vehicle = null 
var current_vehicle_sound:String = ""
var current_vehicle_sound_loop_start:float = 0.0
var current_vehicle_settings:Dictionary = {}
var current_end_zones:Array = []
var current_end_zones_enabled:bool = false

# Audio-related variables
# All the audio files. You will need to provide your own sound files
var audio_clips:Dictionary = {}
var audio_clips_loop_start:Dictionary = {}

var SOUND_PLAYER_SCENE:Resource = null
var created_audios:Dictionary = {}

var MUSIC_PLAYER_SCENE:Resource = null
var music_track_index: int = -1

# Score/Vehicle number-related variables
var score:int: set = _set_score
var score_current_level:int = 0
var score_level_increasing_factor:float = 0.0
var vehicle_number:int: set = _set_vehicle_number
var is_game_over:bool

# ResourceInteractiveLoader-related variables
var loading_new_scene:bool = false
var wait_frames:int
var time_max:int = 1 # msec
var new_scene_path:String
var progress_animation_res:Resource = null
var progress_animation_name:String = "progress"
var progress_animation_lenght:float = 0.0
var progress_animation:AnimationPlayer = null

# AI updated every AI_PHYSICS_FRAME_UPDATE physics frames
const AI_PHYSICS_FRAME_UPDATE: int = 30
# AI: common collision detector
var collision_detectors:Dictionary = {} # Dictionary{mask:AI.CollisionDetector}

# HACK: flag set to true on game startup and reset from main menu when first loaded
@onready var game_startup: bool = true


func _ready():
	# handle joypad mapping (if any)
#	Input.add_joy_mapping("03000000300f00001101000010010000,Jess Tech Colour Rumble Pad,a:b2,b:b3,y:b1,x:b0,start:b9,back:b8,leftstick:b10,rightstick:b11,leftshoulder:b4,rightshoulder:b6,dpup:b12,dpleft:b14,dpdown:b13,dpright:b13,leftx:a0,lefty:a1,rightx:a3,righty:a2,lefttrigger:b5,righttrigger:b7,platform:Linux", true)
	var joypad_map:String = _load_joypad_map()
	if not joypad_map.is_empty():
		Input.add_joy_mapping(joypad_map, true)
	# initialize starting game data
	_setup_game_data()
	_reset_game_state()
	# override the default quit request behavior 
	get_tree().set_auto_accept_quit(false)
	# initialize random and GUI system
	# NOTE: The randomize() function is called
	# automatically when the project is run. 
	canvas_layer = CanvasLayer.new()
	add_child(canvas_layer)


# HACK: use only SHOW GAME MENU
#func _notification(what):
	#if ((what == NOTIFICATION_WM_CLOSE_REQUEST) or
			#(what == NOTIFICATION_WM_GO_BACK_REQUEST)):
		#_show_want_quit_popup()


func _physics_process(_delta):
	# HACK: use only SHOW GAME MENU
	#if Input.is_action_just_pressed("ui_cancel"):
		#if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
			#Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		#else:
			#_show_game_pause_popup()
	if loading_new_scene:
		var progress = []
		var thread_load_status = ResourceLoader.load_threaded_get_status(
				new_scene_path, progress)
		if thread_load_status == ResourceLoader.THREAD_LOAD_IN_PROGRESS:
			update_progress(progress[0])
		elif thread_load_status == ResourceLoader.THREAD_LOAD_LOADED:
			# Finished loading.
			var resource = ResourceLoader.load_threaded_get(new_scene_path)
			update_progress(progress[0])
			await get_tree().create_timer(1.0).timeout
			if progress_animation:
				progress_animation.stop()
				progress_animation.queue_free()
			set_new_scene(resource)
			setup_new_scene(new_scene_path)
			loading_new_scene = false
		else:
			# Error during loading.
			loading_new_scene = false
			printerr("ERROR: Error during loading: ", new_scene_path)


func load_new_scene(scene_path):
	# clear all sounds anyway
	clear_all_sounds()
	# Get rid of all the old scene.
	if is_instance_valid(current_vehicle):
		current_vehicle.queue_free()
	_set_current_vehicle(null)
	get_current_scene().queue_free()
	# wait a frame
	await get_tree().process_frame
	# load the new scene
	new_scene_path = scene_path
	var res = ResourceLoader.load_threaded_request(new_scene_path)
	if res == OK: # Check for errors.
		loading_new_scene = true
	else:
		printerr("ERROR: Game requests to switch to this scene: ", scene_path)
		return
	
	# Start your "loading..." animation.
	if progress_animation_res:
		progress_animation = progress_animation_res.instantiate()
		add_child(progress_animation)
		progress_animation.play(progress_animation_name)
		progress_animation_lenght = progress_animation.current_animation_length
	
	wait_frames = 1


func set_new_scene(scene_resource):
	var current_scene = scene_resource.instantiate()
	get_node("/root").add_child(current_scene)


func setup_new_scene(scene_path):
	if  scene_path == current_level_path:
		# wait for a frame for the scene to actually change 
		var current_level = get_current_scene()
		if not is_instance_valid(current_vehicle):
			_set_current_vehicle(_load_resource_if_exists(current_vehicle_path).instantiate())
		current_vehicle.set_name("current_vehicle")
		current_level.get_node("InstancePos").add_child(current_vehicle)
		# set current control type
		current_control_type = _load_resource_if_exists(GAME_DATA["control_types"][current_control_type_name]["scene"])
		var current_control = current_control_type.instantiate()
		current_vehicle.add_child(current_control)
		_initialize_hud(current_level)
		# set current vehicle settings
		current_vehicle_body.set_current_settings()
		# disable physics temporarily for current vehicle
		current_vehicle_body.set_physics_process(false)
		# handle score/vehicle number
		_set_score(score)
		_set_vehicle_number_ui()


func update_level(vehicle_body, go_new_level=true):
	# disable physics temporarily for current vehicle
	vehicle_body.lift_up = true
	# remove vehicle sounds
	clear_sound("", vehicle_body)
	if "current_sound" in vehicle_body:
		vehicle_body.current_sound = null
	
	# manage score
	if go_new_level:
		# update number of levels completed
		levels_completed += 1
		# update score with end bonus
		_set_score(score + current_score_level_bonus())
		if GAME_DATA["GAME_ADD_COUNT_SCORE"]:
			_set_score(score + get_current_scene().current_count)
	# NOTE: uncomment if you want score updated only at level completion
	else:
		# not going to new level: remove the score accumulated so far
		score -= score_current_level
		
		# play some sound
		globals.play_sound("session_fail", null, true, false, 
				current_vehicle_body.global_position)
	
	# update game session number
	current_game_session += 1
	# reset anyway score related stuff
	_reset_score_related()
	
	# play fade animation if any
	var fade_animation:AnimationPlayer = get_current_scene().get_node("FadeAnimation")
	if fade_animation:
		fade_animation.play("fade")
		await fade_animation.animation_finished
	
	# post-fade animation
	if go_new_level:
		# compute level index
		if GAME_DATA["GAME_CHANGE_LEVEL"]:
			current_level_idx += 1
			if GAME_DATA["GAME_REPEAT"]:
				current_level_idx %= len(LEVELS)
		# handle level
		if (current_level_idx >= len(LEVELS)):
			# no more levels: game over
			load_new_scene(GAME_OVER_SCENE)
		else:
			_set_vehicle_number(vehicle_number + 1)
			var real_level_idx = current_level_idx
			if GAME_DATA["GAME_RANDOM_LEVEL"]:
				real_level_idx = randi_range(0, len(LEVELS) - 1)
			current_level_path = LEVELS[real_level_idx]["path"]
			load_new_scene(current_level_path)
	else:
		# decrease vehicle number
		_set_vehicle_number(vehicle_number - 1)
		# stay on the same level (if possible)
		if not is_game_over:
			# restart the same level
			load_new_scene(current_level_path)
		else:
			# the game is over: save high scores
			save_high_scores()
			# no more vehicles: game over
			load_new_scene(GAME_OVER_SCENE)


func play_sound(sound_name, sound_parent=null, from_begin=true,
		loop_sound=false, sound_position=null, sound_player=null,
		sound_player_scene=SOUND_PLAYER_SCENE):
	if sound_name in audio_clips:
		var new_audio
		if sound_player:
			new_audio = sound_player
		else:
			new_audio = sound_player_scene.instantiate()
		new_audio.should_loop = loop_sound
		new_audio.sound_name = sound_name
		
		if not sound_player:
			if is_instance_valid(sound_parent):
				sound_parent.add_child(new_audio)
			else:
				add_child(new_audio)
		
		if is_instance_valid(sound_parent) and (audio_clips_loop_start.has(sound_name)) and (!from_begin):
			new_audio.start_loop_position = audio_clips_loop_start.get(sound_name, 0.0)
		else:
			new_audio.start_loop_position = 0.0
		new_audio.play_sound(sound_name, audio_clips[sound_name], 
				sound_position)
		return new_audio
		
	else:
		print ("ERROR: cannot play sound '", sound_name, "' that does not exist in audio_clips!")
		return null


func get_sound(sound_name:String, parent=null) -> Array:
	var audios_with_sound_name = []
	if (not sound_name) and parent:
		# return all sounds related to parent
		for audio in created_audios.keys():
			if parent.is_ancestor_of(audio):
				audios_with_sound_name.append(audio)
	elif sound_name and parent:
		# return all sounds named sound_name related to parent
		for audio in created_audios.keys():
			if ((created_audios[audio] == sound_name) and 
					parent.is_ancestor_of(audio)):
				audios_with_sound_name.append(audio)
	elif sound_name and (not parent):
		# return all sounds named sound_name
		for audio in created_audios.keys():
			if created_audios[audio] == sound_name:
				audios_with_sound_name.append(audio)
	return audios_with_sound_name


func get_sound_first(sound_name:String, parent=null):
	var audios:Array = get_sound(sound_name, parent)
	return audios[0] if audios else null


func clear_sound(sound_name:String, parent:Node3D=null):
	if sound_name.is_empty() and parent:
		# remove all sounds related to parent
		for audio in created_audios.keys():
			if parent.is_ancestor_of(audio):
				audio.clear_sound()
	elif sound_name and parent:
		# remove all sounds named sound_name related to parent
		for audio in created_audios.keys():
			if ((created_audios[audio] == sound_name) and 
					parent.is_ancestor_of(audio)):
				audio.clear_sound()
	elif sound_name and (not parent):
		# remove all sounds named sound_name
		for audio in created_audios.keys():
			if created_audios[audio] == sound_name:
				audio.clear_sound()


func clear_sound_first(sound_name:String, parent=null):
	var audio = get_sound_first(sound_name, parent)
	if audio:
		audio.clear_sound() 


func clear_all_sounds():
	for audio in created_audios.keys():
		if is_instance_valid(audio):
			audio.clear_sound()


func play_song(song_name, from_begin=true, loop_sound=false):
	play_sound(song_name, null, from_begin, loop_sound, null, null,
			MUSIC_PLAYER_SCENE)


func play_random_song(is_music_track:bool=true):
	var songs_num = globals.get_songs_num(is_music_track)
	var idx = randi_range(0, songs_num - 1)
	var song_name = globals.get_song_name(idx, is_music_track)
	if is_music_track:
		# set global music_track index
		globals.music_track_index = idx
	globals.play_song(song_name, true, true)
	globals.set_song_volume(song_name, -15.0)


func set_song_volume(song_name, value):
	var player = get_sound_first(song_name)
	if player:
		var music_player:AudioStreamPlayer = player.audio_node
		music_player.volume_db = value


func get_song_volume(song_name):
	var player = get_sound_first(song_name)
	if player:
		var music_player:AudioStreamPlayer = player.audio_node
		return music_player.volume_db
	return null


func get_song_name(index, is_music_track:bool=true):
	# return music_track or interlude given an index
	var song_type:String = "music_track" if is_music_track else "interlude"
	var songs_num = get_songs_num(is_music_track)
	return song_type + str(abs(index) % songs_num)


func get_songs_num(is_music_track:bool=true):
	# return number of music_tracks or interludes
	var song_tracks_num = 0
	var song_type:String = "music_track" if is_music_track else "interlude"
	var end_idx = song_type.length()
	for clip in GAME_DATA["audio_clips"]:
		if clip.substr(0, end_idx) == song_type:
			song_tracks_num += 1
	return song_tracks_num


func enable_all_sounds(enable):
	AudioServer.set_bus_mute(0, not enable)


func all_sounds_muted():
	return AudioServer.is_bus_mute(0)


func saved_file_good(filename):
	var saved_file = FileAccess.open(filename, FileAccess.READ)
	if saved_file:
		saved_file.close()
	return saved_file != null


func save_game():
	if (not current_level_path.is_empty()) and (not current_vehicle_path.is_empty()):
		# save the game actually
		var saved_game = FileAccess.open(
				GAME_DATA["persistent_files"]["save_game"], FileAccess.WRITE)
		var save_dict = {
			"current_level_idx": current_level_idx,
			"current_game_session": current_game_session,
			"levels_completed": levels_completed,
			"current_level_path": current_level_path,
			"current_vehicle_path": current_vehicle_path,
			"current_vehicle_sound": current_vehicle_sound,
			"current_control_type_name": current_control_type_name,
			"score": score - score_current_level,
			"vehicle_number": vehicle_number,
			"music_track_index": music_track_index,
		}
		#
		saved_game.store_line(JSON.stringify(save_dict))
		saved_game.close()


func load_game():
	var saved_dict:Dictionary = _load_and_parse_json(
			GAME_DATA["persistent_files"]["save_game"])
	if saved_dict.is_empty():
		return false
	
	var res = true
	current_level_idx = saved_dict.get("current_level_idx", -1)
	res = res and (current_level_idx != -1)
	current_game_session = saved_dict.get("current_game_session", -1)
	res = res and (current_game_session != -1)
	levels_completed = saved_dict.get("levels_completed", -1)
	res = res and (levels_completed != -1)
	current_level_path = saved_dict.get("current_level_path", "")
	res = res and (current_level_path != "")
	current_vehicle_path = saved_dict.get("current_vehicle_path", "")
	res = res and (current_vehicle_path != "")
	current_vehicle_sound = saved_dict.get("current_vehicle_sound", "")
	res = res and (current_vehicle_sound != "")
	current_control_type_name = saved_dict.get("current_control_type_name", "")
	res = res and (current_control_type_name != "")
	_set_score(saved_dict.get("score", -1))
	res = res and (score != -1)
	_set_vehicle_number(saved_dict.get("vehicle_number", -1))
	res = res and (vehicle_number != -1)
	music_track_index = saved_dict.get("music_track_index", -1)
	res = res and (music_track_index != -1)
	return res


func save_configuration_options():
	var saved_co_game = FileAccess.open(
			GAME_DATA["persistent_files"]["save_conf_opts_game"],
			FileAccess.WRITE)
	var save_dict = {}
	_write_configuration_options(save_dict)
	saved_co_game.store_line(JSON.stringify(save_dict))
	saved_co_game.close()


func load_configuration_options():
	var saved_dict = _load_and_parse_json(
			GAME_DATA["persistent_files"]["save_conf_opts_game"])
	if saved_dict.is_empty():
		return false
	
	var res = true
	res = res and _read_configuration_options(saved_dict)
	return res


func save_vehicle_settings():
	var saved_co_game = FileAccess.open(
			GAME_DATA["persistent_files"]["save_vehicle_settings_game"],
			FileAccess.WRITE)
	var save_dict = {}
	_write_vehicle_settings(save_dict)
	saved_co_game.store_line(JSON.stringify(save_dict))
	saved_co_game.close()


func load_vehicle_settings():
	var saved_dict = _load_and_parse_json(
			GAME_DATA["persistent_files"]["save_vehicle_settings_game"])
	if saved_dict.is_empty():
		return false
	
	var res = true
	res = res and _read_vehicle_settings(saved_dict)
	return res


func save_high_scores():
	# high scores = {"place":{"name":NAME,"level":LEVEL|"session":SESSION,"score":SCORE,"timestamp":TIMESTAMP}}
	var save_dict = Dictionary()
	if FileAccess.file_exists(
			GAME_DATA["persistent_files"]["save_high_scores_game"]):
		save_dict = load_high_scores()
	# insert current item
	save_dict[save_dict.size()] = {
			"name": _get_username(),
			"level": levels_completed,
			"session": current_game_session,
			"score": score,
			"timestamp": _get_timestamp()[1],
			"timestamp_raw": _get_timestamp()[0],
	}
	# sort and trunc save dict
	_sort_trunc_dict(save_dict, GAME_DATA["high_scores_list_size"], "score", 
			"timestamp_raw")
	var saved_hs_game = FileAccess.open(
			GAME_DATA["persistent_files"]["save_high_scores_game"],
			FileAccess.WRITE)
	saved_hs_game.store_line(JSON.stringify(save_dict))
	saved_hs_game.close()


func load_high_scores():
	var saved_dict = Dictionary()
	var saved_hs_game = FileAccess.open(
			GAME_DATA["persistent_files"]["save_high_scores_game"],
			FileAccess.READ)
	if saved_hs_game:
		var test_json_conv = JSON.new()
		test_json_conv.parse(saved_hs_game.get_line())
		saved_dict = test_json_conv.get_data()
		saved_hs_game.close()
	return saved_dict


func update_progress(progress):
	# Update your progress bar?
#	get_node("progress").set_progress(progress)
	# ...or update a progress animation?
	if progress_animation:
		# Call this on a paused animation. Use "true" as the second argument to
		# force the animation to update.
		# forward
		progress_animation.seek(progress * progress_animation_lenght, true)
		# backward
#		progress_animation.seek(progress_animation_lenght * (1.0 - progress), true)
	else:
		print(progress)
	var score_str = "%5d" % int(score * progress)
	progress_animation.get_node("MarginContainer/VBoxContainer/Score").text = "Score: " + score_str
	progress_animation.get_node("MarginContainer/VBoxContainer/Level").text = "Level: " + str(levels_completed)


func get_current_scene():
	var root = get_tree().get_root()
	return root.get_child(root.get_child_count() - 1)


func set_debug_display(display_on):
	if display_on == false:
		if debug_display != null:
			debug_display.queue_free()
			debug_display = null
	else:
		if debug_display == null:
			debug_display = DEBUG_DISPLAY_SCENE.instantiate()
			canvas_layer.add_child(debug_display)


func end_game():
	# end the game
	save_game()
	save_vehicle_settings()
	save_configuration_options()
	## FIXME: this check could be unnecessary
	if is_game_over or (score > 0):
		# the game is over: save high scores
		save_high_scores()
	# some cleanup
	clear_all_sounds()
	for child in get_current_scene().get_children():
		child.queue_free()
	for child in get_children():
		child.queue_free()
	# quit
	get_tree().quit()


func enable_end_zones():
	if current_end_zones_enabled:
		return
	var score_level_base = current_score_enabling_end_zone()
	if score_current_level >= score_level_base:
		for end_zone in current_end_zones:
			end_zone.enabled = true
		current_end_zones_enabled = true
		# update GUI
		await get_tree().create_timer(1.0).timeout
		var score_label = "HUD/MarginContainer/VBoxContainer/HBoxContainer2/ScoreInfos"
		get_current_scene().get_node(score_label).set_score_label_color(Color(0.0, 1.0, 0.0))
		# play sound
		play_sound("level_score_reached", null, true, false,
				current_vehicle_body.global_position)


func current_score_enabling_end_zone():
	return _current_score_level_base()


func current_score_level_bonus():
	return int(LEVELS[current_level_idx]["score_level_bonus"] * 
			current_level_increasing_factor())


func current_level_increasing_factor():
	var score_level_base = float(
			GAME_DATA["LEVELS"][current_level_idx]["score_level_base"])
	return float(_current_score_level_base()) / score_level_base


func get_current_camera() -> CameraBase:
	var current_camera:CameraBase = null
	if is_instance_valid(current_vehicle_body):
		current_camera = current_vehicle_body.camera_group_node.get_current_camera()
	return current_camera


func swap_camera(button:TextureButton=null):
	if button:
		button.release_focus()
	#
	if is_instance_valid(current_vehicle_body):
		current_vehicle_body.camera_group_node.swap_camera()


func get_collision_detector(collision_mask:int=0x1):
	if collision_mask in collision_detectors:
		return collision_detectors[collision_mask]
		
	# No detector: create a common collision detector for a given mask
	var detector_node = Node3D.new()
	add_child(detector_node)
	var collision_detector = AI.CollisionDetector.new(detector_node)
	collision_detector.collision_mask = collision_mask
	# store the collision detector
	collision_detectors[collision_mask] = collision_detector
	return collision_detector


func shuffled_indices(num) -> Array[int]:
	# return the shuffled list of the first num indices
	# (that is integers from 0 to num - 1)
	var indices:Array[int] = []
	for i in range(num):
		indices.append(i)
	indices.shuffle()
	return indices


func _initialize_hud(current_level):
	var container = current_level.get_node(
			"HUD/MarginContainer/VBoxContainer")
	#
	var quit = container.get_node("HBoxContainer/Quit")
	quit.pressed.connect(_show_want_quit_popup.bind(quit))
	#
	var restart = container.get_node("HBoxContainer/Restart")
	restart.pressed.connect(_show_game_pause_popup.bind(restart))
	#
	var unlock = container.get_node("HBoxContainer3/UnlockVehicle")
	unlock.pressed.connect(_unlock_vehicle.bind(unlock))
	#
	if not OS.has_feature("mobile"):
		var aim_with_mouse = container.get_node(
				"HBoxContainer3/AimWithMouse")
		aim_with_mouse.pressed.connect(
				_toggle_aim_with_mouse.bind(aim_with_mouse))
	#
	var toggle_camera = container.get_node("HBoxContainer3/ToggleCamera")
	toggle_camera.pressed.connect(swap_camera.bind(toggle_camera))
	#
	var toggle_mute_sound = container.get_node(
			"HBoxContainer3/ToggleMuteSounds")
	toggle_mute_sound.pressed.connect(
			_enable_mute_all_sounds.bind(toggle_mute_sound))


func _setup_game_data():
	# initialize settings
	_initialize_configuration_options()
	_initialize_vehicle_settings()
	#
	MAIN_MENU_PATH = GAME_DATA["MAIN_MENU_PATH"]
	#
	GAME_OVER_SCENE = GAME_DATA["GAME_OVER_SCENE"]
	#
	GAME_OVER_POPUP = _load_resource_if_exists(GAME_DATA["GAME_OVER_POPUP"])
	#
	GAME_MENU_POPUP = _load_resource_if_exists(GAME_DATA["GAME_MENU_POPUP"])
	#
	GAME_PAUSE_POPUP = _load_resource_if_exists(GAME_DATA["GAME_PAUSE_POPUP"])
	#
	SET_USERNAME_POPUP = _load_resource_if_exists(GAME_DATA["SET_USERNAME_POPUP"])
	#
	WANT_QUIT_POPUP = _load_resource_if_exists(GAME_DATA["WANT_QUIT_POPUP"])
	#
	HIGH_SCORES_POPUP = _load_resource_if_exists(GAME_DATA["HIGH_SCORES_POPUP"])
	#
	HIGH_SCORES_LABEL = _load_resource_if_exists(GAME_DATA["HIGH_SCORES_LABEL"])
	#
	for item in GAME_DATA["LEVELS"]:
		LEVELS.append(item)
	#
	for key in GAME_DATA["audio_clips"]:
		audio_clips[key] = _load_resource_if_exists(GAME_DATA["audio_clips"][key])
	#
	for key in GAME_DATA["audio_clips_loop_start"]:
		audio_clips_loop_start[key] = GAME_DATA["audio_clips_loop_start"][key]
	#
	SOUND_PLAYER_SCENE = _load_resource_if_exists(GAME_DATA["SOUND_PLAYER_SCENE"])
	MUSIC_PLAYER_SCENE = _load_resource_if_exists(GAME_DATA["MUSIC_PLAYER_SCENE"])
	#
	progress_animation_res = _load_resource_if_exists(GAME_DATA["progress_animation_res"])
	# 
	level_debug_idx = len(LEVELS) - 1
	num_levels = len(LEVELS)


func _initialize_configuration_options():
	current_configuration_options = GAME_DATA["configuration_options"].duplicate(true)


func _initialize_vehicle_settings():
	current_vehicle_settings = GAME_DATA["vehicle_settings"].duplicate(true)


func _reset_game_state():
	if is_instance_valid(current_vehicle):
		current_vehicle.queue_free()
	_set_current_vehicle(null)
	# reset current level idx
	current_level_idx = 0
	current_game_session = 0
	levels_completed = 0
	# get current level path
	current_level_path = ""
	# reset score/vehicle number
	_set_score(GAME_DATA["starting_score"])
	_set_vehicle_number(GAME_DATA["starting_available_vehicle_number"])
	is_game_over = false
	# reset score related stuff
	_reset_score_related()


func _reset_score_related():
	for end_zone in current_end_zones:
		end_zone.queue_free()
	current_end_zones.clear()
	current_end_zones_enabled = false
	score_current_level = 0
	score_level_increasing_factor = (
			GAME_DATA["LEVELS"][current_level_idx]["score_level_increasing_factor"])


func _current_score_level_base():
	var score_base = (
			GAME_DATA["LEVELS"][current_level_idx]["score_level_base"])
	var score_level_base = (score_base + 
			ceil(float(current_game_session * score_base * 
			score_level_increasing_factor)))
	return score_level_base


func _enable_mute_all_sounds(toggle_mute_sound:TextureButton):
	toggle_mute_sound.release_focus()
	toggle_mute_sound.toggle_checking(toggle_mute_sound.CHECKED)
	enable_all_sounds(not toggle_mute_sound.CHECKED)


func _show_game_menu_popup(button:TextureButton=null):
	var popup = GAME_MENU_POPUP.instantiate()
	#
	if button:
		button.release_focus()
	#
	var recapture_mouse = false
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		_toggle_aim_with_mouse()
		recapture_mouse = true
	#
	var resume = popup.get_node("VBoxContainer/HBoxContainer/Resume")
	resume.pressed.connect(_want_resume.bind(popup, recapture_mouse))
	#
	var restart_level = popup.get_node(
			"VBoxContainer/HBoxContainer/RestartLevel")
	restart_level.pressed.connect(_want_restart_level.bind(popup))
	#
	@warning_ignore("shadowed_variable")
	var save_game = popup.get_node("VBoxContainer/HBoxContainer/SaveGame")
	# INFO: we only allow you to save your game when you have played at least one session
	if current_game_session > 0:
		save_game.pressed.connect(_want_save_game.bind(popup))
	else:
		save_game.disabled = true
		save_game.focus_mode = Control.FOCUS_NONE
	#
	var restart_game = popup.get_node(
			"VBoxContainer/HBoxContainer/RestartGame")
	restart_game.pressed.connect(_want_restart_game.bind(popup))
	#
	canvas_layer.add_child(popup)
	popup.popup_centered()
	get_tree().paused = true


func _show_game_pause_popup(button:TextureButton=null):
	var popup = GAME_PAUSE_POPUP.instantiate()
	#
	if button:
		button.release_focus()
	#
	var recapture_mouse = false
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		_toggle_aim_with_mouse()
		recapture_mouse = true
	#
	var resume = popup.get_node("VBoxContainer/HBoxContainer/Resume")
	resume.pressed.connect(_want_resume.bind(popup, recapture_mouse))
	#
	var restart_level = popup.get_node(
			"VBoxContainer/HBoxContainer/RestartLevel")
	restart_level.pressed.connect(_want_restart_level.bind(popup))
	#
	var restart_game = popup.get_node(
			"VBoxContainer/HBoxContainer/RestartGame")
	restart_game.pressed.connect(_want_restart_game.bind(popup))
	#
	canvas_layer.add_child(popup)
	popup.popup_centered()
	get_tree().paused = true


func _show_game_over_popup():
	var popup = GAME_OVER_POPUP.instantiate()
	
	popup.get_node("VBoxContainer/BackToMain").pressed.connect(
			_back_to_main.bind(popup))
	
	canvas_layer.add_child(popup)
	popup.popup_centered()
	
	get_tree().paused = true


func _show_set_username_popup():
	var popup = SET_USERNAME_POPUP.instantiate()
	
	popup.get_node("VBoxContainer/OK").pressed.connect(
			_set_username_ok.bind(popup))
		
	popup.get_node("VBoxContainer/LineEdit").text = _get_username()

	canvas_layer.add_child(popup)
	popup.popup_centered()
	
	get_tree().paused = true


func _show_want_quit_popup(button:TextureButton=null):
	var popup = WANT_QUIT_POPUP.instantiate()
	if button:
		button.release_focus()
	#
	var recapture_mouse = false
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		_toggle_aim_with_mouse()
		recapture_mouse = true
	#
	var restart_session = popup.get_node("VBoxContainer/HBoxContainer/RestartSession")
	restart_session.pressed.connect(_want_restart_game.bind(popup))
	#
	@warning_ignore("shadowed_variable")
	var save_game = popup.get_node("VBoxContainer/HBoxContainer/SaveGame")
	if levels_completed > 0:
		save_game.pressed.connect(_want_save_game.bind(popup))
	else:
		save_game.disabled = true
		save_game.focus_mode = Control.FOCUS_NONE
	# 
	var resume = popup.get_node("VBoxContainer/HBoxContainer/Resume")
	resume.pressed.connect(
			_want_quit_popup_resume.bind(popup, recapture_mouse))
	# 
	canvas_layer.add_child(popup)
	popup.popup_centered()
	get_tree().paused = true


func _show_high_scores_popup():
	var popup = HIGH_SCORES_POPUP.instantiate()
	
	popup.get_node("VBoxContainer/OK").pressed.connect(
			_high_scores_popup_ok.bind(popup))
	#
	var save_dict = load_high_scores()
	var ordered_places = save_dict.keys()
	ordered_places.sort()
	ordered_places.reverse()
	for key in ordered_places:
		var item = save_dict[key]
		if item:
			var container = popup.get_node(
					"VBoxContainer/ScrollContainer/GridContainer")
			var label_items = [
				["name", HIGH_SCORES_LABEL.instantiate()],
				["score", HIGH_SCORES_LABEL.instantiate()],
				["level", HIGH_SCORES_LABEL.instantiate()],
				#["session", HIGH_SCORES_LABEL.instantiate()],
				["timestamp", HIGH_SCORES_LABEL.instantiate()],
			]
			for element in label_items:
				if (element[0] == "session") or (element[0] == "level"):
					var res = save_dict[key][element[0]]
					res += 1
					element[1].text = str(res)
					container.add_child(element[1])
					continue
				element[1].text = str(save_dict[key][element[0]])
				container.add_child(element[1])
	canvas_layer.add_child(popup)
	popup.popup_centered()


func _on_popup_hide(popup):
	get_tree().paused = false
	
	if is_instance_valid(popup):
		popup.queue_free()


func _want_resume(popup, recapture_mouse):
	if recapture_mouse:
		_toggle_aim_with_mouse()
		
	get_tree().paused = false
	
	if is_instance_valid(popup):
		popup.queue_free()


func _want_restart_level(popup):
	get_tree().paused = false
	
	if is_instance_valid(popup):
		popup.queue_free()
	
	var current_scene = get_current_scene()
	if "current_count" in current_scene:
		current_scene.current_count = 0


func _want_restart_game(popup):
	get_tree().paused = false
	
	if is_instance_valid(popup):
		popup.queue_free()
	
	_reset_game_state()
	load_new_scene(MAIN_MENU_PATH)


func _back_to_main(popup):
	get_tree().paused = false
	
	if is_instance_valid(popup):
		popup.queue_free()
	
	_reset_game_state()
	load_new_scene(MAIN_MENU_PATH)


func _want_quit_popup_resume(popup, recapture_mouse):
	if recapture_mouse:
		_toggle_aim_with_mouse()
		
	get_tree().paused = false
	
	if is_instance_valid(popup):
		popup.queue_free()


func _want_save_game(popup):
	# save the game
	save_game()
	# say the result
	var popup_label = popup.get_node("VBoxContainer/Label")
	var curr_text = popup_label.text
	popup_label.text = "GAME SAVED"
	await get_tree().create_timer(2.0).timeout
	if is_instance_valid(popup_label):
		popup_label.text = curr_text


func _high_scores_popup_ok(popup):
	get_tree().paused = false
	
	if is_instance_valid(popup):
		popup.queue_free()


func _unlock_vehicle(button:TextureButton=null):
	if button:
		button.release_focus()
	#
	if current_vehicle:
		current_vehicle_body.apply_blast(randf_range(5.0, 10.0))


func _toggle_aim_with_mouse(button:TextureButton=null):
	if button:
		button.release_focus()
	#
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	elif Input.mouse_mode == Input.MOUSE_MODE_VISIBLE:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func _get_username():
	if username:
		return username
	elif OS.has_environment("USERNAME"):
		return OS.get_environment("USERNAME")
	elif OS.has_environment("USER"):
		return OS.get_environment("USER")
	else:
		return "Player"


func _set_username_ok(popup):
	get_tree().paused = false
	
	var line_edit:LineEdit = popup.get_node("VBoxContainer/LineEdit")
	username = line_edit.text.strip_edges()
	
	if is_instance_valid(popup):
		popup.queue_free()


func _get_timestamp():
	var unix_time = Time.get_unix_time_from_system()
	var datetime = Time.get_datetime_string_from_unix_time(unix_time, true)
	return [unix_time, datetime]


func _set_score(value):
	score = value
	var score_to_end_zone_enabled = max(0, 
			current_score_enabling_end_zone() - score_current_level)
	# update GUI
	var score_label_name = "HUD/MarginContainer/VBoxContainer/HBoxContainer2/ScoreInfos"
	var score_label = get_current_scene().get_node_or_null(score_label_name)
	if score_label:
		score_label.set_score_label(score, score_to_end_zone_enabled)


func _sort_trunc_dict(dict:Dictionary, max_size, sort_field1, sort_field2):
	var sort_list = []
	for dict_value in dict.values():
		var inserted = false
		for pos in sort_list.size():
			var d_v1 = dict_value[sort_field1]
			var s_v1 = sort_list[pos][sort_field1]
			if d_v1 >= s_v1:
				var d_v2 = dict_value[sort_field2]
				var s_v2 = sort_list[pos][sort_field2]
				var ins_pos = pos
				if (d_v1 == s_v1) and (d_v2 < s_v2):
					ins_pos = pos + 1
				sort_list.insert(ins_pos, dict_value)
				inserted = true
				break
		if not inserted:
			sort_list.append(dict_value)
	dict.clear()
	var dict_size = min(sort_list.size(), max_size)
	for i in dict_size:
		dict[i] = sort_list[(dict_size - 1) - i]


func _set_vehicle_number(value):
	if value <= 0:
		is_game_over = true
	else:
		vehicle_number = value
		is_game_over = false


func _set_vehicle_number_ui():
	var num_label_name = "HUD/MarginContainer/VBoxContainer/HBoxContainer2/ScoreInfos"
	var num_label = get_current_scene().get_node(num_label_name)
	num_label.set_vehicle_number_label(vehicle_number)


func _set_current_vehicle(value):
	current_vehicle = value
	if current_vehicle:
		current_vehicle_body = current_vehicle.get_node("Body")
	else:
		current_vehicle_body = null


func _delete_saved_configurations():
	var res:bool = true
	for file_pair in _get_saved_configurations():
		res = res and (DirAccess.remove_absolute(file_pair[1]) == OK)
	return res


func _get_saved_configurations():
	var existent_files = []
	var persistent_files = GAME_DATA["persistent_files"]
	for file_key in persistent_files:
		var file = persistent_files[file_key]
		if saved_file_good(file):
			existent_files.append([file_key, file])
	return existent_files


func _write_configuration_options(save_dict):
	save_dict["CO_username"] = _get_username() 
	for key in current_configuration_options:
		var vs_key = "CO_" + key
		save_dict[vs_key] = current_configuration_options[key][0]


func _read_configuration_options(saved_dict):
	var res = true
	var username_value = saved_dict.get("CO_username", "CO_ERROR")
	if str(username_value) != "CO_ERROR":
		username = username_value
	res = res and (str(username_value) != "CO_ERROR")
	for key in current_configuration_options:
		var co_key = "CO_" + key
		var value = saved_dict.get(co_key, "CO_ERROR")
		if str(value) != "CO_ERROR":
			current_configuration_options[key][0] = value
		res = res and (str(value) != "CO_ERROR")
	return res


func _write_vehicle_settings(save_dict):
	for key in current_vehicle_settings:
		var vs_key = "VS_" + key
		save_dict[vs_key] = current_vehicle_settings[key][0] 


func _read_vehicle_settings(saved_dict):
	var res = true
	for key in current_vehicle_settings:
		var vs_key = "VS_" + key
		var value = saved_dict.get(vs_key, "VS_ERROR")
		current_vehicle_settings[key][0] = value
		res = res and (str(value) != "VS_ERROR")
	return res


func _load_and_parse_json(json_file):
	var open_file = FileAccess.open(json_file, FileAccess.READ)
	if not open_file:
		return false
	var test_json_conv = JSON.new()
	if test_json_conv.parse(open_file.get_as_text()) != OK:
		printerr(test_json_conv.get_error_message())
		printerr(test_json_conv.get_error_line())
		return {}
	open_file.close()
	return test_json_conv.data


func _load_joypad_map():
	var file_name = GAME_DATA["persistent_files"]["save_joypad_map_game"]
	var config:ControlConfigClass = _load_resource_if_exists(file_name)
	if config:
		return config.joypad_map
	return ""


func _load_resource_if_exists(file_name):
	# Check if file is a Resource
	if ResourceLoader.exists(file_name):
		return load(file_name)
	if FileAccess.file_exists(file_name):
		return load(file_name)
	return null
