class_name Chassis
extends MeshInstance3D


@export var material_list: Array[Material] = []

@export_node_path("MeshInstance3D") var front_light = NodePath()
@export_node_path("MeshInstance3D") var rear_light = NodePath()
@export_node_path("MeshInstance3D") var stop_light = NodePath()
@export_node_path("MeshInstance3D") var turn_lights_right = NodePath()
@export_node_path("MeshInstance3D") var turn_lights_left = NodePath()

var light_material_present = {
	# "light": [light_node, material]
}


func _ready():
	# choose random material (if requested)
	if material_list.size() > 0:
		var mat = material_list[randi() % material_list.size()]
		if mat is Material:
			set_surface_override_material(0, mat)
	# handle the lights
	_fill_light_material({
		"front_light": get_node_or_null(front_light),
		"rear_light": get_node_or_null(rear_light),
		"stop_light": get_node_or_null(stop_light),
		"turn_lights_right": get_node_or_null(turn_lights_right),
		"turn_lights_left": get_node_or_null(turn_lights_left),
	})


func turn_light(light:String, on:bool):
	# NOTE: works only with MeshInstance3D(s) and StandardMaterial3D(s)
	if not (light in light_material_present):
		return
	
	var mat:StandardMaterial3D = light_material_present[light][1]
	if on and (not mat.emission_enabled):
		mat.emission_enabled = true
		mat.emission_energy = 2.0
		mat.emission = mat.albedo_color
	elif (not on) and (mat.emission_enabled):
		mat.emission_enabled = false
		mat.emission = mat.albedo_color


func _fill_light_material(light_dict):
	for light in light_dict:
		var light_node:MeshInstance3D = light_dict[light]
		if (light_node != null) and (light_node is MeshInstance3D):
			var mat = light_node.mesh.surface_get_material(0)
			if mat is StandardMaterial3D:
				# HACK: we need to create unique material  for the light node
				# NOTE: a unique (i.e. duplicate) mesh should be unnecessary
				var mat_new = mat.duplicate(true)
				light_node.set_surface_override_material(0, mat_new)
				light_material_present[light] = [light_node, mat_new]


func _get_class():
	return "Chassis"


func _is_class(value):
	return "Chassis" == value
