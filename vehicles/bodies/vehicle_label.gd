extends Marker3D


@export_enum("Username", "Random", "None") var custom_label: String = "Username"
@export_node_path("MeshInstance3D") var chassis_path = NodePath()

# Gaming Names 2024
# (excerpt from: https://www.mysmartprice.com/gear/gaming/gaming-features/gaming-names-for-boys-and-girls/)
var common_english_nicknames: Array[String] = [
	"Snow", "Serenity", "Nuzzle", "Climax", "Saki", "Eve", "Zlatan",
	"Remy", "Kiss", "Nana", "Belle", "Roses", "Cotton", "Gretchen",
	"Necromancer", "Rosalie", "Amy", "Kaylin", "Sakura", "Pudding",
	"Peach", "Rockstar", "Lily", "Karen", "Linda", "Polaroid Pal",
	"Legend of Zelda", "Demonic Criminals", "Blistered Outlaw",
	"Super Mario", "Dooming", "Pigeon", "Bowser", "Inch", "Final Fantasy",
	"Orphan", "Aqua", "Inferior", "Invaders", "Numb", "Penguin", "Nest",
	"Fire-Bred", "Widow", "Mademoiselle", "Titanium", "Hurricane",
	"Ironsides", "Iron-Cut", "Tempest", "Iron Heart", "Steel Forge",
	"Pursuit", "Steel Foil", "Sick Rebellious Names", "Upsurge", 
	"Uprising", "Overthrow", "Breaker", "Sabotage", "Dissent", "Subversion",
	"Rebellion", "Insurgent", "Accidental Genius", "Acid Gosling", 
	"Admiral Tot", "AgentHercules", "Airport Hobo", "Alley Frog", "Alpha",
	"AlphaReturns", "Angel", "AngelsCreed", "Arsenic Coo", "Atomic Blastoid", 
	"Automatic Slicer", "Baby Brown", "Back Bett", "Bad Bunny", 
	"Bazooka Har-de-har", "Bearded Angler", "Beetle King", "Betty Cricket", 
	"Bit Sentinel", "Bitmap", "BlacKitten", "Blister", "Blistered Outlaw", 
	"Blitz", "BloodEater", "Bonzai", "BoomBeachLuvr", "Boom Blaster",
	"Bootleg Taximan", "Bowie", "Bowler", "Breadmaker", "Broomspun", 
	"Buckshot", "Bug Blitz", "Bug Fire", "Bugger", "Cabbie", "Candy Butcher", 
	"Capital F", "Captain Peroxide", "Celtic Charger", "Centurion Sherman", 
	"Chord Slayer", "Crossbow King", "Crystal Beast", "Cosmic Blitz", 
	"Dark Mask", "Enigma Seeker", "Fire Drake", "Flashbang", "Galactic Hero", 
	"Hidden Agent", "Torpedo", "Titanium", "Saint La-Z-Boy", "Vulture", 
	"Scare Stone", "The Armor", "Slaughter", "The Green Tiger", "Roadkill", 
	"Red Rhino", "The Brown Fox", "Sienna Princess", "Scary Pumpkin", "Ronin", 
	"The Orange Frog", "Silver Stone", "Tusk", "Steel Foil", "Sythe", "Sniper", 
	"Skull Crusher", "Relative Performance", "Sandbox", "Seal Snake", 
	"The Great Shark", "The Dark Horse", "The Red Wolf", "Unique Identity", 
	"Strong Position", "Sky Bully", "Ultimate Guide", "Rooster", "Roadblock", 
	"Schizo", "Rubble", "Team Arsenic", "Romance Guppy", "Stream Elements", 
	"Tweek", "Sidewalk Enforcer", "Silver Eagle", "Roar Sweetie", "Roller Turtle", 
	"Sherwood Gladiator", "Sidewalk Enforcer", "Wardon", "Trink", "Scalp", 
	"Scalp", "Romance Guppy", "Seal Snake", "Silver Eagle", "Scalp", "Shiver", 
	"Shiver", "Shiver", "Silver Stone", "Silver Eagle", "Sienna Princess", 
	"Steel Foil", "Majesty", "Anomaly", "Malice", "Banshee", "Mannequin", 
	"Belladonna", "Minx", "Beretta", "Mirage", "Black Beauty", "Nightmare", 
	"Calypso", "Nova", "Carbon", "Pumps", "Cascade", "Raven", "Colada", "Resin", 
	"Cosma", "Riveter", "Cougar", "Rogue", "Countess", "Roulette", "Enchantress", 
	"Shadow", "Enigma", "Siren", "Femme Fatale", "Stiletto", "Firecracker", 
	"Tattoo", "Geisha", "T-Back", "Goddess", "Temperance", "Half Pint", "Tequila", 
	"Harlem", "Terror", "Heroin", "Thunderbird", "Infinity", "Ultra", "Insomnia", 
	"Vanity", "Ivy", "Velvet", "Legacy", "Vixen", "Lithium", "Voodoo", "Lolita", 
	"Wicked", "Lotus", 
	]

@onready var chassis: MeshInstance3D = get_node_or_null(chassis_path)
@onready var label: Label3D = ScriptLibrary.find_nodes_by_type(Label3D, self, false)[0]


func _ready():
	if custom_label == "Username":
		label.text = globals._get_username()
	elif custom_label == "Random":
		var idx = randi() % common_english_nicknames.size()
		label.text = common_english_nicknames[idx]
	_setup_if_chassis.call_deferred()


func _setup_if_chassis():
	if chassis:
		var material: Material = null
		for i in chassis.get_surface_override_material_count():
			material = chassis.get_surface_override_material(i)
			if material:
				break
		if material:
			label.modulate = material.albedo_color
		else:
			label.modulate = chassis.mesh.surface_get_material(0).albedo_color
