class_name SteeringWheel
extends Node3D


@export var transform_targets: bool = false

# HACK: targets' transforms corrections by rotation value
@export var target_right_end_basis: Basis
@export var target_right_end_origin: Vector3
@export var target_left_end_basis: Basis
@export var target_left_end_origin: Vector3
@export_range(0.0, 90.0, 1.0, "radians_as_degrees") var MAX_VALUE: float = deg_to_rad(80.0)

# right target
var target_right: RemoteTransform3D
var target_right_start_trans: Transform3D
var target_right_end_trans: Transform3D
# left target
var target_left: RemoteTransform3D
var target_left_start_trans: Transform3D
var target_left_end_trans: Transform3D
# 
@onready var pose_transform = transform


func _ready():
	# HACK
	if transform_targets:
		target_right = find_child("RemoteTransformTargetRight", true)
		target_left = find_child("RemoteTransformTargetLeft", true)
		if target_right and target_left:
			target_right_start_trans = target_right.transform
			target_right_end_trans = Transform3D(target_right_end_basis,
					target_right_end_origin)
			target_left_start_trans = target_left.transform
			target_left_end_trans = Transform3D(target_left_end_basis,
					target_left_end_origin)


func update(value):
	var rotate_transform = Transform3D(Basis(-Vector3.FORWARD, value), Vector3.ZERO)
	transform = pose_transform * rotate_transform
	# HACK
	if transform_targets:
		var value_ratio = value / MAX_VALUE
		if value_ratio <= 0.0:
			# steering right
			var weight = clamp(-value_ratio, 0.0, 1.0)
			target_left.transform = target_left_start_trans.interpolate_with(
					target_left_end_trans, weight)
		else:
			# steering left
			var weight = clamp(value_ratio, 0.0, 1.0)
			target_right.transform = target_right_start_trans.interpolate_with(
					target_right_end_trans, weight)


func _get_class():
	return "SteeringWheel"


func _is_class(value):
	return "SteeringWheel" == value
