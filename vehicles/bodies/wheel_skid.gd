class_name WheelSkid
extends Node3D
# Directly inspired by the project: https://gitlab.com/febueldo/godot-vehicle.git


@export var enabled: bool = true: set = _set_enabled

@export var wheel: VehicleWheel3D = null
@export var sound: Resource = null

@export var POINTS_MAX: int = 255

@export var SKID_MIN: float = 0.1
@export var SKID_SIZE: float = 0.125
@export var SKID_ELEV: Vector3 = Vector3(0.0, 0.01, 0.0)

@export var material_draw_triangle: Material = null
@export var material_draw_point: Material = null


class Point extends Resource:
	var origin: Vector3
	var normal: Vector3
	var drawable: bool
	
	func _init(p_origin=Vector3.ZERO, p_normal=Vector3.UP,
			p_drawable=false):
		origin = p_origin
		normal = p_normal
		drawable = p_drawable
	
	func set_drawable(p_drawable):
		drawable = p_drawable


class PointRingContainer extends Resource:
	# implemented as a prebuild ring container (of fixed size)
	var size: int
	var beg_iter: int # first element
	var end_iter: int # one past last element
	# private variables
	var _container: Array[Point]
	
	func _init(p_size):
		size = p_size
		beg_iter = 0
		end_iter = size - 1
		_container.resize(size)
		for i in size:
			_container[i] = Point.new()
	
	func append(origin:Vector3, normal:Vector3, drawable:bool):
		end_iter = _incr(end_iter, 1)
		#
		_container[end_iter].origin = origin
		_container[end_iter].normal = normal
		_container[end_iter].drawable = drawable
		#
		beg_iter = _incr(beg_iter, 1)
	
	# optimization
	func append_non_drawable():
		end_iter = _incr(end_iter, 1)
		#
		_container[end_iter].drawable = false
		#
		beg_iter = _incr(beg_iter, 1)
	
	func front():
		return at(0)
	
	func back():
		return at(size - 1)
	
	func at(idx:int):
		assert(idx < size)
		return _container[_incr(beg_iter, idx)]
	
	func _incr(idx, value):
		var new_idx = idx + value
		if new_idx >= size:
			new_idx -= size
		return new_idx


const DOWN_RAY = Vector3.DOWN * 10.0

var mesh:ImmediateMesh = null

@onready var mesh_instance:MeshInstance3D = $TopLevel
@onready var player := $AudioStreamPlayer3D
@onready var points := PointRingContainer.new(POINTS_MAX)
@onready var collision_query := PhysicsRayQueryParameters3D.new()
@onready var direct_space_state := get_world_3d().direct_space_state
@onready var drawable_points := 0


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	mesh_instance.cast_shadow = GeometryInstance3D.SHADOW_CASTING_SETTING_OFF
	mesh_instance.mesh = ImmediateMesh.new()
	mesh = mesh_instance.mesh
	#
	if not material_draw_triangle:
		material_draw_triangle = StandardMaterial3D.new()
		#material_draw_triangle.transparency = 0.25
		material_draw_triangle.shading_mode = BaseMaterial3D.SHADING_MODE_PER_VERTEX
		material_draw_triangle.albedo_color = Color.hex(0x000000ff)
	if not material_draw_point:
		material_draw_point = StandardMaterial3D.new()
		material_draw_point.shading_mode = BaseMaterial3D.SHADING_MODE_PER_VERTEX
		material_draw_point.albedo_color = Color.hex(0xff0000ff)
		material_draw_point.use_point_size = true
		material_draw_point.point_size = 5.0
	# set sound
	if sound:
		player.stream = sound
	
	# wheel skid are not allowed on "low end device"
	if globals.current_configuration_options["low_end_device"][0]:
		queue_free()


func _physics_process(_delta):
	set_points()
	draw_tracks()


func set_points():
	if (wheel.is_in_contact() and 
			(wheel.get_skidinfo() < SKID_MIN)):
		# HACK: being a ring structure, when a point is appended
		# the first one in the ring is replaced by the appended one
		if not points.front().drawable:
			drawable_points = clampi(drawable_points + 1, 0, POINTS_MAX)
		# cast a ray
		collision_query.from = wheel.global_position
		collision_query.to = wheel.global_position + DOWN_RAY
		var result = direct_space_state.intersect_ray(collision_query)
		if result:
			# append a drawable point
			points.append(mesh_instance.to_local(result["position"]),
					result["normal"], true)
			# play sound
			if sound and (not player.playing):
				player.play()
	else:
		# HACK: being a ring structure, when a point is appended
		# the first one in the ring is removed
		if points.front().drawable:
			drawable_points = clampi(drawable_points - 1, 0, POINTS_MAX)
		# append a non-drawable point
		#points.append(Vector3.ZERO, Vector3.UP, false)
		# optimization
		points.append_non_drawable()
		
		# stop sound
		if sound and player.playing:
			player.stop()


func draw_tracks():
	# clear surfaces anyway
	mesh.clear_surfaces()
	var added_points = 0
	if drawable_points > 0:
		# start drawing
		added_points = _draw_triangles()
		#added_points = _draw_points()
	#
	assert(drawable_points == added_points)


func _draw_triangles():
	var added_points = 0
	var quad_drawn = false
	# TRIANGLES
	mesh.surface_begin(Mesh.PRIMITIVE_TRIANGLES,
			material_draw_triangle)
	for i in (points.size - 1):
		var a = points.at(i)
		var b = points.at(i + 1)
		if a.drawable:
			added_points += 1
			var up_a = a.normal
			var forward = b.origin - a.origin
			var right_a = forward.cross(up_a).normalized()
			var delta_a = right_a * SKID_SIZE + SKID_ELEV
			if b.drawable:
				# draw a quad
				quad_drawn = true
				var up_b = b.normal
				var right_b = forward.cross(up_b).normalized()
				var delta_b = right_b * SKID_SIZE + SKID_ELEV
				#
				mesh.surface_add_vertex((a.origin - delta_a) + SKID_ELEV) 
				mesh.surface_add_vertex((b.origin - delta_b) + SKID_ELEV)
				mesh.surface_add_vertex((a.origin + delta_a) + SKID_ELEV)
				#
				mesh.surface_add_vertex((a.origin + delta_a) + SKID_ELEV)
				mesh.surface_add_vertex((b.origin - delta_b) + SKID_ELEV)
				mesh.surface_add_vertex((b.origin + delta_b) + SKID_ELEV)
		if (i == points.size - 2) and b.drawable:
			# last point
			added_points += 1
	# HACK: to avoid the "No vertices were added, surface can't be created."
	# warning (issued when only isolated points are present)
	if not quad_drawn:
		var single_point = points.at(0).origin + SKID_ELEV
		mesh.surface_add_vertex(single_point)
		mesh.surface_add_vertex(single_point)
		mesh.surface_add_vertex(single_point)
	# commit drawing
	mesh.surface_end()
	#
	return added_points


func _draw_points():
	var added_points = 0
	# POINTS
	mesh.surface_begin(Mesh.PRIMITIVE_POINTS,
			material_draw_point)
	
	for i in points.size:
		var a = points.at(i)
		if a.drawable:
			added_points += 1
			mesh.surface_add_vertex(a.origin)
		else:
			continue
	# commit drawing
	mesh.surface_end()
	#
	return added_points


func _get_class():
	return "WheelSkid"


func _is_class(value):
	return "WheelSkid" == value


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)
