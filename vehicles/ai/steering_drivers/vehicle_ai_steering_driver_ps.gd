class_name VehicleAIDriverPS
extends VehicleAIDriver


@export var priority_steering_identifier_list: Array[String] = []


### NOTE: VehicleAIPrioritySteering's fms states
class PS_BaseState extends StateMachine.State:
		
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
	
	func _on_enter_state() -> void:
		print("entering: ", id)
	
	func _on_leave_state() -> void:
		print("leaving: ", id)


class PS_Pursuing extends PS_BaseState:
	# permanence in state as long as pursuing items or player
	
	func _on_enter_state() -> void:
		#super()
		var first_entered = target.gathered_targets.values()[0]
		target.vehicle_steering.target = first_entered
		target.vehicle_steering.set_deferred_priority_steering.call_deferred(
					target.priority_steering_identifier_list[0])
	
	func _on_leave_state() -> void:
		#super()
		pass


class PS_Wandering extends PS_BaseState:
	# permanence in state as long as wandering
	
	func _on_enter_state() -> void:
		#super()
		target.vehicle_steering.target = null
		target.vehicle_steering.set_deferred_priority_steering.call_deferred(
					target.priority_steering_identifier_list[1])
		
	func _on_leave_state() -> void:
		#super()
		pass


func assign_detector_to_behaviors(detector):
	# iterate over all inner behaviors of the characters
	# to set the detector for behaviors requiring it
	for priority_steering_identifier in vehicle_steering.priority_steering_dict:
		var priority_steering = vehicle_steering.priority_steering_dict[priority_steering_identifier]
		_assign_property_to_priority_steering(priority_steering,
				"detector", detector)


func assign_paths_to_behaviors():
	# iterate over all inner behaviors of the characters
	# to set the detector for behaviors requiring it
	for priority_steering_identifier in vehicle_steering.priority_steering_dict:
		var priority_steering = vehicle_steering.priority_steering_dict[priority_steering_identifier]
		_assign_property_to_priority_steering(priority_steering,
				"path", path)


func assign_targets_to_behaviors(all_vehicle_drivers):
	# get all characters (i.e. the targets)
	var all_targets = []
	for vehicle_driver in all_vehicle_drivers:
		var vehicle_driver_steering:AI.SteeringBehavior
		# all priority steering's have the same character
		vehicle_driver_steering = (
				vehicle_driver.vehicle_steering.priority_steering)
		all_targets.append(vehicle_driver_steering.character)
	# assign the targets
	for priority_steering_identifier in vehicle_steering.priority_steering_dict:
		# assign the targets (i.e. the others characters) to the 
		# "targets" attribute of all the behaviors that has it
		var priority_steering = vehicle_steering.priority_steering_dict[
				priority_steering_identifier]
		_assign_targets_to_priority_steering(priority_steering, all_targets)


func _setup():
	super()
	# set default behavior
	assert(vehicle_steering is VehicleAIPrioritySteering)
	vehicle_steering.set_deferred_priority_steering.call_deferred(
			priority_steering_identifier_list[1])


func _setup_fsm():
	fsm = StateMachineFactory.new().create({
			"target": self,
			"current_state": "PS_Pursuing",
			"states": [
				{"id": "PS_Pursuing", "state": PS_Pursuing},
				{"id": "PS_Wandering", "state": PS_Wandering},
			],
			"transitions": [
				{"state_id": "PS_Pursuing", "to_states": [
					"PS_Wandering"]},
				{"state_id": "PS_Wandering", "to_states": [
					"PS_Pursuing"]},
			]
		})


func _update_fsm():
	if fsm:
		var current_state = fsm.current_state
		match current_state:
			"PS_Pursuing":
				if gathered_targets.is_empty():
					fsm.transition("PS_Wandering")
				else:
					# always prefer the default target
					if default_target in gathered_targets:
						vehicle_steering.target = default_target
					else:
						# choose the target with the highest priority
						_choose_target_by_priority()
			"PS_Wandering":
				if not gathered_targets.is_empty():
					fsm.transition("PS_Pursuing")


func _assign_property_to_priority_steering(priority_steering,
		property, value):
	for group in priority_steering.groups:
		for bhv_wght in group.behaviors:
			var bhv = bhv_wght.behavior
			# check if property is present
			if property in bhv:
				bhv.set(property, value)
			# check errors
			assert(priority_steering.character == bhv.character, 
							"ERROR: characters don't match!")


func _assign_targets_to_priority_steering(priority_steering, all_targets):
	for group in priority_steering.groups:
		for bhv_wght in group.behaviors:
			var bhv = bhv_wght.behavior
			# check if targets is needed
			if "targets" in bhv:
				var targets = []
				for other in all_targets:
					if other != bhv.character:
						targets.append(other)
				bhv.targets = targets
			# check errors
			assert(priority_steering.character == bhv.character, 
							"ERROR: characters don't match!")


func _get_class():
	return "VehicleAIDriverPS"


func _is_class(value):
	return "VehicleAIDriverPS" == value
