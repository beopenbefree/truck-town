class_name VehicleAIDriver
extends Node3D


signal repair_finished(vehicle_ai_driver:VehicleAIDriver)
signal repair_started(vehicle_ai_driver:VehicleAIDriver)


@export var enabled: bool = true: set = _set_enabled

@export var path: Path3D = null
@export var gather_area_radius: float = 30.0
@export var max_chasing_distance: float = 50.0
@export_range(0.0, 1.0, 0.05) var gather_area_max_delta_radius: float = 0.2 # fraction
@export var vehicle_sound_name: String = String()

@export_range(0.0, 1.0, 0.05) var min_resource_refill_ratio: float = 0.1

var default_target: Node3D = null
var gathered_targets: Dictionary = {}
var fsm: StateMachine = null

# working variables
@onready var vehicle_steering: VehicleAISteeringBehavior = ScriptLibrary.find_nodes_by_type(
		VehicleAISteeringBehavior, self, false)[0]
@onready var gather_area: Area3D = ScriptLibrary.find_nodes_by_type(
		GatherArea, self, false)[0]
@onready var vehicle_body: VehicleAI = ScriptLibrary.find_nodes_by_type(
		VehicleAI, self, false)[0]
@onready var max_chasing_distance_2: float = max_chasing_distance ** 2
@onready var chasing_distance_2: float = 0.0

@onready var max_health_no_refill: int = int(min_resource_refill_ratio *
		vehicle_body.MAX_VEHICLE_AI_HEALTH)
@onready var max_ammo_no_refill: int = int(min_resource_refill_ratio *
		vehicle_body.shooter_node.AMMO_AMOUNT)

# optimization
@onready var AI_PHYSICS_FRAME_UPDATE: int = globals.AI_PHYSICS_FRAME_UPDATE
@onready var accumulated_delta: float = 0.0


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	_setup()
	_setup_fsm()


func _physics_process(delta):
	accumulated_delta += delta
	if Engine.get_physics_frames() % AI_PHYSICS_FRAME_UPDATE == 0:
		# update state
		var delta_pos = (vehicle_body.global_position -
				default_target.global_position)
		chasing_distance_2 = delta_pos.dot(delta_pos)
		# check for fsm state change
		_update_fsm()
		#
		accumulated_delta = 0.0


func assign_detector_to_behaviors(_detector):
	# to be overridden
	push_warning("Unimplemented assign_detector_to_behaviors()")


func assign_paths_to_behaviors():
	# to be overridden
	push_warning("Unimplemented assign_paths_to_behaviors()")


func assign_targets_to_behaviors(_all_vehicle_drivers):
	# to be overridden
	push_warning("Unimplemented assign_targets_to_behaviors()")


func _setup():
	# setup the vehicle steering
	vehicle_steering.enabled = enabled
	# complete the inner behaviors' configuration
	# get collision detector for obstacle avoidance behaviors
	# with mask = 0x20000000 = 0b00100000000000000000000000000000 (bit #30)
	var collision_detector = globals.get_collision_detector(0x20000000)
	# assign detector to behaviors
	assign_detector_to_behaviors(collision_detector)
	# assign paths to behaviors
	assign_paths_to_behaviors()
	# play engine sound for vehicle body
	vehicle_body.play_sound_engine(vehicle_sound_name)
	# setup the gather area
	gather_area.radius = (gather_area_radius * 
			(1.0 + randf_range(-gather_area_max_delta_radius,
					gather_area_max_delta_radius)))
	
	# started/finished repair signal connection
	vehicle_body.repair_started.connect(_on_vehicle_body_repair_started)
	vehicle_body.repair_finished.connect(_on_vehicle_body_repair_finished)
	
	# HACK: gather area could have (uniform) scale, so radius, 
	# which is given in global space, should be corrected
	var scale_vec = gather_area.global_basis.get_scale()
	assert(is_equal_approx(scale_vec.x, scale_vec.y) and 
			is_equal_approx(scale_vec.y, scale_vec.z))
	var scale_correction = 1.0 / scale_vec.y
	gather_area.radius *= scale_correction
	
	gather_area.area_entered.connect(_on_Area_area_entered)
	gather_area.area_exited.connect(_on_Area_area_exited)
	gather_area.body_entered.connect(_on_Area_body_entered)
	gather_area.body_exited.connect(_on_Area_body_exited)
	#
	_set_deferred_default_target.call_deferred()


func _setup_fsm():
	# to be overridden
	push_warning("Unimplemented assign_paths_to_behaviors()")


func _set_deferred_default_target():
	var target = globals.current_vehicle_body
	while not (target is Vehicle):
		vehicle_steering.enabled = false
		target = globals.current_vehicle_body
	default_target = target
	vehicle_steering.enabled = enabled


func _choose_target_by_priority():
	var best_target:Node3D = null
	for target in gathered_targets.values():
		if ((not best_target) or 
				(target.target_priority > 
						best_target.target_priority)):
			best_target = target
	if best_target:
		vehicle_steering.target = best_target


func _choose_target_by_needs():
	var need_health: bool = (vehicle_body.vehicle_ai_health <
			max_health_no_refill)
	var need_ammo: bool = false
	for weapon:WeaponBase in vehicle_body.shooter_node.weapons.values():
		if weapon and (weapon.spare_ammo < max_ammo_no_refill):
			need_ammo = true
			break
	# choose the most suited target
	var best_target:Node3D = default_target
	for target in gathered_targets.values():
		if need_health and (target is HealthPickupTrigger):
			best_target = target
			break
		if need_ammo and (target is AmmoPickupTrigger):
			best_target = target
			break
	vehicle_steering.target = best_target


func _update_fsm():
	# to be overridden
	push_warning("Unimplemented assign_paths_to_behaviors()")


func _on_vehicle_body_repair_started(_vehicle_ai:VehicleAI):
	repair_started.emit(self)


func _on_vehicle_body_repair_finished(_vehicle_ai:VehicleAI):
	repair_finished.emit(self)


func _on_Area_area_entered(area):
	if enabled:
		_insert_in_gathered(area)


func _on_Area_area_exited(area):
	if enabled:
		_remove_from_gathered(area)


func _on_Area_body_entered(body):
	if enabled:
		_insert_in_gathered(body)


func _on_Area_body_exited(body):
	if enabled:
		_remove_from_gathered(body)


func _insert_in_gathered(object):
	if "target_priority" in object:
		gathered_targets[object] = object


func _remove_from_gathered(object):
	if "target_priority" in object:
		gathered_targets.erase(object)


func _get_class():
	return "VehicleAIDriver"


func _is_class(value):
	return "VehicleAIDriver" == value


func _set_enabled(value):
	enabled = value
