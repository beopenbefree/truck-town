class_name VehicleAIDriverSP
extends VehicleAIDriver


@export var steering_pipeline_identifier_list: Array[String] = []


#### NOTE: VehicleAISteeringPipeline's fms states
class SP_BaseState extends StateMachine.State:
		
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
	
	func _on_enter_state() -> void:
		print("entering: ", id)
	
	func _on_leave_state() -> void:
		print("leaving: ", id)


class SP_GatheringItems extends SP_BaseState:
	# permanence in state as long as gathering items
	
	func _on_enter_state() -> void:
		#super()
		target.vehicle_steering.set_deferred_steering_pipeline.call_deferred(
				target.steering_pipeline_identifier_list[0])
	
	func _on_leave_state() -> void:
		#super()
		pass


class SP_ChasingPlayer extends SP_BaseState:
	# permanence in state as long as chasing player
	
	func _on_enter_state() -> void:
		#super()
		target.vehicle_steering.target = target.default_target
		target.vehicle_steering.set_deferred_steering_pipeline.call_deferred(
				target.steering_pipeline_identifier_list[0])
	
	func _on_leave_state() -> void:
		#super()
		pass


class SP_FightingPlayer extends SP_BaseState:
	# permanence in state as long as fighting player
	
	func _on_enter_state() -> void:
		#super()
		target.vehicle_steering.target = target.default_target
		target.vehicle_steering.set_deferred_steering_pipeline.call_deferred(
				target.steering_pipeline_identifier_list[1])
	
	func _on_leave_state() -> void:
		#super()
		pass


func assign_detector_to_behaviors(detector):
	# iterate over all steering_pipelines to set
	# the detector for components requiring it
	for steering_pipeline_identifier in vehicle_steering.steering_pipeline_dict:
		#  constraints
		var steering_pipeline = vehicle_steering.steering_pipeline_dict[steering_pipeline_identifier]
		var constraints = steering_pipeline.constraints
		for constraint in constraints:
			if "detector" in constraint:
				constraint.detector = detector


func assign_paths_to_behaviors():
	pass


func assign_targets_to_behaviors(all_vehicle_drivers):
	# get all characters (i.e. the targets)
	var all_targets = []
	for vehicle_driver in all_vehicle_drivers:
		var vehicle_driver_steering:AI.SteeringBehavior
		vehicle_driver_steering = (
				vehicle_driver.vehicle_steering.steering_pipeline)
		all_targets.append(vehicle_driver_steering.character)
	# assign the targets
	# constraints
	var constraints = vehicle_steering.steering_pipeline.constraints
	for constraint in constraints:
		# check if targets is needed
		if "targets" in constraint:
			var targets = []
			var this_character = vehicle_steering.steering_pipeline.character
			for other in all_targets:
				if other != this_character:
					targets.append(other)
			constraint.targets = targets


func _setup():
	super()
	# set default behavior
	assert(vehicle_steering is VehicleAISteeringPipeline)
	vehicle_steering.set_deferred_steering_pipeline.call_deferred(
			steering_pipeline_identifier_list[0])


func _setup_fsm():
	fsm = StateMachineFactory.new().create({
			"target": self,
			"current_state": "SP_GatheringItems",
			"states": [
				{"id": "SP_GatheringItems", "state": SP_GatheringItems},
				{"id": "SP_ChasingPlayer", "state": SP_ChasingPlayer},
				{"id": "SP_FightingPlayer", "state": SP_FightingPlayer},
			],
			"transitions": [
				{"state_id": "SP_GatheringItems", "to_states": [
					"SP_ChasingPlayer"]},
				{"state_id": "SP_ChasingPlayer", "to_states": [
					"SP_GatheringItems", "SP_FightingPlayer"]},
				{"state_id": "SP_FightingPlayer", "to_states": [
					"SP_ChasingPlayer"]},
			]
		})


func _update_fsm():
	if fsm:
		var current_state = fsm.current_state
		match current_state:
			"SP_GatheringItems":
				if not gathered_targets.is_empty():
					# OPTION 1: choose the target by needs
					_choose_target_by_needs()
					# OPTION 2: choose the target with the highest priority
					#_choose_target_by_priority()
					# OPTION 3: choose the target by heuristic
					#_choose_target_by_heuristic()
					if vehicle_steering.target == default_target:
						# go to chase player
						fsm.transition("SP_ChasingPlayer")
				else:
					# always prefer the chase player
					fsm.transition("SP_ChasingPlayer")
			"SP_ChasingPlayer":
				assert(vehicle_steering.target == default_target)
				if chasing_distance_2 < max_chasing_distance_2:
					# INFO: we could use a raycast for visibility
					# check if target is visible
					var player_dir = default_target.global_position - vehicle_body.global_position
					var player_visible = vehicle_body.global_basis.z.dot(player_dir) > 0.0
					if player_visible:
						# go to fight player
						fsm.transition("SP_FightingPlayer")
				elif not gathered_targets.is_empty():
					# OPTION 1: choose the target by needs
					_choose_target_by_needs()
					# OPTION 2: choose the target with the highest priority
					#_choose_target_by_priority()
					# OPTION 3: choose the target by heuristic
					#_choose_target_by_heuristic()
					if vehicle_steering.target != default_target:
						fsm.transition("SP_GatheringItems")
			"SP_FightingPlayer":
				# INFO: we could use a raycast for visibility
				# check if target far away or not visible
				var player_dir = default_target.global_position - vehicle_body.global_position
				var player_not_visible = vehicle_body.global_basis.z.dot(player_dir) <= 0.0
				if ((chasing_distance_2 >= max_chasing_distance_2) or 
						player_not_visible):
					fsm.transition("SP_ChasingPlayer")


func _choose_target_by_heuristic():
	# HACK: apply an heuristic to choose the target between
	# the current and the last entered object in the gather area
	var last_entered:Node3D = gathered_targets.values()[gathered_targets.size() - 1]
	if is_instance_valid(vehicle_steering.target):
		# the current target is valid
		if vehicle_steering.target != last_entered:
			# NOTE: we only consider the last entered object as an
			# approximation, since, even though several objects have
			# entered since the last update, we assume they are all
			# within a similar distance from the vehicle
			var sp_decomposer:AISteeringPipelineImpl.PlanningDecomposer = (
					vehicle_steering.steering_pipeline.decomposers[0])
			# HEURISTIC: compare the "sizes" of the two paths, and not their
			# "lengths" (since this would too expensive)
			if sp_decomposer.path:
				# current path is 20% more "preferred"
				var current_path_size = sp_decomposer.path.size * 1.2
				var start_pos = vehicle_steering.steering_pipeline.character.position
				var end_pos = last_entered.global_position
				var last_path_size = AI.NavMapPath.new(NavigationServer3D.map_get_path(
						sp_decomposer.map, start_pos, end_pos, sp_decomposer.optimize)).size
				# choose the last entered only if it's on the "shortest" path
				if last_path_size <= current_path_size:
					vehicle_steering.target = last_entered
	else:
		vehicle_steering.target = last_entered


func _get_class():
	return "VehicleAIDriverSP"


func _is_class(value):
	return "VehicleAIDriverSP" == value
