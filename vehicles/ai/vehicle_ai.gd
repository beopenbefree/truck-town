class_name VehicleAI
extends VehicleBase


signal repair_finished(vehicle_ai:VehicleAI)
signal repair_started(vehicle_ai:VehicleAI)


@export_node_path("VehicleAISteeringBehavior") var steering_behavior_path = NodePath()

@export var MAX_VEHICLE_AI_HEALTH: int = 100
@export var DESTROYED_TIME: int = 20

# sound data for gears
var current_volume_db: float = 0.0

# AI
var action_pressed_turn: bool = false
var action_strength_turn: float = 0.0
var action_pressed_motion: bool = false
var action_strength_motion: float = 0.0

# current data
@onready var steering_behavior: Node3D = get_node(steering_behavior_path)
@onready var vehicle_ai_health: int = MAX_VEHICLE_AI_HEALTH
@onready var destroyed_timer: float = -1.0 # when <=0 then healthy
@onready var fire_holder: Node3D = $FireHolder


func _ready():
	super()
	
	# set fire related stuff
	fire_holder.emit(false)


func _physics_process(delta):
	super(delta)
	
	# check if healty
	if vehicle_ai_health <= 0:
		if destroyed_timer > 0:
			# recovering from destruction
			destroyed_timer -= delta
			if current_sound:
				# fade current sound
				var volume_db = current_sound.get_volume_db()
				current_sound.set_volume_db(volume_db - 10e-1)
			return
		else:
			# recovering complete: restore operations
			destroyed_timer = -1.0 # when <=0 then healthy
			vehicle_ai_health = MAX_VEHICLE_AI_HEALTH
			repair_finished.emit(self)
			fire_holder.emit(false)
			steering_behavior.enabled = true
			shooter_node.enabled = true
			if current_sound:
				# restore current sound
				current_sound.set_volume_db(current_volume_db)
	
	# reset target
	steer_target = 0.0
	if steering_enabled:
		if action_pressed_turn:
			steer_target = action_strength_turn
		steer_target *= steer_limit
	
	# reset target
	motion_target = 0.0
	var speed = linear_velocity.length()
	var speed_inv = 1.0 / speed if speed != 0.0 else INF
	# AI
	velocity = linear_velocity
	var motion_direction = linear_velocity.dot(transform.basis.z)
	engine_force = 0
	
	if not changing_gear:
		if action_pressed_motion:
			motion_target = action_strength_motion
		engine_force = motion_target * engine_force_boost
		if motion_target > 0.0:
			engine_force *= engine_force_value
			if get_tilt() > tilt_max:
				engine_force *= max(10.0 * speed_inv, 
						tilt_engine_force_factor)
		else:
			engine_force *= engine_force_value_backward
			if get_tilt() < -tilt_max:
				engine_force *= max(8.0 * speed_inv, 
						tilt_engine_force_factor_backward)
	
	# motion resistance = drag force + rolling resistance
	var drag_force = pow(speed, 2.0) * drag_force_correction
	var rolling_resistance = speed * rolling_resistance_correction
	linear_damp = drag_force + rolling_resistance
	
	steering = move_toward(steering, steer_target, steer_speed * delta)
	
	# perform post processing
	_post_physics_process(delta, speed, motion_direction)


func bullet_hit(damage, bullet_global_transform,
		raycast=false, with_hit=false):
	if raycast or with_hit:
		# hit direction and position
		hit_impulse = -bullet_global_transform.basis.z * damage
		hit_impulse_position = bullet_global_transform.origin - global_transform.origin
		do_hit = true
	if vehicle_ai_health > 0:
		# subtract -damage to vehicle's health
		add_health(-damage)
	
	# play sound
	globals.play_sound("vehicle_collision_dynamic", null, true, false, 
			bullet_global_transform.origin)


func rocket_hit(damage, rocket_global_transform):
	## NOTE: enhance physic collision with supplementary impulse
	# hit direction and position
	hit_impulse = rocket_global_transform.basis.z * damage
	hit_impulse_position = rocket_global_transform.origin - global_transform.origin
	do_hit = true
	#
	if vehicle_ai_health > 0:
		# subtract -damage to vehicle's health
		add_health(-damage)
	
	# play sound
	globals.play_sound("vehicle_collision_dynamic", null, true, false, 
			rocket_global_transform.origin)


func raycast_hit(damage, hit_position):
	if vehicle_ai_health > 0:
		# subtract -damage to vehicle's health
		add_health(-damage)
	
	# play sound
	globals.play_sound("vehicle_collision_dynamic", null, true, false, 
			hit_position)


func add_health(value:float):
	var current_value = super(value)
	# delta could be positive or negative
	vehicle_ai_health += current_value
	# check if unhealthy
	if vehicle_ai_health <= 0:
		# check if recovering from destruction
		if destroyed_timer > 0:
			# wait
			return
		# stop operations
		repair_started.emit(self)
		fire_holder.emit(true)
		destroyed_timer = DESTROYED_TIME
		steering_behavior.enabled = false
		
		# stop all workings
		action_pressed_turn = false
		action_strength_turn = 0.0
		action_pressed_motion = false
		action_strength_motion = 0.0
		engine_force = 0
		brake = 0.0
		
		# disable the shooter
		shooter_node.enabled = false


func play_sound_engine(current_sound_name):
	globals.play_sound(current_sound_name, self, false, true)
	current_sound = globals.get_sound_first(current_sound_name, self)
	current_volume_db = current_sound.get_volume_db()


func _on_Body_body_entered(body:PhysicsBody3D):
	if collision_elapsed_time <= 0.0:
		# damage if too speedy
		var damage = 0.0
		if current_speed > MAX_NO_DAMAGE_SPEED:
			damage = (current_speed - MAX_NO_DAMAGE_SPEED) * DAMAGE_SPEED_FACTOR
		# physics body: environment
		if (body.collision_layer & 0b1):
			if ((body is RigidBody3D) and 
					(PhysicsServer3D.body_get_mode(body.get_rid()) == PhysicsServer3D.BODY_MODE_RIGID)):
				# body is a (dynamic) RigidBody
				damage += body.mass * DAMAGE_SPEED_FACTOR
			add_health(-damage)
			globals.play_sound("vehicle_collision_static", null, true, false, 
						global_transform.origin)
		# static body: turret
		elif (body.collision_layer & 0b100000):
			body.bullet_hit(damage, global_transform)
			add_health(-damage)
		# vehicle's body/trailer: handled in vehicle's collision logics
		# reset collision timer
		collision_elapsed_time = COLLISION_TIME_INTERVAL


func _get_class():
	return "VehicleAI"


func _is_class(value):
	return "VehicleAI" == value
