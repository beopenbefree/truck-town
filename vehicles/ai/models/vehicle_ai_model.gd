class_name VehicleAIModel
extends VehicleModelBase


class AggressionAI extends VehicleModelBase.AggressionBase:
	
	func increment(percent:float):
		var fraction = super(percent)
		# vehicle
		_mul_and_clamp(vehicle, "steer_limit", 
				(1.0 + fraction), 0.0, 1.0)
		# shooter: vehicle contains always shooter driver
		var shooting_area_shape = ScriptLibrary.find_nodes_by_type(
				ShootingArea, vehicle, false)[0].get_node("CollisionShape3D").shape
		_mul_and_clamp(shooting_area_shape, "radius", 
				(1.0 + fraction), 10.0, 200.0)
		return fraction
	
	func decrement(percent:float):
		var fraction = super(percent)
		# vehicle
		_mul_and_clamp(vehicle, "steer_limit", 
				(1.0 - fraction), 0.0, 1.0)
		# shooter: vehicle contains always shooter driver
		var shooting_area_shape = ScriptLibrary.find_nodes_by_type(
				ShootingArea, vehicle, false)[0].get_node("CollisionShape3D").shape
		_mul_and_clamp(shooting_area_shape, "radius", 
				(1.0 - fraction), 10.0, 200.0)
		return fraction


var aggression: AggressionAI = null


func _ready():
	aggression = AggressionAI.new(body, shooter)


func _get_class():
	return "VehicleAIModel"


func _is_class(value):
	return "VehicleAIModel" == value
