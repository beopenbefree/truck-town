class_name VehicleAISteeringBehavior
extends Node3D


@export var enabled: bool = true: set = _set_enabled

@export var dims: AI_TOOLS.Dimensions = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

# vehicle's parameters
@export_range(0.01, 1.2, 0.01) var aggressiveness: float = 1.0
# AI4G: 3.8.3 COMMON ACTUATION PROPERTIES (pp. 175-177)
@export_range(0.0, 180.0, 1.0) var forward_arc_max_angle_deg: float = 90.0
@export_range(100, 1000, 50) var arc_dec_factor_by_speed: float = 250
# HACK: to prevent the steering wheels from wagging
@export_range(0.0, 15.0, 0.5) var turn_min_angle: float = 5.0
@export var turn_max_angle: float = 45.0

# post update (actuator) items
enum MotionState {STOPPED, FORWARD, BACKWARD}
@export var MIN_SPEED: float = 0.833 # m/s = 3 Km/h

# ai export data definitions
@export var stop_on_zero_steering: bool = false
# steering < this value is considered 0
@export var zero_steering_threshold: float = 0.0 : set = _set_zero_steering_threshold

# ai data definitions
var steering_output: AI.SteeringOutput = null

# working variables
var forward_orientation: Vector3 = Vector3.ZERO
var steering_strength_2: float = 0.0
var direction_diff_cos: float = 0.0
var forward_arc_angle_cos: float = 0.0
var rear_arc_angle_cos: float = 0.0
var desired_dir: Vector3 = Vector3.ZERO
var body_velocity: Vector3 = Vector3.ZERO
var body_speed: float = 0.0
var motion_direction: float = 0.0

@onready var motion_state: MotionState = MotionState.FORWARD

@onready var target: Node3D: set = _set_target

# vehicle's stationary max forward/rear arcs
@onready var forward_arc_max_angle: float  = deg_to_rad(forward_arc_max_angle_deg)
@onready var rear_arc_max_angle: float  = PI - forward_arc_max_angle
# vehicle's turn limits
@onready var turn_max_angle_cos: float  = cos(deg_to_rad(turn_max_angle))
@onready var turn_one_minus_max_angle_inv: float  = 1.0 / (1 - turn_max_angle_cos)
@onready var turn_min_angle_cos: float  = cos(deg_to_rad(turn_min_angle))
@onready var body: VehicleAI = ScriptLibrary.find_nodes_by_type(VehicleAI, self, false)[0]
# ai data definitions
@onready var zero_steering_threshold_2: float = zero_steering_threshold * zero_steering_threshold
# optimization
@onready var AI_PHYSICS_FRAME_UPDATE: int = globals.AI_PHYSICS_FRAME_UPDATE
@onready var accumulated_delta: float = 0.0


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	zero_steering_threshold = zero_steering_threshold


func _physics_process(delta):
	accumulated_delta += delta
	if Engine.get_physics_frames() % AI_PHYSICS_FRAME_UPDATE == 0:
		_pre_update(accumulated_delta)
		_update(accumulated_delta)
		_post_update(accumulated_delta)
		#
		accumulated_delta = 0.0


func _initialize():
	push_warning("Unimplemented _initialize")


func _pre_update(_delta):
	push_warning("Unimplemented _pre_update")


func _set_target(value):
	target = value


func _update(_delta):
	push_warning("Unimplemented _update")


func _post_update(_delta):
	# this implements the "actuator"
	body.action_pressed_turn = false
	body.action_strength_turn = 0.0
	body.action_pressed_motion = false
	body.action_strength_motion = 0.0
	body._set_current_brake(0.0)
	# get vehicle's forward orientation
	forward_orientation = body.global_transform.basis.z
	# get the desired_dir (normalized) and desired_speed to the target.
	var steering_output_linear = steering_output.linear
	if dims == AI_TOOLS.Dimensions.TWO:
		steering_output_linear = Vector3(steering_output_linear.x,
				0.0, steering_output_linear.y)
	steering_strength_2 = steering_output_linear.dot(steering_output_linear)
	assert(steering_strength_2 >= 0.0)
	# check if steering should be considered zero
	if steering_strength_2 < zero_steering_threshold_2:
		# no steering needed: neither forward nor
		# backward, neither left nor right.
		if stop_on_zero_steering:
			# stop is requested: brake
			body._set_current_brake(body.brake_size)
		return
	else:
		# steering_strength_2 > 0.0: continue normally
		desired_dir = steering_output_linear.normalized()
	# compute desired dir w.r.t. vehicle's forward orientation: the (cosine of)
	# difference angle
	direction_diff_cos = forward_orientation.dot(desired_dir)
	
	# get current velocity/speed
	body_velocity = body.linear_velocity
	body_speed = body_velocity.length()
	
	# get the motion direction: i.e. velocity w.r.t forward orientation 
	motion_direction = forward_orientation.dot(body_velocity)
	# compute current forward arc
	forward_arc_angle_cos = cos(
			arc_dec_factor_by_speed * forward_arc_max_angle /
			(forward_arc_max_angle * body_speed + arc_dec_factor_by_speed))
	# compute current rear arc
	rear_arc_angle_cos = cos(
			PI - (arc_dec_factor_by_speed * rear_arc_max_angle) /
			(rear_arc_max_angle * body_speed + arc_dec_factor_by_speed))
	
	### compute the driving's commands ###
	if body_speed > MIN_SPEED:
		# we are moving
		if motion_direction > 0.0:
			# going forward direction
			motion_state = MotionState.FORWARD
			if direction_diff_cos >= forward_arc_angle_cos:
				_steer_forward()
			elif steering_strength_2 > 0.0:
				# we are outside the forward arc and moving: let's brake
				body._set_current_brake(body.brake_size)
		elif motion_direction < 0.0:
			# going backward direction
			motion_state = MotionState.BACKWARD
			if direction_diff_cos <= rear_arc_angle_cos:
				_steer_backward()
			elif steering_strength_2 > 0.0:
				# we are outside the rear arc and moving: let's brake
				body._set_current_brake(body.brake_size)
	else:# body_speed <= MIN_SPEED
		motion_state = MotionState.STOPPED
		# we are stopped
		if direction_diff_cos >= forward_arc_angle_cos:
			_steer_forward()
		elif direction_diff_cos <= rear_arc_angle_cos:
			_steer_backward()


func _steer_forward():
	# we are into the forward arc: compute turning's commands
	if direction_diff_cos <= turn_min_angle_cos:
		# turn toward the target
		body.action_pressed_turn = true
		if forward_orientation.cross(desired_dir).y >= 0.0:
			if direction_diff_cos >= turn_max_angle_cos:
				body.action_strength_turn = (
						(turn_min_angle_cos - direction_diff_cos) * 
						turn_one_minus_max_angle_inv)
			else:
				body.action_strength_turn = 1.0
		else:
			if direction_diff_cos >= turn_max_angle_cos:
				body.action_strength_turn = ((
						direction_diff_cos - turn_min_angle_cos) * 
						turn_one_minus_max_angle_inv)
			else:
				body.action_strength_turn = -1.0
	# compute motion's commands: go forward
	body.action_pressed_motion = true
	body.action_strength_motion = aggressiveness


func _steer_backward():
	# we are in the rear arc: compute turning's commands
	# turn opposite the target
	body.action_pressed_turn = true
	if forward_orientation.cross(desired_dir).y >= 0.0:
		body.action_strength_turn = -1.0
	else:
		body.action_strength_turn = 1.0
	# compute motion's commands: go backward
	body.action_pressed_motion = true
	body.action_strength_motion = -aggressiveness


func _set_zero_steering_threshold(value):
	zero_steering_threshold = value
	zero_steering_threshold_2 = value ** 2


func _get_class():
	return "VehicleAISteeringBehavior"


func _is_class(value):
	return "VehicleAISteeringBehavior" == value


func _set_enabled(value):
	enabled = value
	set_physics_process(value)
