class_name VehicleAIPrioritySteering
extends VehicleAISteeringBehavior


# priority steering parameters
@export_file("*.json") var behaviors_file: String = String()
@export var behavior_group_file_dict: Dictionary = {} # {String:String}
@export var initial_behavior_group_identifier: String = String()
@export_range(0.0, 1.0, 0.01) var damping: float = 0.0
@export var epsilon: float = 1.0e-3
@export var max_acceleration: float = 5.0
@export var max_rotation: float = 2.0
@export var max_speed: float = 5.0

# ai data definitions
var priority_steering_factory: AI.PrioritySteeringFactory
var priority_steering_dict: Dictionary = {} # {String:AI.PrioritySteering}
var priority_steering_params_dict: Dictionary = {} # {String:Dictionary}
# currently active priority steering
var priority_steering: AI.PrioritySteering

# behaviors' data definitions
var behaviors_pre_update_dict: Dictionary = {}
var behaviors_pre_update: Dictionary = {}
var current_behavior_group_identifier: String = String()

#<DEBUG
#var dbg_lib = preload("res://ai/debug_draw.gd")
#var dbg_character_instance = MeshInstance3D.new()
#var dbg_collision_avoidance: AI.CollisionAvoidance = null
#var dbg_collision_avoidance_instance = MeshInstance3D.new()
#var dbg_separation: AI.SeparationAttraction = null
#var dbg_separation_instance = MeshInstance3D.new()
#var dbg_obstacle_avoidance: AI.ObstacleAvoidance = null
#var dbg_obstacle_avoidance_instance = MeshInstance3D.new()
#@onready var dbg_circle = MeshInstance3D.new()
#@onready var dbg_circle1 = MeshInstance3D.new()
#@onready var dbg_segment_r = ImmediateMesh.new()
#@onready var dbg_segment_l = ImmediateMesh.new()
#@onready var dbg_segment_steering_output = ImmediateMesh.new()
#DEBUG>


func _ready():
	priority_steering_factory = AI.PrioritySteeringFactory.new(body, dims)
	for priority_steering_identifier in behavior_group_file_dict:
		_setup(priority_steering_identifier)
		# factory's behaviors_pre_update is updated based on the behavior
		var behaviors_pre_update_copy = priority_steering_factory.behaviors_pre_update.duplicate(true)
		behaviors_pre_update_dict[priority_steering_identifier] = behaviors_pre_update_copy
	# setup the initial behavior
	priority_steering = priority_steering_dict[initial_behavior_group_identifier]
	behaviors_pre_update = behaviors_pre_update_dict[initial_behavior_group_identifier]
	current_behavior_group_identifier = initial_behavior_group_identifier
	
	# make a unique AI character for all priority steering behaviors
	var character: AI.Kinematic = AI.Kinematic.new(priority_steering.dims)
	for priority_steering_identifier in behavior_group_file_dict:
		priority_steering_dict[priority_steering_identifier].character = character
	
	_initialize()


func set_deferred_priority_steering(priority_steering_identifier:String):
	priority_steering = priority_steering_dict[priority_steering_identifier]
	behaviors_pre_update = behaviors_pre_update_dict[priority_steering_identifier]
	# common parameters
	var params = priority_steering_params_dict[priority_steering_identifier]
	stop_on_zero_steering = params["stop_on_zero_steering"]
	zero_steering_threshold = params["zero_steering_threshold"]
	#
	current_behavior_group_identifier = priority_steering_identifier


func _setup(priority_steering_identifier:String):
	var behavior_group_file = behavior_group_file_dict[priority_steering_identifier]
	var params:Dictionary = globals._load_and_parse_json(behavior_group_file)
	# priority steering common parameters
	var new_params_ps = {}
	new_params_ps["stop_on_zero_steering"] = params.get(
			"stop_on_zero_steering", false)
	new_params_ps["zero_steering_threshold"] = params.get(
			"zero_steering_threshold", 0.0)
	# create a new AI.PrioritySteering
	var new_ps = priority_steering_factory.create_and_setup(
			behaviors_file, behavior_group_file,
			max_acceleration, max_rotation, epsilon, damping)
	# new_ps contains the newly created behavior
	priority_steering_dict[priority_steering_identifier] = new_ps
	# new_params_ps contains the common parameters
	priority_steering_params_dict[priority_steering_identifier] = new_params_ps
	
	
	#<DEBUG
	#for group:AI.BlendedSteering in new_ps.groups:
		#for bhv in group.behaviors:
			#if bhv is AI.ObstacleAvoidance:
				#dbg_obstacle_avoidance = bhv
				#break
		#_add_mesh_instance(Color(1,0,1,1), dbg_obstacle_avoidance_instance)
		##
		#for bhv in group.behaviors:
			#if bhv is AI.CollisionAvoidance:
				#dbg_collision_avoidance = bhv
				#break
		#_add_mesh_instance(Color(1,1,0,1), dbg_collision_avoidance_instance)
		##
		#for bhv in group.behaviors:
			#if bhv is AI.SeparationAttraction:
				#dbg_separation = bhv
				#break
		#_add_mesh_instance(Color(1,1,0,1), dbg_separation_instance)
	#_add_mesh_instance(Color(0,1,1,1), dbg_character_instance)
	#if dbg_collision_avoidance:
		#dbg_circle.mesh = dbg_lib.get_circle_mesh(dbg_collision_avoidance.radius)
	#if dbg_separation:
		#dbg_circle1.mesh = dbg_lib.get_circle_mesh(dbg_separation.threshold)
	#body.add_child(dbg_circle)
	#body.add_child(dbg_segment_r)
	#body.add_child(dbg_segment_l)
	#var mat = StandardMaterial3D.new()
	#mat.albedo_color = Color(1,0,0,1)
	#dbg_segment_steering_output.material_override = mat
	#body.add_child(dbg_segment_steering_output)
	#DEBUG>


func _initialize():
	var p_position = body.global_transform.origin
	var velocity = body.linear_velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		priority_steering.character.position = Vector2(p_position.x, p_position.z)
		priority_steering.character.velocity = Vector2(velocity.x, velocity.z)
	else:
		priority_steering.character.position = p_position
		priority_steering.character.velocity = velocity


func _pre_update(delta):
	# synchronize character's motion state with the body's one
	var p_position = body.global_transform.origin
	var velocity = body.linear_velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		priority_steering.character.position = Vector2(p_position.x, p_position.z)
		priority_steering.character.velocity = Vector2(velocity.x, velocity.z)
	else:
		priority_steering.character.position = p_position
		priority_steering.character.velocity = velocity
	# pre-update every inner behavior
	if is_instance_valid(target):
		for group in priority_steering.groups:
			for bhv_wght in group.behaviors:
				var bhv = bhv_wght.behavior
				behaviors_pre_update[bhv_wght].call(bhv, delta, target)


func _update(_delta):
	steering_output = priority_steering.get_steering()
	
	#<DEBUG
	#if current_behavior_group_identifier == initial_behavior_group_identifier:
		#dbg_character_instance.global_transform.origin = (
				#priority_steering.character.position + Vector3(0.0, 3.0, 0.0))
		#dbg_obstacle_avoidance_instance.global_transform.origin = (
				#dbg_obstacle_avoidance.character.position + Vector3(0.0, 4.0, 0.0))
		#if dbg_collision_avoidance:
			#dbg_collision_avoidance_instance.global_transform.origin = (
					#dbg_collision_avoidance.character.position + Vector3(0.0, 5.0, 0.0))
		#if dbg_separation:
			#dbg_separation_instance.global_transform.origin = (
					#dbg_separation.character.position + Vector3(0.0, 5.0, 0.0))
		#dbg_lib.debug_draw_obstacle_avoidance(dbg_obstacle_avoidance, body, 
				#dbg_segment_r, dbg_segment_l, dims)
		#dbg_lib.debug_draw_vector(body, steering_output.linear, 
				#dbg_segment_steering_output, dims)
	#DEBUG>


func _on_steering_behavior_signal(_name, action):
	print(_name, " - ", action)


#<DEBUG
#func _add_mesh_instance(color, mesh_instance, root=self):
	#var mesh = SphereMesh.new()
	#var mat = StandardMaterial3D.new()
	#mesh.radius = 0.25
	#mesh.height = 0.5
	#mat.albedo_color = color
	#mesh_instance.mesh = mesh
	#mesh_instance.material_override = mat
	#root.add_child(mesh_instance)
#DEBUG>


func _get_class():
	return "VehicleAIPrioritySteering"


func _is_class(value):
	return "VehicleAIPrioritySteering" == value
