class_name VehicleAISteeringPipeline
extends VehicleAISteeringBehavior


@export var steering_pipeline_file_dict: Dictionary = {} # {String:String}
@export var initial_steering_pipeline_identifier: String = String()

# ai data definitions
var steering_pipeline_dict: Dictionary = {} # {String:AI.SteeringPipeline}
var steering_pipeline_params_dict: Dictionary = {} # {String:Dictionary}
# currently active steering pipeline
var steering_pipeline: AI.SteeringPipeline = null
var current_steering_pipeline_identifier: String = String()
var collision_detector: AI.CollisionDetector = null


func _ready():
	for steering_pipeline_identifier in steering_pipeline_file_dict:
		_setup(steering_pipeline_identifier)
	
	# setup the initial behavior
	steering_pipeline = steering_pipeline_dict[initial_steering_pipeline_identifier]
	current_steering_pipeline_identifier = initial_steering_pipeline_identifier
	
	# make a unique AI character for all steering pipeline behaviors
	var character: AI.Kinematic = AI.Kinematic.new(steering_pipeline.dims)
	for steering_pipeline_identifier in steering_pipeline_dict:
		steering_pipeline_dict[steering_pipeline_identifier].character = character
	
	# initialize 
	_initialize()


func set_deferred_steering_pipeline(steering_pipeline_identifier:String):
	steering_pipeline = steering_pipeline_dict[steering_pipeline_identifier]
	# common parameters
	var params = steering_pipeline_params_dict[steering_pipeline_identifier]
	dims = params["dims"]
	stop_on_zero_steering = params["stop_on_zero_steering"]
	zero_steering_threshold = params["zero_steering_threshold"]
	#
	current_steering_pipeline_identifier = steering_pipeline_identifier


func _setup(steering_pipeline_identifier:String):
	# load params
	var steering_pipeline_file = steering_pipeline_file_dict[steering_pipeline_identifier]
	var params:Dictionary = _load_params(steering_pipeline_file)
	# steering pipeline common parameters
	var new_params_sp = {}
	new_params_sp["dims"] = params.get("dims", 3)
	new_params_sp["stop_on_zero_steering"] = params.get(
			"stop_on_zero_steering", false)
	new_params_sp["zero_steering_threshold"] = params.get(
			"zero_steering_threshold", 0.0)
	#
	var body_aabb = body.chassis.get_aabb().abs()
	#var vehicle_half_length = body_aabb.size.z * 0.5
	var vehicle_half_length = body_aabb.get_longest_axis_size() * 0.5
	
	var curr_params = null
	### STEERING PIPELINE
	# create a new AI.SteeringPipeline
	var new_sp = AI.SteeringPipeline.new()
	curr_params = params.get("steering_pipeline")
	if curr_params:
		new_sp.setup(
				curr_params.get("constraint_steps", 2),
				AI.SteeringBehavior.new(),
				curr_params.get("damping", 0.2), 
				dims)
		curr_params = null
	
	### TARGETERS
	curr_params = params.get("chase_targeter")
	if curr_params:
		var targeter = AISteeringPipelineImpl.ChaseTargeter.new(
				new_sp, 
				curr_params.get("lookahead", 1.0), 
				dims, get_world_3d())
		new_sp.targeters.append(targeter)
		curr_params = null
	
	curr_params = params.get("stand_still_targeter")
	if curr_params:
		var targeter = AISteeringPipelineImpl.StandStillTargeter.new(
				new_sp, 
				dims, get_world_3d())
		new_sp.targeters.append(targeter)
		curr_params = null
		
	### DECOMPOSERS
	curr_params = params.get("planning_decomposer")
	if curr_params:
		var decomposer = AISteeringPipelineImpl.PlanningDecomposer.new(
				new_sp, body, get_world_3d(),
				curr_params.get("navmesh_path_optimize", true),
				curr_params.get("min_distance", 0.0),
				curr_params.get("go_to_min_distance", false),
				deg_to_rad(curr_params.get("straight_angle_max", 0.0)))
		new_sp.decomposers.append(decomposer)
		curr_params = null
	
	### CONSTRAINTS
	curr_params = params.get("obstacle_avoidance_constraint")
	if curr_params:
		# HACK adjust some parameter first
		# lookahead interpreted as length factor
		var lookahead = (curr_params.get("lookahead", 1.0) *
				vehicle_half_length)
		#
		var oa_constraint = AISteeringPipelineImpl.ObstacleAvoidanceConstraint.new(
				new_sp, body, collision_detector,
				curr_params.get("min_speed", MIN_SPEED),
				curr_params.get("avoid_distance", 1.5), 
				lookahead, 
				curr_params.get("num_rays", 1), 
				deg_to_rad(curr_params.get("whiskers_angle", 15.0)),
				deg_to_rad(curr_params.get("plane_surface_slope_cos", 45.0)),
				curr_params.get("suggested_pos_weight", 0.6),
				curr_params.get("max_maneuver_period", 1.0),
				dims, get_world_3d())
		new_sp.constraints.append(oa_constraint)
		curr_params = null
	
	curr_params = params.get("separation_constraint")
	if curr_params:
		# HACK adjust some parameter first
		# lookahead interpreted as length factor
		var lookahead = (curr_params.get("lookahead", 1.0) *
				vehicle_half_length)
		#
		var s_constraint = AISteeringPipelineImpl.SeparationConstraint.new(
				new_sp, body, [], 
				curr_params.get("threshold", 20.0),
				curr_params.get("decay_coefficient", 15.0),
				lookahead,
				curr_params.get("max_acceleration", 5.0),
				dims, get_world_3d())
		new_sp.constraints.append(s_constraint)
		curr_params = null
	
	curr_params = params.get("min_speed_constraint")
	if curr_params:
		var ms_constraint = MinSpeedConstraint.new(
				new_sp, body,
				curr_params.get("min_speed", MIN_SPEED),
				curr_params.get("low_speed_period", 1.0),
				curr_params.get("maneuver_period", 1.0),
				curr_params.get("maneuver_delta_dir", 0.2),
				curr_params.get("locked_period", 5.0),
				dims, get_world_3d())
		new_sp.constraints.append(ms_constraint)
		# connect to lock state signal
		ms_constraint.lock_state.connect(
				_on_min_speed_constraint_lock_state)
		curr_params = null
	
	curr_params = params.get("max_speed_constraint")
	if curr_params:
		var ms_constraint = MaxSpeedConstraint.new(
				new_sp, body,
				curr_params.get("max_speed", 12.0),
				curr_params.get("tolerance_factor", 1.1),
				dims, get_world_3d())
		new_sp.constraints.append(ms_constraint)
		curr_params = null
	
	curr_params = params.get("dead_lock_constraint")
	if curr_params:
		var dd_constraint = DeadLockConstraint.new(
				new_sp, body,
				dims, get_world_3d())
		new_sp.constraints.append(dd_constraint)
		curr_params = null
	
	### ACTUATOR
	curr_params = params.get("seek_actuator")
	if curr_params:
		var actuator = AISteeringPipelineImpl.SeekActuator.new(
				new_sp, body,
				curr_params.get("max_acceleration", 5.0),
				curr_params.get("damping", 0.2),
				deg_to_rad(curr_params.get("straight_angle_max", 0.0)),
				dims, get_world_3d())
		new_sp.actuator = actuator
		curr_params = null
	
	curr_params = params.get("arrive_actuator")
	if curr_params:
		var actuator = AISteeringPipelineImpl.ArriveActuator.new(
				new_sp, body,
				curr_params.get("max_acceleration", 5.0),
				curr_params.get("max_speed", 5.0),
				curr_params.get("target_radius", 5.0),
				curr_params.get("slow_radius", 5.0),
				curr_params.get("time_to_target_radius", 5.0),
				curr_params.get("damping", 0.2),
				deg_to_rad(curr_params.get("straight_angle_max", 0.0)),
				dims, get_world_3d())
		new_sp.actuator = actuator
		curr_params = null
	
	### DEADLOCK BEHAVIOR
	curr_params = params.get("dead_lock_behavior")
	if curr_params:
		var deadlock = _create_deadlock("dead_lock_behavior",
				new_sp, curr_params)
		new_sp.deadlock = deadlock
		curr_params = null
	
	curr_params = params.get("wander_dead_lock_behavior")
	if curr_params:
		var deadlock = _create_deadlock("wander_dead_lock_behavior",
				new_sp, 	curr_params)
		new_sp.deadlock = deadlock
		curr_params = null
	# new_sp contains the newly created behavior
	steering_pipeline_dict[steering_pipeline_identifier] = new_sp
	# new_params_sp contains the common parameters
	steering_pipeline_params_dict[steering_pipeline_identifier] = new_params_sp


func _initialize():
	var p_position = body.global_transform.origin
	var velocity = body.linear_velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		steering_pipeline.character.position = Vector2(p_position.x, p_position.z)
		steering_pipeline.character.velocity = Vector2(velocity.x, velocity.z)
	else:
		steering_pipeline.character.position = p_position
		steering_pipeline.character.velocity = velocity


func _pre_update(_delta):
	# synchronize character's motion state with the body's one
	_initialize()
	# steering depends on: target.position
	if is_instance_valid(target):
		for targeter in steering_pipeline.targeters:
			if targeter.channel == "position":
				var p_position = target.global_transform.origin
				if dims == AI_TOOLS.Dimensions.TWO:
					targeter.target.position = (
							Vector2(p_position.x, p_position.z))
				else:
					targeter.target.position = p_position
			#if targeter.channel == "orientation":
				#pass
			#targeter.channel == "velocity":
				#pass
			#targeter.channel == "rotation":
				#pass


func _update(_delta):
	steering_output = steering_pipeline.get_steering()


# CONSTRAINTS

class MinSpeedConstraint extends AISteeringPipelineImpl.ConstraintBase:
	@warning_ignore("unused_signal")
	signal lock_state
	
	# constraint violated when character's speed
	# is less than a minimum value 
	var min_speed_2: float
	var low_speed_period: float # sec
	var maneuver_period: float # sec
	var maneuver_delta_dir: float
	var locked_period: float # sec
	# private variables
	var _low_speed_start_time
	var _maneuver_start_time
	var _locked_start_time
	var _min_speed_increase_factor
	# fsm
	var _fsm: StateMachine
	
	class MSC_BaseState extends StateMachine.State:
		
		func _init():
			process_enabled = false
			physics_process_enabled = false
			input_enabled = false
		
		func _on_enter_state() -> void:
			print("entering: ", id)
		
		func _on_leave_state() -> void:
			print("leaving: ", id)
	
	class MSC_NormalSpeed extends MSC_BaseState:
		# permanence in state as long as speed >= min
		
		func _on_enter_state() -> void:
			#super()
			target.get_ref()._min_speed_increase_factor = 1.0
		
		func _on_leave_state() -> void:
			#super()
			pass
	
	class MSC_LowSpeed extends MSC_BaseState:
		# permanence in state as long as speed < min
		# or a period has elapsed 
		
		func _on_enter_state() -> void:
			#super()
			target.get_ref()._low_speed_start_time = Time.get_ticks_msec()
		
		func _on_leave_state() -> void:
			#super()
			target.get_ref()._low_speed_start_time = 0.0
	
	class MSC_Maneuver extends MSC_BaseState:
		# permanence in state as long a period has elapsed
		var toggle_dir: float = 1.0
		var maneuver_dir: Vector3
		
		func _on_enter_state() -> void:
			#super()
			var curr_time = Time.get_ticks_msec()
			target.get_ref()._maneuver_start_time = curr_time
			target.get_ref()._locked_start_time = curr_time
			_set_maneuver_direction()
		
		func _on_leave_state() -> void:
			#super()
			target.get_ref()._maneuver_start_time = 0.0
			target.get_ref()._locked_start_time = 0.0
			toggle_dir = 1.0
		
		func _set_maneuver_direction():
			toggle_dir *= -1.0
			var delta_dir = target.get_ref().maneuver_delta_dir
			var dir_x = (target.get_ref().body.global_basis.x *
					randf_range(-delta_dir, delta_dir))
			maneuver_dir = toggle_dir * (dir_x + target.get_ref().body.global_basis.z)
			return maneuver_dir
	
	func _init(p_object, p_body, min_speed, p_low_speed_period,
			p_maneuver_period, p_maneuver_delta_dir, p_lock_state_time,
			p_dims, world):
		super(p_object, p_body, p_dims, world)
		type_name = "MinSpeedConstraint"
		min_speed_2 = min_speed ** 2
		low_speed_period = int(p_low_speed_period * 1000)
		maneuver_period = int(p_maneuver_period * 1000)
		maneuver_delta_dir = p_maneuver_delta_dir
		locked_period = int(p_lock_state_time * 1000)
		_low_speed_start_time = 0.0
		_maneuver_start_time = 0.0
		_locked_start_time = 0.0
		_min_speed_increase_factor = 1.0
		#
		_fsm = StateMachineFactory.new().create({
			"target": weakref(self),
			"current_state": "MSC_NormalSpeed",
			"states": [
				{"id": "MSC_NormalSpeed", "state": MSC_NormalSpeed},
				{"id": "MSC_LowSpeed", "state": MSC_LowSpeed},
				{"id": "MSC_Maneuver", "state": MSC_Maneuver},
			],
			"transitions": [
				{"state_id": "MSC_NormalSpeed", "to_states": ["MSC_LowSpeed"]},
				{"state_id": "MSC_LowSpeed", "to_states": ["MSC_NormalSpeed", "MSC_Maneuver"]},
				{"state_id": "MSC_Maneuver", "to_states": ["MSC_NormalSpeed"]},
			]
		})
	
	func will_violate(_path:AI.NavMapPath) -> bool:
		var character = steering_pipeline.get_ref().character
		var speed_2 = character.velocity.dot(character.velocity)
		var violated: bool = false
		var current_state = _fsm.current_state
		match current_state:
			"MSC_NormalSpeed":
				if speed_2 < min_speed_2 * _min_speed_increase_factor:
					# speed low -> waiting
					_fsm.transition("MSC_LowSpeed")
				# speed normal: remain unviolated
				violated = false
			"MSC_LowSpeed":
				# check if waiting period elapsed
				var elapsed_time = Time.get_ticks_msec() - _low_speed_start_time
				if elapsed_time >= low_speed_period:
					# waiting period elapsed -> maneuver
					_fsm.transition("MSC_Maneuver")
				elif speed_2 >= min_speed_2 * _min_speed_increase_factor:
					# speed normal -> unviolated
					_fsm.transition("MSC_NormalSpeed")
				else:
					# waiting continues
					_min_speed_increase_factor *= (1.0 + 
							float(elapsed_time) / float(low_speed_period))
				violated = false
			"MSC_Maneuver":
				# check if maneuver period elapsed
				var curr_time = Time.get_ticks_msec()
				if (curr_time - _locked_start_time) >= locked_period:
					emit_signal("lock_state")
					# locked period elapsed -> unviolated
					_fsm.transition("MSC_NormalSpeed")
					violated = false
				elif (curr_time - _maneuver_start_time) < maneuver_period:
					# maneuver continues -> request suggested goal
					violated = true
				elif speed_2 >= min_speed_2:
					# maneuver period elapsed and speed normal -> unviolated
					_fsm.transition("MSC_NormalSpeed")
					violated = false
				else:
					# low speed -> repeat maneuver period (but don't
					# request suggested goal)
					_fsm._current_state._set_maneuver_direction()
					_maneuver_start_time = curr_time
					violated = false
		#
		return violated
	
	func suggest(character:AI.Kinematic, _path:AI.NavMapPath, 
			goal:AI.Goal) -> AI.Goal:
		var body_z = body.global_basis.z
		var body_x = body.global_basis.x
		# HACK: this function is called only when
		# fsm is in MSC_Maneuver state
		var new_pos = (character.position + 
				_fsm._current_state.maneuver_dir * _bounding_sphere_radius)
		# project new_pos on the vehicle's x-z plane and 
		# place on the bounding sphere
		var dir_real = new_pos - character.position
		var dir = (body_x * dir_real.dot(body_x) +
					body_z * dir_real.dot(body_z)).normalized()
		new_pos = character.position + dir * _bounding_sphere_radius
		goal.position = new_pos
		return goal


class MaxSpeedConstraint extends AISteeringPipelineImpl.ConstraintBase:
	# constraint violated when character's speed
	# is grater than a maximum value 
	var max_speed_2: float
	var max_speed_tolerance_2: float
	# private variables
	var _speed_2
	
	func _init(p_object, p_body, max_speed, tolerance_factor,
			p_dims, world):
		super(p_object, p_body, p_dims, world)
		type_name = "MaxSpeedConstraint"
		max_speed_2 = max_speed ** 2
		max_speed_tolerance_2 = max_speed_2 * tolerance_factor * tolerance_factor
	
	func will_violate(_path:AI.NavMapPath) -> bool:
		var character = steering_pipeline.get_ref().character
		_speed_2 = character.velocity.dot(character.velocity)
		return _speed_2 >= max_speed_2
	
	func suggest(character:AI.Kinematic, _path:AI.NavMapPath, 
			goal:AI.Goal) -> AI.Goal:
		# check if the speed is within the tolerance factor
		var new_pos
		if _speed_2 <= max_speed_tolerance_2:
			# stay stationary, i.e. don't ask to brake
			new_pos = character.position
		else:
			var body_z = body.global_basis.z
			var body_x = body.global_basis.x
			new_pos = -character.velocity
			# project new_pos on the vehicle's x-z plane and 
			# place on the bounding sphere
			var dir_real = new_pos - character.position
			var dir = (body_x * dir_real.dot(body_x) +
						body_z * dir_real.dot(body_z)).normalized()
			new_pos = character.position + dir * _bounding_sphere_radius
		#
		goal.position = new_pos
		return goal


class DeadLockConstraint extends AISteeringPipelineImpl.ConstraintBase:
	# always violated constraint
	
	func _init(p_object, p_body, p_dims, world):
		super(p_object, p_body, p_dims, world)
		type_name = "DeadLockConstraint"
	
	func will_violate(_path:AI.NavMapPath) -> bool:
		return true
	
	func suggest(_character:AI.Kinematic, _path:AI.NavMapPath, 
			goal:AI.Goal) -> AI.Goal:
		return goal


class DeadLockBehavior extends AI.SteeringBehavior:
	var body: Node3D
	var target_distance: float
	var active_period: float # sec
	var constraint: AI.Constraint
	# delegation
	var seek_flee: AI.SeekFlee
	# private variables
	var _running_direction
	var _toggle_dir
	var _active_start_time
	var _active_period_started
	var _map
	
	func _set_character(value):
		super(value)
		# override delegate's character
		seek_flee.character = value
	
	func _set_name(value):
		super(value)
		seek_flee.name = name
	
	func setup(_steering_pipeline:AI.SteeringPipeline, p_body,
			p_constraint, max_acceleration_delegate,
			p_active_period, p_target_distance,
			p_damping = 0.0, p_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.CUSTOM
		dims = p_dims
		body = p_body
		active_period = int(p_active_period * 1000)
		target_distance = p_target_distance
		# delegation setup
		seek_flee = AI.SeekFlee.new()
		seek_flee.setup(max_acceleration_delegate, p_damping, dims, 
				AI_TOOLS.Behavior.SEEK)
		constraint = p_constraint
		seek_flee.name = name
		#
		character = AI.Kinematic.new(dims)
		character.damping = p_damping
		# initialize private variables
		if dims == AI_TOOLS.Dimensions.THREE:
			_running_direction = Vector3.ZERO
		else:
			_running_direction = Vector2.ZERO
		_active_start_time = 0.0
		_active_period_started = false
		_map = constraint.map
		_toggle_dir = -1
	
	func get_steering() -> AI.SteeringOutput:
		var current_time = Time.get_ticks_msec()
		if not _active_period_started:
			# FIXME: adding/removing the constraint is of UNCERTAIN utility
			#constraint.steering_pipeline.get_ref().constraints.append(
					#constraint)
			_active_period_started = true
			_active_start_time = current_time
			_toggle_dir *= -1
			_running_direction = _toggle_dir * body.global_transform.basis.z
			_running_direction.x += _running_direction.x * (0.2 * randf() - 0.1)
			_running_direction.z += _running_direction.z * (0.2 * randf() - 0.1)
		elif ((current_time - _active_start_time) > active_period):
			# FIXME: see above
			#constraint.steering_pipeline.get_ref().constraints.pop_back()
			_active_period_started = false
		
		# Put the target together.
		if dims == AI_TOOLS.Dimensions.TWO:
			var forward_orientation2d = Vector2(_running_direction.x,
					_running_direction.z)
			seek_flee.target.position = (character.position +
					forward_orientation2d * target_distance)
		else:
			var new_pos = (character.position +
					_running_direction * target_distance)
			seek_flee.target.position = NavigationServer3D.map_get_closest_point(
					_map, new_pos)
		# 2. Delegate to seek.
		return seek_flee.get_steering()


func _create_deadlock(type_name:String, p_steering_pipeline,
		d_params) -> AI.SteeringBehavior:
	var deadlock:AI.SteeringBehavior
	
	match type_name:
		# wander
		"wander_dead_lock_behavior":
			var max_angular_acceleration = d_params.get(
					"max_angular_acceleration", 5.0)
			var max_rotation = d_params.get("max_rotation", 5.0)
			var target_radius = d_params.get(
					"target_radius", 5.0) # degrees
			var slow_radius = d_params.get(
					"slow_radius", 45.0) # degrees
			var time_to_target = d_params.get(
					"time_to_target", 0.1)
			var wander_offset = d_params.get("wander_offset", 15.0)
			var wander_radius = d_params.get("wander_radius", 7.5) 
			var wander_rate = d_params.get(
					"wander_rate", 30.0) # degrees
			var wander_orientation = d_params.get(
					"wander_orientation", 0.0) # degrees
			var max_acceleration = d_params.get(
					"max_acceleration", 5.0)
			var damping = d_params.get("damping", 0.2)
			deadlock = AI._behavior_factory("WANDER",
					[max_angular_acceleration, 
					max_rotation, 
					deg_to_rad(target_radius), 
					deg_to_rad(slow_radius), 
					time_to_target, 
					wander_offset, 
					wander_radius,
					deg_to_rad(wander_rate),
					deg_to_rad(wander_orientation),
					max_acceleration, 
					damping, dims])
		# custom
		"dead_lock_behavior":
			deadlock = DeadLockBehavior.new()
			var constraint = DeadLockConstraint.new(
					p_steering_pipeline, body, 
					dims, get_world_3d())
			deadlock.setup(p_steering_pipeline, body, constraint,
					d_params.get("max_acceleration", 5.0),
					d_params.get("active_period", 1.0),
					d_params.get("target_distance", 15.0),
					d_params.get("damping", 0.2),
					dims)
			deadlock.name = "DeadLockBehavior" + str(deadlock.get_instance_id())
	#
	return deadlock


func _on_min_speed_constraint_lock_state():
	body.apply_blast(randf_range(5.0, 10.0))


func _load_params(json_file):
	return globals._load_and_parse_json(json_file)


func _get_class():
	return "VehicleAISteeringPipeline"


func _is_class(value):
	return "VehicleAISteeringPipeline" == value
