class_name Vehicle
extends VehicleBase


func _ready():
	super()
	# get current engine sound
	current_sound = globals.get_sound_first(
				globals.current_vehicle_sound, self)


func _physics_process(delta):
	super(delta)
	
	if steering_enabled:
		if Input.is_action_pressed("turn_left") or Input.is_action_pressed("turn_right"):
			steer_target = Input.get_action_strength("turn_left") - Input.get_action_strength("turn_right")
		else:
			steer_target = -touch_steer_direction.x
		steer_target *= steer_limit
	
	var speed = linear_velocity.length()
	var speed_inv = 1.0 / speed if speed != 0.0 else INF
	# AI
	velocity = linear_velocity
	var motion_direction = linear_velocity.dot(transform.basis.z)
	engine_force = 0
	
	if not changing_gear:
		if (Input.is_action_pressed("accelerate") or Input.is_action_pressed("reverse")):
			motion_target = Input.get_action_strength("accelerate") - Input.get_action_strength("reverse")
		else:
			motion_target = -touch_move_direction.y
		engine_force = motion_target * engine_force_boost
		if motion_target > 0.0:
			engine_force *= engine_force_value
			if get_tilt() > tilt_max:
				engine_force *= max(10.0 * speed_inv, 
						tilt_engine_force_factor)
		else:
			engine_force *= engine_force_value_backward
			if get_tilt() < -tilt_max:
				engine_force *= max(8.0 * speed_inv, 
						tilt_engine_force_factor_backward)
	
	# motion resistance = drag force + rolling resistance
	var drag_force = pow(speed, 2.0) * drag_force_correction
	var rolling_resistance = speed * rolling_resistance_correction
	linear_damp = drag_force + rolling_resistance
	
	steering = move_toward(steering, steer_target, steer_speed * delta)
	
	# perform post processing
	_post_physics_process(delta, speed, motion_direction)


func _unhandled_input(event):
	var handled_input:bool = false
	if event.is_action_pressed("brake"):
		handled_input = true
		_set_current_brake(brake_size)
	elif event.is_action_released("brake"):
		handled_input = true
		_set_current_brake(0.0)
	if handled_input:
		get_viewport().set_input_as_handled()


func bullet_hit(damage, bullet_global_transform,
		raycast=false, with_hit=false):
	if raycast or with_hit:
		# hit direction and position
		hit_impulse = -bullet_global_transform.basis.z * damage
		hit_impulse_position = bullet_global_transform.origin - global_transform.origin
		do_hit = true
	# subtract -damage to vehicle's health
	add_health(-damage)
	# play sound
	globals.play_sound("vehicle_collision_dynamic", null, true, false, 
			bullet_global_transform.origin)


func rocket_hit(damage, rocket_global_transform):
	## NOTE: enhance physic collision with supplementary impulse
	# hit direction and position
	hit_impulse = rocket_global_transform.basis.z * damage
	hit_impulse_position = rocket_global_transform.origin - global_transform.origin
	do_hit = true
	#
	# subtract -damage to vehicle's health
	add_health(-damage)
	# play sound
	globals.play_sound("vehicle_collision_dynamic", null, true, false, 
			rocket_global_transform.origin)


func raycast_hit(damage, hit_position):
	# subtract -damage to vehicle's health
	add_health(-damage)
	# play sound
	globals.play_sound("vehicle_collision_dynamic", null, true, false, 
			hit_position)


func add_health(value:float):
	var current_value = super(value)
	var scene = globals.get_current_scene()
	scene.current_count += current_value
	scene.current_count = clampi(scene.current_count, -1, scene.total_count_down)


func set_current_settings():
	drag_force_factor = globals.current_vehicle_settings["drag_force_factor"][0]
	steer_speed = globals.current_vehicle_settings["steer_speed"][0]
	steer_limit = globals.current_vehicle_settings["steer_limit"][0]
	tilt_angle_max = globals.current_vehicle_settings["tilt_angle_max"][0]
	tilt_engine_force_factor = globals.current_vehicle_settings["tilt_engine_force_factor"][0]
	accel_steer_speed = globals.current_vehicle_settings["accel_steer_speed"][0]
	engine_force_value = globals.current_vehicle_settings["engine_force_value"][0]
	brake_size_factor = globals.current_vehicle_settings["brake_size_factor"][0]
	lift_force.x = globals.current_vehicle_settings["lift_force_x"][0]
	lift_force.y = globals.current_vehicle_settings["lift_force_y"][0]
	lift_force.z = globals.current_vehicle_settings["lift_force_z"][0]
	lift_force_position.x = globals.current_vehicle_settings["lift_force_position_x"][0]
	lift_force_position.y = globals.current_vehicle_settings["lift_force_position_y"][0]
	lift_force_position.z = globals.current_vehicle_settings["lift_force_position_z"][0]
	gear_max_speed1 = globals.current_vehicle_settings["gear_max_speed1"][0]
	gear_max_speed2 = globals.current_vehicle_settings["gear_max_speed2"][0]
	gear_max_speed3 = globals.current_vehicle_settings["gear_max_speed3"][0]
	gear_max_speed4 = globals.current_vehicle_settings["gear_max_speed4"][0]
	gear_speed_factor = globals.current_vehicle_settings["gear_speed_factor"][0]
	gear_engine_factor = globals.current_vehicle_settings["gear_engine_factor"][0]
	gear_max_low_factor = globals.current_vehicle_settings["gear_max_low_factor"][0]
	steering_wheel_angle_max = globals.current_vehicle_settings["steering_wheel_angle_max"][0]
	MAX_NO_DAMAGE_SPEED = globals.current_vehicle_settings["MAX_NO_DAMAGE_SPEED"][0]
	DAMAGE_SPEED_FACTOR = globals.current_vehicle_settings["DAMAGE_SPEED_FACTOR"][0]
	COLLISION_TIME_INTERVAL = globals.current_vehicle_settings["COLLISION_TIME_INTERVAL"][0]
	_set_mouse_sensitivity(globals.current_vehicle_settings["mouse_sensitivity"][0])
	_set_camera_sensitivity(globals.current_vehicle_settings["camera_sensitivity"][0])


func get_current_settings():
	globals.current_vehicle_settings["drag_force_factor"][0] = drag_force_factor
	globals.current_vehicle_settings["steer_speed"][0] = steer_speed
	globals.current_vehicle_settings["steer_limit"][0] = steer_limit
	globals.current_vehicle_settings["tilt_angle_max"][0] = tilt_angle_max
	globals.current_vehicle_settings["tilt_engine_force_factor"][0] = tilt_engine_force_factor
	globals.current_vehicle_settings["accel_steer_speed"][0] = accel_steer_speed
	globals.current_vehicle_settings["engine_force_value"][0] = engine_force_value
	globals.current_vehicle_settings["brake_size_factor"][0] = brake_size_factor
	globals.current_vehicle_settings["lift_force_x"][0] = lift_force.x
	globals.current_vehicle_settings["lift_force_y"][0] = lift_force.y
	globals.current_vehicle_settings["lift_force_z"][0] = lift_force.z
	globals.current_vehicle_settings["lift_force_position_x"][0] = lift_force_position.x
	globals.current_vehicle_settings["lift_force_position_y"][0] = lift_force_position.y
	globals.current_vehicle_settings["lift_force_position_z"][0] = lift_force_position.z
	globals.current_vehicle_settings["gear_max_speed1"][0] = gear_max_speed1
	globals.current_vehicle_settings["gear_max_speed2"][0] = gear_max_speed2
	globals.current_vehicle_settings["gear_max_speed3"][0] = gear_max_speed3
	globals.current_vehicle_settings["gear_max_speed4"][0] = gear_max_speed4
	globals.current_vehicle_settings["gear_speed_factor"][0] = gear_speed_factor
	globals.current_vehicle_settings["gear_engine_factor"][0] = gear_engine_factor
	globals.current_vehicle_settings["gear_max_low_factor"][0] = gear_max_low_factor
	globals.current_vehicle_settings["steering_wheel_angle_max"][0] = steering_wheel_angle_max
	globals.current_vehicle_settings["MAX_NO_DAMAGE_SPEED"][0] = MAX_NO_DAMAGE_SPEED
	globals.current_vehicle_settings["DAMAGE_SPEED_FACTOR"][0] = DAMAGE_SPEED_FACTOR
	globals.current_vehicle_settings["COLLISION_TIME_INTERVAL"][0] = COLLISION_TIME_INTERVAL
	globals.current_vehicle_settings["mouse_sensitivity"][0] = _get_mouse_sensitivity()
	globals.current_vehicle_settings["camera_sensitivity"][0] = _get_camera_sensitivity()


func on_weapon_instantiation(weapon_name:String):
	camera_group_node.set_current_camera_rotator_pitch_curve(
			weapon_name)


func on_weapon_freeing(weapon_name:String, unarm:bool):
	if unarm:
		camera_group_node.set_current_camera_rotator_pitch_curve(
				weapon_name)


func _set_mouse_sensitivity(sensitivity):
	# mouse sensitivity
	if camera_group_node:
		# all rotators' mouse sensitivities should be equal
		for camera in ScriptLibrary.find_nodes_by_type(
				CameraBase, camera_group_node, true):
			if ("rotator" in camera) and camera.rotator:
				camera.rotator.mouse_sensitivity = sensitivity
		for ponting_arm in ScriptLibrary.find_nodes_by_type(
				CameraPointingArm, camera_group_node, true):
			ponting_arm.mouse_sensitivity = sensitivity


func _get_mouse_sensitivity():
	# mouse sensitivity
	var sensitivity = 0.0
	var sensitivity_set = false
	if camera_group_node:
		# all rotators' mouse sensitivities should be equal
		for camera in ScriptLibrary.find_nodes_by_type(
				CameraBase, camera_group_node, true):
			if ("rotator" in camera) and camera.rotator:
				var new_sensitivity = camera.rotator.mouse_sensitivity
				if not sensitivity_set:
					sensitivity_set = true
					sensitivity = new_sensitivity
				assert(is_equal_approx(new_sensitivity, sensitivity))
		for camera_ponting_arm in ScriptLibrary.find_nodes_by_type(
				CameraPointingArm, camera_group_node, true):
			var new_sensitivity = camera_ponting_arm.mouse_sensitivity
			if not sensitivity_set:
				sensitivity_set = true
				sensitivity = new_sensitivity
			assert(is_equal_approx(new_sensitivity, sensitivity))
	return sensitivity


func _set_camera_sensitivity(speed):
	# camera rotating speed
	if camera_group_node:
		# all rotators' sensitivities should be equal
		for camera in ScriptLibrary.find_nodes_by_type(
				CameraBase, camera_group_node, true):
			if ("rotator" in camera) and camera.rotator:
				camera.rotator.yaw_speed = speed
				camera.rotator.pitch_speed = speed
		for ponting_arm in ScriptLibrary.find_nodes_by_type(
				CameraPointingArm, camera_group_node, true):
			ponting_arm.yaw_speed = speed
			ponting_arm.pitch_speed = speed


func _get_camera_sensitivity():
	# camera rotating speed
	var speed = 0.0
	var speed_set = false
	if camera_group_node:
		# all objects' sensitivities should be equal
		for camera in ScriptLibrary.find_nodes_by_type(
				CameraBase, camera_group_node, true):
			if ("rotator" in camera) and camera.rotator:
				var new_speed = camera.rotator.yaw_speed
				if not speed_set:
					speed_set = true
					speed = new_speed
				assert(is_equal_approx(
						camera.rotator.pitch_speed, new_speed))
				assert(is_equal_approx(new_speed, speed))
		for ponting_arm in ScriptLibrary.find_nodes_by_type(
				CameraPointingArm, camera_group_node, true):
			var new_speed = ponting_arm.yaw_speed
			if not speed_set:
				speed_set = true
				speed = new_speed
			assert(is_equal_approx(
					ponting_arm.pitch_speed, new_speed))
			assert(is_equal_approx(new_speed, speed))
	return speed


func _on_Body_body_entered(body:PhysicsBody3D):
	if collision_elapsed_time <= 0.0:
		# damage if too speedy
		var damage = 0.0
		if current_speed > MAX_NO_DAMAGE_SPEED:
			damage = (current_speed - MAX_NO_DAMAGE_SPEED) * DAMAGE_SPEED_FACTOR
		# physics body: environment
		if (body.collision_layer & 0b1):
			if ((body is RigidBody3D) and 
					(PhysicsServer3D.body_get_mode(body.get_rid()) == PhysicsServer3D.BODY_MODE_RIGID)):
				# body is a (dynamic) RigidBody
				damage += body.mass * DAMAGE_SPEED_FACTOR
			add_health(-damage)
			globals.play_sound("vehicle_collision_static", 
					globals.current_vehicle_body)
		# static body: turret/platform
		elif (body.collision_layer & 0b100000):
			body.bullet_hit(damage, global_transform)
			add_health(-damage)
		# ai vehicle's body/trailer
		elif (body.collision_layer & 0b11000):
			damage = 0.0
			var rel_speed = (linear_velocity - body.linear_velocity).length()
			if rel_speed > MAX_NO_DAMAGE_SPEED:
				damage = (rel_speed - MAX_NO_DAMAGE_SPEED) * DAMAGE_SPEED_FACTOR
			if body.has_method("bullet_hit"):
				body.bullet_hit(damage, global_transform)
			add_health(-damage)
		# reset collision timer
		collision_elapsed_time = COLLISION_TIME_INTERVAL


func _get_class():
	return "Vehicle"


func _is_class(value):
	return "Vehicle" == value
