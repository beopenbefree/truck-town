class_name RearviewMirror
extends CameraBase


@export var visibility: bool = true: set = _set_visibility

enum Position {LEFT, CENTER, RIGHT}
@export var viewport_location: Position = Position.CENTER

var rearview_mirror_camera_type: String = String()


func _ready():
	super()
	# HACK: call setters with (potential) side effects
	visibility = visibility
	# set viewport location
	if viewport_location == Position.LEFT:
		$CenterContainer.offset_left = -400
		$CenterContainer.offset_right = -200
	elif viewport_location == Position.RIGHT:
		$CenterContainer.offset_left = 200
		$CenterContainer.offset_right = 400
	
	# rearview-mirrors are not allowed on "low end device"
	if globals.current_configuration_options["low_end_device"][0]:
		queue_free()


func _physics_process(_delta):
	real_camera.global_transform = global_transform


func _set_visibility(value):
	visibility = value
	@warning_ignore("standalone_ternary")
	show() if visibility else hide()
	for child in get_children():
		if "visible" in child:
			if visibility:
				child.show()
			else:
				child.hide()


func _get_class():
	return "RearviewMirror"


func _is_class(value):
	return "RearviewMirror" == value
