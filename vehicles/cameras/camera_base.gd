class_name CameraBase
extends Node3D


@export var enabled: bool = true: set = _set_enabled
@export var current: bool = true: set = _set_current

@export var primary: bool = false

var real_camera_fov_base: float = 0.0

@onready var real_camera: Camera3D = ScriptLibrary.find_nodes_by_type(Camera3D, self, false)[0]


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	current = current
	#
	if real_camera:
		real_camera_fov_base = real_camera.fov


func zoom(enable:bool):
	if real_camera:
		if enable:
			real_camera.fov = real_camera_fov_base * 0.5
		else:
			real_camera.fov = real_camera_fov_base


func _set_current(value):
	current = value
	if real_camera:
		real_camera.current = current


func _set_enabled(value):
	enabled = value
	set_physics_process(value)


func _get_class():
	return "CameraBase"


func _is_class(value):
	return "CameraBase" == value
