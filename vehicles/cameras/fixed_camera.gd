class_name FixedCamera
extends CameraBase


@export_range(0, 1, 0.01) var virtual_region_x_ratio: float = 0.4
@export_range(0, 1, 0.01) var virtual_region_width_ratio: float = 0.2
@export_range(0, 1, 0.01) var virtual_region_y_ratio: float = 0.25
@export_range(0, 1, 0.01) var virtual_region_height_ratio: float = 0.25

@onready var rotator: Rotator = ScriptLibrary.find_nodes_by_type(
		Rotator, self, true)[0]
@onready var virtual_analog: VirtualAnalog = ScriptLibrary.find_nodes_by_type(
		VirtualAnalog, self, true)[0]


func _ready():
	super()
	#
	if virtual_analog:
		virtual_analog.analog_move.connect(_on_VirtualRotate_move)
		virtual_analog.analog_touch.connect(_on_VirtualRotate_touch)
		virtual_analog.region_x_ratio = virtual_region_x_ratio
		virtual_analog.region_y_ratio = virtual_region_y_ratio
		virtual_analog.region_width_ratio = virtual_region_width_ratio
		virtual_analog.region_height_ratio = virtual_region_height_ratio
		# not mobile platforms don't use touch controls
		if not OS.has_feature("mobile"):
			virtual_analog.queue_free()


func _on_VirtualRotate_move(direction):
	if enabled:
		rotator.touch_rotating_direction = direction


func _on_VirtualRotate_touch(touching):
	if enabled:
		rotator.touch_touching = touching
		if not touching:
			rotator.touch_rotating_direction = Vector2(0, 0)


func _set_enabled(value):
	super(value)
	if is_instance_valid(virtual_analog):
		virtual_analog.enabled = value


func _get_class():
	return "FixedCamera"


func _is_class(value):
	return "FixedCamera" == value
