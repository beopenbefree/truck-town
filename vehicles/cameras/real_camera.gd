class_name RealCamera
extends Camera3D


@export_node_path("CameraBase") var camera_base_path = NodePath()

@onready var camera_base: CameraBase = get_node_or_null(camera_base_path)


func _get_class():
	return "RealCamera"


func _is_class(value):
	return "RealCamera" == value
