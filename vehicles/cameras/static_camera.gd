class_name StaticCamera
extends CameraBase


@export_range(0.0, 100.0, 1.0, "or_greater") var ray_length: float = 50.0
@export_flags_3d_physics var ray_collisions: int = 0xffffffff

@onready var hit_point: Marker3D = $HitPoint
@onready var hit_length: float = 0.0 # option 1
@onready var view_finder: TextureRect = $SpringArm3D/Camera3D/ViewFinder
@onready var screen_center := get_viewport().get_visible_rect().size * 0.5
@onready var ray_query := PhysicsRayQueryParameters3D.create(
		Vector3.ZERO, Vector3.ONE, ray_collisions)


func _physics_process(_delta):
	# raycast from the camera screen's center
	var space_state = get_world_3d().direct_space_state
	ray_query.from = real_camera.project_ray_origin(screen_center)
	ray_query.to = ray_query.from + real_camera.project_ray_normal(screen_center) * ray_length
	var result = space_state.intersect_ray(ray_query)
	if result:
		var collision_point = result["position"]
		hit_point.global_position = collision_point
		# option 1
		hit_length = (collision_point - real_camera.global_position).length()
	else:
		# option 1
		hit_point.global_position = (real_camera.global_position - 
				real_camera.global_basis.z.normalized() * hit_length)
		# option 2
		#hit_point.global_position = (real_camera.global_position - 
				#real_camera.global_basis.z.normalized() * ray_length)


func _set_current(value):
	super(value)
	view_finder.visible = value


func _set_enabled(value):
	super(value)
	view_finder.visible = value


func _get_class():
	return "StaticCamera"


func _is_class(value):
	return "StaticCamera" == value
