class_name FollowCamera
extends CameraBase
# Directly inspired by the project: https://gitlab.com/febueldo/godot-vehicle.git


@export_node_path("VehicleBase") var vehicle_path = NodePath()

@export_range(0.0, 5.0, 0.1) var pivot_height: float = 1.0
@export_range(0.0, 90.0, 1.0, "radians_as_degrees") var pivot_inclination: float = deg_to_rad(15.0)
@export_range(0.0, 60.0, 0.1) var pivot_linear_speed: float = 10.0
@export_range(0.0, 60.0, 0.1) var pivot_rotation_speed: float = 30.0

@export_range(3.0, 30.0, 0.1) var spring_length: float = 5.0
@export_range(3.0, 30.0, 0.1) var spring_length_min: float = 3.0
@export_range(3.0, 30.0, 0.1) var spring_length_max: float = 30.0
@export_range(0.0, 3.0) var distance_factor: float = 1.5
@export_range(0.0, 1.0) var velocity_factor: float = 0.1

const max_fov: float = 75.0
@onready var fov_ratio: float = 1.0

@onready var vehicle: VehicleBase = get_node(vehicle_path)
@onready var spring_arm: SpringArm3D = $SpringArm3D


func _ready():
	super()
	#
	top_level = true
	# HACK: adjust some parameters
	if spring_length_min > spring_length_max:
		var tmp = spring_length_min
		spring_length_min = spring_length_max
		spring_length_max = tmp
	spring_arm.spring_length = clampf(spring_length, spring_length_min, spring_length_max)
	spring_arm.rotation.x = -pivot_inclination


func _physics_process(delta):
	var distance = spring_arm.get_hit_length() * distance_factor
	
	var weight: float
	# adjust pivot position
	weight = clampf(pivot_linear_speed * delta, 0.0, 1.0)
	global_position = lerp(global_position, 
			vehicle.global_position + pivot_height * Vector3.UP, weight)
	
	# adjust pivot rotation
	weight = clampf((pivot_rotation_speed - distance) * delta, 0.0, 1.0)
	# FIXME
	global_rotation.y = cubic_interpolate_angle(
		global_rotation.y, vehicle.global_rotation.y,
		global_rotation.y, vehicle.global_rotation.y,
		weight)
	
	# adjust field of view
	real_camera.fov = clampf(max_fov - distance, 1.0, 180.0) * fov_ratio
	
	# look at target
	real_camera.look_at(vehicle.camera_target.global_position + 
			vehicle.linear_velocity * velocity_factor)


func _input(_event):
	var dzoom = Input.get_axis("tp_camera_zoom_in", "tp_camera_zoom_out")
	spring_arm.spring_length = clampf(spring_arm.spring_length + dzoom,
		spring_length_min, spring_length_max)


func zoom(enable:bool):
	if real_camera:
		if enable:
			fov_ratio = 0.5
		else:
			fov_ratio = 1.0


func _set_enabled(value):
	super(value)
	set_process_input(value)


func _get_class():
	return "FollowCamera"


func _is_class(value):
	return "FollowCamera" == value
