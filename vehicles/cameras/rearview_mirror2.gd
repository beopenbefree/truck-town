class_name RearviewMirror2
extends CameraBase


# WARNING: This type of RearviewMirror must be built "in place",
# i.e. inside the model tree, because up to version 4.3 the
# "viewport path" configuration of the viewport texture is
# "lost" during the instantiation process.
@export var visibility: bool = true: set = _set_visibility

var rearview_mirror_camera_type: String = String()


func _ready():
	super()
	# HACK: call setters with (potential) side effects
	visibility = visibility
	# rearview-mirrors are not allowed on "low end device"
	if globals.current_configuration_options["low_end_device"][0]:
		queue_free()


func _set_visibility(value):
	visibility = value
	@warning_ignore("standalone_ternary")
	show() if visibility else hide()
	for child in get_children():
		if "visible" in child:
			if visibility:
				child.show()
			else:
				child.hide()


func _get_class():
	return "RearviewMirror2"


func _is_class(value):
	return "RearviewMirror2" == value
