class_name CameraPointingArm
extends Rotator


@onready var camera: CameraBase = ScriptLibrary.find_nodes_by_type(
		CameraBase, self, true)[0]
@onready var rotator: Rotator = self


func _get_class():
	return "CameraPointingArm"


func _is_class(value):
	return "CameraPointingArm" == value


func _set_enabled(value):
	super(value)
	if camera:
		camera.enabled = value
