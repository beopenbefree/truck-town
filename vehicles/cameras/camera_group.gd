class_name CameraGroup
extends Node3D


var CAMERA_OBJECTS = [
	FixedCamera,
	StaticCamera,
	TrackingCamera,
	FollowCamera,
	RearviewMirror,
	DebugCamera,
]
var cameras: Array = []
var current_camera_idx: int = 0
var default_camera: String = String()


func _ready():
	cameras = ScriptLibrary.find_nodes_by_type(CameraBase, self, true)
	if cameras.size() != 0:
		_set_enabled_and_current(cameras[0])
	else:
		# no viable camera
		var camera = CAMERA_OBJECTS[0].new()
		add_child(camera)
		_set_enabled_and_current(camera)


func swap_camera():
	current_camera_idx = (current_camera_idx + 1) % cameras.size()
	var current_camera = cameras[current_camera_idx]
	if (not current_camera.enabled) or (not current_camera.current):
		_set_enabled_and_current(current_camera)


func get_current_camera() -> CameraBase:
	return cameras[current_camera_idx]


func set_current_camera_rotator_pitch_curve(pitch_curve_name):
	# UTILITY used for cameras or their parents (e.g. arms),
	# using a Rotator with use_pitch_curve_sets=true
	var current_camera = get_current_camera()
	var current_rotator:Rotator = null
	if "rotator" in current_camera:
		current_rotator = current_camera.rotator
	elif "rotator" in current_camera.get_parent():
		current_rotator = current_camera.get_parent().rotator
	if current_rotator and (current_rotator.use_pitch_curve_sets):
		current_rotator.set_pitch_curve_set(pitch_curve_name)


func _set_enabled_and_current(camera:CameraBase):
	camera.enabled = true
	camera.current = true
	for camera_child in camera.get_children():
		# Check if there are rear view mirrors as a child
		if ((camera_child is RearviewMirror) and 
				("visibility" in camera_child)):
			camera_child.enabled = true
			camera_child.visibility = true
	# check if there is a camera pointing arm as parent
	if camera.get_parent() is CameraPointingArm:
		camera.get_parent().enabled = true
	# make other camera's not current, disabled and reset
	for other_camera:CameraBase in cameras:
		if other_camera != camera:
			other_camera.enabled = false
			other_camera.current = false
			# Check if there are rear view mirrors as a child
			for other_camera_child in other_camera.get_children():
				if ((other_camera_child is RearviewMirror) and 
						("visibility" in other_camera_child)):
					other_camera_child.enabled = false
					other_camera_child.visibility = false
			# check if there is a camera pointing arm as parent
			if other_camera.get_parent() is CameraPointingArm:
				other_camera.get_parent().enabled = false


func _get_class():
	return "CameraGroup"


func _is_class(value):
	return "CameraGroup" == value
