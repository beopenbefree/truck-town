class_name VehicleModel
extends VehicleModelBase


class Aggression extends VehicleModelBase.AggressionBase:
	pass


var aggression: Aggression = null


func _ready():
	aggression = Aggression.new(body, shooter)


func _get_class():
	return "VehicleModel"


func _is_class(value):
	return "VehicleModel" == value
