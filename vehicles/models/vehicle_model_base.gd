class_name VehicleModelBase
extends Node


class AggressionBase extends Resource:
	var vehicle:VehicleBase = null
	var shooter:ShooterBase = null
	
	func _init(p_vehicle:VehicleBase, p_shooter:ShooterBase):
		vehicle = p_vehicle
		shooter = p_shooter
	
	func increment(percent:float):
		var fraction = clampf(percent, 0.0, 100.0) * 0.01
		# vehicle
		_mul_and_clamp(vehicle, "engine_force_value", 
				(1.0 + fraction), 2.0, 300.0)
		_mul_and_clamp(vehicle, "MAX_NO_DAMAGE_SPEED", 
				(1.0 + fraction), 1.0, 10.0)
		_mul_and_clamp(vehicle, "DAMAGE_SPEED_FACTOR", 
				(1.0 - fraction), 0.0, 1.0)
		# shooter
		_mul_and_clamp(shooter, "AMMO_AMOUNT", 
				(1.0 + fraction), 100.0, 800.0, true)
		_mul_and_clamp(shooter, "GRENADE_AMOUNT", 
				(1.0 + fraction), 5.0, 30.0, true)
		_mul_and_clamp(shooter, "GRENADE_THROW_FORCE", 
				(1.0 + fraction), 10.0, 50.0)
		_mul_and_clamp(shooter, "OBJECT_THROW_FORCE", 
				(1.0 + fraction), 10.0, 50.0)
		return fraction
	
	func decrement(percent:float):
		var fraction = clampf(percent, 0.0, 100.0) * 0.01
		# vehicle
		_mul_and_clamp(vehicle, "engine_force_value", 
				(1.0 - fraction), 2.0, 300.0)
		_mul_and_clamp(vehicle, "MAX_NO_DAMAGE_SPEED", 
				(1.0 - fraction), 1.0, 10.0)
		_mul_and_clamp(vehicle, "DAMAGE_SPEED_FACTOR", 
				(1.0 + fraction), 0.0, 1.0)
		# shooter
		_mul_and_clamp(shooter, "AMMO_AMOUNT", 
				(1.0 - fraction), 100.0, 800.0, true)
		_mul_and_clamp(shooter, "GRENADE_AMOUNT", 
				(1.0 - fraction), 5.0, 30.0, true)
		_mul_and_clamp(shooter, "GRENADE_THROW_FORCE", 
				(1.0 - fraction), 10.0, 50.0)
		_mul_and_clamp(shooter, "OBJECT_THROW_FORCE", 
				(1.0 - fraction), 10.0, 50.0)
		return fraction
	
	func _mul_and_clamp(object:Object, property_name:String, 
			factor:float, p_min:float=0.0, p_max:float=INF,
			int_round_up:bool=false):
		var property = object.get(property_name) * factor
		property = clampf(property, p_min, p_max)
		if int_round_up:
			# property is declared int in the object
			property = ceili(property)
		object.set(property_name, property)


@onready var body: VehicleBase = ScriptLibrary.find_nodes_by_type(
		VehicleBase, self, false)[0]
@onready var shooter: ShooterBase = ScriptLibrary.find_nodes_by_type(
		ShooterBase, self, false)[0]


func _get_class():
	return "VehicleModelBase"


func _is_class(value):
	return "VehicleModelBase" == value
