extends Node3D


func _ready():
	var vehicle = get_node_or_null("../Body")
	if vehicle:
		set_signal_handling(vehicle)


func set_signal_handling(vehicle:Vehicle):
	# forward
	$FW_VB.analog_touch_true.connect(Callable(vehicle, "_on_VirtualMove_move").bind(Vector2(0, -1)))
	$FW_VB.analog_touch_false.connect(Callable(vehicle, "_on_VirtualMove_move_stop").bind(false))
	# backward
	$BW_VB.analog_touch_true.connect(Callable(vehicle, "_on_VirtualMove_move").bind(Vector2(0, 1)))
	$BW_VB.analog_touch_false.connect(Callable(vehicle, "_on_VirtualMove_move_stop").bind(false))
	# brake
	$BR_VB.analog_touch_true.connect(Callable(vehicle, "_on_VirtualBrake_brake").bind(true))
	$BR_VB.analog_touch_false.connect(Callable(vehicle, "_on_VirtualBrake_brake").bind(false))
	# steer
	$ST_AC.target_node = vehicle
