class_name VirtualButton
extends Control


@warning_ignore("unused_signal")
signal analog_touch_true
@warning_ignore("unused_signal")
signal analog_touch_false

@export var enabled: bool = true: set = _set_enabled

@export var texture_normal: Texture2D = null
@export var texture_pressed: Texture2D = null
@export var current_scale: Vector2 = Vector2.ONE

@export var region_x_ratio = 0: set = _set_region_x_ratio
@export var region_width_ratio = 1: set = _set_region_width_ratio
@export var region_y_ratio = 0: set = _set_region_y_ratio
@export var region_height_ratio = 1: set = _set_region_height_ratio

@export var max_distance:float = 55.0

var screen_region: Rect2

var index:int = -1

var current_scale_inv: Vector2


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	region_x_ratio = region_x_ratio
	region_width_ratio = region_width_ratio
	region_y_ratio = region_y_ratio
	region_height_ratio = region_height_ratio
	# set the texture if any
	if texture_normal != null:
		$Button.texture = texture_normal
	# scale drift correction
	$Button.scale = current_scale
	current_scale_inv = Vector2(1.0/current_scale.x, 1.0/current_scale.y)
	# compute screen region
	_update_screen_region()


func _unhandled_input(event):
	if event is InputEventScreenTouch:
	
		# Only set index if there is nothing assigned
		if index == -1:
			#if event is InputEventScreenTouch and event.pressed and correct_side(event.position):
			if event.pressed and correct_side(event.position):
				index = event.index
				$Button.position = event.position
				emit_signal("analog_touch_true")
				visible = true
		
		# return early so we don't process unecessary events.
		if index == -1 || event.index != index:
			get_viewport().set_input_as_handled()
			return
			
		# finish the drag
		#if event is InputEventScreenTouch and not event.pressed:
		if not event.pressed:
			index = -1
#			$OuterAnalog/InnerAnalog.position = Vector2(0,0)
			visible = false
			emit_signal("analog_touch_false")
			get_viewport().set_input_as_handled()
			return


func correct_side(touch_position):
	# check if touch_position is inside screen_region
	return screen_region.has_point(touch_position)


func _set_region_x_ratio(value):
	region_x_ratio = value
	_update_screen_region()


func _set_region_y_ratio(value):
	region_y_ratio = value
	_update_screen_region()


func _set_region_width_ratio(value):
	region_width_ratio = value
	_update_screen_region()


func _set_region_height_ratio(value):
	region_height_ratio = value
	_update_screen_region()


func _update_screen_region():
	if get_viewport():
		var viewport_rect = get_viewport_rect()
		screen_region = Rect2(viewport_rect.position.x + viewport_rect.size.x * region_x_ratio,
							viewport_rect.position.y + viewport_rect.size.y * region_y_ratio,
							viewport_rect.size.x * region_width_ratio,
							viewport_rect.size.y * region_height_ratio)
		# debug screen region rect
		$DebugRegionRect.position = screen_region.position
		$DebugRegionRect.position *= current_scale_inv
		$DebugRegionRect.size = screen_region.size
		$DebugRegionRect.scale *= current_scale_inv


func _get_class():
	return "VirtualButton"


func _is_class(value):
	return "VirtualButton" == value


func _set_enabled(value):
	enabled = value
	set_process_unhandled_input(enabled)
