class_name Accelerometer
extends Node3D


@export var enabled: bool = true: set = _set_enabled
@export_node_path("Node3D") var target = NodePath()

var target_node: Node = null


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled


func _physics_process(_delta):
	# Get our data
	var acc = Input.get_accelerometer()
	# apply filter and compute rotation angle
	acc = $LowPassFilter.apply_low_pass_vector(acc)
	# call apply_accelerometer on target (if any)
	if target_node and (target_node.has_method("apply_accelerometer")):
		target_node.apply_accelerometer(acc)


func _get_class():
	return "Accelerometer"


func _is_class(value):
	return "Accelerometer" == value


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)
