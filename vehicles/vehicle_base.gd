class_name VehicleBase
extends VehicleBody3D


@export var enabled: bool = true: set = _set_enabled

@export_group("Vehicle structure")
@export var wheels: Array[VehicleWheel3D] = []

@export_group("Vehicle settings")
@export_range(0.1, 5.0, 0.1) var steer_speed: float = 1.5
@export_range(0.0, 1.0, 0.05) var steer_limit: float = 0.4
@export_range(10.0, 60.0, 1.0) var tilt_angle_max: float = 25
@export_range(1.0, 3.0, 0.1) var tilt_engine_force_factor: float = 1.0
@export_range(0.5, 2.0, 0.05) var accel_steer_speed: float = 1.0
@export_range(2.0, 200.0, 1.0) var engine_force_value: float = 40
@export_range(1.0, 120.0, 0.5) var drag_force_factor: float = 20.0
@export var brake_size_factor: float = 1.0
@export var lift_force: Vector3 = Vector3(0.0, 500.0, 0.0)
@export var lift_force_position: Vector3 = Vector3.ZERO

@export_subgroup("Gears")
@export_range(1.0, 20.0, 0.25) var gear_max_speed1: float = 3.0
@export_range(1.0, 40.0, 0.25) var gear_max_speed2: float = 6.0
@export_range(1.0, 60.0, 0.5) var gear_max_speed3: float = 9.0
@export_range(1.0, 80.0, 0.5) var gear_max_speed4: float = 12.0
@export_range(0.1, 1.0, 0.05) var gear_speed_factor: float = 0.3
@export_range(0.01, 4.0, 0.01) var gear_engine_factor: float = 0.4
@export_range(0.8, 1.0, 0.001) var gear_max_low_factor: float = 0.9

@export_group("Misc")
@export_range(30, 90.0, 1.0) var steering_wheel_angle_max: float = 80

# HACK: DAMAGE_MULTIPLIER heuristic: "engine force" / "mass"
@export_range(0.0, 2.0, 0.01) var DAMAGE_MULTIPLIER: float = 1.0

@export_range(1.0, 10.0, 0.5) var MAX_NO_DAMAGE_SPEED: float = 5.0 # m/s
@export_range(0, 1, 0.01) var DAMAGE_SPEED_FACTOR: float = 0.05
@export_range(0, 2.0, 0.01) var COLLISION_TIME_INTERVAL: float = 0.5

# AI: target priority
@export var target_priority: int = 0

var steering_enabled: bool = true
var motion_target: float = 0
var steer_target: float = 0
var touch_move_direction: Vector2 = Vector2.ZERO
var touch_steer_direction: Vector2 = Vector2.ZERO
var accel_steer: float = 0
const max_accel_steer: float = 9.81 # Android accelerometer's component max value
const max_accel_steer_inv: float = 1.0 / max_accel_steer
var brake_size: float = 1.0
var contact_body_per_wheel: Dictionary = {}
var initial_friction_slip_per_wheel: Dictionary = {}

# boost area stuff
var engine_force_boost: float = 1.0
var boost_timer: Timer

# blast data
var do_blast: bool = false
var blast_impulse: Vector3 = Vector3.ZERO
const BLAST_TORQUE_IMPULSE: float = 50.0
const DO_BLAST_INTERVAL: float = 2.0
var do_blast_timer: float = 0.0

# lift data
var lift_up: bool = false

# hit data
var do_hit: bool = false
var hit_impulse: Vector3 = Vector3.ZERO
var hit_impulse_position: Vector3 = Vector3.ZERO
const DO_HIT_INTERVAL: float = 0.01
var do_hit_timer: float = 0.0

# sound data for gears
var current_sound: Node3D = null

# steering wheel stuff
var steering_wheel_node: Node3D = null
var steering_wheel_angle_factor: float = 0.0

# AI: when used as target
var velocity: Vector3 = Vector3.ZERO

# current data
const ENGINE_FORCE_VALUE_BACKWARD_FACTOR: float = 1.0
const DRAG_FORCE_CORRECTION_FACTOR: float = 1e-5
const ROLLING_RESISTANCE_CORRECTION_FACTOR: float = 30.0 * 1e-5
const TILT_ENGINE_FORCE_FACTOR_BACKWARD_FACTOR: float = 0.8
#
@onready var chassis: MeshInstance3D = ScriptLibrary.find_nodes_by_type(
		Chassis, self, true)[0]
@onready var engine_force_value_backward: float = (engine_force_value * 
		ENGINE_FORCE_VALUE_BACKWARD_FACTOR)
@onready var drag_force_correction: float = (drag_force_factor *
		DRAG_FORCE_CORRECTION_FACTOR)
@onready var rolling_resistance_correction: float = (drag_force_factor *
		ROLLING_RESISTANCE_CORRECTION_FACTOR)
@onready var tilt_max: float = sin(deg_to_rad(tilt_angle_max))
@onready var tilt_engine_force_factor_backward: float = (tilt_engine_force_factor *
		TILT_ENGINE_FORCE_FACTOR_BACKWARD_FACTOR)
@onready var current_speed: float = 0.0
@onready var current_gear: String = "0"
@onready var changing_gear: bool = false
@onready var gear_max_low_speed1: float = gear_max_low_factor * gear_max_speed1
@onready var gear_max_low_speed2: float = gear_max_low_factor * gear_max_speed2
@onready var gear_max_low_speed3: float = gear_max_low_factor * gear_max_speed3
@onready var gear_max_low_speed4: float = gear_max_low_factor * gear_max_speed4
@onready var collision_elapsed_time: float = 0.0
# used by other game objects
@onready var visible_target: Node3D = find_child("VisibleTarget", true)
@onready var camera_target: Node3D = find_child("CameraTarget", true)
# current data
@onready var shooter_node: ShooterBase = ScriptLibrary.find_nodes_by_type(
		ShooterBase, self, true)[0]
@onready var camera_group_node: CameraGroup = ScriptLibrary.find_nodes_by_type(
	CameraGroup, self, true)[0]


func _ready():
	# HACK: call setters with (potential) side effects
	enabled = enabled
	#
	_initialize()
	# fill contact bodies per wheels and initial friction slips
	for wheel in wheels:
		contact_body_per_wheel[wheel] = null
		initial_friction_slip_per_wheel[wheel] = wheel.wheel_friction_slip


func _physics_process(delta):
	# re-compute wheels' friction slips if needed
	for wheel in wheels:
		if wheel.is_in_contact():
			var body = wheel.get_contact_body()
			if body != contact_body_per_wheel[wheel]:
				if "physics_material_override" in body:
					# update friction slip
					var body_friction = body.physics_material_override.friction
					wheel.wheel_friction_slip = initial_friction_slip_per_wheel[wheel] * body_friction
					# update contact body
					contact_body_per_wheel[wheel] = body
	# collision timer
	if collision_elapsed_time > 0.0:
		collision_elapsed_time = maxf(collision_elapsed_time - delta, 0.0)


func _initialize():
	# boost area stuff
	boost_timer = Timer.new()
	boost_timer.timeout.connect(_on_boost_timer_timeout)
	boost_timer.one_shot = true
	add_child(boost_timer)
	
	# compute brake size
	brake_size = (mass / 1000.0 * 30.0) * abs(brake_size_factor)
	
	# connect body_entered signal
	body_entered.connect(_on_Body_body_entered)
	
	# steering wheel node (if any)
	steering_wheel_node = ScriptLibrary.find_nodes_by_type(
			SteeringWheel, self, true)[0]
	if steering_wheel_node:
		steering_wheel_angle_factor = ((1.0 / steer_limit) * 
				deg_to_rad(steering_wheel_angle_max))
	
	# map target
	var map_target = find_child("MapTarget", true)
	if map_target:
		map_target.show()


func _post_physics_process(delta:float, speed:float, motion_direction:float):
	# turn the steering wheel
	if steering_wheel_node:
		steering_wheel_node.update(steering * steering_wheel_angle_factor)
	
	# set engine sound pitch
	if current_sound:
		var pitch_scale = 1.0
		# (simple) fsm to change gears
		if current_gear == "0":
			if speed > 0.15:
				if motion_direction < 0:
					_change_gear("reverse")
				else:
					_change_gear("1")
		elif current_gear == "reverse":
			if motion_direction > 0:
				_change_gear("0")
			pitch_scale = 1 + gear_speed_factor * speed + gear_engine_factor
		elif current_gear == "1":
			if speed > gear_max_speed1:
				_change_gear("2")
			elif speed < 0.15:
				_change_gear("0")
			pitch_scale = 1 + gear_speed_factor * speed + gear_engine_factor
		elif current_gear == "2":
			if speed > gear_max_speed2:
				_change_gear("3")
			elif speed < gear_max_low_speed1:
				_change_gear("1")
			pitch_scale = 1 + gear_speed_factor * max(speed - gear_max_speed1, 0) + gear_engine_factor
		elif current_gear == "3":
			if speed > gear_max_speed3:
				_change_gear("4")
			elif speed < gear_max_low_speed2:
				_change_gear("2")
			pitch_scale = 1 + gear_speed_factor * max(speed - gear_max_speed2, 0) + gear_engine_factor
		elif current_gear == "4":
			if speed > gear_max_speed4:
				_change_gear("5")
			elif speed < gear_max_low_speed3:
				_change_gear("3")
			pitch_scale = 1 + gear_speed_factor * max(speed - gear_max_speed3, 0) + gear_engine_factor
		elif current_gear == "5":
			if speed < gear_max_low_speed4:
				_change_gear("4")
			pitch_scale = 1 + gear_speed_factor * max(speed - gear_max_speed4, 0) + gear_engine_factor
		current_sound.set_pitch_scale(pitch_scale)
	current_speed = speed
	
	# NOTE: _integrate_forces() not used (TODO)
	# handle blast requests
	if do_blast:
		if do_blast_timer <= 0.0:
			apply_central_impulse(blast_impulse)
			apply_torque_impulse(Vector3(0.0, abs(randf()), 0.0) * BLAST_TORQUE_IMPULSE)
			do_blast_timer = DO_BLAST_INTERVAL
		else:
			do_blast_timer -= delta
		do_blast = false
	elif do_blast_timer > 0.0:
		do_blast_timer -= delta
	# handle hit requests
	if do_hit:
		if do_hit_timer <= 0.0:
			apply_impulse(hit_impulse, hit_impulse_position)
			do_hit_timer = DO_HIT_INTERVAL
		else:
			do_hit_timer -= delta
		do_hit = false
	elif do_hit_timer > 0.0:
		do_hit_timer -= delta
	# handle lift up requests
	if lift_up:
		apply_force(lift_force, lift_force_position)


func _set_current_brake(value):
	brake = value
	if value != 0.0:
		chassis.turn_light("stop_light", true)
	else:
		chassis.turn_light("stop_light", false)


func _change_gear(gear):
	current_gear = gear
	changing_gear = true
	await get_tree().create_timer(0.2).timeout
	changing_gear = false


func get_tilt():
	var forward  = get_global_transform().basis.z
	var up = Vector3(0, 1.0, 0)
	return up.dot(forward)


func apply_accelerometer(acc):
	accel_steer = acc.x * accel_steer_speed
	accel_steer = clampf(accel_steer, -max_accel_steer, 
			max_accel_steer)
	var x = accel_steer * max_accel_steer_inv
	var y = sqrt(1.0 - x * x)
	touch_steer_direction = Vector2(x, y)


func apply_boost(factor, duration):
	if (not boost_timer.is_stopped()) or (factor == 0.0):
		return
	
	engine_force_boost = factor
	boost_timer.start(duration)
	add_health(factor)


func apply_blast(magnitude):
	# blast direction: (approximately) global UP
	var x = randf_range(-1.0, 1.0)
	var z = randf_range(-1.0, 1.0)
	var impulse_dir = Vector3(x, 1.0, z).normalized()
	blast_impulse = impulse_dir * magnitude * mass
	do_blast = true
	add_health(-magnitude)


func bullet_hit(_damage, _bullet_global_transform,
		_raycast=false, _with_hit=false):
	push_warning("Unimplemented bullet_hit")


func rocket_hit(_damage, _rocket_global_transform):
	push_warning("Unimplemented rocket_hit")


func raycast_hit(_damage, _hit_position):
	push_warning("Unimplemented raycast_hit")


func add_health(value:float):
	if value < 0.0:
		# compute real damage
		value *= DAMAGE_MULTIPLIER
	return value


func _on_boost_timer_timeout():
	engine_force_boost = 1.0


func _on_VirtualSteer_steer(direction):
	touch_steer_direction = direction


func _on_VirtualSteer_steer_stop(touching):
	if not touching:
		touch_steer_direction = Vector2(0, 0)


func _on_VirtualMove_move(direction):
	touch_move_direction = direction


func _on_VirtualMove_move_stop(touching):
	if not touching:
		touch_move_direction = Vector2(0, 0)


func _on_VirtualBrake_brake(p_enabled):
	if p_enabled:
		brake = brake_size
	else:
		brake = 0.0


func _on_Body_body_entered(_body:PhysicsBody3D):
	push_warning("Unimplemented _on_Body_body_entered")


func _get_class():
	return "VehicleBase"


func _is_class(value):
	return "VehicleBase" == value


func _set_enabled(value):
	enabled = value
	set_process_unhandled_input(value)
	set_process_input(value)
	set_physics_process(value)
